#ifndef LFPEXPORTHANDLER_H
#define LFPEXPORTHANDLER_H
#include "abstractexporthandler.h"
#include "iirFilter.h"


class DATExportHandler: public AbstractExportHandler
{
    Q_OBJECT

public:
    DATExportHandler(QStringList arguments);
    int processData();
    bool createNextOutputFile();
    ~DATExportHandler();

protected:

    void parseArguments();
    void printHelpMenu();

    //void printCustomMenu();
    //void parseCustomArguments(int &argumentsProcessed);

private:

    int splitDataGap;
    int decimationVal;
    int decimationMod;
    uint32_t numPacketsRemoved;
    uint32_t startSplitTime;
    uint32_t endSplitTime;
    QList<QFile*> neuroFilePtrs;
    QList<QDataStream*> neuroStreamPtrs;
    int currentOutputFileNum;



};

#endif // LFPEXPORTHANDLER_H
