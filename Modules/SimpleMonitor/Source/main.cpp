#include "mainWindow.h"
#include <QApplication>
#include <QDebug>

int main(int argc, char *argv[])
{
    qInstallMessageHandler(moduleMessageOutput);
    setModuleName("SimpleMonitor");
    QApplication a(argc, argv);
    qDebug().noquote() << "SimpleMonitor Version Info:\n" << GlobalConfiguration::getVersionInfo(false); //print version info to debug log
    MainWindow w(a.arguments());

    w.show();
    return a.exec();
}
