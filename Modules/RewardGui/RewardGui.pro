include(../module_defaults.pri)

QT       += core gui xml widgets multimedia

cache()

TARGET = RewardGui
TEMPLATE = app
CONFIG += c++11

INCLUDEPATH += ../../Trodes/src-config
INCLUDEPATH += ../../Trodes/src-main

SOURCES += main.cpp\
        rewardControl.cpp\
        sharedfunctions.cpp \
        ../../Trodes/src-config/configuration.cpp \
        ../../Trodes/src-main/trodesSocket.cpp \
        ../../Trodes/src-main/eventHandler.cpp \
        ../../Trodes/src-main/trodesdatastructures.cpp \



HEADERS  += \
    rewardControl.h \
    sharedinclude.h \
    ../../Trodes/src-config/configuration.h \
    ../../Trodes/src-main/trodesSocket.h \
    ../../Trodes/src-main/trodesSocketDefines.h \
    ../../Trodes/src-main/eventHandler.h \
    ../../Trodes/src-main/trodesdatastructures.h
