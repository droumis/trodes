#ifndef _SHARED_STRUCT_H
#define _SHARED_STRUCT_H



typedef uint32_t u32;

typedef struct _DIOBuffer {
        u32         timestamp;
	unsigned short status[4];
} DIOBuffer;


#endif
