#include "workspaceEditor.h"

ColorGenerator::ColorGenerator() {
    colorCounter = 0;
    sequenceColors.append(QColor("#ff4646"));
    sequenceColors.append(QColor("white"));
    sequenceColors.append(QColor("#24c600"));
    sequenceColors.append(QColor("#55bdf4"));
}

/**
QColor ColorGenerator::randColor(float saturation, float value) {
    float rf,gf,bf;
    int r,g,b;

    float hue = static_cast<float>(rand()) / static_cast<float>(RAND_MAX); //gen float between 0.0-1.0
    float goldenRatio = 0.618033988749895;
}**/

QColor ColorGenerator::naiveRandColor() {
    int r,g,b;
    r = rand()%255;
    g = rand()%255;
    b = rand()%255;
    //qDebug() << "color: " << r << " " << g << " " << b;
    return(QColor(r,g,b));
}

QColor ColorGenerator::getSequenceColor() {
    QColor retColor = sequenceColors.at(colorCounter);
    colorCounter++;
    if (colorCounter >= sequenceColors.length())
        colorCounter = 0;
    return(retColor);
}

ClickableFrame::ClickableFrame(QWidget *parent) : QFrame(parent) {

}

void ClickableFrame::mousePressEvent(QMouseEvent *event) {
    emit(sig_clicked());
}

ClickableLabel::ClickableLabel(QString labelTxt, QWidget *parent) : QLabel(parent) {
    setText(labelTxt);
}

void ClickableLabel::mousePressEvent(QMouseEvent *event) {
    emit(sig_clicked());
}

ClickableLineEdit::ClickableLineEdit(const QString &text, QWidget *parent) : QLineEdit(parent) {
    setText(text);
}

void ClickableLineEdit::mousePressEvent(QMouseEvent *event) {
    emit(sig_clicked(text()));
}

TagGroupingPanel::TagGroupingPanel(QWidget *parent) : QGroupBox(parent) {
    this->setTitle(tr("Grouping Tags"));

    mainLayout = new QVBoxLayout();

    //Add Tag Group Panel

    addTagLayout = new QHBoxLayout();

    labelCategory = new QLabel(tr("Category:"));
    addTagLayout->addWidget(labelCategory);

    categorySelectBox = new QComboBox();
    categorySelectBox->addItem("All");
//    WorkspaceEditor::setComboBoxFirstItemUnselectable(categorySelectBox);
    categorySelectBox->addItem("+Create New");
    connect(categorySelectBox,SIGNAL(activated(int)),this,SLOT(processCategorySelectBox(int)));
    addTagLayout->addWidget(categorySelectBox);

    labelTags = new QLabel(tr("Tag:"));
    addTagLayout->addWidget(labelTags);

    tagSelectBox = new QComboBox();
    tagSelectBox->addItem("Select Tag to Add");
        //set first item non-selectable by user
    WorkspaceEditor::setComboBoxFirstItemUnselectable(tagSelectBox);

    tagSelectBox->addItem("+Create New");
    connect(tagSelectBox,SIGNAL(activated(int)),this,SLOT(processTagSelectBox(int)));
    addTagLayout->addWidget(tagSelectBox);
    buttonAddTag = new QPushButton();
    buttonAddTag->setText(tr("Add"));
    buttonAddTag->setMaximumSize(buttonAddTag->sizeHint());
    connect(buttonAddTag,SIGNAL(released()),this,SLOT(addTagToList()));
    addTagLayout->addWidget(buttonAddTag);

    //Tag List Layouts
    tagListLayout = new QHBoxLayout();

    //tag lists
    tagAllList = new QListWidget();
    tagSharedList = new QListWidget();

    //Tag view
    tagListView = new QTabWidget();
    tagListView->addTab(tagAllList, "All");
    tagListView->addTab(tagSharedList, "Shared");
    connect(tagListView, SIGNAL(currentChanged(int)), this, SIGNAL(sig_tabChanged(int)));

    tagListLayout->addWidget(tagListView);
    buttonRemoveTag = new QPushButton();
    buttonRemoveTag->setText(tr("Remove"));
    connect(buttonRemoveTag,SIGNAL(released()),this,SLOT(removeTagFromList()));
    tagListLayout->addWidget(buttonRemoveTag);

    mainLayout->addLayout(addTagLayout);
    mainLayout->addLayout(tagListLayout);
    this->setLayout(mainLayout);
}

QHash<QString,int> TagGroupingPanel::getNewTagHash() {
    QHashIterator<QString,int> i(tempTags);
    while(i.hasNext()) {
        i.next();
        if (!availTags.contains(i.key()))
            availTags.insert(i.key(),i.value());
    }
    tempTags.clear();
    return(availTags);
}

//DEPRECIATED, dont use this anymore
QList<QString> TagGroupingPanel::getTagList() {
    QList<QString> tagListL;
    for (int i = 0; i < tagAllList->count(); i++) {
        tagListL.append(tagAllList->item(i)->text());
    }
    return(tagListL);
}

QList<GroupingTag> TagGroupingPanel::getGTagList() {
    QList<GroupingTag> tagListL;
    //possibly add in sortTagLists() here, but the list should already be sorted
    for (int i = 0; i < tagAllList->count(); i++) {
        GroupingTag curTag;
        curTag.readInTagStr(tagAllList->item(i)->text());
        tagListL.append(curTag);
    }
    //todo: make sure to sort the list before returning it
    return(tagListL);
}

QList<GroupingTag> TagGroupingPanel::getAddedTags() {
    return(addedTags);
}

QList<GroupingTag> TagGroupingPanel::getRemovedTags() {
    return(removedTags);
}

void TagGroupingPanel::clearAddedTagList() {
    addedTags.clear();
}

void TagGroupingPanel::clearRemovedTagList() {
    removedTags.clear();
}

void TagGroupingPanel::updateDict(CategoryDictionary dict) {
//    cataDict.setCategories(categories);
    cataDict = dict;
    populateCategorySelectBox();
}

void TagGroupingPanel::updateTags(QHash<QString, int> tags) {
    availTags.clear();
    tempTags.clear();
    availTags = tags;
    populateTagSelectBox();

}

void TagGroupingPanel::loadCurrentCategoryTags() {
    populateTagSelectBox();
}

void TagGroupingPanel::loadNTrodeTags(QHash<GroupingTag, int> loadedTags) {
    setSharedTabEnabled(false);
    for (int i = 0; i < tagSelectBox->count(); i++) {
        if (tempTags.contains(tagSelectBox->itemText(i)) && !availTags.contains(tagSelectBox->itemText(i))) {
            tagSelectBox->removeItem(i);
            i--;
        }
    }
    tempTags.clear();

    tagAllList->clear();
    tagSharedList->clear();

    QHashIterator<GroupingTag,int> i(loadedTags);
//    qDebug() << "Loading all tags";
    int counter = 0;
    while(i.hasNext()) {
        counter++;
        i.next();
        GroupingTag curTag = i.key();
        QString tagStr = curTag.toTagString();
        tagAllList->addItem(tagStr);
//        qDebug() << "   Tag " << counter << ": " << tagStr;
    }
    sortTagLists();
}

void TagGroupingPanel::loadMultipleNTrodeTags(QList<SingleSpikeTrodeConf> nTrodes) {
    setSharedTabEnabled(true);
//    qDebug() << "Loading NTrodes into the Panel!";
    tagAllList->clear();
    tagSharedList->clear();

    QList<QList<QString> > inputTags;
    for (int k = 0; k < nTrodes.length(); k++) {
        SingleSpikeTrodeConf curNTrode = nTrodes.at(k);
        //curNTrode.ntrodeIndex IS NOT SAFE, will not always be accurate, use ID to look up nTrode
//        qDebug() << "   - Ntrode " << curNTrode.nTrodeId << " at index " << curNTrode.nTrodeIndex;
        QHashIterator<GroupingTag, int> i(curNTrode.gTags);

        while (i.hasNext()) {
            i.next();
//                QString curCata = spikeConf->groupingDict.getTagsCategory(i.key());
            QString curCata = i.key().category;
            QString curTag = i.key().tag;
//                qDebug() << "[" << curCata << "] " << curTag;
            bool createNewCata = true;
            for (int j = 0; j < inputTags.length(); j++) {
                if (inputTags.at(j).at(0) == curCata) { //first item of each sub list will be catagory
                    createNewCata = false;
                    QList<QString> curTagList = inputTags.at(j);
                    bool tagAlreadyInList = false;
                    for (int y = 1; y < curTagList.length(); y++) { //check the list to see if the tag is already in it
                        if (curTagList.at(y) == curTag) {
                            tagAlreadyInList = true;
                            break;
                        }

                    }
                    if (!tagAlreadyInList) { //only add the tag if it's not already in the list
                        curTagList.push_back(curTag);
                        inputTags.replace(j, curTagList);
                    }
                    break;
                }
            }
            if (createNewCata) {
                QList<QString> newCataList;
                newCataList.push_back(curCata);
                newCataList.push_back(curTag);
                inputTags.push_back(newCataList);
            }
        }
    }
    //now put tags into the groupingTagAllList
    for (int j = 0; j < inputTags.length(); j++) {
        QString curCata = inputTags.at(j).at(0); //first item of each sub list is the catagorey
        for (int k = 1; k < inputTags.at(j).length(); k++) {
            QString curTag = inputTags.at(j).at(k);
            GroupingTag curGTag;
            curGTag.tag = curTag;
            curGTag.category = curCata;
//            allTags.insert(curGTag, 1);
            QString newRow = curGTag.toTagString();
            tagAllList->addItem(newRow);
        }
    }

    //shared tags
    inputTags.clear();

    for (int k = 0; k < nTrodes.length(); k++) {
        SingleSpikeTrodeConf curNTrode = nTrodes.at(k);

        if (k == 0) {
            inputTags = Helper::getSortedTagList(&curNTrode);
        }
        else {
            for (int i = 0; i < inputTags.length(); i++) {
                QList<QString> curTagList = inputTags.at(i);

                QString curCata = inputTags.at(i).at(0);
                for (int j = 1; j < curTagList.length(); j++) {
                    QString curTag = curTagList.at(j);
                    GroupingTag curGTag;
                    curGTag.category = curCata;
                    curGTag.tag = curTag;
                    if (!curNTrode.gTags.contains(curGTag)) {
                        curTagList.removeAt(j);
                        j--;
                    }
                }
                inputTags.replace(i, curTagList);
            }
        }
    }
    //now put tags into the groupingTagSharedList
    for (int j = 0; j < inputTags.length(); j++) {
        QString curCata = inputTags.at(j).at(0); //first item of each sub list is the catagorey
        for (int k = 1; k < inputTags.at(j).length(); k++) {
            QString curTag = inputTags.at(j).at(k);
            QString newRow = QString("[%1] %2").arg(curCata).arg(curTag);
            tagSharedList->addItem(newRow);
        }
    }

}

void TagGroupingPanel::sortTagLists() {
    sortTagList(tagAllList);
    sortTagList(tagSharedList);
}

void TagGroupingPanel::sortTagList(QListWidget *list) {
    QHash<QString, QHash<QString, int> > tagHash;

    for (int i = 0; i < list->count(); i++) {
        GroupingTag curTag;
        curTag.readInTagStr(list->item(i)->text());
        if (tagHash.contains(curTag.category)) { //if the category is in the hash
            QHash<QString, int> subHash = tagHash.value(curTag.category);
            if (!subHash.contains(curTag.tag)) { //add the tag if it hasn't already been added
                subHash.insert(curTag.tag, 1);
                tagHash.remove(curTag.category);
                tagHash.insert(curTag.category, subHash);
            }

        }
        else { //if the category isn't yet in the tagHash, add it along with the tag
            QHash<QString, int> subHash;
            subHash.insert(curTag.tag, 1);
            tagHash.insert(curTag.category, subHash);
        }

    }

    list->clear();
    QHashIterator<QString, QHash<QString, int> > mainI(tagHash);
    while (mainI.hasNext()) {
        mainI.next();
//        qDebug() << "- [" << mainI.key() << "]";
        QHashIterator<QString, int> subI(mainI.value());
        while (subI.hasNext()) {
            subI.next();
//            qDebug() << "       - " << subI.key();
            GroupingTag gt;
            gt.category = mainI.key();
            gt.tag = subI.key();
            list->addItem(gt.toTagString());
        }
    }
}

void TagGroupingPanel::processCategorySelectBox(int selectedIndex) {
//    qDebug() << "selected: " << categorySelectBox->itemText(selectedIndex);
    if (selectedIndex == categorySelectBox->count()-1) {
//        qDebug() << "create new category";
        addCategory();
    }
    //then load tags
    populateTagSelectBox();

}

void TagGroupingPanel::processTagSelectBox(int selectedIndex) {
//    qDebug() << "processing " << selectedIndex;
    QString curCata = categorySelectBox->currentText();
    if (curCata != "All" && selectedIndex == (tagSelectBox->count()-1)) {

        AddTagDialog *askUser = new AddTagDialog();
        askUser->setWindowFlags(Qt::WindowStaysOnTopHint);
        int retVal = askUser->exec();

        if (retVal == QDialog::Accepted) {
            QString newTag = askUser->getText();
            if (!newTag.isEmpty()) {
                if (newTag.contains(";") || newTag.contains(",") || newTag.contains("=") || newTag.contains("/") || newTag.contains("]") || newTag.contains("[")
                        || (newTag == "All")) {
                    QMessageBox::warning(this,tr("Invalid Character"), tr("Warning: You cannot include the characters {';' ',' '=', '/', '[', ']', 'All'} in a tag."));
                    tagSelectBox->setCurrentIndex(tagSelectBox->count()-2);
                    delete askUser;
                    return;
                }

                QString category = categorySelectBox->currentText(); // category that we'll be adding the tag to
                if (cataDict.isTagInCategory(category,newTag)) {
                    tagSelectBox->setCurrentIndex(tagSelectBox->count()-2);
                    delete askUser;
                    return;
                }
                cataDict.addTagToCategory(category,newTag);
                emit sig_newTagAdded(category,newTag); //send new tag back to the main window's dictionary

                tagSelectBox->insertItem((tagSelectBox->count()-1),newTag);
            }
        }
        delete askUser;
        tagSelectBox->setCurrentIndex(tagSelectBox->count()-2);
    }
}

void TagGroupingPanel::addCategory() {
//    qDebug() << "addCategory()";

    AddTagDialog *askUser = new AddTagDialog();
    askUser->setAddType("Category");
    askUser->setWindowFlags(Qt::WindowStaysOnTopHint);
    int retVal = askUser->exec();

    if (retVal == QDialog::Accepted) {
        QString newCategory = askUser->getText();
        if (!newCategory.isEmpty()) {
            if (newCategory.contains(";") || newCategory.contains(",") || newCategory.contains("=") || newCategory.contains("/") || newCategory.contains("]") || newCategory.contains("[")
                    || (newCategory == "All")) {
                QMessageBox::warning(this,tr("Invalid Character"), tr("Warning: You cannot include the characters {';' ',' '=', '/', '[', ']', 'All'} in a category name."));
                categorySelectBox->setCurrentIndex(0);
                delete askUser;
                return;
            }

            if (cataDict.categoryExists(newCategory)) {
                categorySelectBox->setCurrentIndex(0);
                delete askUser;
                return;
            }

            cataDict.addCategory(newCategory);
            emit sig_categoryAdded(newCategory); //send new category back to the main window's dictionary

            delete askUser;
            categorySelectBox->insertItem((categorySelectBox->count()-1),newCategory);
            categorySelectBox->setCurrentIndex(categorySelectBox->count()-2);
            return;
        }
    }
    delete askUser;
    categorySelectBox->setCurrentIndex(0);


//    emit sig_categoryAdded(category);
}

void TagGroupingPanel::populateCategorySelectBox() {
//    qDebug() << "Populating category selection box";
    categorySelectBox->clear();
    categorySelectBox->addItem("All");

    QHash<QString, QHash<QString,QString> >categories = cataDict.getCategories();
    QHashIterator<QString, QHash<QString,QString> > iter(categories);
    while(iter.hasNext()) {
        iter.next();
        if (iter.key() != "All") {
            categorySelectBox->addItem(iter.key());
        }
    }
    categorySelectBox->addItem("+Create New");
    categorySelectBox->setCurrentIndex(0);
}

void TagGroupingPanel::addTagToList() {
//    qDebug() << "Adding Tag to List";
    if ((tagSelectBox->currentIndex() != 0) /*&& (tagSelectBox->currentIndex() != (tagSelectBox->count()-1))*/) {

        if (categorySelectBox->currentIndex() != 0 && (tagSelectBox->currentIndex() == tagSelectBox->count()-1) ) { //if the category is not All, then don't process an add of the bottom element
            return;
        }

        QListWidget *curList = NULL;
        if (tagListView->currentIndex() == 0) {
            curList = tagAllList;
        }
        else
            curList = tagSharedList;


        QString addedCategory = categorySelectBox->currentText();
        QString addedTag = tagSelectBox->currentText();
        GroupingTag addedGTag;
        if (addedCategory == "All") {
            addedGTag.readInTagStr(addedTag);
//            qDebug() << "addedTag=" << addedTag;
//            qDebug() <<" [" << addedGTag.category << "] [" << addedGTag.tag << "]";
        }
        else {
            addedGTag.category = addedCategory;
            addedGTag.tag = addedTag;
        }

        //check for duplicates in the addedGTag list
        for (int i = 0; i < addedTags.length(); i++) {
            GroupingTag curItem;
            curItem = addedTags.at(i);
//            curItem.readInTagStr(tagAllList->item(i)->text());
            if (addedGTag == curItem) {
//                qDebug() << "Duplicate found";
                return;
            }
        }
        bool tagAlreadyAdded = false;

        for (int i = 0; i < addedTags.length(); i++) {
            if (addedGTag == addedTags.at(i)) {
                tagAlreadyAdded = true;
                break;
            }
        }
        if (!tagAlreadyAdded) {
            addedTags.append(addedGTag);
            for (int i = 0; i < removedTags.length(); i++) { //remove the added tag from the removedTags list if it had previously been removed
                if (addedGTag == removedTags.at(i)) {
                    removedTags.removeAt(i);
                    break;
                }
            }
        }

        addTagToGUILists(addedGTag.toTagString());
        emit sig_GTagAddedToNTrodes(addedGTag);
        emit sig_enableApplyButtons();
    }
}

void TagGroupingPanel::removeTagFromList() {
//    qDebug() << "Removing tag from list";

    QListWidget *curList = NULL;
    if (tagListView->currentIndex() == 0) { //if the removed tag was selected from the tagAllList
        curList = tagAllList;
    }
    else { //otherwise it was from the tagSharedList
        curList = tagSharedList;
    }

    if (curList->count() > 0) {
        GroupingTag removedTag;
        removedTag.readInTagStr(curList->currentItem()->text());

        bool tagAlreadyInList = false;
        for (int i = 0; i < removedTags.length(); i++) {
            if (removedTag == removedTags.at(i)) {
                tagAlreadyInList = true;
                break;
            }
        }
        if (!tagAlreadyInList) {
            removedTags.append(removedTag);
            for (int i = 0; i < addedTags.length(); i++) { //remove the removed tag from the addedTags list if it had previously been added
                if (removedTag == addedTags.at(i)) {
                    addedTags.removeAt(i);
                    break;
                }
            }
        }

//        delete tagAllList->currentItem();
        //remove the item from both the tagAllList and the tagSharedList
        QString curItemStr = curList->currentItem()->text();
        removeTagFromGUILists(curItemStr);
        emit sig_GTagRemovedFromNTrodes(removedTag);
        emit sig_enableApplyButtons();
    }
}

void TagGroupingPanel::addTagToGUILists(QString tagStr) {
    QListWidgetItem *newTag = new QListWidgetItem();
    newTag->setText(tagStr);

    bool alreadyAddedToAll = false;
    bool alreadyAddedToShared = false;
    for (int i = 0; i < tagAllList->count(); i++) {
        if (tagAllList->item(i)->text() == tagStr) {
            alreadyAddedToAll = true;
            break;
        }
    }
    if (!alreadyAddedToAll) {
        tagAllList->insertItem(tagAllList->count()-1,tagStr);
    }


    for (int i = 0; i < tagSharedList->count(); i++) {
        if (tagSharedList->item(i)->text() == tagStr) {
            alreadyAddedToShared = true;
            break;
        }
    }
    if (!alreadyAddedToShared) {
        tagSharedList->insertItem(tagSharedList->count()-1,tagStr);
    }

    if (alreadyAddedToAll && alreadyAddedToShared)
        delete newTag;

    sortTagLists();
}

void TagGroupingPanel::removeTagFromGUILists(QString tagStr) {
    for (int i = 0; i < tagAllList->count(); i++) {
        if (tagAllList->item(i)->text() == tagStr) {
            delete tagAllList->item(i);
            break;
        }
    }
    for (int i = 0; i < tagSharedList->count(); i++) {
        if (tagSharedList->item(i)->text() == tagStr) {
            delete tagSharedList->item(i);
            break;
        }
    }
}

void TagGroupingPanel::populateTagSelectBox() {
    tagSelectBox->clear();
    tagSelectBox->addItem("Select Tag to Add");
    QString curCategory = categorySelectBox->currentText();
    WorkspaceEditor::setComboBoxFirstItemUnselectable(tagSelectBox);

    if (curCategory == "All") {
        QHashIterator<GroupingTag, int> i(cataDict.getAllTags());
//        cataDict.printAll();
        while (i.hasNext()) {
            i.next();
            GroupingTag curTag = i.key();
            tagSelectBox->addItem(curTag.toTagString());
        }
    }
    else {
        //    qDebug() << "  from category -" << categorySelectBox->currentText();
        QHashIterator<QString,QString> i(cataDict.getCategorysTags(categorySelectBox->currentText()));

        //    QHashIterator<QString,int> i(availTags);
        while(i.hasNext()) {
            i.next();
            tagSelectBox->addItem(i.key());
        }
        tagSelectBox->addItem("+Create New"); //only have create new option on categories other than All
    }


    tagSelectBox->setCurrentIndex(0);
}

void TagGroupingPanel::setSharedTabEnabled(bool enabled) {
    tagListView->setTabEnabled(1,enabled);
}

TrodesTreeDelegate::TrodesTreeDelegate(QObject *parent, TrodesTree *thisTree) :
    QStyledItemDelegate(parent), tree(thisTree)
{
}

void TrodesTreeDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
//    qDebug() << "TrodesTree paint event";
    QStyledItemDelegate::paint(painter, option, index);
    if (tree != NULL && tree->currentItem() != NULL) {
        painter->setPen(QPen(Qt::black, 2, Qt::SolidLine, Qt::SquareCap));
        painter->drawRect(tree->visualItemRect(tree->currentItem()));
//        tree->currentItem()->setBackgroundColor(0, DEFAULT_CURRENT_ITEM_COLOR);
    }

}

TrodesTree::TrodesTree(QWidget *parent) : QTreeWidget(parent) {
    this->setSelectionMode(QAbstractItemView::SingleSelection);
    this->setAutoFillBackground(true);
    prevSelectedItem = NULL;
    focusKey = FK_NULL;
    focusTarget = FT_NULL;

    //delegate = new TrodesTreeDelegate(parent, this);
    //this->setItemDelegate(delegate);
}

void TrodesTree::setFocusPartner(FocusKey activatingKey, FocusTarget partner) {
    focusKey = activatingKey;
    focusTarget = partner;
}

void TrodesTree::deselectAll(void) {
    while(this->selectedItems().length() > 0) { //deselect all previously selected items
        this->selectedItems().at(0)->setSelected(false);
    }
}

void TrodesTree::setHighlightChannel(int HWChan) {
    deselectAll();

    for (int i=0; i<topLevelItemCount(); i++) {
        QTreeWidgetItem *tmpItem = topLevelItem(i);
        if (tmpItem->childCount() > 0) {
            QTreeWidgetItem *tmpChild = tmpItem;
            for (int j=0;j<tmpItem->childCount();j++) {
                tmpChild = itemBelow(tmpChild);
                bool success;
                int HWNum = tmpChild->text(0).split(" ").last().toInt(&success);
                if (success && HWNum == HWChan) {
                    setCurrentItem(tmpChild);
                    return;

                }
            }
        } else {
            bool success;
            int HWNum = tmpItem->text(0).split(" ").last().toInt(&success);
            if (success && HWNum == HWChan) {
                setCurrentItem(tmpItem);
                return;

            }
        }
    }

}

void TrodesTree::processRangeHighlight(void) {
    if (prevSelectedItem == NULL) { //if no previously selected item, set it to the first item in the list
        prevSelectedItem = this->topLevelItem(0);
    }
    //Aquire range indexs
    int prevItemTopIndex, curItemTopIndex; //top level index of the previous and current items
    int prevItemChildIndex = -1; //previous items index within a top level item.  -1 means it's not a child
    int curItemChildIndex = -1;  //current items index within a top level item.  -1 means it's not a child

    prevItemTopIndex = this->indexOfTopLevelItem(prevSelectedItem);
    curItemTopIndex = this->indexOfTopLevelItem(this->currentItem());

    if (prevItemTopIndex == -1) { //if prev item is a child
        prevItemTopIndex = this->indexOfTopLevelItem(prevSelectedItem->parent());
        prevItemChildIndex = prevSelectedItem->parent()->indexOfChild(prevSelectedItem);
    }

    if (curItemTopIndex == -1) { //if current item is a child
        curItemTopIndex = this->indexOfTopLevelItem(this->currentItem()->parent());
        curItemChildIndex = this->currentItem()->parent()->indexOfChild(this->currentItem());
    }

    //determine absolute highlight range
    int rangeTopStartInd, rangeTopEndInd, rangeChildStartInd, rangeChildEndInd;

    if (prevItemTopIndex == curItemTopIndex) { //they are in the same nTrode
        rangeTopStartInd = curItemTopIndex;
        rangeTopEndInd = curItemTopIndex;
        if (curItemChildIndex > prevItemChildIndex) { //cur child is below prev
            rangeChildStartInd = prevItemChildIndex;
            rangeChildEndInd = curItemChildIndex;
        }
        else if (curItemChildIndex < prevItemChildIndex) { //cur child is above prev
            rangeChildStartInd = curItemChildIndex;
            rangeChildEndInd = prevItemChildIndex;
        }

    }
    else if (curItemTopIndex > prevItemTopIndex) { //selected item is below the previous one
        rangeTopStartInd = prevItemTopIndex;
        rangeTopEndInd = curItemTopIndex;
        rangeChildStartInd = prevItemChildIndex;
        rangeChildEndInd = curItemChildIndex;
    }
    else if (curItemTopIndex < prevItemTopIndex) { //selected item is above the previous one
        rangeTopStartInd = curItemTopIndex;
        rangeTopEndInd = prevItemTopIndex;
        rangeChildStartInd = curItemChildIndex;
        rangeChildEndInd = prevItemChildIndex;

    }
    else
        qDebug() << "If you see this, something went terribly wrong";

    //now highlight
    if (rangeTopStartInd == rangeTopEndInd) { //they were within the same nTrode...
        if (rangeChildStartInd == -1 || rangeChildEndInd == -1) {
            topLevelItem(rangeTopStartInd)->setSelected(true);
            rangeChildStartInd = 0;
        }

        for (int i = rangeChildStartInd; i <= rangeChildEndInd; i++) {
            topLevelItem(rangeTopStartInd)->child(i)->setSelected(true);
        }
    }
    else //outside the same nTrode
        for (int i = rangeTopStartInd; i <= rangeTopEndInd; i++) {
            QTreeWidgetItem *curTopItem = topLevelItem(i);
            if (i == rangeTopStartInd && rangeChildStartInd != -1) { //first item was not a selected nTrode, but a child, dont select it

            }
            else //select each nTrode within the range
                curTopItem->setSelected(true);

            if (i == rangeTopStartInd && rangeChildStartInd != -1)
                for (int j = rangeChildStartInd; j < curTopItem->childCount(); j++) {
                    curTopItem->child(j)->setSelected(true);
                }
            else if (i == rangeTopEndInd && rangeChildEndInd != -1)
                for (int j = 0; j <= rangeChildEndInd; j++) {
                    curTopItem->child(j)->setSelected(true);
                }
            else
                for (int j = 0; j < curTopItem->childCount(); j++) {
                    curTopItem->child(j)->setSelected(true);
                }
        }
}

void TrodesTree::keyPressEvent(QKeyEvent *event) {

    if (event->key() == Qt::Key_Left) {
        if (focusKey == FK_LeftArrow)
            emit sig_sendFocus((int)focusTarget);
    }
    if (event->key() == Qt::Key_Right) {
        if (focusKey == FK_RightArrow)
            emit sig_sendFocus((int)focusTarget);
    }
    if (event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return)
        emit sig_enterKeyPressed();

    QTreeWidgetItem *newCurItem = NULL;

    if (this->topLevelItemCount() > 0 && this->currentItem() != NULL) {
        if (event->key() == Qt::Key_Space) { //select on space bar press.  Multi-select engaged by default
            if (!(event->modifiers() & Qt::ControlModifier) /*&& !(event->modifiers() & Qt::ShiftModifier)*/) { //disable multi-select when control or shift isn't clicked
                this->deselectAll();
            }

            if (event->modifiers() & Qt::ShiftModifier) { //if the shift modifier was pressed, select all within the range of the this click and the previous
                processRangeHighlight();
                this->currentItem()->setSelected(false); //this is dumb, but QTreeWidget::keyPressEvent(event) actually automatically selects the current item on "space" so we have to set it false here so it doesn't get auto false'd by Qt
            }
            else { //if shift was not pressed, then remember the selected item
                setPrevSelectedItem(this->currentItem());
            }

        }

        if (event->key() == Qt::Key_Down) {

            int curIndex = this->indexOfTopLevelItem(currentItem());
            if (curIndex != -1) { //if the current item is a top level item
                if (curIndex == this->topLevelItemCount() - 1) { //if this is the last nTrode
                    if (this->currentItem()->childCount() == 0) //no children, exit
                        return;
                    //move highlight down to first child
                    newCurItem = currentItem()->child(0);
                }
                //otherwise, move down one
                if (currentItem()->childCount() == 0) { //if no children, select next parent
                    newCurItem = topLevelItem((curIndex+1));
                }
                else { //it has children, select the first child
                    newCurItem = currentItem()->child(0);
                }

            }
            else { //if the current item is a child
                QTreeWidgetItem *curParent = currentItem()->parent(); //get the currentItem's parent
                curIndex = curParent->indexOfChild(currentItem()); //set curIndex to the index of the child in the nTrode
                int parentIndex = this->indexOfTopLevelItem(curParent); //get the currentItem's parent's index

                if (curIndex == curParent->childCount() - 1) { //if this is the last child, move to the next parent
                    if (parentIndex == this->topLevelItemCount() - 1) //curParent is the last nTrode, exit
                        return;
                    newCurItem = topLevelItem((parentIndex+1));
                }
                else { //this is not the last child, highlight the next child
                    newCurItem = curParent->child(curIndex+1);
                }

            }
            if (newCurItem != NULL) {
                bool setSelected = true;
                if (!newCurItem->isSelected()) //if the newCurItem was not set selected by the user, make sure to flip as setCurrentItem() sets the item to selected by default
                    setSelected = false;

                currentItem()->setBackground(0, QColor("white"));
//                this->setCurrentItem(newCurItem);
                newCurItem->setSelected(setSelected);
//                newCurItem->setBackgroundColor(0, QColor(curBackgroundColor));
            }
        }

        if (event->key() == Qt::Key_Up) {
            int curIndex = this->indexOfTopLevelItem(currentItem());
            if (curIndex != -1) { //if the current item is a top level item
                if (curIndex == 0) { //if this is the first top level item
                    return; //exit
                }
                //otherwise, move up one
                if (this->topLevelItem((curIndex-1))->childCount() == 0) { //if the next parent has no children, select next parent
                    newCurItem = topLevelItem((curIndex-1));
                }
                else { //it has children, select the last child
                    newCurItem = topLevelItem((curIndex-1))->child(topLevelItem((curIndex-1))->childCount()-1);
                }

            }
            else { //if the current item is a child
                QTreeWidgetItem *curParent = currentItem()->parent(); //get the currentItem's parent
                curIndex = curParent->indexOfChild(currentItem()); //set curIndex to the index of the child in the nTrode
                int parentIndex = this->indexOfTopLevelItem(curParent); //get the currentItem's parent's index

                if (curIndex == 0) { //if this is the first child, move to the parent
                    newCurItem = curParent;
                }
                else { //this is not the first child, highlight the next child above it
                    newCurItem = curParent->child(curIndex-1);
                }

            }
            if (newCurItem != NULL) {
                bool setSelected = true;
                if (!newCurItem->isSelected()) //if the newCurItem was not set selected by the user, make sure to flip as setCurrentItem() sets the item to selected by default
                    setSelected = false;

                currentItem()->setBackground(0, QColor("white"));
//                this->setCurrentItem(newCurItem);
                newCurItem->setSelected(setSelected);
            }
        }
    }
    QTreeWidget::keyPressEvent(event);
    if (newCurItem != NULL) {
//        newCurItem->setBackgroundColor(0, DEFAULT_CURRENT_ITEM_COLOR);
        this->viewport()->repaint();
    }
}

void TrodesTree::mousePressEvent(QMouseEvent *event) {
    if (currentItem() != NULL) { //if currentItem exists
        //reset to non currentItem color
        currentItem()->setBackground(0, QColor("white"));
    }

    if (!(event->modifiers() & Qt::ControlModifier)) { //if channels were not control clicked
        this->deselectAll();
    }

    QTreeWidget::mousePressEvent(event);

    if (event->modifiers() & Qt::ShiftModifier) { //shift click, range highlight
        this->processRangeHighlight();
    }
    else
        this->setPrevSelectedItem(this->currentItem());

    this->viewport()->repaint();
}

HWChannelTreeWidget::HWChannelTreeWidget(QWidget *parent) : TrodesTree(parent) {
    this->setSelectionMode(QAbstractItemView::MultiSelection);
    this->setFocusPartner(FK_LeftArrow, FT_nTrodeTree);

    QHeaderView *topBar = new QHeaderView(Qt::Horizontal);
    QString headerSS = QString("QHeaderView::section { background-color:%1; border: 2px solid %1;}").arg(WE_TABLE_HEADER_COLOR);
    topBar->setStyleSheet(headerSS);

    //possible solution...
    QHBoxLayout *hLay = new QHBoxLayout();
    QLabel *barTitle = new QLabel(tr("Unassigned Channels"));

    hLay->addWidget(barTitle, 1, Qt::AlignCenter);
    hLay->setContentsMargins(0,0,0,0);
    topBar->setLayout(hLay);

    //topBar->setSectionResizeMode(QHeaderView::Stretch);

    this->setHeader(topBar);
    this->setHeaderLabel("");
}

void HWChannelTreeWidget::keyPressEvent(QKeyEvent *event) {
    if (this->topLevelItemCount() > 0) {
//        if (event->key() == Qt::Key_Left) {
//            emit sig_sendFocus((int)FT_nTrodeTree);
//        }

//        if (event->key() == Qt::Key_Up) {
//            qDebug() << "Go up! (HW)";
//        }

//        if (event->key() == Qt::Key_Down) {
//            qDebug() << "Go Down! (HW)";
//        }

        if (event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return) {
            emit(sig_sendChannels());
        }
    }

    TrodesTree::keyPressEvent(event);
}

void HWChannelTreeWidget::mouseMoveEvent(QMouseEvent *event) {
    //qDebug() << "Mouse Move";
    QTreeWidget::mouseMoveEvent(event);
}

void HWChannelTreeWidget::mousePressEvent(QMouseEvent *event) {

    TrodesTree::mousePressEvent(event);

    if (this->currentItem() != NULL) {
        QStringList chanL = this->currentItem()->text(0).split(" ");
        int chanID = chanL.at(chanL.length()-1).toInt();
        //qDebug() << "Clicked on channel: " << chanID;
        emit sig_clickedOnChannel(chanID);
    }

}

void HWChannelTreeWidget::addHardwareChannel(QString name) {
    QStringList nameL = name.split(" ");
    QTreeWidgetItem *newChan = new QTreeWidgetItem();
    newChan->setText(0,name);
    int chNumToAdd = nameL.at(nameL.length()-1).toInt();
    for (int i = 0; i < this->topLevelItemCount(); i++) {
       QStringList curRowList = this->topLevelItem(i)->text(0).split(" ");
       //qDebug() << curRowList;
       int curChNum = curRowList.at(curRowList.length()-1).toInt();
       if (curChNum > chNumToAdd) {
           this->insertTopLevelItem(i, newChan);
//           this->insertItem(i, name);
           return;
       }
    }
//    this->addItem(name);
    this->addTopLevelItem(newChan);
}

void HWChannelTreeWidget::removeHardwareChannel(int channelId) {
    this->setPrevSelectedItem(NULL);
    for (int i = 0; i < this->topLevelItemCount(); i++) {
        QStringList curRowList = this->topLevelItem(i)->text(0).split(" ");
        if (channelId == curRowList.at(curRowList.length()-1).toInt()) {
            delete this->topLevelItem(i);
        }
    }
}

QList<int> HWChannelTreeWidget::getChannelsInList() {
    QList<int> output;
    for (int i = 0; i < this->topLevelItemCount(); i++) {
        output.append(topLevelItem(i)->text(0).split(" ").last().toInt());
    }

    return output;
}

void HWChannelTreeWidget::setChannelsInList(QList<int> channels) {
    clear();
    for (int i=0;i<channels.length();i++) {
        QTreeWidgetItem *newChan = new QTreeWidgetItem();
        QString name = QString("HW %1").arg(channels[i]);
        newChan->setText(0,name);

        this->addTopLevelItem(newChan);
    }
}

QList<QTreeWidgetItem*> HWChannelTreeWidget::getSelectedChannels() {
    return(selectedItems());
}

QTreeWidgetItem* HWChannelTreeWidget::getFirstAvailChan() {
    if (topLevelItemCount() == 0)
        return(NULL);

    return(topLevelItem(0));
}
//---------------------------------------------------------

NTrodeChannelMappingTable::NTrodeChannelMappingTable(QWidget *parent) : QTableWidget(parent) {
    //setSizeAdjustPolicy(QTableWidget::AdjustToContents);
    setSelectionMode(QAbstractItemView::ExtendedSelection);
    columnHeaders << "HW Channel" << "nTrode ID";
    setColumnCount(2);
    setHorizontalHeaderLabels(columnHeaders);

    verticalHeader()->hide();
    setSortingEnabled(true);
    setColumnWidth(0,100);

    editProcessorLocked = false;
    _isEditing = false;
    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(cellChanged(int,int)),this,SLOT(processCellEdit(int,int)));
    connect(this, SIGNAL(cellClicked(int,int)), this,SLOT(processCellClicked(int,int)));
    connect(this,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(rightClickMenuRequested(QPoint)));
    connect(this,SIGNAL(itemSelectionChanged()),this,SLOT(processNewItemSelection()));
    this->horizontalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this->horizontalHeader(), &QHeaderView::customContextMenuRequested, this, &NTrodeChannelMappingTable::rightClickHeaderMenuRequested);
}

void NTrodeChannelMappingTable::setEditProcessorLock(bool l) {
    editProcessorLocked = l;
}

void NTrodeChannelMappingTable::assignNTrodeIDToChannel(int HWChan, int nTrodeID) {
    //assign an nTrode ID to a HW channel in the table.  A -1 value means "Not assigned"

    int nTrodeCol = columnHeaders.indexOf("nTrode ID");


    for (int i=0; i<rowCount();i++) {
        if (item(i,0) && item(i,0)->data(0).toInt() == HWChan) {
            /*if (item(i,nTrodeCol)) {
                QTableWidgetItem *oldNTrodeItem = takeItem(i,nTrodeCol);
            }*/
            QTableWidgetItem *newNTrodeItem = new QTableWidgetItem();

            if (nTrodeID >= 0) {
                newNTrodeItem->setData(0,QVariant(nTrodeID));
            } else {
                newNTrodeItem->setData(0,QVariant("Not assigned"));
            }
            setItem(i, nTrodeCol, newNTrodeItem); //set object
            break;
        }

    }

    nTrodeHWAssignments[HWChan] = nTrodeID;
}

void NTrodeChannelMappingTable::setHighlightChannel(int HWChan) {

    if (!_isEditing) {
        int nTrodeCol = columnHeaders.indexOf("nTrode ID");
        for (int i=0; i<rowCount();i++) {
            if (item(i,0) && item(i,0)->data(0).toInt() == HWChan) {
                setCurrentCell(i,nTrodeCol);
                return;
            }

        }
    }

}

QList<int> NTrodeChannelMappingTable::getChannelAssignments() {
    return nTrodeHWAssignments;
}

void NTrodeChannelMappingTable::setNumHardwareChannels(int numHWChan) {
    //Changes the total number of hardware channels in the table

    setEditProcessorLock(true);
    sortItems(0);
    setSortingEnabled(false);

    if (rowCount() > numHWChan) {
        for (int i = numHWChan; i < rowCount(); i++) {
            for (int j = 0; j < columnCount(); j++) {
                if (item(i,j)) {
                    //item(i,j)->view = 0;
                    delete takeItem(i,j);
                }
            }
        }
    }

    setRowCount(numHWChan);
    totalHwChannels = numHWChan;
    nTrodeHWAssignments.clear();

    int nTrodeCol = columnHeaders.indexOf("nTrode ID");



    for (int i = 0; i < totalHwChannels; i++) {
        QTableWidgetItem *newItem = new QTableWidgetItem();
        setItem(i, 0, newItem); //set object
        //newItem->setText(QString("%1").arg(i));
        newItem->setData(0,QVariant(i));
        newItem->setBackground(QBrush(QColor(200,200,200)));
        newItem->setFlags(Qt::NoItemFlags|Qt::ItemIsEnabled);

        QTableWidgetItem *newNTrodeItem = new QTableWidgetItem();
        setItem(i, nTrodeCol, newNTrodeItem); //set object
        newNTrodeItem->setData(0,QVariant("Not assigned"));

        nTrodeHWAssignments.append(-1);
        //newNTrodeItem->setBackground(QBrush(QColor(256,256,256)));

    }

    setSortingEnabled(true);
    setEditProcessorLock(false);


}

bool NTrodeChannelMappingTable::isEditing() {
    return _isEditing;
}

void NTrodeChannelMappingTable::processNewItemSelection() {
    lastSelectionList = selectedIndexes();
}

void NTrodeChannelMappingTable::processCellClicked(int row, int col) {
    lastSelectionList = selectedIndexes();
    const QTableWidgetItem *tmpItem;

    tmpItem = item(row,col);

    tmpItem = item(row,0); //Get the HW channel
    int HWChanVal = tmpItem->data(0).toInt();

    _isEditing = true;
    emit channelClicked(HWChanVal);
    _isEditing = false;
}

void NTrodeChannelMappingTable::processCellEdit(int row, int col) {
    //Responsible for processing when a value was edited directly in the table
    if (!editProcessorLocked) {

        setEditProcessorLock(true);
        int nTrodeCol = columnHeaders.indexOf("nTrode ID");
        const QTableWidgetItem *tmpItem;

        tmpItem = item(row,col);
        bool isAssigned;
        int nTrodeVal = tmpItem->data(0).toInt(&isAssigned);
        if (!isAssigned) {
            nTrodeVal = -1;
        }

        //QModelIndexList sItems = selectedIndexes();

        //Update the assignement record for the edit cell
        tmpItem = item(row,0); //Get the HW channel
        int HWChanVal = tmpItem->data(0).toInt();
        nTrodeHWAssignments[HWChanVal] = nTrodeVal;


        //If additional cells are selected, copy the new value into those as well
        for (int i=0;i<lastSelectionList.length();i++) {

            if (lastSelectionList[i].row() != row) {
                int HWChan = item(lastSelectionList[i].row(),0)->data(0).toInt();
                QTableWidgetItem *newNTrodeItem = new QTableWidgetItem();

                if (nTrodeVal >= 0) {
                    newNTrodeItem->setData(0,QVariant(nTrodeVal));
                } else {
                    newNTrodeItem->setData(0,QVariant("Not assigned"));
                }
                setItem(lastSelectionList[i].row(), nTrodeCol, newNTrodeItem); //set object
                nTrodeHWAssignments[HWChan] = nTrodeVal;
            }

        }




        _isEditing = true;
        emit newChannelAssignment();
        _isEditing = false;

        setEditProcessorLock(false);

    }
}

void NTrodeChannelMappingTable::rightClickMenuRequested(QPoint pos) {

    //QModelIndex index=indexAt(pos);

    QMenu *menu=new QMenu(this);
    QAction *action1 = new QAction("Unassign", this);
    QAction *action2 = new QAction("Copy", this);
    QAction *action3 = new QAction("Paste", this);

    connect(action1,SIGNAL(triggered(bool)),this,SLOT(deleteSelected()));
    connect(action2,SIGNAL(triggered(bool)),this,SLOT(copyToClipboard()));
    connect(action3,SIGNAL(triggered(bool)),this,SLOT(pasteFromClipboard()));

    menu->addAction(action1);
    menu->addAction(action2);
    menu->addAction(action3);

    //First make sure everything in the clipboard is an integer. If not, gray out paste option
    bool allGood = true;
    QString pasted_text;

    const QClipboard *clipboard = QApplication::clipboard();
    const QMimeData *mimeData = clipboard->mimeData();

    if (mimeData->hasText()) {
        pasted_text = mimeData->text();
        QStringList list = pasted_text.split(QRegExp("[\r\n]"),QString::SkipEmptyParts);
        for (int i=0; i<list.length();i++) {
            bool conversionOk;
            int tmpNtrode;

            tmpNtrode = list.at(i).toInt(&conversionOk);
            if (!conversionOk) {
               allGood = false;
            }
            if (tmpNtrode < -1) {
                allGood = false;
            }
        }
    } else {
        allGood = false;
    }

    if (!allGood) {
        action3->setEnabled(false);
    }

    menu->popup(viewport()->mapToGlobal(pos));
}

void NTrodeChannelMappingTable::rightClickHeaderMenuRequested(QPoint pos){
    QMenu *menu=new QMenu(this);
    QAction *action1 = new QAction("Auto-assign all channels", this);

    connect(action1, &QAction::triggered,this, [this](){
        qDebug() << "Auto-assigning hardware channels";
        for(int i = 0; i < totalHwChannels; ++i){
            this->assignNTrodeIDToChannel(i, i+1);
        }
    });

    menu->addAction(action1);
    menu->popup(viewport()->mapToGlobal(pos));
}

void NTrodeChannelMappingTable::deleteSelected() {
    //Make the currently selected rows unassigned channels

    int nTrodeCol = columnHeaders.indexOf("nTrode ID");

    int row = currentIndex().row();
    int col = currentIndex().column();

    if (col == nTrodeCol) {

        editProcessorLocked=true;
        setSortingEnabled(false);
        QModelIndexList sItems = selectedIndexes();
        for (int i=0;i<sItems.length();i++) {
            int HWChanVal = item(sItems[i].row(),0)->data(0).toInt();
            assignNTrodeIDToChannel(HWChanVal,-1);

        }
        setSortingEnabled(true);
        _isEditing = true;
        emit newChannelAssignment();
        _isEditing = false;
        editProcessorLocked=false;

        //const QTableWidgetItem *tmpItem;
        //tmpItem = item(row,0); //Get the HW channel
        //int HWChanVal = tmpItem->data(0).toInt();


    }
}

void NTrodeChannelMappingTable::copyToClipboard() {

    //Copy the currently selected rows in the nTrodeID column to the clipboard
    int nTrodeCol = columnHeaders.indexOf("nTrode ID");

    int col = currentIndex().column();
    QString selected_text;


    if (col == nTrodeCol) {


        QModelIndexList sItems = selectedIndexes();
        for (int i=0;i<sItems.length();i++) {
            QString nTrodeText = item(sItems[i].row(),nTrodeCol)->data(0).toString();
            if (nTrodeText.compare("Not assigned")==0) {
                nTrodeText = "-1";
            }
            selected_text.append(nTrodeText);
            selected_text.append('\r');
            selected_text.append('\n');
        }
        QClipboard *clipboard = QApplication::clipboard();
        clipboard->setText(selected_text);

    }
}

void NTrodeChannelMappingTable::pasteFromClipboard() {
    pasteFromClipboard(0);
}

void NTrodeChannelMappingTable::pasteFromClipboard(int valueToAdd) {
    //Paste a column of integers from the clipboard into the nTrode ID column, starting at the currently selected row
    int nTrodeCol = columnHeaders.indexOf("nTrode ID");

    int col = currentIndex().column();
    int row = currentIndex().row();
    QString pasted_text;


    if (col == nTrodeCol) {
        const QClipboard *clipboard = QApplication::clipboard();
        const QMimeData *mimeData = clipboard->mimeData();

        if (mimeData->hasText()) {
            pasted_text = mimeData->text();
            //setTextFormat(Qt::PlainText);
        } else {
            return;
        }

        //First make sure everything in the clipboard is an integer. If not, return.
        QStringList list = pasted_text.split(QRegExp("[\r\n]"),QString::SkipEmptyParts);
        for (int i=0; i<list.length();i++) {
            bool conversionOk;
            int tmpNtrode;

            tmpNtrode = list.at(i).toInt(&conversionOk);
            if (!conversionOk) {
               return;
            }
            if (tmpNtrode < -1) {
                return;
            }
        }

        //Now we copy the info in the clipboard to the table
        setEditProcessorLock(true);

        QModelIndexList sItems = selectedIndexes();
        if ((list.length() == 1) && (sItems.length() > 1)) {
            //Special case: one number in clipboard and many cells selected. Copy one to many.

            int tmpNtrode = list.at(0).toInt();
            if (tmpNtrode > -1) {
                tmpNtrode = tmpNtrode + valueToAdd;
                if (tmpNtrode < -1) {
                    tmpNtrode = -1;
                }
            }
            for (int i=0;i<sItems.length();i++) {
                int HWChan = item(sItems[i].row(),0)->data(0).toInt();
                QTableWidgetItem *newNTrodeItem = new QTableWidgetItem();

                if (tmpNtrode >= 0) {
                    newNTrodeItem->setData(0,QVariant(tmpNtrode));
                } else {
                    newNTrodeItem->setData(0,QVariant("Not assigned"));
                }
                setItem(sItems[i].row(), nTrodeCol, newNTrodeItem); //set object
                nTrodeHWAssignments[HWChan] = tmpNtrode;

            }
        } else {
            //Do exact copy of list starting from the currently selected item (current index)
            for (int i=0; i<list.length();i++) {

                int tmpNtrode;
                int HWChan;

                tmpNtrode = list.at(i).toInt();
                if (tmpNtrode > -1) {
                    tmpNtrode = tmpNtrode + valueToAdd;
                    if (tmpNtrode < -1) {
                        tmpNtrode = -1;
                    }
                }

                HWChan = item(row,0)->data(0).toInt();
                if (HWChan >= nTrodeHWAssignments.length()) {
                    break;
                }

                QTableWidgetItem *newNTrodeItem = new QTableWidgetItem();

                if (tmpNtrode >= 0) {
                    newNTrodeItem->setData(0,QVariant(tmpNtrode));
                } else {
                    newNTrodeItem->setData(0,QVariant("Not assigned"));
                }
                setItem(row, nTrodeCol, newNTrodeItem); //set object


                nTrodeHWAssignments[HWChan] = tmpNtrode;
                row++;

                //stop copying if we are at the end of the table
                if (row >= this->rowCount()) {
                    break;
                }
            }
        }

        _isEditing = true;
        emit newChannelAssignment();
        _isEditing = false;

        setEditProcessorLock(false);


    }

}

void NTrodeChannelMappingTable::keyPressEvent(QKeyEvent *pEvent) {

    if (pEvent->key() == Qt::Key_Return) {

        // we captured the Enter key press, now we need to move to the next row
        qint32 nNextRow = currentIndex().row() + 1;
        if (!(nNextRow + 1 > model()->rowCount(currentIndex()))) {
            // we are all the way down, we can't go any further
            nNextRow = nNextRow - 1;
        }

        if (state() == QAbstractItemView::EditingState && (lastSelectionList.length()==1)) {

            // if we are editing, confirm and move to the row below
            /*QModelIndexList tmplastSelectionList = lastSelectionList;
            QModelIndex oNextIndex = model()->index(nNextRow, currentIndex().column());
            setCurrentIndex(oNextIndex);
            selectionModel()->select(oNextIndex, QItemSelectionModel::ClearAndSelect);
            lastSelectionList = tmplastSelectionList;*/
        } else {

            // if we're not editing, start editing
            edit(currentIndex());
        }
    } else if (pEvent->key() == Qt::Key_Delete || pEvent->key() == Qt::Key_Backspace) {
        //Make the selected channels unassigned
        deleteSelected();

    } else if (pEvent->matches(QKeySequence::Copy)) {
        //Control-c copies all of the selected nTrodeId's to the clipboard

        copyToClipboard();

    } else if (pEvent->matches(QKeySequence::Paste)) {
        //Control-v pastes a column of integers into the nTrode IDs, starting at the currently selected row
        pasteFromClipboard();
    } else if (pEvent->key() == Qt::Key_Up) {

        QTableWidget::keyPressEvent(pEvent);
        lastSelectionList = selectedIndexes();
    } else if (pEvent->key() == Qt::Key_Down) {

        QTableWidget::keyPressEvent(pEvent);
        lastSelectionList = selectedIndexes();

    } else if (pEvent->key() == Qt::Key_Left) {

        QTableWidget::keyPressEvent(pEvent);
        lastSelectionList = selectedIndexes();
    } else if (pEvent->key() == Qt::Key_Right) {

        QTableWidget::keyPressEvent(pEvent);
        lastSelectionList = selectedIndexes();

    } else {

        // any other key was pressed, inform base class
        QTableWidget::keyPressEvent(pEvent);
    }


}

//-------------------------------------------------------

NTrodeTreeWidget::NTrodeTreeWidget(QWidget *parent) : TrodesTree(parent) {
    loadingXML = false;
    processingSelection = false;
    this->setFocusPartner(FK_RightArrow, FT_ChannelTree);
//    prevSelectedItem = NULL;

//    this->setHeaderHidden(true);
//    this->setHeaderLabel("nTrodeTree");
//    this->setHead
    nTrodeConfigs = NULL;
    QHeaderView *topBar = new QHeaderView(Qt::Horizontal);
    QString headerSS = QString("QHeaderView::section { background-color:%1; border: 2px solid %1;}").arg(WE_TABLE_HEADER_COLOR);
    topBar->setStyleSheet(headerSS);

    //Create the header bar
    QHBoxLayout *hLay = new QHBoxLayout();
//    QChar(0x25b6); Unicode Right Arrow
//    QChar(0x25be); Unicode Down Arrow
    //create the expanding arrow button
    buttonExpandAll = new ClickableLabel(QChar(0x25be));
    QFont f = buttonExpandAll->font();
    f.setPointSize(WE_RIGHT_ARROW_POINT);
    buttonExpandAll->setFont(f);
    buttonExpandAll->setContentsMargins(5,0,0,0);
    QSize buttonSize = QSize(WE_RIGHT_ARROW_POINT,WE_RIGHT_ARROW_POINT);
    buttonExpandAll->setMaximumSize(buttonSize);
    buttonExpandAll->setMinimumSize(buttonSize);
    connect(buttonExpandAll, SIGNAL(sig_clicked()), this, SLOT(expandAllButtonPushed()));

    //add the header's title
    QLabel *barTitle = new QLabel(tr("nTrode View  "));
    barTitle->setContentsMargins(0,0,20,0);

    hLay->addWidget(buttonExpandAll, 0, Qt::AlignLeft);
    hLay->addWidget(barTitle, 1, Qt::AlignCenter);
    hLay->setContentsMargins(0,0,0,0);
    topBar->setLayout(hLay);

    topBar->setSectionResizeMode(QHeaderView::Stretch);

    this->setHeader(topBar);
    this->setHeaderLabel("");

    this->setSelectionMode(QAbstractItemView::MultiSelection);
    //this->setSelectionMode(QAbstractItemView::SingleSelection);
    //disable dropping events on the root level (top level)
    QTreeWidgetItem* rooItem = invisibleRootItem();
    rooItem->setFlags(rooItem->flags() ^ Qt::ItemIsDropEnabled);

    isDragging = false;
    for (int i = 0; i < 10000; i++) {
        availableIDs.append(i+1);
    }

    setItemsExpanded(true); //we do this so the arrow is now pointed down.  For some reason, trying to set the down arrow first causes strange sizing issues.

    connect(this,SIGNAL(itemSelectionChanged()),this,SLOT(processSelection()));
}

void NTrodeTreeWidget::allClear() {
    this->clear();
    //nTrodes.clear();
    availableIDs.clear();
    for (int i = 0; i < 10000; i++) {
        availableIDs.append(i+1);
    }
}

/*NTrodeSettings NTrodeTreeWidget::getNTrode(int index) {
    NTrodeSettings retNTrode;
    if (index >= 0 && index < nTrodes.length()) {
        retNTrode = nTrodes.at(index);
        return(retNTrode);
    }
    else {
        qDebug() << "Error: Invalid Index Access. (NTrodeTreeWidget::getNTrode)";
    }
    retNTrode.settings.nTrodeId = -1;
    return(retNTrode);
}*/

/*void NTrodeTreeWidget::setNTrodeSettings(int index, SingleSpikeTrodeConf settings) {
    NTrodeSettings curNTrode = getNTrode(index);
    curNTrode.settings = settings;
    nTrodes.replace(index,curNTrode);
}*/

int NTrodeTreeWidget::findHWChannelsNtrode(int HWChannel) {
    for (int i = 0; i < this->topLevelItemCount(); i++) {
        QStringList curItemL = this->topLevelItem(i)->text(0).split(" ");
        int nTrodeId = curItemL.at(curItemL.length()-1).toInt();

        for (int ch = 0; ch < topLevelItem(i)->childCount(); ch++) {
            int tmpHWChan = topLevelItem(i)->child(ch)->text(0).split(" ").last().toInt();
            if (tmpHWChan == HWChannel) {
                return nTrodeId;
            }
        }



    }
    return -1;
}

void NTrodeTreeWidget::updateMapping() {
    updateMapping(tableMap);
    emit sig_changed();
}

void NTrodeTreeWidget::updateMapping(QList<SingleSpikeTrodeConf> *_nTrodeConfigs) {

    //Update mapping using a list of configuration objects.
    //Important: this assumes that setTotalHardwareChannels has already been called.


    nTrodeConfigs = _nTrodeConfigs;


    tableMap.clear();
    nTrodeIDs.clear();

    for (int i=0; i<totalHwChannels; i++) {
        tableMap.append(-1);
    }


    for (int i=0; i<nTrodeConfigs->length(); i++) {
        int ntID = nTrodeConfigs->at(i).nTrodeId;
        nTrodeIDs.append(ntID);
        for (int j=0; j<nTrodeConfigs->at(i).unconverted_hw_chan.length();j++) {
            if (nTrodeConfigs->at(i).unconverted_hw_chan[j]<tableMap.length()) {
                tableMap[nTrodeConfigs->at(i).unconverted_hw_chan[j]] = ntID;
            }
        }
    }

    updateMapping(tableMap);



}

void NTrodeTreeWidget::updateMapping(QList<int> nTrodeMap) {
    //Update the tree display from scratch using a complete map


    if (nTrodeConfigs == NULL) {
        //The GUI has not yet been full initialized with data, so do not proceed
        return;
    }


    int numCards = nTrodeMap.length() / 32;


    clear();
    tableMap = nTrodeMap;
    nTrodeHWAssignments.clear();


    for (int i=0;i<nTrodeConfigs->length();i++) {
        (*nTrodeConfigs)[i].unconverted_hw_chan.clear();
        (*nTrodeConfigs)[i].hw_chan.clear();
        //nTrodes[i].settings.unconverted_hw_chan.clear();
    }

    //nTrodeIDs.clear();

    //We keep the existing nTrode order (defined in nTrodeIds)
    for (int i=0;i<nTrodeIDs.length();i++) {
        QList<int> tmp;
        nTrodeHWAssignments.append(tmp);
    }

    //Now we populate the nTrodeHWAssignments list.  If the nTrode has not yet been defined, we add it
    for (int hwChan = 0; hwChan < nTrodeMap.length(); hwChan++) {
        if (nTrodeMap[hwChan] > -1) {
            bool foundNT = false;
            for (int IDind = 0; IDind<nTrodeIDs.length(); IDind++) {
                if (nTrodeIDs[IDind] == nTrodeMap[hwChan]) {
                    foundNT = true;
                    nTrodeHWAssignments[IDind].append(hwChan);
                    (*nTrodeConfigs)[IDind].unconverted_hw_chan.append(hwChan);
                    int new_hw_chan = ((hwChan % 32) * numCards) + floor(hwChan / 32);
                    (*nTrodeConfigs)[IDind].hw_chan.append(new_hw_chan);

                    //nTrodes[IDind].settings.unconverted_hw_chan.append(hwChan);
                    break;
                }
            }
            if (!foundNT) {

                int NtCreateInd = 0; //Defines where in ther nTrode list the new nTrode will go
                for (int nInd = 0; nInd < nTrodeIDs.length(); nInd++) {
                    if (nTrodeIDs[nInd] < nTrodeMap[hwChan]) {
                        NtCreateInd++;
                    } else {
                        break; //We found an ntrode id that is greater, so we place the new ntrode before that
                    }
                }

                QList<int> tmplist;
                tmplist.append(hwChan);
                nTrodeIDs.insert(NtCreateInd,nTrodeMap[hwChan]);
                nTrodeHWAssignments.insert(NtCreateInd,tmplist);


                SingleSpikeTrodeConf curSettings;

                curSettings.filterOn = true;
                curSettings.lowFilter = 300;
                curSettings.highFilter = 6000;
                curSettings.refOn = false;
                curSettings.lfpRefOn = false;
                curSettings.refNTrode = 0;
                curSettings.refChan = 0;
                curSettings.moduleDataChan = 0;
                curSettings.nTrodeId = nTrodeMap[hwChan];

                curSettings.moduleDataHighFilter = 200;
                curSettings.moduleDataOn = 0; //default to false;

                curSettings.channelSettings.thresh = DEFAULT_HWCHAN_THRESH;
                curSettings.channelSettings.maxDisp = DEFAULT_HWCHAN_MAXDISP;

                curSettings.channelSettings.triggerOn = true;
                curSettings.unconverted_hw_chan.append(hwChan);
                int new_hw_chan = ((hwChan % 32) * numCards) + floor(hwChan / 32);
                curSettings.hw_chan.append(new_hw_chan);

                nTrodeConfigs->insert(NtCreateInd,curSettings);
                //nTrodes.insert(NtCreateInd,newNtrodeSettings);
            }
        }
    }

    //Any empty nTrodes are removed
    bool completelyPurged = false;
    while (!completelyPurged) {
        completelyPurged = true;
        for (int nTrodeInd=0; nTrodeInd<nTrodeIDs.length();nTrodeInd++) {

            if (nTrodeHWAssignments[nTrodeInd].isEmpty()) {
                nTrodeIDs.removeAt(nTrodeInd);
                nTrodeConfigs->removeAt(nTrodeInd);
                //nTrodes.removeAt(nTrodeInd);
                nTrodeHWAssignments.removeAt(nTrodeInd);
                completelyPurged = false;
                break;
            }
        }
    }



    //Populate the tree list widget
    for (int nTrodeInd=0; nTrodeInd<nTrodeIDs.length();nTrodeInd++) {
        QTreeWidgetItem *newNT = new QTreeWidgetItem();
        QString fullName = QString("NTrode %1").arg(nTrodeIDs[nTrodeInd]);
        newNT->setText(0,fullName);
        newNT->setExpanded(true);

        //nTrodes[nTrodeInd].treeItem = newNT;

        for (int hwChanInd=0; hwChanInd<nTrodeHWAssignments[nTrodeInd].length();hwChanInd++) {
            QTreeWidgetItem *chld = new QTreeWidgetItem();
            QString chName = QString("HW %1").arg(nTrodeHWAssignments[nTrodeInd][hwChanInd]);
            chld->setText(0,chName);
            newNT->addChild(chld);
        }
        this->addTopLevelItem(newNT);
        this->topLevelItem(this->topLevelItemCount()-1)->setExpanded(true);

    }


    //Fix any mismatch in the nTrode settings
    for (int ntInd=0; ntInd<nTrodeConfigs->length(); ntInd++) {

        //Check the make sure values match number of channels in nTrode
        int numChanInNt = nTrodeConfigs->at(ntInd).unconverted_hw_chan.length();
        if (nTrodeConfigs->at(ntInd).maxDisp.length() != numChanInNt) {
            int val = nTrodeConfigs->at(ntInd).maxDisp[0];
            (*nTrodeConfigs)[ntInd].maxDisp.clear();
            for (int r = 0; r < numChanInNt; r++) {
                (*nTrodeConfigs)[ntInd].maxDisp.append(val);
            }
        }

        if (nTrodeConfigs->at(ntInd).thresh.length() != numChanInNt) {
            int val = nTrodeConfigs->at(ntInd).thresh[0];
            (*nTrodeConfigs)[ntInd].thresh.clear();
            for (int r = 0; r < numChanInNt; r++) {
                (*nTrodeConfigs)[ntInd].thresh.append(val);
            }
        }

        if (nTrodeConfigs->at(ntInd).triggerOn.length() != numChanInNt) {

            (*nTrodeConfigs)[ntInd].triggerOn.clear();
            for (int r = 0; r < numChanInNt; r++) {
                (*nTrodeConfigs)[ntInd].triggerOn.append(true);
            }
        }

        if (nTrodeConfigs->at(ntInd).moduleDataChan > (numChanInNt-1)) {
            (*nTrodeConfigs)[ntInd].moduleDataChan = 0;
        }

        //Check reference



    }



}

/*QTreeWidgetItem* NTrodeTreeWidget::addNewNTrode(QString name, SingleSpikeTrodeConf settings, bool loadingFromXML) {
    if (availableIDs.length() > 0) {
        QTreeWidgetItem *newNT = new QTreeWidgetItem();
        int idToAdd;

        if (loadingFromXML) {
            idToAdd = settings.nTrodeId;
            for (int i = 0; i < availableIDs.length(); i++) {
                if (idToAdd == availableIDs.at(i)) {
                    availableIDs.removeAt(i); //find id in the available IDs list and remove it
                    break;
                }
            }
        }
        else {
            idToAdd = availableIDs.first();
            availableIDs.pop_front(); //pull ID off
        }
        QString fullName = QString("%1 %2").arg(name).arg(idToAdd);
        NTrodeSettings newNTS;
        newNTS.settings = settings;
        newNTS.settings.nTrodeId = idToAdd;

        newNT->setText(0,fullName);
        newNT->setExpanded(true);
        int addedIndex;
        for (int i = 0; i < this->topLevelItemCount(); i++) {
            QStringList curItemL = this->topLevelItem(i)->text(0).split(" ");
            int curId = curItemL.at(curItemL.length()-1).toInt();
            if (idToAdd < curId) {
                addedIndex = i;
                this->insertTopLevelItem(i, newNT);
                this->topLevelItem(i)->setExpanded(true);
                newNTS.treeItem = newNT;
                nTrodes.insert(i,newNTS);

                emit sig_addedNTrode(newNTS.settings.nTrodeId,loadingFromXML);

//                if (loadingFromXML) {
//                    //if loaded in from XML, remove all children from hw list...
//                    for (int j = 0; j < settings.unconverted_hw_chan.length(); j++) {
////                        emit sig_removeChildFromList(settings.unconverted_hw_chan.at(j));
//                    }
//                }

                return(newNT);
            }
        }
        addedIndex = topLevelItemCount();
        this->addTopLevelItem(newNT);
        this->topLevelItem(this->topLevelItemCount()-1)->setExpanded(true);
        //NTrodeSettings newNTS;
        newNTS.treeItem = newNT;
        nTrodes.append(newNTS);
        emit sig_addedNTrode(newNTS.settings.nTrodeId, loadingFromXML);

//        if (loadingFromXML) {
//            //if loaded in from XML, remove children from the hw list...
//            for (int i = 0; i < settings.unconverted_hw_chan.length(); i++) {
////                emit sig_removeChildFromList(settings.unconverted_hw_chan.at(i));
//            }
//        }
        return(newNT);
    }
    else
        qDebug() << "Error: No more available IDs. (NTrodeTreeWidget::addNewNTrode)";
    return(NULL);
}*/

/*void NTrodeTreeWidget::removeNTrode(QTreeWidgetItem *removedNTrode) {
    bool nTrodeRemoved = false;
    if (isTopLevelItem(removedNTrode)) {
//        qDebug() << "removing top level ID, add ID to list";
        //we assume that the last element in any nTrode will be it's integer ID
        QStringList ntrodeElements = removedNTrode->text(0).split(" ");
        QString back = ntrodeElements.back();
        int removedID = back.toInt();
        bool added = false;
        //availableIDs.push_front(back.toInt());
        for (int i = 0; i < nTrodes.length(); i++) {
            if (nTrodes.at(i).treeItem == removedNTrode) {
                //qDebug() << "remove ntrode from nTrode";
                nTrodes.removeAt(i);
                if (!loadingXML)
                    correctDigitalReferences(removedID);
                emit sig_removedNTrode(i);
                nTrodeRemoved = true;
                //mark: send setfocus signal here to link to digRef
            }
        }

        for (int i = 0; i < availableIDs.length(); i++) {

            if (availableIDs.at(i) > removedID) {
                availableIDs.insert(i,removedID);
                added = true;
                break;
            }
        }
        if (!added)
            availableIDs.append(back.toInt()); //put ID back in list
    }
    else { //if it wasn't a top level item, then a child was removed
        int hwChanIndex = removedNTrode->parent()->indexOfChild(removedNTrode);
        int nTrodeIndex = this->indexOfTopLevelItem(removedNTrode->parent());
        removeChildFromNTrode(nTrodeIndex, hwChanIndex);
    }
    delete removedNTrode;
    if (nTrodeRemoved) {
        emit sig_reloadNTrodeSettings(this->currentIndex().row());
//        qDebug() << "new focus: " << this->currentItem()->text(0);
    }
}*/

/*QTreeWidgetItem* NTrodeTreeWidget::addChildToNTrode(int index, int indexInNtrode, QString childToAdd) {
    if (index < 0 || index >= nTrodes.length()) {
        qDebug() << "Error: Invalid Index (NTrodeTreeWidget::addChildToNTrode)";
        return(NULL);
    }
    QTreeWidgetItem *chld = new QTreeWidgetItem();
    chld->setText(0,childToAdd);
    QStringList chldStr = childToAdd.split(" ");
    int chldId = chldStr.at(chldStr.length()-1).toInt();
    NTrodeSettings curNtrode = nTrodes.at(index);

    correctLFPSettings(indexInNtrode, &curNtrode);



    if (indexInNtrode < 0 || indexInNtrode == nTrodes.at(index).treeItem->childCount()) {
        curNtrode.settings.maxDisp.append(DEFAULT_HWCHAN_MAXDISP);
        curNtrode.settings.thresh.append(DEFAULT_HWCHAN_THRESH);
        curNtrode.settings.thresh_rangeconvert.append(DEFAULT_HWCHAN_THRESH_RANGECONVERT);
        curNtrode.settings.streamingChannelLookup.append(DEFAULT_HWCHAN_STREAMINGCHANLOOKUP);
        curNtrode.settings.triggerOn.append(DEFAULT_HWCHAN_TRIGGERON);
        int channel = chldId;
        int numCards = totalHwChannels / 32;
        int conv_hw_chan = ((channel % 32) * numCards) + floor(channel / 32);
        curNtrode.settings.unconverted_hw_chan.append(channel);
        curNtrode.settings.hw_chan.append(conv_hw_chan);

        nTrodes.replace(index,curNtrode);


        nTrodes.at(index).treeItem->addChild(chld);
    }
    else {
        curNtrode.settings.maxDisp.insert(indexInNtrode, DEFAULT_HWCHAN_MAXDISP);
        curNtrode.settings.thresh.insert(indexInNtrode, DEFAULT_HWCHAN_THRESH);
        curNtrode.settings.thresh_rangeconvert.insert(indexInNtrode, DEFAULT_HWCHAN_THRESH_RANGECONVERT);
        curNtrode.settings.streamingChannelLookup.insert(indexInNtrode, DEFAULT_HWCHAN_STREAMINGCHANLOOKUP);
        curNtrode.settings.triggerOn.insert(indexInNtrode, DEFAULT_HWCHAN_TRIGGERON);
        int channel = chldId;
        int numCards = totalHwChannels / 32;
        int conv_hw_chan = ((channel % 32) * numCards) + floor(channel / 32);
        curNtrode.settings.unconverted_hw_chan.insert(indexInNtrode, channel);
        curNtrode.settings.hw_chan.insert(indexInNtrode, conv_hw_chan);

        nTrodes.replace(index,curNtrode);


       nTrodes.at(index).treeItem->insertChild(indexInNtrode, chld);
    }
    emit sig_reloadNTrodeSettings(index);
    return(chld);
}*/

/*void NTrodeTreeWidget::loadChildIntoTree(int index, QString childToAdd) {
    if (index < 0 || index >= nTrodes.length()) {
        qDebug() << "Error: Invalid Index (NTrodeTreeWidget::loadChildIntoTree)";
        return;
    }

    QTreeWidgetItem *chld = new QTreeWidgetItem();
    chld->setText(0,childToAdd);
    QStringList chldStr = childToAdd.split(" ");
//    int chldId = chldStr.at(chldStr.length()-1).toInt();
//    NTrodeSettings curNtrode = nTrodes.at(index);


    nTrodes.at(index).treeItem->addChild(chld);
}*/

/*void NTrodeTreeWidget::removeChildFromNTrode(int nTrodeIndex, int childIndex) {
    //NOTE this function removes hwChannel data ONLY FROM nTrode.settings.LISTS at the spcified indicis
    NTrodeSettings curNtrode = nTrodes.at(nTrodeIndex);
//    int originNTrodeID = curNtrode.settings.nTrodeId;
//    int refNTrodeID = curNtrode.settings.refNTrodeID;
//    int channelID = curNtrode.settings.unconverted_hw_chan.at(childIndex);
    curNtrode.settings.hw_chan.removeAt(childIndex);
    curNtrode.settings.unconverted_hw_chan.removeAt(childIndex);
    curNtrode.settings.thresh.removeAt(childIndex);
    curNtrode.settings.maxDisp.removeAt(childIndex);
    curNtrode.settings.thresh_rangeconvert.removeAt(childIndex);
    curNtrode.settings.streamingChannelLookup.removeAt(childIndex);
    curNtrode.settings.triggerOn.removeAt(childIndex);
    nTrodes.replace(nTrodeIndex, curNtrode);
    if (!loadingXML) {
        correctDigitalReferences(nTrodeIndex, childIndex);
        correctLFPSettings(nTrodeIndex, childIndex);
    }
    emit sig_reloadNTrodeSettings(nTrodeIndex);
}*/

//DEBUG FUNCTION
/*void NTrodeTreeWidget::printNTrodeList(void) {
    qDebug() << "Printing Ntrode list";

    for (int i = 0; i < nTrodes.length(); i++) {
        NTrodeSettings cur = nTrodes.at(i);

        qDebug() << "-NTrode: " << cur.settings.nTrodeId;

        qDebug() << "   -refNTrode: " << cur.settings.refNTrode;
        qDebug() << "   -refNTrodeID: " << cur.settings.refNTrodeID;
        qDebug() << "   -refChan: " << cur.settings.refChan;

        for (int j = 0; j < cur.settings.unconverted_hw_chan.length(); j++) {
//            qDebug() << "     -Chan: " << cur.settings.unconverted_hw_chan.at(j);
        }

//        qDebug() << "-" << cur.treeItem->text(0);
//        for (int j = 0; j < cur.treeItem->childCount(); j++) {
//            qDebug() << "  -" << cur.treeItem->child(j)->text(0);
//        }
    }


}*/

/*void NTrodeTreeWidget::dropEvent(QDropEvent *event) {
    //const QMimeData *mime = event->mimeData();
//    qDebug() << "dropped at - " << event->pos();
    this->isDragging = false;
    bool accept = false;
//    bool isTopLevel = false;
    int index = -1; //this is the index of the topLevelItem that we're adding the dropped hardware channel to
    int indexInNtrode = -1; //this is the index inside of the topLevelItem where we're adding the dropped hardware channel to.
    QTreeWidgetItem *channelDroppedOn = NULL;
    for (int i = 0; i < this->topLevelItemCount(); i++) {
        //qDebug() << "checking topItem " << this->topLevelItem(i)->text(0);
        if (this->visualItemRect(this->topLevelItem(i)).contains(event->pos())) {
            accept = true;
            index = i;
            break;
        }
        //check this item's children, if item was dropped on child, add to the parent object
        for (int j = 0; j < this->topLevelItem(i)->childCount(); j++) {
            if (this->visualItemRect(this->topLevelItem(i)->child(j)).contains(event->pos())) {
                channelDroppedOn = this->topLevelItem(i)->child(j);

                if (event->source() == this) {
                    QTreeWidgetItem *sourceItem = static_cast<NTrodeTreeWidget*>(event->source())->currentItem();
                    if (sourceItem == channelDroppedOn) {
                        return; //if the user picked up a channel and dropped it on itself, then reject the event
                    }
                }

                QPoint center = this->visualItemRect(this->topLevelItem(i)->child(j)).center();
                if (event->pos().y() >= center.y()) { //for some reason the logic here is reversed...
//                    qDebug() << "add Below";
                    indexInNtrode = j + 1;
                }
                else {
//                    qDebug() << "add Above";
                    indexInNtrode = j;
                }
                accept = true;
                index = i;
                break;
            }
        }
    }
    if (event->source() == this) { //check if any selected are nTrodes
        for (int i = 0; i < this->selectedItems().length(); i++) { //Move all selected channels
            if (this->indexOfTopLevelItem(this->selectedItems().at(i)) != -1)
                accept = false; //reject the drop if it was a top level item
        }
    }
    if (accept) {
        event->accept();
        emit sig_channelMoved();
        //qDebug() << "Sending row: " << static_cast<HWChannelTreeWidget*>(event->source())->currentItem()->text();
        QTreeWidgetItem *addedChild = NULL;
        if (event->source() == this) { //if moving channels within the nTrodeTree View
            for (int i = 0; i < this->selectedItems().length(); i++) { //Move all selected channels
                if (indexOfTopLevelItem(this->selectedItems().at(i)) == -1) {
//                    qDebug() << "  - moving " << this->selectedItems().at(i)->text(0);
                    QTreeWidgetItem *sourceItem = this->selectedItems().at(i);

                    bool lfpChanMoved = false; //determine whether or not the moved channel was the LFP channel
                    if (nTrodes.at(this->indexOfTopLevelItem(sourceItem->parent())).settings.moduleDataChan == sourceItem->parent()->indexOfChild(sourceItem)) {
                        lfpChanMoved = true;
                    }

                    //add the dropped channel to the destination nTrode
                    addedChild = this->addChildToNTrode(index, indexInNtrode, sourceItem->text(0));
                    if (indexInNtrode >= 0) //make sure that inserted nTrodes are inserted in the same order they were selected in
                        indexInNtrode++;

                    int newInd = this->topLevelItem(index)->indexOfChild(addedChild); //index of the newly added channel

                    int sourceIndex = sourceItem->parent()->indexOfChild(sourceItem); //index of the channel
                    int sourceParentIndex = this->indexOfTopLevelItem(sourceItem->parent()); //index of the channel's nTrode

                    bool sourceIsDest = false;
                    if (sourceParentIndex == index) { //if a channel was moved within the same nTrode
                        if (lfpChanMoved) { //if the channel moved was an LFP channel, fix it
                            SingleSpikeTrodeConf sourceSettings = nTrodes.at(sourceParentIndex).settings;
                            sourceSettings.moduleDataChan = newInd;
                            setNTrodeSettings(sourceParentIndex, sourceSettings);
                        }
                    }

                    //delete the dropped channel from it's original nTrode
                    this->removeChildFromNTrode(sourceParentIndex, sourceIndex); //this removes the child from the underlying nTrodes data structure
                    delete sourceItem; //this removes the child/channel from the tree view itself
                    i--;//iterate backward one to account for the deletion
                }
            }
            emit sig_changed();
        }
        else { //Channel(s) dropped from the availHardwareChan tree
            HWChannelTreeWidget* sourceTree = static_cast<HWChannelTreeWidget*>(event->source());
            for (int i = 0; i < sourceTree->selectedItems().length(); i++ ) { //add all selected channels to the specified location
                this->addChildToNTrode(index, indexInNtrode, sourceTree->selectedItems().at(i)->text(0));
                if (indexInNtrode >= 0) //make sure that inserted nTrodes are inserted in the same order they were selected in
                    indexInNtrode++;
            }
        }
    }
}

void NTrodeTreeWidget::dragEnterEvent(QDragEnterEvent *event) {
//    qDebug() << "start drag";

    if (event->source() == this) {
        //qDebug() << "Moving: " << static_cast<NTrodeTreeWidget*>(event->source())->currentItem()->text(0);
        for (int i = 0; i < this->topLevelItemCount(); i++)  { //check if it's a top level item
            if (static_cast<NTrodeTreeWidget*>(event->source())->currentItem() == this->topLevelItem(i))
                return; //reject drag enter event if it is form the top level
        }
        //if (this->to)
        //qDebug() << "check can't drag top level items";
    }
    this->isDragging = true;
    QTreeWidget::dragEnterEvent(event);
}

void NTrodeTreeWidget::dragLeaveEvent(QDragLeaveEvent *event) {
//    qDebug() << "LEAVING DRAG";
    this->isDragging = false;
    QTreeWidget::dragLeaveEvent(event);
}

void NTrodeTreeWidget::dragMoveEvent(QDragMoveEvent *event) {
    //disable the move event to fix the 'QNSView mouseDragged: Internal mouse button tracking invalid (missing Qt::LeftButton)' erro
    //super janky, find better solution
    //QTreeWidget::dragMoveEvent(e);
    overNTrode = NULL;
    for (int i = 0; i < this->topLevelItemCount(); i++) {
        //qDebug() << "checking topItem " << this->topLevelItem(i)->text(0);
        if (this->visualItemRect(this->topLevelItem(i)).contains(event->pos())) {
            overNTrode = this->topLevelItem(i);
            this->viewport()->repaint();
            break;
        }
        //check this item's children, if item was dropped on child, add to the parent object
        for (int j = 0; j < this->topLevelItem(i)->childCount(); j++) {
            if (this->visualItemRect(this->topLevelItem(i)->child(j)).contains(event->pos())) {
                QPoint center = this->visualItemRect(this->topLevelItem(i)->child(j)).center();
//                int chanID = this->topLevelItem(i)->child(j)->text(0).split(" ").last().toInt();
                this->hwChan = this->topLevelItem(i)->child(j);
                if (event->pos().y() >= center.y()) { //for some reason the logic here is reversed...
                    this->borderUp = false;
                    this->viewport()->repaint();
                }
                else {
                    this->borderUp = true;
                    this->viewport()->repaint();
                }
                break;
            }
        }
    }

}*/

void NTrodeTreeWidget::keyPressEvent(QKeyEvent *event) {
    if (this->topLevelItemCount() > 0) {

        //DEBUG KEY PRESS
        /*if (event->key() == Qt::Key_P) {
            printNTrodeList();
        }*/

//        if (event->key() == Qt::Key_Left) { //collapse all tree objects
//        }

//        if (event->key() == Qt::Key_Right) { //expand all tree objects
//            emit sig_sendFocus((int)FT_ChannelTree);
//        }

//        if (event->key() == Qt::Key_Up) {
//            qDebug() << "Go up! (nTrode)";
//        }

//        if (event->key() == Qt::Key_Down) {
//            qDebug() << "Go Down! (nTrode)";
//        }

        //selectively expand or condense tree element
        if ((event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return) && currentItem() != NULL) {
            this->currentItem()->setExpanded(!this->currentItem()->isExpanded());
        }

        //remove tree element, return all children to other list view OR return element to list
        if ((event->key() == Qt::Key_Backspace || event->key() == Qt::Key_Delete) && currentItem() != NULL) {
            removeSelectedItems();
        }
    }

    TrodesTree::keyPressEvent(event);
}

void NTrodeTreeWidget::mousePressEvent(QMouseEvent *event) {

    TrodesTree::mousePressEvent(event);

    if (this->currentItem() != NULL) {
        //only process item if it was selected as opposed to deselected
        if (this->currentItem()->isSelected()) {
            if (!this->isTopLevelItem(this->currentItem())) {
                QStringList chanL = this->currentItem()->text(0).split(" ");
                int chanID = chanL.at(chanL.length()-1).toInt();
                int nTrodeIndex = this->indexOfTopLevelItem(this->currentItem()->parent());

                emit sig_selectedNTrode(nTrodeIndex);
                emit sig_clickedOnChannel(chanID);
            }
            else {
                int nTrodeIndex = this->indexOfTopLevelItem(this->currentItem());
                emit sig_selectedNTrode(nTrodeIndex);
            }
        }
        else { //otherwise, load the first selected NTrode
            if (this->selectedItems().length() > 0) {
                QTreeWidgetItem *firstNTrode = this->selectedItems().at(0);
                if (!this->isTopLevelItem(firstNTrode)) {
                    firstNTrode = firstNTrode->parent();
                }
                int nTrodeIndex = this->indexOfTopLevelItem(firstNTrode);
                emit sig_selectedNTrode(nTrodeIndex);
            }
        }
    }
}

bool NTrodeTreeWidget::isTopLevelItem(QTreeWidgetItem *checkItem) {
    for (int i = 0; i < this->topLevelItemCount(); i++) {
        if (this->topLevelItem(i) == checkItem)
            return(true);
    }
    return(false);
}

QTreeWidgetItem* NTrodeTreeWidget::getFirstSelectedNTrode() {
    if (topLevelItemCount() == 0)
        return(NULL);

    if (selectedItems().length() == 0) { //possibly select first nTrode that is empty?
        return(topLevelItem(0));
    }

    QTreeWidgetItem* retItem;
    retItem = selectedItems().at(0);
    if (indexOfTopLevelItem(retItem) == -1) {
        retItem = retItem->parent();
    }
    return(retItem);
}

//correct digital references when deleting a channel
/*void NTrodeTreeWidget::correctDigitalReferences(int removedNTrodeID) {
//    qDebug() << "Ntrode " << removedNTrodeID << " removed";
    bool nTrodeDepopulated = false;
    int nTrodeCounter = 0;
    for (int i = 0; i < nTrodes.length(); i++) {
        SingleSpikeTrodeConf curNTrode = nTrodes.at(i).settings;
        if (curNTrode.refNTrodeID == removedNTrodeID) {
            nTrodeDepopulated = true;
            curNTrode.refOn = false;
            curNTrode.refNTrodeID = -1;
            curNTrode.refChan = -1;
            nTrodeCounter++;
        }
        setNTrodeSettings(i, curNTrode);
    }
    if (nTrodeDepopulated) {
        //thorw warning message here
        QString warningMsg = QString("Warning: An nTrode referenced by %1 other nTrode(s) has been deleted.  Their digital referencing settings have been reset.").arg(nTrodeCounter);
        QMessageBox::warning(this, tr("Referenced nTrode Deleted"), tr(qPrintable(warningMsg)));
    }
}*/

//correct digital references when moving a channel to another NTrode
/*void NTrodeTreeWidget::correctDigitalReferences(int originNTrodeIndex, int removedChanIndex) {
    SingleSpikeTrodeConf originNTrode = nTrodes.at(originNTrodeIndex).settings;
    int originNTrodeID = originNTrode.nTrodeId;
    bool nTrodeDepopulated = false;
    int nTrodeCounter = 0;
//    qDebug() << "HW chan at index " << removedChanIndex << " from nTrode " << originNTrodeID;

    for (int i = 0; i < nTrodes.length(); i++) {
        NTrodeSettings curNTrodeSettings = nTrodes.at(i);
        SingleSpikeTrodeConf curNTrode = curNTrodeSettings.settings;
        if (curNTrode.refNTrodeID == originNTrodeID) {
            if (removedChanIndex < curNTrode.refChan) {
//                qDebug() << "iterating ref chan down one";
                curNTrode.refChan -= 1;
            }
            else if (removedChanIndex == curNTrode.refChan) {
                if (originNTrode.unconverted_hw_chan.length() > 0) {
//                    qDebug() << "settign refChan to default index (0)";
                    curNTrode.refChan = 0;
                }
                else {
//                    qDebug() << "no more available channels, set default";
                    nTrodeDepopulated = true;
                    curNTrode.refOn = false;
                    curNTrode.refNTrodeID = -1;
                    curNTrode.refChan = -1;
                    nTrodeCounter++;
                }
            }
        }
        curNTrodeSettings.settings = curNTrode;
        setNTrodeSettings(i, curNTrode);
//        nTrodes.replace(i, curNTrodeSettings);
    }
    if (nTrodeDepopulated) {
        //throw warning message here
        QString warningMsg = QString("Warning: An nTrode referenced by %1 other nTrode(s) has been fully depopulated.  Their digital referencing settings have been reset.").arg(nTrodeCounter);
        QMessageBox::warning(this, tr("Referenced nTrode Depopulated"), tr(qPrintable(warningMsg)));
    }
}*/

/*void NTrodeTreeWidget::correctLFPSettings(int indexInNtrode, NTrodeSettings *curNTrode) {
    if (loadingXML)
        return; //don't call this function if loading from XML

    //if the we're adding the first channel, set LFP settings to the first channel
    if (!loadingXML && curNTrode->settings.unconverted_hw_chan.length() == 0) {
        curNTrode->settings.moduleDataChan = 0;
        return;
    }

    //If the channel is being added below the current LFP channel, iterate it up by one
    if (curNTrode->settings.moduleDataChan >= indexInNtrode && indexInNtrode > -1)
        curNTrode->settings.moduleDataChan++;
}*/

/*void NTrodeTreeWidget::correctLFPSettings(int originNTrodeIndex, int removedChanIndex) {
//    qDebug() << "Check LFP Settings for nTrode " << nTrodes.at(originNTrodeIndex).settings.nTrodeId;
    SingleSpikeTrodeConf originNTrode = nTrodes.at(originNTrodeIndex).settings;

    int newLFPchan;
    if (originNTrode.unconverted_hw_chan.length() == 0) {
//        qDebug() << "No more channels in nTrode, set LFP chan to -1 default";
        newLFPchan = -1;
    }
    else if (originNTrode.moduleDataChan == removedChanIndex) {
//        qDebug() << "Current LFP channel removed, reset to first available LFP channel";
        newLFPchan = 0;
    }
    else if (removedChanIndex < originNTrode.moduleDataChan) {
//        qDebug() << "The removed channel was below the LFP channel, we iterate LFP channel down one";
        newLFPchan = originNTrode.moduleDataChan - 1;
    }
    else {
//        qDebug() << "The LFP channel does not have to be changed";
        return;
    }

    originNTrode.moduleDataChan = newLFPchan;
    setNTrodeSettings(originNTrodeIndex, originNTrode);
}*/

void NTrodeTreeWidget::setTotalHwChannels(int hwChannels) {
    totalHwChannels = hwChannels;
    QList<int> nTrodeMap;
    for (int i=0; i<hwChannels; i++) {
        nTrodeMap.append(-1);
    }
    tableMap = nTrodeMap;
    nTrodeIDs.clear();
    if (nTrodeConfigs != NULL) {
        (*nTrodeConfigs).clear();
    }
    updateMapping();
}

void NTrodeTreeWidget::removeSelectedItems() {

    this->setPrevSelectedItem(NULL);
    for (int i = 0; i < this->selectedItems().length(); i++) { //delete all currently selected items
        QTreeWidgetItem *curItem = this->selectedItems().at(i);
        if (!isTopLevelItem(curItem)) {
            int hwChan = curItem->text(0).split(" ").last().toInt();
            tableMap[hwChan] = -1;
        }

    }
    updateMapping();


    /*for (int i = 0; i < this->selectedItems().length(); i++) { //delete all currently selected items
        QTreeWidgetItem *curItem = this->selectedItems().at(i);
        if (isTopLevelItem(curItem)) {
            for (int j = 0; j < curItem->childCount(); j++) {
                emit sig_returnChild(curItem->child(j)->text(0));
                curItem->removeChild(curItem->child(j));
                j--;
            }
        }
        else {
            emit sig_returnChild(curItem->text(0));
        }
        removeNTrode(curItem);
        i--;
    }*/
    //emit sig_changed();
}

void NTrodeTreeWidget::processSelection() {
    if (!processingSelection) {
        processingSelection = true;

        QList<QTreeWidgetItem *> sItems = selectedItems();
        for (int i=0; i<sItems.length();i++) {
            if (isTopLevelItem(sItems[i])) {
                sItems[i]->setSelected(false);
            }

        }

        processingSelection = false;
    }

}

void NTrodeTreeWidget::setItemsExpanded(bool expanded) {
    for (int i = 0; i < this->topLevelItemCount(); i++) {
        QTreeWidgetItem *curItem = this->topLevelItem(i);
        curItem->setExpanded(expanded);
    }

    if (buttonExpandAll != NULL) {
        int ptSize;
        //when setting expanded/collapsed, change the unicode character to the corresponding arrow
        if (expanded) {
             buttonExpandAll->setText(QChar(0x25be));
             ptSize = WE_RIGHT_ARROW_POINT;
        }
        else {
            buttonExpandAll->setText(QChar(0x25b6));
            ptSize = WE_DOWN_ARROW_POINT;
        }
        QFont f = buttonExpandAll->font();
        f.setPointSize(ptSize); //change the font size because the two unicode characters are not the same size by default
        buttonExpandAll->setFont(f);
    }
}

void NTrodeTreeWidget::expandAllButtonPushed() {
    if (buttonExpandAll == NULL)
        return;

    bool expand;
    if (buttonExpandAll->text() == QChar(0x25b6)) //expand
        expand = true;
    else //collapse
        expand = false;

    setItemsExpanded(expand);
}

NTrodeTreeWidgetDelegate::NTrodeTreeWidgetDelegate(QObject *parent, NTrodeTreeWidget *treeWidget) :
    TrodesTreeDelegate(parent, treeWidget), nTrodeTreeWidget(treeWidget)
{
}

void NTrodeTreeWidgetDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
    TrodesTreeDelegate::paint(painter, option, index);

    if (nTrodeTreeWidget == NULL)
        qDebug() << "NULL ptr detected | 1";

    if (nTrodeTreeWidget->isChanBeingDragged()) {
        int x1, x2, y;
        painter->setPen(QPen(Qt::blue, 2, Qt::SolidLine, Qt::SquareCap));
        if (nTrodeTreeWidget->getHovNTrode() != NULL) { //this tells the delegate that the drag occured over an nTrode
            if (nTrodeTreeWidget->getHovNTrode()->childCount() > 0) { //if the nTrode has children, add a zone box under the last child

                if (nTrodeTreeWidget->getHovNTrode()->child(nTrodeTreeWidget->getHovNTrode()->childCount()-1) == NULL)
                    qDebug() << "NULL ptr detected | 2";

                x1 = nTrodeTreeWidget->visualItemRect(nTrodeTreeWidget->getHovNTrode()->child(nTrodeTreeWidget->getHovNTrode()->childCount()-1)).left();
                x2 = nTrodeTreeWidget->visualItemRect(nTrodeTreeWidget->getHovNTrode()->child(nTrodeTreeWidget->getHovNTrode()->childCount()-1)).right();
                y = nTrodeTreeWidget->visualItemRect(nTrodeTreeWidget->getHovNTrode()->child(nTrodeTreeWidget->getHovNTrode()->childCount()-1)).bottom();
            }
            else { //if empty, put the zone box under the nTrode
//                nTrodeTreeWidget->printNTrodeList();
                if (nTrodeTreeWidget->getHovNTrode() == NULL)
                    qDebug() << "NULL ptr detected | 3";

                x1 = nTrodeTreeWidget->visualItemRect(nTrodeTreeWidget->getHovNTrode()).left();
                x2 = nTrodeTreeWidget->visualItemRect(nTrodeTreeWidget->getHovNTrode()).right();
                y = nTrodeTreeWidget->visualItemRect(nTrodeTreeWidget->getHovNTrode()).bottom();
            }
            painter->drawLine(x1,y,x2,y);
            return;
        }

        if (nTrodeTreeWidget->getHovChan() == NULL)
            qDebug() << "NULL ptr detected | 4";

        if (nTrodeTreeWidget->getHovChan() != NULL) {
            //if the dragged object wasn't hovering above an ntrode, then it was hovering above a channel
            x1 = nTrodeTreeWidget->visualItemRect(nTrodeTreeWidget->getHovChan()).left();
            x2 = nTrodeTreeWidget->visualItemRect(nTrodeTreeWidget->getHovChan()).right();
            if (nTrodeTreeWidget->renderBorderUp()) { //render zone box above the channel
                y = nTrodeTreeWidget->visualItemRect(nTrodeTreeWidget->getHovChan()).top();
            }
            else{ //render zone box below the channel
                y = nTrodeTreeWidget->visualItemRect(nTrodeTreeWidget->getHovChan()).bottom();
            }
            painter->drawLine(x1,y,x2,y);
        }


    }

}

//TO DO: look into click-drag highlighting, all we need to do is make the click dragging event update the aux display pannel with the displayed shiznit;
AuxChanTreeWidget::AuxChanTreeWidget(QWidget *parent) : TrodesTree(parent) {
    this->setSelectionMode(QAbstractItemView::MultiSelection);
    this->setFocusPartner(FK_RightArrow, FT_AvailAux);
}

void AuxChanTreeWidget::keyPressEvent(QKeyEvent *event) {
    if (this->topLevelItemCount() > 0) {
        if ((event->key() == Qt::Key_Backspace || event->key() == Qt::Key_Delete) && currentItem() != NULL) {
            emit sig_deleteKeyPressed();
        }
    }

    QTreeWidgetItem *prevItem = this->currentItem();
    TrodesTree::keyPressEvent(event);

    if (this->currentItem() != NULL) {
        //only process item if it ws selected and not deselected
        if (this->currentItem()->isSelected()) {
            emit sig_selectedAuxChannel(this->currentItem(), prevItem);
        }
        else { //otherwise, load the first selected aux channel
            if (this->selectedItems().length() > 0) {
                emit sig_selectedAuxChannel(this->selectedItems().at(0), prevItem);
            }
        }
    }
}

void AuxChanTreeWidget::mousePressEvent(QMouseEvent *event) {
    bool isShifty = false;
    QTreeWidgetItem *prevItem = this->currentItem();
    TrodesTree::mousePressEvent(event);

    if (this->currentItem() != NULL) {
        //only process item if it ws selected and not deselected
        if (this->currentItem()->isSelected()) {
            emit sig_selectedAuxChannel(this->currentItem(), prevItem);
        }
        else { //otherwise, load the first selected aux channel
            if (this->selectedItems().length() > 0) {
                emit sig_selectedAuxChannel(this->selectedItems().at(0), prevItem);
            }
        }
    }
}

AddTagDialog::AddTagDialog(QWidget *parent) : QDialog(parent) {
    mainLayout = new QVBoxLayout();
    editLineLayout = new QHBoxLayout();

    QFont header( "Arial", 14, QFont::Bold);
    labelMainText = new QLabel(tr("Create New Tag"));
    labelMainText->setFont(header);
    labelTagName = new QLabel(tr("Tag: "));
    editTagName = new QLineEdit("");

    editLineLayout->addWidget(labelTagName);
    editLineLayout->addWidget(editTagName);

    buttonBox = new QDialogButtonBox();
    QPushButton *buttonCancel = buttonBox->addButton("Cancel", QDialogButtonBox::RejectRole);
    QPushButton *buttonAdd = buttonBox->addButton("Create", QDialogButtonBox::AcceptRole);

    connect(buttonCancel, SIGNAL(released()), this, SLOT(reject()));
    connect(buttonAdd, SIGNAL(released()), this, SLOT(accept()));

    mainLayout->addWidget(labelMainText,0,Qt::AlignCenter);
    mainLayout->addLayout(editLineLayout);
    mainLayout->addWidget(buttonBox);

    setLayout(mainLayout);
    setWindowTitle(tr("Add New Tag"));
}

void AddTagDialog::setAddType(QString type) {
    labelMainText->setText(QString("Create new %1").arg(type));
    labelTagName->setText(QString("%1:").arg(type));
    setWindowTitle(QString("Add New %1").arg(type));
}

EditTagDialog::EditTagDialog(QList<QString> curTags, QHash<QString, int> availableTags, QWidget *parent) : QDialog(parent) {
    availTags = availableTags;
//    tags = curTags;

    mainLayout = new QVBoxLayout();

    //Add Tag Group Panel
    addTagGroup = new QGroupBox();
    addTagGroup->setTitle(tr("Add Tag"));

    addTagGroupLayout = new QHBoxLayout();

    labelAvailableTags = new QLabel(tr("Available Tags: "));
    addTagGroupLayout->addWidget(labelAvailableTags);
    tagSelectBox = new QComboBox();
    tagSelectBox->addItem("");
        //set first item non-selectable by user
    QStandardItemModel* model = qobject_cast<QStandardItemModel*>(tagSelectBox->model());
    QModelIndex firstIndex = model->index(0, tagSelectBox->modelColumn(), tagSelectBox->rootModelIndex());
    QStandardItem* firstItem = model->itemFromIndex(firstIndex);
    firstItem->setSelectable(false);
        //add all availale tags to the combobox
    QHashIterator<QString, int> i(availableTags);
    while(i.hasNext()) {
        i.next();
        tagSelectBox->addItem(i.key());
    }
    tagSelectBox->addItem("+Create New");
    connect(tagSelectBox,SIGNAL(activated(int)),this,SLOT(processTagSelectBox(int)));
    addTagGroupLayout->addWidget(tagSelectBox);
    buttonAddTag = new QPushButton();
    buttonAddTag->setText(tr("Add"));
    connect(buttonAddTag,SIGNAL(released()),this,SLOT(addTagToList()));
    addTagGroupLayout->addWidget(buttonAddTag);

    addTagGroup->setLayout(addTagGroupLayout);

    //Tag List Layout
    tagListLayout = new QHBoxLayout();

    tagList = new QListWidget();
    for (int i = 0; i < curTags.length(); i++) {
        QListWidgetItem *curTag = new QListWidgetItem();
        curTag->setText(curTags.at(i));
        tagList->addItem(curTag);
        if (!availTags.contains(curTags.at(i))) //adds any assigned tagsto the avilable tag list if not there already
            availTags.insert(curTags.at(i),1);
    }
    tagListLayout->addWidget(tagList);
    buttonRemoveTag = new QPushButton();
    buttonRemoveTag->setText(tr("Remove"));
    connect(buttonRemoveTag,SIGNAL(released()),this,SLOT(removeTagFromList()));
    tagListLayout->addWidget(buttonRemoveTag);

    //Button Box Layout
    buttonBox = new QDialogButtonBox();
    QPushButton *buttonCancel = buttonBox->addButton("Cancel", QDialogButtonBox::RejectRole);
    QPushButton *buttonAdd = buttonBox->addButton("Accept", QDialogButtonBox::AcceptRole);

    connect(buttonCancel, SIGNAL(released()), this, SLOT(reject()));
    connect(buttonAdd, SIGNAL(released()), this, SLOT(acceptAndClose()));

    mainLayout->addWidget(addTagGroup);
    mainLayout->addLayout(tagListLayout);
    mainLayout->addWidget(buttonBox);

    setLayout(mainLayout);
    setWindowTitle(tr("Edit nTrode Tags"));
}

void EditTagDialog::processTagSelectBox(int selectedIndex) {
    if (tagSelectBox->currentIndex() == (tagSelectBox->count() - 1)) {
        AddTagDialog *askUser = new AddTagDialog();
        int retVal = askUser->exec();

        if (retVal == QDialog::Accepted) {
            QString newTag = askUser->getText();
            if (!newTag.isEmpty()) {
                if (newTag.contains(";")) {
                    QMessageBox::warning(this,tr("Invalid Character"), tr("Warning: You cannot include the ';' character in a tag."));
                    tagSelectBox->setCurrentIndex(tagSelectBox->count()-2);
                    delete askUser;
                    return;
                }

                if (availTags.contains(newTag)) {
                    tagSelectBox->setCurrentIndex(tagSelectBox->count()-2);
                    delete askUser;
                    return;
                }

                availTags.insert(newTag, 1);
//                qDebug() << "New tag '" << newTag << "' added.";
                tagSelectBox->insertItem((tagSelectBox->count()-1),newTag);

            }
        }
        delete askUser;
        tagSelectBox->setCurrentIndex(tagSelectBox->count()-2);
    }
}

void EditTagDialog::addTagToList() {
//    qDebug() << "clicked add button";
    int curSelection = tagSelectBox->currentIndex();
    if (curSelection != 0 && curSelection != (tagSelectBox->count()-1)) {
//        qDebug() << "adding tag";
        //check for duplicates
        for (int i = 0; i < tagList->count(); i++) {
            if (tagList->item(i)->text() == tagSelectBox->currentText()) {
//                qDebug() << "DUPLICATE";
                return;
            }
        }

        QListWidgetItem *newTag = new QListWidgetItem();
        newTag->setText(tagSelectBox->currentText());
        tagList->insertItem(tagList->count()-1,newTag);

    }

}

void EditTagDialog::removeTagFromList() {
//    qDebug() << "removing tag";
    if (tagList->count() > 0) {
        delete tagList->currentItem();
    }
}

void EditTagDialog::acceptAndClose() {
    for (int i = 0; i < tagList->count(); i++) {
        tags.append(tagList->item(i)->text());
    }
    accept();
}


AddModuleDialog::AddModuleDialog(QWidget *parent) : QDialog(parent) {
    mainLayout = new QVBoxLayout();
    editLineLayout = new QHBoxLayout();

    QFont header( "Arial", 14, QFont::Bold);
    labelMainText = new QLabel(tr("Add New Module"));
    labelMainText->setFont(header);
    labelModuleName = new QLabel(tr("Name: "));
    editModuleName = new QLineEdit("");

    editLineLayout->addWidget(labelModuleName);
    editLineLayout->addWidget(editModuleName);

    buttonBox = new QDialogButtonBox();
    QPushButton *buttonCancel = buttonBox->addButton("Cancel", QDialogButtonBox::RejectRole);
    QPushButton *buttonAdd = buttonBox->addButton("Add", QDialogButtonBox::AcceptRole);

    connect(buttonCancel, SIGNAL(released()), this, SLOT(reject()));
    connect(buttonAdd, SIGNAL(released()), this, SLOT(accept()));

    mainLayout->addWidget(labelMainText,0,Qt::AlignCenter);
    mainLayout->addLayout(editLineLayout);
    mainLayout->addWidget(buttonBox);

    setLayout(mainLayout);
    setWindowTitle(tr("Add New Module"));
}

AddArgumentDialog::AddArgumentDialog(QWidget *parent) : QDialog(parent) {
    mainLayout = new QVBoxLayout();
    editLineLayout = new QHBoxLayout();

    QFont header( "Arial", 14, QFont::Bold);
    labelMainText = new QLabel(tr("Add New Argument"));
    labelMainText->setFont(header);

    labelFlag = new QLabel(tr("Flag: "));
    editFlag = new QLineEdit("");
    labelValue = new QLabel(tr("Value: "));
    editValue = new QLineEdit("");
    editLineLayout->addWidget(labelFlag);
    editLineLayout->addWidget(editFlag);
    editLineLayout->addWidget(labelValue);
    editLineLayout->addWidget(editValue);

    buttonBox = new QDialogButtonBox();
    QPushButton *buttonCancel = buttonBox->addButton("Cancel", QDialogButtonBox::RejectRole);
    QPushButton *buttonAdd = buttonBox->addButton("Add", QDialogButtonBox::AcceptRole);
    connect(buttonCancel, SIGNAL(released()), this, SLOT(reject()));
    connect(buttonAdd, SIGNAL(released()), this, SLOT(accept()));

    mainLayout->addWidget(labelMainText,0,Qt::AlignCenter);
    mainLayout->addLayout(editLineLayout);
    mainLayout->addWidget(buttonBox);

    setLayout(mainLayout);
    setWindowTitle(tr("Add New Argument"));
}


WorkspaceEditor::WorkspaceEditor(Mode iniMode, QWidget *parent) :
    QTabWidget(parent)
  , loadedWorkspace()

{



    loadedWorkspace.spikeConf.ntrodes.setSize(8);
    mode = iniMode;
    if (mode != M_EMBEDDED) {
        /*globalConf = NULL;
        hardwareConf = NULL;
        headerConf = NULL;
        streamConf = NULL;
        spikeConf = NULL;
        moduleConf = NULL;
        benchConfig = NULL;*/
    }


    globalConfig = &loadedWorkspace.globalConf;
    hardwareConfig = &loadedWorkspace.hardwareConf;
    headerConfig = &loadedWorkspace.headerConf;
    streamConfig = &loadedWorkspace.streamConf;
    spikeConfig = &loadedWorkspace.spikeConf;
    moduleConfig = &loadedWorkspace.moduleConf;
    benchmarkConfig = &loadedWorkspace.benchConfig;


    loadingXML = false;

    guiHeight = -1;
    guiWidth = -1;

    labelPlaceholder = new QLabel(tr("Placeholder"));
    curSaveFile = ""; //ini current save file name to blank
    curLoadedFile = ""; //ini current loaded file name to blank

    parseDeviceProfiles();
    iniIntroTab();
    iniAuxConfigTab();
    //iniSpikeConfigTab();
    iniChannelMappingTab();
    iniReferenceGroupsTab();
    iniModuleConfigTab();
    connectTabElements();



    this->addTab(introView, "General Settings");
    auxTab = this->addTab(auxConfView, "Auxiliary Display");
    //spikeTab = this->addTab(spikeConfView, "Trode settings");
    channelMappingTab = this->addTab(channelMappingView, "Channel Map");
    this->addTab(cargrouppanel, "Reference Groups");
    moduleTab = this->addTab(moduleConfView, "Module Configuration");
    setTabEnabled(auxTab, false);
    //setTabEnabled(spikeTab,false);
    setTabEnabled(moduleTab,false);

    guiHeight = this->sizeHint().height();
    guiWidth = this->sizeHint().width();
    //this->setTabEnabled(1,false);
    if (mode == M_EMBEDDED)
        setTabOneToGeneralView();
    initializeDefaults(); // initialize all default values after all GUI elements are initialized

    unsavedChanges = false; //no unsaved changes.  We put this here b/c the constructor (list view for hw channels) erroneously calls configChanged() two times

#if defined (__linux__) //by default, linux doesn't format groupbox's correctly.  This directive puts it inline with how Windows and Mac format them
    this->setStyleSheet("QGroupBox {border: 1px solid #C5C5C5; margin-top: 1ex;}"
                        "QGroupBox::title {subcontrol-origin: margin; top: 0px; left: 10px; padding: 0 0px}");
#else
#endif

}

void WorkspaceEditor::resizeEvent(QResizeEvent *ev) {
    //defaultGroup->activateWindow();

    QTabWidget::resizeEvent(ev);
}

//gets a hash table of nTrodes currently selected by the user
QHash<QString, int> WorkspaceEditor::getTopLevelHash(QTreeWidget *tree) {
    QHash<QString, int> hash;

    for (int i = 0; i < tree->selectedItems().length(); i++) {
        QTreeWidgetItem *curItem = tree->selectedItems().at(i);
        if (tree->indexOfTopLevelItem(curItem) == -1)
            curItem = curItem->parent();

        if (!hash.contains(curItem->text(0)))
            hash.insert(curItem->text(0), tree->indexOfTopLevelItem(curItem));
    }

    return(hash);
}

bool WorkspaceEditor::parseDeviceProfiles() {

    QFile myfile;

    //TO DO: have build deploy the DeviceProfiles.xml to the workspaceGUI.app/executable location

    QString filePath = ":/DeviceProfiles.xml";
//    qDebug() << " -- " << filePath;
    myfile.setFileName(filePath);
    if (!myfile.open(QIODevice::ReadOnly)) {
        qDebug() << "Error: unable to open file. (WorkspaceEditor::parseDeviceProfiles)";
        return(false);
    }
    //read the file

    QTextStream fileText(&myfile);
//    bool readingSpecificDevice = false;

    QDomDocument profile("DeviceProfiles");

    QString profileContent, fileLine;
    QStringList fileContent;

    //retreive the profiles from the xml file
    while (!myfile.atEnd()) {
        fileLine = myfile.readLine();
        if (fileLine.contains("<Profile")) {
            //qDebug() << fileLine;
            profileContent = "";
        }
        profileContent += fileLine;
        if (fileLine.contains("</Profile")) {
            fileContent.append(profileContent);
            //qDebug() << fileLine;
        }
        //qDebug() << fileLine;
    }
    myfile.close();

    for (int i = 0; i < fileContent.length(); i++) {
        //qDebug() << " __________Profile " << i;
        QString errorMessage;
        int errLine, errCol;
        if (!profile.setContent(fileContent.at(i), true, &errorMessage, &errLine, &errCol)) {
            qDebug() << "Error: Parse Read Error. (WorkspaceEditor::parseDeviceProfiles)";
            qDebug() << errorMessage << " : [-L: " << errLine << " -C: " << errCol << "]";
            return(false);
        }

        DeviceInfo curDevice;

        QDomElement root = profile.documentElement();
        curDevice.wsDisplayName = root.attribute("name");

        QDomNodeList deviceEleList = root.elementsByTagName("Device");
        QDomElement deviceElement = deviceEleList.item(0).toElement();
        curDevice.name = deviceElement.attribute("name", "noName");
        curDevice.available = deviceElement.attribute("available", "0").toInt();
        curDevice.numBytes = deviceElement.attribute("numBytes", "-1").toInt();
        curDevice.packetOrderPreference = deviceElement.attribute("packetOrderPreference", "-1").toInt();

        QDomNodeList channelNodeList = root.elementsByTagName("Channel");
        //qDebug() << "channels: " << channelNodeList.length();
        //read all the device's channels
        for (int j = 0; j < channelNodeList.length(); j++) {
            QDomElement channelElement = channelNodeList.item(j).toElement();
            DeviceChannel curChannel;

            curChannel.idString = channelElement.attribute("id","noId");
            QString dataType = channelElement.attribute("dataType", "-1");
            if (dataType == "digital") {
                curChannel.dataType = DeviceChannel::DIGITALTYPE;
            }
            else if (dataType == "analog") {
                curChannel.dataType = DeviceChannel::INT16TYPE;
            }
            else if (dataType == "uint32") {
                curChannel.dataType = DeviceChannel::UINT32TYPE;
            }
            else {
                qDebug() << "Error: Parsing Error while ready channel. (WorkspaceEditor::parseDeviceProfiles)";
                curChannel.dataType = DeviceChannel::DIGITALTYPE;
            }

            curChannel.startByte = channelElement.attribute("startByte", "-1").toInt();
            curChannel.digitalBit = channelElement.attribute("bit","0").toInt();

            curChannel.interleavedDataIDByte = channelElement.attribute("interleavedDataIDByte", "-1").toInt();
            curChannel.interleavedDataIDBit = channelElement.attribute("interleavedDataIDBit", "-1").toInt();

            if (curChannel.startByte >= 0 && curChannel.startByte < 4 && curChannel.dataType == DeviceChannel::DIGITALTYPE) {
                //Hardcoded digital input port
                // port number determined by its relative bit position in the header (one indexed)
                curChannel.port = (curChannel.startByte * 8 + curChannel.digitalBit) + 1;
                curChannel.input = true;
            }
            else if (curChannel.startByte >= 4 && curChannel.startByte < 8 && curChannel.dataType == DeviceChannel::DIGITALTYPE) {
                //Hardcoded digital output port
                curChannel.port = (curChannel.startByte * 8 + curChannel.digitalBit) + 1 - 32;
                curChannel.input = false;
            }
            else if (curChannel.idString.contains("Ain") && curChannel.dataType  == DeviceChannel::INT16TYPE) { //make sure analog Ains have their input set to TRUE
                curChannel.input = true;
            }
            else if (curChannel.dataType == DeviceChannel::DIGITALTYPE) {
                //QMessageBox::information(0, "error", QString("Config file error: Channel startByte 0 to 8 must be digital type."));
                qDebug() << "Error: startByte and dataTypes not compatable. (WorkspaceEditor::parseDeviceProfiles)";
                return false;
            }
            curDevice.channels.append(curChannel);
        }
        curDevice.byteOffset = 0; // this must be calculated when adding devices to config.

        availableDevices.append(curDevice); //append first. index is 1-based, since 0 is "Select Devices"
        if(curDevice.name == "MCU_IO"){
            MCUIndex = availableDevices.length();
        }
        else if(curDevice.name == "ECU"){
            ECUIndex = availableDevices.length();
        }
        else if(curDevice.name == "headstageSensor"){
            HSIndex = availableDevices.length();
        }
        else if(curDevice.name == "RF"){
            RFIndex = availableDevices.length();
        }

    }
    //printDeviceInfoConfigs();
//    qDebug() << "Done reading file";

    return(true);
}

// ****************************** Tab Initialization Functions ******************************
// ******************************************************************************************

void WorkspaceEditor::iniIntroTab() {

    //Initial View Initialization, this is what the user sees upon opening the program
    introView = new QWidget();
    introMainLayout = new QVBoxLayout;
    if (mode == M_STAND_ALONE || true) {
        labelWelcome = new QLabel(introView);
        labelWelcome->setText(tr("Configuration not loaded."));
        buttonLoad = new QPushButton(introView);
        buttonNew = new QPushButton(introView);

        buttonLoad->setText("Load");
        buttonNew->setText("New");
        buttonBar = new QHBoxLayout;
        buttonBar->addWidget(buttonLoad);
        buttonBar->addWidget(buttonNew);

        connect(buttonLoad,SIGNAL(released()),this,SLOT(buttonLoadPressed()));
        connect(buttonNew,SIGNAL(released()),this,SLOT(setTabOneToGeneralView()));
        introMainLayout->addWidget(labelWelcome,0, Qt::AlignCenter);
        introMainLayout->addLayout(buttonBar);
    }
    introView->setLayout(introMainLayout);

    //Second Editing View Initialization
    generalInfoView = new QWidget();
    labelGenInfoTitle = new QLabel();
    labelGenInfoTitle->setText(tr("General Settings"));

    generalInfoMainLayout = new QVBoxLayout;
    iniGlobalConfigGroup(generalInfoMainLayout);
    iniHardwareConfigGroup(generalInfoMainLayout);
    iniStreamConfigGroup(generalInfoMainLayout);
    generalInfoView->setLayout(generalInfoMainLayout);


}

void WorkspaceEditor::iniAuxConfigTab() {
    auxConfView = new QWidget();
    auxMainLayout = new QVBoxLayout();
    //headerConfig = new headerDisplayConfiguration(this);

    iniAuxChannelSettingsPanel(auxMainLayout);
    iniAuxListLayout(auxMainLayout);

    auxConfView->setLayout(auxMainLayout);
}

void WorkspaceEditor::iniChannelMappingTab() {
    channelMappingView = new QWidget();
    //labelSCHeader = new QLabel(tr("Spike Configuration stuff here."));

    QVBoxLayout *channelLayout = new QVBoxLayout();

    //iniSpikeConfigDefaultGroup(scMainLayout);
//    iniFilterBar(scMainLayout);
    //iniAddNTrodeGroup(scMainLayout);
    iniListLayout(channelLayout);




    channelMappingView->setLayout(channelLayout);
    setHardwareChannels(32);

//    spikeConfView->setLayout(scMainLayout);
}

void WorkspaceEditor::iniReferenceGroupsTab(){
    cargrouppanel = new CARGroupPanel();
}

void WorkspaceEditor::loadRefGroupsIntoGUI(){
    cargrouppanel->loadGroups(spikeConfig);
}

void WorkspaceEditor::setRefGroupConfigValues(){
    spikeConfig->carGroups = cargrouppanel->getGroups();
}
/*void WorkspaceEditor::iniSpikeConfigTab() {
    spikeConfView = new QWidget();
    labelSCHeader = new QLabel(tr("Spike Configuration stuff here."));

    //Default Settings
//    defaultSettings.nTrodeId = -1;
//    defaultSettings.refNTrode = 1;
//    defaultSettings.refNTrodeID = -1;
//    defaultSettings.refChan = 1;
//    defaultSettings.lowFilter = 600;
//    defaultSettings.highFilter = 6000;
//    defaultSettings.moduleDataChan = 1;
//    defaultSettings.moduleDataHighFilter = 100;
//    defaultSettings.refOn = false;
//    defaultSettings.filterOn = false;
//    defaultSettings.moduleDataOn = false;

    scMainLayout = new QVBoxLayout();

    iniSpikeConfigDefaultGroup(scMainLayout);
//    iniFilterBar(scMainLayout);
    //iniAddNTrodeGroup(scMainLayout);
    //iniListLayout(scMainLayout);

    //scMainLayout->addWidget(labelSCHeader);
    //scMainLayout->addWidget(defaultGroup);
    //scMainLayout->addLayout(addNTrodeLayout);
    //scMainLayout->addLayout(listLayout);



    //scMainHorLayout->addLayout(scMainLayout);


    //setHardwareChannels(32);
    //scMainHorLayout->addLayout(scMainSecondaryLayout);


    //spikeConfView->setLayout(scMainHorLayout);

    spikeConfView->setLayout(scMainLayout);
}*/

void WorkspaceEditor::iniModuleConfigTab() {
    moduleConfView = new QWidget();

    //labelMCHeader = new QLabel(tr("Module Configuration stuff here."));

    mcMainLayout = new QVBoxLayout();

    iniAddModuleGroup(mcMainLayout);
    iniLowerLayout(mcMainLayout);
    //mcMainLayout->addWidget(labelMCHeader);
    moduleConfView->setLayout(mcMainLayout);
}

// *************************** Intro Tab Initialization Functions ***************************
// ******************************************************************************************

void WorkspaceEditor::iniGlobalConfigGroup(QBoxLayout *masterLayout) {
    //global config settigns
    globalGroup = new QGroupBox(tr("Global Configuration"));
    hardwareGroup = new QGroupBox(tr("Hardware Configuration"));

    ggVertLayout = new QVBoxLayout;
    ggTopLayout = new QHBoxLayout;
    ggBottomLayout = new QHBoxLayout;
    hgLayout = new QHBoxLayout;

    realTimeMode = new QCheckBox();
    saveDisplayedChanOnly = new QCheckBox();
    realTimeMode->setText("Enable Real Time Mode");
    saveDisplayedChanOnly->setText("Save Displayed Channels Only");
    realTimeMode->setCheckable(true);
    saveDisplayedChanOnly->setCheckable(true);
    realTimeMode->setToolTip(tr("Enabling spreads channels across multiple threads for closed loop, low latency data acquisition/visualization.  Enabling this option will greatly increase CPU demand."));
    saveDisplayedChanOnly->setToolTip(tr("Enabling this option saves channel data only for hardware channels displayed/assigned to specific nTrodes."));
    connect(realTimeMode, SIGNAL(clicked(bool)), this, SLOT(configChanged()));
    connect(saveDisplayedChanOnly, SIGNAL(clicked(bool)), this, SLOT(configChanged()));

    labelFilePath = new QLabel();
    labelFilePath->setText(tr("Set File Path:"));
    labelFilePath->setToolTip(tr("Default save location for recording files."));
    labelFilePrefix = new QLabel();
    labelFilePrefix->setText(tr("Set File Prefix:"));
    labelFilePrefix->setToolTip(tr("String attached to the front of saved recording files."));

    editFilePath = new ClickableLineEdit("");
    editFilePath->setToolTip(tr("Default save location for recording files."));
    editFilePrefix = new QLineEdit("");
    editFilePrefix->setToolTip(tr("String attached to the front of saved recording files."));
    connect(editFilePrefix, SIGNAL(textEdited(QString)), this, SLOT(configChanged()));
    connect(editFilePath,SIGNAL(sig_clicked(QString)),this,SLOT(clickedEditFilePath(QString)));

    ggTopLayout->addWidget(realTimeMode);
    ggTopLayout->addWidget(saveDisplayedChanOnly);
    ggBottomLayout->addWidget(labelFilePath);
    ggBottomLayout->addWidget(editFilePath);
    ggBottomLayout->addWidget(labelFilePrefix);
    ggBottomLayout->addWidget(editFilePrefix);

    ggVertLayout->addLayout(ggTopLayout);
    ggVertLayout->addLayout(ggBottomLayout);

    globalGroup->setLayout(ggVertLayout);

    //globalConfig = new GlobalConfiguration(this);
    setGlobalConfigValues();

    masterLayout->addWidget(globalGroup);
}

void WorkspaceEditor::iniHardwareConfigGroup(QBoxLayout *masterLayout) {
    //channel selector menu
    hgChannelFreqLayout = new QVBoxLayout();
    channelSelector = new QComboBox();
    for (int i = 0; (i*32) <= 1024; i++) {
        channelSelector->addItem(QString("%1 Channels").arg(i*32));
    }
    channelSelector->setToolTip(tr("Number of hardware channels."));
    channelSelector->setCurrentIndex(1); //set initial value to 32
    hgChannelFreqLayout->addWidget(channelSelector);

    //sampling rate option
    labelSamplingRate = new QLabel(tr("Hardware Sampling Rate:"));
    editSamplingRate = new ClickableLineEdit(QString("%1").arg(DEFAULT_SAMPLING_RATE));
    editSamplingRate->setToolTip(tr("Frequency (Hz)"));
    connect(editSamplingRate, SIGNAL(textEdited(QString)), this, SLOT(configChanged()));
//    connect(editSamplingRate,SIGNAL(sig_clicked(QString)),this,SLOT(clickedEditSamplingRate(QString)));

    QHBoxLayout *samplingRateLayout = new QHBoxLayout();
    samplingRateLayout->addWidget(labelSamplingRate);
    samplingRateLayout->addWidget(editSamplingRate);
    hgChannelFreqLayout->addLayout(samplingRateLayout);
    hgLayout->addLayout(hgChannelFreqLayout);

    //Device Group options
    deviceGroup = new QGroupBox();
    deviceGroup->setTitle(tr("Hardware Devices"));
    deviceGroupMainLayout = new QVBoxLayout();

    //add device layout
    deviceGroupAddLayout = new QHBoxLayout();
    deviceSelector = new QComboBox();
    deviceSelector->addItem("Select Device");
    for (int i = 0; i < availableDevices.length(); i++) {
        deviceSelector->addItem(availableDevices.at(i).wsDisplayName); //add an item for each device
    }
    deviceGroupAddLayout->addWidget(deviceSelector);
    buttonAddDevice = new QPushButton();
    buttonAddDevice->setText(tr("+Add Device"));
    connect(buttonAddDevice,SIGNAL(released()),this,SLOT(addDevice()));
    deviceGroupAddLayout->addWidget(buttonAddDevice);
    deviceGroupMainLayout->addLayout(deviceGroupAddLayout);

    //device list and remove layout
    labelDeviceList = new QLabel(tr("Current Devices"));
    deviceGroupMainLayout->addWidget(labelDeviceList, 0, Qt::AlignLeft);

    deviceGroupListButtonLayout = new QHBoxLayout();
    deviceList = new QListWidget();
    deviceGroupListButtonLayout->addWidget(deviceList);
    buttonRemoveDevice = new QPushButton();
    buttonRemoveDevice->setText(tr("Remove"));
    connect(buttonRemoveDevice,SIGNAL(released()),this,SLOT(removeDevice()));
    deviceGroupListButtonLayout->addWidget(buttonRemoveDevice, 0, Qt::AlignTop);

    deviceGroupMainLayout->addLayout(deviceGroupListButtonLayout);

    deviceGroup->setLayout(deviceGroupMainLayout);
    hgLayout->addWidget(deviceGroup);
    hardwareGroup->setLayout(hgLayout);

    //hardwareConfig = new HardwareConfiguration(this);
    setHardwareConfigValues();

    masterLayout->addWidget(hardwareGroup);
}

void WorkspaceEditor::iniStreamConfigGroup(QBoxLayout *masterLayout) {
    //streamConfig = new streamConfiguration(true);
    streamGroup = new QGroupBox();
    streamGroup->setTitle("Stream Display Settings");

    streamGroupMainLayout = new QHBoxLayout();
    //column picker
    streamGroupColumnsLayout = new QHBoxLayout();
    labelStreamColumns = new QLabel(tr("Display Columns:"));
    streamColumnComboBox = new QComboBox();
    streamColumnComboBox->addItem("1");
    streamColumnComboBox->addItem("2");
    streamColumnComboBox->addItem("3");
    streamColumnComboBox->addItem("4");
    connect(streamColumnComboBox, SIGNAL(activated(int)), this, SLOT(setStreamDisplayColumns()));
    streamGroupColumnsLayout->addWidget(labelStreamColumns);
    streamGroupColumnsLayout->addWidget(streamColumnComboBox);

    QHBoxLayout *streamGroupPagesLayout = new QHBoxLayout();
    labelStreamPages = new QLabel(tr("Display Pages:"));
    streamPagesComboBox = new QComboBox();
    streamPagesComboBox->addItem("1");
    streamPagesComboBox->addItem("2");
    streamPagesComboBox->addItem("3");
    streamPagesComboBox->addItem("4");
    streamPagesComboBox->addItem("5");
    streamPagesComboBox->addItem("6");
    streamPagesComboBox->addItem("7");
    streamPagesComboBox->addItem("8");
    connect(streamPagesComboBox, SIGNAL(activated(int)), this, SLOT(setStreamDisplayPages()));
    streamGroupPagesLayout->addWidget(labelStreamPages);
    streamGroupPagesLayout->addWidget(streamPagesComboBox);

    streamGroupMainLayout->addLayout(streamGroupColumnsLayout);
    streamGroupMainLayout->addLayout(streamGroupPagesLayout);

    labelStreamColumns->setToolTip(tr("Number of columns used in each page of the nTrode stream display."));
    streamColumnComboBox->setToolTip(tr("Number of columns used in each page of the nTrode stream display."));
    labelStreamPages->setToolTip(tr("Number of pages used in the nTrode stream display."));
    streamPagesComboBox->setToolTip(tr("Number of pages used in the nTrode stream display."));

    //color picker box
    streamGroupColorButtonLayout = new QHBoxLayout();
    labelStreamColorButton = new QLabel(tr("Background Color:"));
    streamColorButton = new ClickableFrame();
    streamColorButton->setFixedWidth(25);
    streamColorButton->setFixedHeight(25);
    streamColorButton->setStyleSheet("background-color:#808080");
    streamConfig->backgroundColor = QColor("#808080");
    connect(streamColorButton, SIGNAL(sig_clicked()), this, SLOT(getBackgroundColorDialog()));
    labelStreamColorButton->setToolTip(tr("NTrode stream display background color."));
    streamColorButton->setToolTip(tr("Click to set color"));

    streamGroupColorButtonLayout->addWidget(labelStreamColorButton);
    streamGroupColorButtonLayout->addWidget(streamColorButton);
    streamGroupMainLayout->addLayout(streamGroupColorButtonLayout);
    streamGroup->setLayout(streamGroupMainLayout);
    masterLayout->addWidget(streamGroup);

}

// ************************* Auxiliary Tab Initialization Functions *************************
// ******************************************************************************************

void WorkspaceEditor::iniAuxChannelSettingsPanel(QBoxLayout *masterLayout) {
    auxChannelGroup = new QGroupBox();
    auxChannelGroup->setTitle(tr("Auxiliary Channel Settings"));
    auxChannelGroupMainLayout = new QVBoxLayout();
    auxChannelGroupTopLayout = new QHBoxLayout();

    //ID and Device labels
    labelAuxChannelID = new QLabel();
    labelAuxChannelID->setText(tr("ID: None Selected"));
    labelAuxChannelDevice = new QLabel();
    labelAuxChannelDevice->setText(tr("Device: None Selected"));
    auxChannelGroupTopLayout->addWidget(labelAuxChannelID);
    auxChannelGroupTopLayout->addWidget(labelAuxChannelDevice);
    auxChannelGroupMainLayout->addLayout(auxChannelGroupTopLayout);


    auxChannelGroupBottomLayout = new QHBoxLayout();
    //Set max display spin box
    auxChannelGroupMaxDispLayout = new QHBoxLayout();
    labelAuxMaxDispSpin = new QLabel(tr("Maximum Display:"));
    auxMaxDispSpinBox = new QSpinBox();
    auxMaxDispSpinBox->setMinimum(1);
    auxMaxDispSpinBox->setMaximum(30000);
    auxMaxDispSpinBox->setSingleStep(100);
    auxMaxDispSpinBox->setToolTip(tr("Voltage Display Range (+/-)"));
//    connect(auxMaxDispSpinBox, SIGNAL(valueChanged(int)), this, SLOT(auxSetApplyButtonsEnabled()));
    connect(auxMaxDispSpinBox, SIGNAL(editingFinished()), this, SLOT(auxSaveMaxDisplay()));
    auxChannelGroupMaxDispLayout->addWidget(labelAuxMaxDispSpin);//, 0, Qt::AlignLeft);
    auxChannelGroupMaxDispLayout->addWidget(auxMaxDispSpinBox);//, 0, Qt::AlignLeft);
    auxChannelGroupBottomLayout->addLayout(auxChannelGroupMaxDispLayout);

    //Create Color Picker Box
    labelColor = new QLabel(tr("Channel Color: "));
    buttonColorBox = new ClickableFrame();
    buttonColorBox->setFixedWidth(25);
    buttonColorBox->setFixedHeight(25);
    buttonColorBox->setStyleSheet("background-color:#aaaaaa");
    connect(buttonColorBox, SIGNAL(sig_clicked()), this, SLOT(getColorDialog()));
    auxChannelGroupBottomLayout->addWidget(labelColor);
    auxChannelGroupBottomLayout->addWidget(buttonColorBox);
    auxChannelGroupMainLayout->addLayout(auxChannelGroupBottomLayout);

    //Channel Settings Button Bar
    auxChannelGroupButtonLayout = new QHBoxLayout();
//    buttonAuxCancel = new QPushButton();
//    buttonAuxCancel->setText(tr("Cancel"));
//    auxChannelGroupButtonLayout->addWidget(buttonAuxCancel, 0, Qt::AlignLeft);
//    connect(buttonAuxCancel, SIGNAL(released()), this, SLOT(auxCancelButtonPressed()));
    labelMismatchAuxSettingsWarning = new QLabel("");
    setWidgetTextPaletteColor(labelMismatchAuxSettingsWarning, QColor("red"));
    labelMismatchAuxSettingsWarning->setToolTip("Settings across auxiliary channels differ.  Clicking 'Apply' will set the first selected channel's settings to all others.");
    auxChannelGroupButtonLayout->addWidget(labelMismatchAuxSettingsWarning, 0, Qt::AlignRight);
    buttonAuxApply = new QPushButton();
    buttonAuxApply->setText(tr("Apply to selected"));
    auxChannelGroupButtonLayout->addWidget(buttonAuxApply);
    connect(buttonAuxApply, SIGNAL(released()), this, SLOT(auxApplyButtonPressed()));
    buttonAuxApply->setFixedSize(buttonAuxApply->sizeHint());
    buttonAuxApply->setEnabled(false);

    buttonAuxApplyToAll = new QPushButton();
    buttonAuxApplyToAll->setEnabled(true);
    buttonAuxApplyToAll->setText(tr("Apply To All"));
    auxChannelGroupButtonLayout->addWidget(buttonAuxApplyToAll);
    connect(buttonAuxApplyToAll, SIGNAL(released()), this, SLOT(auxApplyToAllButtonPressed()));
    buttonAuxApplyToAll->setFixedSize(buttonAuxApplyToAll->sizeHint());
//    auxSetApplyButtonsEnabled(false);
    auxChannelGroupMainLayout->addLayout(auxChannelGroupButtonLayout);

    auxChannelGroup->setLayout(auxChannelGroupMainLayout);
    masterLayout->addWidget(auxChannelGroup);
    auxChannelGroup->setEnabled(false);
}

void WorkspaceEditor::iniAuxListLayout(QBoxLayout *masterLayout) {
    auxListLayout = new QHBoxLayout();

    //Tree layout ini
    availableAuxChannelsTree = new TrodesTree();
    availableAuxChannelsTree->setHeaderHidden(true);
    availableAuxChannelsTree->setFocusPartner(FK_LeftArrow, FT_ConfiguredAux);
    configuredAuxChannelsTree = new AuxChanTreeWidget();
    configuredAuxChannelsTree->setHeaderHidden(true);
//    configuredAuxChannelsTree->setFocusPartner(FK_RightArrow, FT_AvailAux);

//    connect(configuredAuxChannelsTree,SIGNAL(itemClicked(QTreeWidgetItem*,int)), this, SLOT(clickedAuxChannel(QTreeWidgetItem*,int)));
//    connect(configuredAuxChannelsTree, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)), this, SLOT(selectedAuxChannel(QTreeWidgetItem*,QTreeWidgetItem*)));
    connect(configuredAuxChannelsTree, SIGNAL(sig_deleteKeyPressed()), this, SLOT(auxRemoveChannelButtonPressed()));
    connect(configuredAuxChannelsTree, SIGNAL(sig_selectedAuxChannel(QTreeWidgetItem*,QTreeWidgetItem*)), this, SLOT(selectedAuxChannel(QTreeWidgetItem*,QTreeWidgetItem*)));
    connect(configuredAuxChannelsTree, SIGNAL(sig_sendFocus(int)), this, SLOT(setFocusedWiget(int)));
    connect(availableAuxChannelsTree, SIGNAL(sig_sendFocus(int)), this, SLOT(setFocusedWiget(int)));
    connect(availableAuxChannelsTree, SIGNAL(sig_enterKeyPressed()), this, SLOT(auxAddChannelButtonPressed()));


    //button layout ini
    auxListButtonLayout = new QVBoxLayout();
    buttonAuxAddChannel = new QPushButton();
    buttonAuxAddChannel->setText(tr("<<Add"));
    buttonAuxRemoveChannel = new QPushButton();
    buttonAuxRemoveChannel->setText(tr("Remove>>"));
    connect(buttonAuxAddChannel, SIGNAL(released()), this, SLOT(auxAddChannelButtonPressed()));
    connect(buttonAuxRemoveChannel, SIGNAL(released()), this, SLOT(auxRemoveChannelButtonPressed()));
    QSize buttonSize = buttonAuxAddChannel->sizeHint();
    if (buttonSize.width() < buttonAuxRemoveChannel->sizeHint().width())
        buttonSize = buttonAuxRemoveChannel->sizeHint();
    buttonAuxAddChannel->setFixedSize(buttonSize);
    buttonAuxRemoveChannel->setFixedSize(buttonSize);
    auxListButtonLayout->addWidget(buttonAuxAddChannel);
    auxListButtonLayout->addWidget(buttonAuxRemoveChannel);

    auxListLayout->addWidget(configuredAuxChannelsTree);
    auxListLayout->addLayout(auxListButtonLayout);
    auxListLayout->addWidget(availableAuxChannelsTree);


    masterLayout->addLayout(auxListLayout);
}

// *************************** Spike Tab Initialization Functions ***************************
// ******************************************************************************************
/*void WorkspaceEditor::iniSpikeConfigDefaultGroup(QBoxLayout *masterLayout, int arranged) {

    defaultGroup = new QGroupBox();
    defaultGroup->setTitle(tr("nTrode Settings"));

        //    referencing group
    dgDigitalRefLayout = new QVBoxLayout();
    dgDigitalRefGroup = new QGroupBox();
    dgDigitalRefGroupLayout = new QVBoxLayout();
    dgDigitalRefGroupTopLayout = new QHBoxLayout();
    dgDigitalRefGroupBottomLayout = new QHBoxLayout();
    dgDigitalRefGroupNTrodeLayout = new QVBoxLayout();
    dgDigitalRefGroupChannelLayout = new QVBoxLayout();
    nTrodeBox = new QComboBox();
//    connect(nTrodeBox, SIGNAL(activated(int)), this, SLOT(nTrodeSetApplyButtonsEnabled()));
    connect(nTrodeBox, SIGNAL(activated(int)), this, SLOT(nTrodeSaveRefNTrodeID()));
    //for (int i = 0; i < 32; i++)
      //  nTrodeBox->addItem(QString("%1").arg(i+1));
    channelBox = new QComboBox();
    //channelBox->addItem("1");
//    connect(channelBox, SIGNAL(activated(int)), this, SLOT(nTrodeSetApplyButtonsEnabled()));
    connect(channelBox, SIGNAL(activated(int)), this, SLOT(nTrodeSaveRefNTrodeChan()));

    enableDigitalRef = new QCheckBox(tr("Spikes"));
    connect(enableDigitalRef, SIGNAL(clicked(bool)), this, SLOT(nTrodeSaveDigitalRef()));

    enableLFPRef = new QCheckBox(tr("LFP"));
    connect(enableLFPRef, SIGNAL(clicked(bool)), this, SLOT(nTrodeSaveLFPRef()));

    labelNTrode = new QLabel("nTrode");
    labelChannel = new QLabel("Channel");
    labelRefOptions = new QLabel("Apply Refs to: ");

    dgDigitalRefGroupNTrodeLayout->addWidget(labelNTrode);
    dgDigitalRefGroupNTrodeLayout->addWidget(nTrodeBox);
    dgDigitalRefGroupChannelLayout->addWidget(labelChannel);
    dgDigitalRefGroupChannelLayout->addWidget(channelBox);
    dgDigitalRefGroupTopLayout->addLayout(dgDigitalRefGroupNTrodeLayout);
    dgDigitalRefGroupTopLayout->addLayout(dgDigitalRefGroupChannelLayout);

    dgDigitalRefGroupBottomLayout->addWidget(labelRefOptions, 0, Qt::AlignLeft);
    dgDigitalRefGroupBottomLayout->addWidget(enableDigitalRef, 0, Qt::AlignCenter);
    dgDigitalRefGroupBottomLayout->addWidget(enableLFPRef, 0, Qt::AlignCenter);

    dgDigitalRefGroupLayout->addLayout(dgDigitalRefGroupTopLayout);
    dgDigitalRefGroupLayout->addLayout(dgDigitalRefGroupBottomLayout);

    dgDigitalRefGroup->setLayout(dgDigitalRefGroupLayout);
    dgDigitalRefGroup->setEnabled(true);
    dgDigitalRefGroup->setTitle(tr("Referencing Settings"));

    int minheight = dgDigitalRefGroup->sizeHint().height();
    dgDigitalRefGroup->setMaximumHeight(minheight);
    dgDigitalRefGroup->setMinimumHeight(minheight);
//    dgDigitalRefLayout->addWidget(enableDigitalRef);
    dgDigitalRefLayout->addWidget(dgDigitalRefGroup);
//    connect(enableDigitalRef, SIGNAL(toggled(bool)), dgDigitalRefGroup, SLOT(setEnabled(bool)));
//    connect(enableDigitalRef, SIGNAL(clicked(bool)), this, SLOT(nTrodeSetApplyButtonsEnabled()));
//    connect(enableDigitalRef, SIGNAL(clicked(bool)), this, SLOT(nTrodeSaveDigitalRef()));
    connect(nTrodeBox, SIGNAL(activated(int)), this, SLOT(setDigChannelBoxNum(int)));

        //    spike filter group
    dgSpikeFilterLayout = new QVBoxLayout();
    dgSpikeFilterGroup = new QGroupBox();
    dgSpikeFilterGroupLayout = new QHBoxLayout();
    dgSpikeFilterGroupLowLayout = new QVBoxLayout();
    dgSpikeFilterGroupHighLayout = new QVBoxLayout();
    lowFilterBox = new QComboBox();
    lowFilterBox->addItem("200");
    lowFilterBox->addItem("300");
    lowFilterBox->addItem("400");
    lowFilterBox->addItem("500");
    lowFilterBox->addItem("600");
//    connect(lowFilterBox, SIGNAL(activated(int)), this, SLOT(nTrodeSetApplyButtonsEnabled()));
    connect(lowFilterBox, SIGNAL(activated(int)), this, SLOT(nTrodeSaveLowFilter()));
    highFilterBox = new QComboBox();
    highFilterBox->addItem("5000");
    highFilterBox->addItem("6000");
//    connect(highFilterBox, SIGNAL(activated(int)), this, SLOT(nTrodeSetApplyButtonsEnabled()));
    connect(highFilterBox, SIGNAL(activated(int)), this, SLOT(nTrodeSaveHighFilter()));
    lowFilterBox->setToolTip(tr("Lower frequency bound (Hz)"));
    highFilterBox->setToolTip(tr("Upper frequency bound (Hz)"));
    labelLowFilter = new QLabel("Low");
    labelHighFilterFilter = new QLabel("High");
    labelLowFilter->setToolTip(tr("Lower bound for bandpass frequency filter."));
    labelHighFilterFilter->setToolTip(tr("Upper bound for bandpass frequency filter."));
    dgSpikeFilterGroupLowLayout->addWidget(labelLowFilter);
    dgSpikeFilterGroupLowLayout->addWidget(lowFilterBox);
    dgSpikeFilterGroupHighLayout->addWidget(labelHighFilterFilter);
    dgSpikeFilterGroupHighLayout->addWidget(highFilterBox);
    dgSpikeFilterGroupLayout->addLayout(dgSpikeFilterGroupLowLayout);
    dgSpikeFilterGroupLayout->addLayout(dgSpikeFilterGroupHighLayout);
    dgSpikeFilterGroup->setLayout(dgSpikeFilterGroupLayout);
//    enableSpikeFilter = new QCheckBox();
//    enableSpikeFilter->setText(tr("Enable Spike Filter"));
//    enableSpikeFilter->setToolTip(tr("Enable bandpass filtering for spike detection/sorting.  Applies to data visualization only, not to saved data."));
    dgSpikeFilterGroup->setTitle(tr("Spike Filter"));
    dgSpikeFilterGroup->setCheckable(true);



//    dgSpikeFilterLayout->addWidget(enableSpikeFilter);
    dgSpikeFilterLayout->addWidget(dgSpikeFilterGroup);
//    dgSpikeFilterGroup->setEnabled(false);
    connect(dgSpikeFilterGroup,SIGNAL(clicked(bool)), this, SLOT(nTrodeSaveSpikeFilter()));
//    connect(enableSpikeFilter,SIGNAL(toggled(bool)),dgSpikeFilterGroup,SLOT(setEnabled(bool)));
//    connect(enableSpikeFilter, SIGNAL(clicked(bool)), this, SLOT(nTrodeSaveSpikeFilter()));
    dgSpikeFilterGroup->setMinimumHeight(minheight);
    dgSpikeFilterGroup->setMaximumHeight(minheight);

        //   LFP settings group
    dgLFPsettingsGroup = new QGroupBox();
    //dgLFPsettingsGroup->setTitle(tr("LFP Settings"));
    dgLFPsettingsLayout = new QVBoxLayout();
    dgLFPsettingsGroupLayout = new QHBoxLayout();
    dgLFPsettingsGroupChannelLayout = new QVBoxLayout();
    dgLFPsettingsGroupHighLayout = new QVBoxLayout();
    LFPChannelsBox = new QComboBox();
    int numOfGroupedTrodes = 4;  //this number should be set to the number of 'n' of nTrode the user is making
    for (int i = 0; i < numOfGroupedTrodes; i++) {
        LFPChannelsBox->addItem(QString("%1").arg(i+1));
    }
//    connect(LFPChannelsBox, SIGNAL(activated(int)), this, SLOT(nTrodeSetApplyButtonsEnabled()));
    connect(LFPChannelsBox, SIGNAL(activated(int)), this, SLOT(nTrodeSaveLFPChannel()));
    LFPHighFilterBox = new QComboBox();
    LFPHighFilterBox->addItem("100");
    LFPHighFilterBox->addItem("200");
    LFPHighFilterBox->addItem("300");
    LFPHighFilterBox->addItem("400");
    LFPHighFilterBox->addItem("500");
    LFPHighFilterBox->setToolTip(tr("High pass filter (Hz)"));
//    connect(LFPHighFilterBox, SIGNAL(activated(int)), this, SLOT(nTrodeSetApplyButtonsEnabled()));
    connect(LFPHighFilterBox, SIGNAL(activated(int)), this, SLOT(nTrodeSaveLFPHighFilter()));
    labelLFPChannelsFilter = new QLabel(tr("Channel"));
    labelLFPHighFilter = new QLabel(tr("High cutoff"));
    dgLFPsettingsGroupChannelLayout->addWidget(labelLFPChannelsFilter);
    dgLFPsettingsGroupChannelLayout->addWidget(LFPChannelsBox);
    dgLFPsettingsGroupHighLayout->addWidget(labelLFPHighFilter);
    dgLFPsettingsGroupHighLayout->addWidget(LFPHighFilterBox);
    dgLFPsettingsGroupLayout->addLayout(dgLFPsettingsGroupChannelLayout);
    dgLFPsettingsGroupLayout->addLayout(dgLFPsettingsGroupHighLayout);
    dgLFPsettingsGroup->setLayout(dgLFPsettingsGroupLayout);
    dgLFPsettingsGroup->setTitle("LFP Filter");
    dgLFPsettingsGroup->setCheckable(true);
    connect(dgLFPsettingsGroup, SIGNAL(clicked(bool)), this, SLOT(nTrodeSaveLFPFilter()));
//    labelLFPsettings = new QLabel("LFP Settings");
//    dgLFPsettingsLayout->addWidget(labelLFPsettings, 0);
    dgLFPsettingsGroup->setMinimumHeight(minheight);
    dgLFPsettingsGroup->setMaximumHeight(minheight);
    dgLFPsettingsLayout->addWidget(dgLFPsettingsGroup);

        //  Spike Trigger Group.  This used to be a channel settings panel hence the function
    dgHardwareChannelLayout = new QVBoxLayout();
    iniHardwareChannelPanel(dgHardwareChannelLayout);
    channelSettingsGroup->setMinimumHeight(minheight);
    channelSettingsGroup->setMaximumHeight(minheight);

        //  Display Settings Group
    dgDisplaySettingsGroup = new QGroupBox(tr("Display Settings"));
    dgDisplaySettingsGroupLayout = new QHBoxLayout();

    //Max display spin box layout
    dgDisplaySettingsMaxDispLayout = new QVBoxLayout();
    labelMaxDispSpin = new QLabel(tr("Max Display"));
    maxDispSpinBox = new QSpinBox();
    maxDispSpinBox->setMinimum(50);
    maxDispSpinBox->setMaximum(4000);
    maxDispSpinBox->setSingleStep(25);
//    maxDispSpinBox->setFixedSize(50,22);
    maxDispSpinBox->setToolTip(tr("Spike Display Range (+/- μV)"));
    dgDisplaySettingsMaxDispLayout->addWidget(labelMaxDispSpin, 0, Qt::AlignCenter);
    dgDisplaySettingsMaxDispLayout->addWidget(maxDispSpinBox, 0, Qt::AlignCenter);
    dgDisplaySettingsGroupLayout->addLayout(dgDisplaySettingsMaxDispLayout);
//    connect(maxDispSpinBox, SIGNAL(valueChanged(int)), this, SLOT(nTrodeSetApplyButtonsEnabled()));
    connect(maxDispSpinBox, SIGNAL(editingFinished()), this, SLOT(nTrodeSaveMaxDisp()));

    //Channel color layout
    dgDisplaySettingsGroupChannelColorLayout = new QVBoxLayout();
    labelChannel = new QLabel(tr("Channel Color"));
    labelChannel->setMinimumSize(labelChannel->sizeHint());
    buttonChannelColorBox = new ClickableFrame();
    buttonChannelColorBox->setFixedSize(25,25);
    buttonChannelColorBox->setStyleSheet("background-color:#aaaaaa");
    connect(buttonChannelColorBox, SIGNAL(sig_clicked()), this, SLOT(getChanColorDialog()));
    //TODO: Add routines to check channel colors across nTrodes.  Make sure to add to red label changer and black label changer
    //TODO: make a SAVE CHANNEL COLOR routine to save the ntrodes' channel colors
    //TODO: make sure to load channel color
    dgDisplaySettingsGroupChannelColorLayout->addWidget(labelChannel, 0, Qt::AlignCenter);
    dgDisplaySettingsGroupChannelColorLayout->addWidget(buttonChannelColorBox, 0, Qt::AlignCenter);
    dgDisplaySettingsGroupLayout->addLayout(dgDisplaySettingsGroupChannelColorLayout);

    dgDisplaySettingsGroup->setLayout(dgDisplaySettingsGroupLayout);
    dgDisplaySettingsGroup->setMinimumHeight(dgDisplaySettingsGroup->sizeHint().height());
    dgDisplaySettingsGroup->setMaximumHeight(dgDisplaySettingsGroup->sizeHint().height());

        //  tag pannel group
    tagGroupPanel = new TagGroupingPanel();
    connect(tagGroupPanel, SIGNAL(sig_categoryAdded(QString)), this, SLOT(addCategoryToDict(QString)));
    connect(tagGroupPanel, SIGNAL(sig_newTagAdded(QString,QString)), this, SLOT(addTagToDict(QString,QString)));
    connect(this, SIGNAL(updatedAvailTags(QHash<QString,int>)), tagGroupPanel, SLOT(updateTags(QHash<QString,int>)));
//    connect(tagGroupPanel,SIGNAL(sig_enableApplyButtons()),this,SLOT(nTrodeSetApplyButtonsEnabled()));
    connect(tagGroupPanel,SIGNAL(sig_enableApplyButtons()),this,SLOT(nTrodeSaveTags()));
//    dgTagPanelLayout = new QVBoxLayout();
//    iniTagPannel(dgTagPanelLayout);

        //  add button layout here
    nTrodeSettingsButtonLayout = new QHBoxLayout();
//    buttonNTrodeCancel = new QPushButton();
//    buttonNTrodeCancel->setText(tr("Cancel"));
//    nTrodeSettingsButtonLayout->addWidget(buttonNTrodeCancel, 0, Qt::AlignLeft);
//    connect(buttonNTrodeCancel, SIGNAL(released()), this, SLOT(nTrodeCancelButtonPressed()));
    labelMismatchSettingsWarning = new QLabel("");
    setWidgetTextPaletteColor(labelMismatchSettingsWarning, QColor("red"));
    nTrodeSettingsButtonLayout->addWidget(labelMismatchSettingsWarning,0,Qt::AlignRight);
    buttonNTrodeApply = new QPushButton();
    buttonNTrodeApply->setText(tr("Apply to selected"));
    buttonNTrodeApply->setEnabled(false);
    buttonNTrodeApply->setFixedSize(buttonNTrodeApply->sizeHint());
    nTrodeSettingsButtonLayout->addWidget(buttonNTrodeApply);//, Qt::AlignRight);
    connect(buttonNTrodeApply, SIGNAL(released()), this, SLOT(nTrodeApplyButtonPressed()));
    buttonNTrodeApplyToAll = new QPushButton();
    buttonNTrodeApplyToAll->setText(tr("Apply to all"));
    buttonNTrodeApplyToAll->setEnabled(true);
    buttonNTrodeApplyToAll->setFixedSize(buttonNTrodeApplyToAll->sizeHint());
    nTrodeSettingsButtonLayout->addWidget(buttonNTrodeApplyToAll);//, 0, Qt::AlignRight);
    connect(buttonNTrodeApplyToAll, SIGNAL(released()), this, SLOT(nTrodeApplyToAllButtonPressed()));
//    buttonNTrodeAdd = new QPushButton();
//    buttonNTrodeAdd->setText(tr("Add nTrode"));
//    buttonNTrodeAdd->setEnabled(true);
//    buttonNTrodeAdd->setFixedSize(buttonNTrodeAdd->sizeHint());
//    nTrodeSettingsButtonLayout->addWidget(buttonNTrodeAdd);//, 0, Qt::AlignRight);
//    connect(buttonNTrodeAdd, SIGNAL(released()), this, SLOT(nTrodeAddbuttonPressed()));

    dgMainLayout = new QVBoxLayout();
    dgSecondaryLayout = new QHBoxLayout();
//    dgSecondaryLayout->addLayout(dgDigitalRefLayout);
//    dgSecondaryLayout->addLayout(dgSpikeFilterLayout);
//    dgSecondaryLayout->addLayout(dgLFPsettingsLayout);
//    dgSecondaryLayout->addLayout(dgHardwareChannelLayout);
//    dgSecondaryLayout->addLayout(dgTagPanelLayout);
    dgPanelsLayout = new QGridLayout();
    dgPanelsLayout->addLayout(dgDigitalRefLayout, 0, 0, 1, 1);
    dgPanelsLayout->addLayout(dgLFPsettingsLayout, 0, 1, 1, 1);
    dgPanelsLayout->addLayout(dgSpikeFilterLayout, 1, 0, 1, 1);
    dgPanelsLayout->addLayout(dgHardwareChannelLayout, 1, 1, 1, 1);
    dgPanelsLayout->addWidget(dgDisplaySettingsGroup, 2, 0, 1, 3,Qt::AlignCenter);

    dgSecondaryLayout->addLayout(dgPanelsLayout);
    dgSecondaryLayout->addWidget(tagGroupPanel);
    int maxHeight = minheight*2 + dgDisplaySettingsGroup->sizeHint().height();
            //dgDigitalRefLayout->sizeHint().height() + dgLFPsettingsGroupLayout->sizeHint().height() + dgSpikeFilterLayout->sizeHint().height() + dgHardwareChannelLayout->sizeHint().height() + dgDisplaySettingsGroup->sizeHint().height();
    tagGroupPanel->setMaximumHeight(maxHeight);
    tagGroupPanel->setMinimumHeight(maxHeight);

    dgMainLayout->addLayout(dgSecondaryLayout);
    dgMainLayout->addLayout(nTrodeSettingsButtonLayout);
    defaultGroup->setLayout(dgMainLayout);

    masterLayout->addWidget(defaultGroup);
//    masterLayout->addLayout(nTrodeSettingsButtonLayout);
}*/

void WorkspaceEditor::iniAddNTrodeGroup(QBoxLayout *masterLayout) {
    //FUNCTION DEPRECIATED
    //add nTrode
    //buttonAddNTrode = new QPushButton();
    //buttonAddNTrode->setText("Add nTrode group");
    //editNumToAdd = new QLineEdit;
    //editNumToAdd->setText("1");
    //addNTrodeLayout = new QHBoxLayout();
    //addNTrodeLayout->addWidget(buttonAddNTrode);
    //addNTrodeLayout->addWidget(editNumToAdd);
    //connect(buttonAddNTrode,SIGNAL(pressed()),this,SLOT(addNTrodesToList()));
    //masterLayout->addLayout(addNTrodeLayout);
}

//DEPRECIATED: New grouping tag system makes this function obsolete
void WorkspaceEditor::iniFilterBar(QBoxLayout *masterLayout) {

    filterGroup = new QGroupBox();
    filterGroup->setTitle(tr("nTrode Filters"));
    filterGroupLayout = new QVBoxLayout();

    //Top Layout
    filterGroupTopLayout = new QHBoxLayout();


    labelFilterSelector = new QLabel(tr("Filters:"));
    filterSelector = new QComboBox();
    filterSelector->addItem("No Filter");
    setComboBoxFirstItemUnselectable(filterSelector);
    connect(filterSelector,SIGNAL(activated(int)),this,SLOT(processFilterSelector()));

    labelFilterOperationSelector = new QLabel(tr("Filter Operation:"));
    labelFilterOperationSelector->setMaximumWidth(labelFilterOperationSelector->sizeHint().width());
    filterOperationSelector = new QComboBox();
    filterOperationSelector->addItem("nTrodes with one filter (OR)");
    filterOperationSelector->addItem("nTrodes with all filters (AND)");
    filterOperationSelector->setCurrentIndex(0);
    connect(filterOperationSelector,SIGNAL(activated(int)),this,SLOT(applyFilters()));

    buttonClearFilters = new QPushButton();
    buttonClearFilters->setText("Clear Filters");
    buttonClearFilters->setMaximumWidth(buttonClearFilters->sizeHint().width());
    connect(buttonClearFilters,SIGNAL(released()),this,SLOT(clearFilters()));

    filterGroupTopLayout->addWidget(labelFilterSelector, 0, Qt::AlignLeft);
    filterGroupTopLayout->addWidget(filterSelector, 0, Qt::AlignLeft);
    filterGroupTopLayout->addWidget(labelFilterOperationSelector, 0, Qt::AlignLeft);
    filterGroupTopLayout->addWidget(filterOperationSelector, 0, Qt::AlignLeft);
    filterGroupTopLayout->addWidget(buttonClearFilters, 0, Qt::AlignRight);

    //Bottom Layout
    labelActiveFilters = new QLabel(tr("Active Filters: "));


    filterGroupLayout->addLayout(filterGroupTopLayout);
    filterGroupLayout->addWidget(labelActiveFilters, 0, Qt::AlignLeft);

    filterGroup->setLayout(filterGroupLayout);
    masterLayout->addWidget(filterGroup);


}

void WorkspaceEditor::iniListLayout(QBoxLayout *masterLayout) {
    //List Layout
    nTrodeTree = new NTrodeTreeWidget();
    //nTrodeTree->header()->close();
    availHardChanList = new HWChannelTreeWidget();
    //availHardChanList->setDragEnabled(true);
    //availHardChanList->setDragDropMode(QAbstractItemView::DragOnly);
    //availHardChanList->setDefaultDropAction(Qt::MoveAction);
    availHardChanList->setFixedWidth(200);






    //nTrodeTree->setDragEnabled(true);
    //nTrodeTree->setDragDropMode(QAbstractItemView::DragDrop);
    //nTrodeTree->setDefaultDropAction(Qt::MoveAction);
    nTrodeTree->setTotalHwChannels(32); //set default hardware channel count
//    nTrodeTree->setHeaderHidden(false);

    QSize buttonSize = QSize(0,0);
    /*buttonNTrodeAdd = new QPushButton();
    buttonNTrodeAdd->setText(tr("Create nTrode"));
    buttonNTrodeAdd->setEnabled(true);
    if (buttonSize.width() < buttonNTrodeAdd->sizeHint().width())
        buttonSize = buttonNTrodeAdd->sizeHint();
    connect(buttonNTrodeAdd, SIGNAL(released()), this, SLOT(nTrodeAddbuttonPressed()));*/

    /*buttonAddListItems = new QPushButton();
    buttonAddListItems->setText(tr("<<Add Channel"));
    buttonAddListItems->setEnabled(true);
    if (buttonSize.width() < buttonAddListItems->sizeHint().width())
        buttonSize = buttonAddListItems->sizeHint();
    connect(buttonAddListItems, SIGNAL(released()), this, SLOT(addListItemsButtonPressed()));*/

    /*buttonRemoveListItems = new QPushButton();
    buttonRemoveListItems->setText(tr("Remove>>"));
    buttonRemoveListItems->setEnabled(true);
    if (buttonSize.width() < buttonRemoveListItems->sizeHint().width())
        buttonSize = buttonRemoveListItems->sizeHint();
    connect(buttonRemoveListItems, SIGNAL(released()), nTrodeTree, SLOT(removeSelectedItems()));*/


    buttonAutoPopulate = new QPushButton("Autopopulate");
    connect(buttonAutoPopulate, &QPushButton::clicked, this, &WorkspaceEditor::autopopulateButtonPressed);

    //normalize button sizes
    //buttonNTrodeAdd->setFixedSize(buttonSize);
    //buttonAddListItems->setFixedSize(buttonSize);
    //buttonRemoveListItems->setFixedSize(buttonSize);

    //listButtonLayout = new QVBoxLayout();
    //listButtonLayout->addWidget(buttonNTrodeAdd);
    //listButtonLayout->addWidget(buttonAddListItems);
    //listButtonLayout->addWidget(buttonRemoveListItems);

    channelTable = new NTrodeChannelMappingTable();
    channelTable->setNumHardwareChannels(32);

    listLayout = new QHBoxLayout();
    listLayout->addWidget(nTrodeTree);
    //listLayout->addLayout(listButtonLayout);
    listLayout->addWidget(availHardChanList);
    listLayout->addWidget(channelTable);
    listLayout->setStretch(3,1);


    //connect(nTrodeTree, SIGNAL(sig_removedNTrode(int)), this, SLOT(configChanged()));
    //connect(nTrodeTree,SIGNAL(sig_returnChild(QString)), this, SLOT(configChanged()));
    //connect(nTrodeTree, SIGNAL(sig_channelMoved()), this, SLOT(configChanged()));
    connect(nTrodeTree,SIGNAL(sig_changed()),this,SLOT(channelMapChangedInList()));
    connect(nTrodeTree,SIGNAL(sig_clickedOnChannel(int)),this,SLOT(setHardwareChannelHighlight(int)));
    connect(channelTable,SIGNAL(newChannelAssignment()),this,SLOT(channelMapChangedInTable()));
    connect(channelTable,SIGNAL(channelClicked(int)),this,SLOT(setHardwareChannelHighlight(int)));
    //connect(nTrodeTree, SIGNAL(sig_returnChild(QString)), availHardChanList, SLOT(addHardwareChannel(QString)));
    //connect(nTrodeTree, SIGNAL(sig_removeChildFromList(int)), availHardChanList, SLOT(removeHardwareChannel(int)));
    //connect(nTrodeTree, SIGNAL(sig_selectedNTrode(int)), this, SLOT(loadNTrodeSettingsIntoPanel(int)));
    //connect(nTrodeTree, SIGNAL(sig_reloadNTrodeSettings(int)), this, SLOT(loadNTrodeSettingsIntoPanel(int)));
    connect(nTrodeTree, SIGNAL(sig_sendFocus(int)), this, SLOT(setFocusedWiget(int)));
    connect(availHardChanList, SIGNAL(sig_sendFocus(int)), this, SLOT(setFocusedWiget(int)));
    connect(availHardChanList,SIGNAL(sig_clickedOnChannel(int)),this,SLOT(setHardwareChannelHighlight(int)));
    //connect(availHardChanList, SIGNAL(sig_sendChannels()), this, SLOT(addListItemsButtonPressed()));
//    connect(nTrodeTree, SIGNAL(sig_channelRemovedFromNTrode(int,int)),this,SLOT(checkDigitalRefs(int,int)));
//    connect(nTrodeTree, SIGNAL(sig_removedNTrode(int)),this,SLOT(checkDigitalRefs(int)));
    //connect(nTrodeTree, SIGNAL(sig_clickedOnChannel(int)), this, SLOT(setHardwareEditViewToChannel(int)));

    //create delegate to customize NTrodeTreeWidget's paint events
    //nTrodeDelegate = new NTrodeTreeWidgetDelegate(this, nTrodeTree);
    //nTrodeTree->setItemDelegate(nTrodeDelegate);

    masterLayout->addLayout(listLayout);
}

void WorkspaceEditor::setHardwareChannelHighlight(int HWchan) {
    //Sets the highlighted channel in the channel mapping tab

    nTrodeTree->setHighlightChannel(HWchan);
    availHardChanList->setHighlightChannel(HWchan);
    channelTable->setHighlightChannel(HWchan);
}

void WorkspaceEditor::setChannelMap(QList<int> channelMap) {

    int currentNumChannels = channelTable->getChannelAssignments().length();

    if (channelMap.length() == currentNumChannels) {
        if (!channelTable->isEditing()) { //Only change the table if not currently in edit mode
            channelTable->setEditProcessorLock(true); //Prevent changes to the channelTable from emitting signals back out
            for (int chInd = 0; chInd < channelMap.length(); chInd++) {
                channelTable->assignNTrodeIDToChannel(chInd,channelMap[chInd]);
            }
            channelTable->setEditProcessorLock(false);
            channelMapChangedInTable();

         }
    }

}

void WorkspaceEditor::channelMapChangedInList() {

    configChanged();
    if (!channelTable->isEditing()) { //Only change the table if these changed are not the result of changes that occured directly in the table
        channelTable->setEditProcessorLock(true); //Prevent changes to the channelTable from emitting signals back out
        QList<int> ntmap = nTrodeTree->getMap();
        for (int chInd = 0; chInd < ntmap.length(); chInd++) {
            channelTable->assignNTrodeIDToChannel(chInd,ntmap[chInd]);
        }

        /*for (int i = 0; i < nTrodeTree->topLevelItemCount(); i++) {
            int tempID = nTrodeTree->getNTrode(i).settings.nTrodeId;
            for (int chInd = 0; chInd < nTrodeTree->getNTrode(i).settings.unconverted_hw_chan.length(); chInd++) {
                channelTable->assignNTrodeIDToChannel(nTrodeTree->getNTrode(i).settings.unconverted_hw_chan[chInd],tempID);
            }
        }

        QList<int> aList = availHardChanList->getChannelsInList();
        for (int i=0; i<aList.length(); i++) {
            channelTable->assignNTrodeIDToChannel(aList[i],-1);

        }*/

        //Calculate which channels are unused
        QList<int> unusedHWChannels;

        for (int i=0; i<ntmap.length();i++) {
            if (ntmap[i] == -1) {
                unusedHWChannels.append(i);
            }
        }

        //Update the available channels list
        availHardChanList->setChannelsInList(unusedHWChannels);

        channelTable->setEditProcessorLock(false);
    }

}

void WorkspaceEditor::channelMapChangedInTable() {
    //Called when the channel mapping was changed in the table   

    //Get the new map from the table
    QList<int> newMap = channelTable->getChannelAssignments();

    //Update the tree list
    nTrodeTree->updateMapping(newMap);

    //Calculate which channels are unused
    QList<int> unusedHWChannels;
    for (int i=0; i<newMap.length();i++) {
        if (newMap[i] == -1) {
            unusedHWChannels.append(i);
        }
    }

    //Update the available channels list
    availHardChanList->setChannelsInList(unusedHWChannels);

    configChanged();


}

void WorkspaceEditor::iniHardwareChannelPanel(QBoxLayout *masterLayout) {
    scChannelSettingsMainLayout = new QVBoxLayout();
    scChannelSettingsGroupLayout = new QHBoxLayout();

    channelSettingsGroup = new QGroupBox();

    //Enable Trigger Option
    scChannelSettingsEnableTriggerLayout = new QHBoxLayout();
    channelSettingsGroup->setTitle(tr("Spike Trigger"));
//    enableTrigger = new QCheckBox();
//    enableTrigger->setText(tr("Enable Spike Trigger"));
//    enableTrigger->setChecked(false);
//    scChannelSettingsEnableTriggerLayout->addWidget(enableTrigger);
    scChannelSettingsMainLayout->addLayout(scChannelSettingsEnableTriggerLayout);
//    connect(enableTrigger, SIGNAL(clicked(bool)), this, SLOT(nTrodeSetApplyButtonsEnabled()));
//    connect(enableTrigger, SIGNAL(clicked(bool)), this, SLOT(nTrodeSaveEnableTrigger()));
//    connect(enableTrigger, SIGNAL(toggled(bool)), channelSettingsGroup, SLOT(setEnabled(bool)));

    //Threashold spin box layout
    scChannelSettingsThreshLayout = new QVBoxLayout();
    labelThreshSpin = new QLabel(tr("Threshold"));
    threshSpinBox = new QSpinBox();
    threshSpinBox->setMinimum(10);
    threshSpinBox->setMaximum(4000);
    threshSpinBox->setSingleStep(10);
//    threshSpinBox->setFixedSize(50,22);
    threshSpinBox->setToolTip(tr("Trigger threshold (μV)"));
    threshSpinBox->setMaximumSize(threshSpinBox->sizeHint());
    scChannelSettingsThreshLayout->addWidget(labelThreshSpin);
    scChannelSettingsThreshLayout->addWidget(threshSpinBox);
    scChannelSettingsGroupLayout->addLayout(scChannelSettingsThreshLayout);
//    connect(threshSpinBox, SIGNAL(valueChanged(int)), this, SLOT(nTrodeSetApplyButtonsEnabled()));
    connect(threshSpinBox, SIGNAL(editingFinished()), this, SLOT(nTrodeSaveThresh()));

    channelSettingsGroup->setLayout(scChannelSettingsGroupLayout);
//    channelSettingsGroup->setEnabled(false);

    scChannelSettingsMainLayout->addWidget(channelSettingsGroup);


    masterLayout->addLayout(scChannelSettingsMainLayout);

    //masterLayout->addWidget(channelSettingsGroup);

    //channelSettingsGroup->setMaximumWidth(channelSettingsGroup->sizeHint().width());
}

void WorkspaceEditor::iniTagPannel(QBoxLayout *masterLayout) {

//    masterLayout->setSpacing(0);
//    masterLayout->setContentsMargins(0,0,0,0);

    labelTagPanel = new QLabel(tr("Grouping Tags"));
//    labelTagPanel->setMaximumHeight(labelTagPanel->sizeHint().height());
    masterLayout->addWidget(labelTagPanel, 0, Qt::AlignTop);


    tagGroup = new QGroupBox();
    tagGroupLayout = new QVBoxLayout();

    labelTagSelection = new QLabel(tr("Tags"));

    tagSelectionBox = new QComboBox();
    tagSelectionBox->addItem("");
    tagSelectionBox->addItem("+Add/-Remove Tag");
    setComboBoxFirstItemUnselectable(tagSelectionBox);
    tagGroupLayout->addWidget(labelTagSelection);
    tagGroupLayout->addWidget(tagSelectionBox);
    connect(tagSelectionBox,SIGNAL(activated(int)),this,SLOT(processTagSelectionBox(int)));

    tagGroup->setLayout(tagGroupLayout);

    //masterLayout->addWidget(tagGroup);
    QListWidget *list = new QListWidget();
    masterLayout->addWidget(list);
//    masterLayout->setStretch(0,0);
//    masterLayout->setSpacing(0);
//    masterLayout->setContentsMargins(0,0,0,0);

}


// *************************** Module Tab Initialization Functions **************************
// ******************************************************************************************

void WorkspaceEditor::iniAddModuleGroup(QBoxLayout *masterLayout) {
    amGroup = new QGroupBox();
    amGroup->setTitle("Add Module");

    amGroupLayout = new QHBoxLayout();

    modulePullDownMenu = new QComboBox();
    modulePullDownMenu->addItem("Select Module");
    modulePullDownMenu->addItem("cameraModule");
    modulePullDownMenu->addItem("stateScript");
    modulePullDownMenu->addItem("SimpleCommunicator");
    modulePullDownMenu->addItem("SimpleMonitor");
    modulePullDownMenu->addItem("FSGui");
    modulePullDownMenu->addItem("+ Add New");

    connect(modulePullDownMenu, SIGNAL(activated(int)), this, SLOT(processModulePullDownMenuIndex(int)));

    labelSendNetworkInfo = new QLabel(tr("Send Network Info"));
    enableSendNetworkInfo = new QCheckBox();
    enableSendNetworkInfo->setChecked(true);

    labelSendTrodesConfig = new QLabel(tr("Send Trodes Config"));
    enableSendTrodesConfig = new QCheckBox();
    enableSendNetworkInfo->setChecked(true);

    buttonAddModule = new QPushButton();
    buttonAddModule->setText(tr("Add"));
    connect(buttonAddModule, SIGNAL(released()), this, SLOT(addEditModuleToConfig()));

    amGroupLayout->addWidget(modulePullDownMenu);

    amGroupLayout->addWidget(labelSendNetworkInfo);
    amGroupLayout->addWidget(enableSendNetworkInfo);

    amGroupLayout->addWidget(labelSendTrodesConfig);
    amGroupLayout->addWidget(enableSendTrodesConfig);

    amGroupLayout->addWidget(buttonAddModule);

    amGroup->setLayout(amGroupLayout);
    masterLayout->addWidget(amGroup);
}

void WorkspaceEditor::iniLowerLayout(QBoxLayout *masterLayout) {
    //remember to connect the moduleArgumentList to the 'add' button in the addmoduleGroup
    amLowerMainLayout = new QHBoxLayout();

    //moduleArgumentList = new QListWidget();
    //amLowerMainLayout->addWidget(moduleArgumentList);
    moduleArgumentTree = new QTreeWidget();
    moduleArgumentTree->setHeaderHidden(true);
    amLowerMainLayout->addWidget(moduleArgumentTree);

    amLowerButtonLayout = new QVBoxLayout();
    buttonAddArgument = new QPushButton();
    buttonAddArgument->setText("+Add Argument");
    buttonEdit = new QPushButton();
    buttonEdit->setText("Edit");
    buttonDelete = new QPushButton();
    buttonDelete->setText("Delete");
    amLowerButtonLayout->addWidget(buttonAddArgument);
    amLowerButtonLayout->addWidget(buttonEdit);
    QSpacerItem *spacer = new QSpacerItem(1,1);
    amLowerButtonLayout->addSpacerItem(spacer);
    amLowerButtonLayout->addWidget(buttonDelete);
    connect(buttonAddArgument,SIGNAL(released()),this,SLOT(addArgument()));
    connect(buttonEdit,SIGNAL(released()),this,SLOT(editModuleArgument()));
    connect(buttonDelete,SIGNAL(released()),this,SLOT(removeModuleArgument()));

    amLowerMainLayout->addLayout(amLowerButtonLayout);

    masterLayout->addLayout(amLowerMainLayout);
}


// ******************************************************************************************

void WorkspaceEditor::connectTabElements() {
    //this function will connect different tab elements together after they have all been initialized
    connect(this->channelSelector, SIGNAL(activated(QString)), this, SLOT(channelNumberSelected(QString)));
    connect(this,SIGNAL(hardwareChannelsInitialized(int)),this->nTrodeTree, SLOT(setTotalHwChannels(int)));
    //connect(this->nTrodeTree, SIGNAL(sig_addedNTrode(int,bool)), this, SLOT(addedNTrode(int,bool)));
    //connect(this->nTrodeTree, SIGNAL(sig_removedNTrode(int)), this, SLOT(removedNTrode(int)));
}

void WorkspaceEditor::replaceTab(int index, QWidget *page, QString name) {
    if (index >= this->count() || index < 0) {
        qDebug() << "Error: Invalid index passed. (WorkspaceEditor::replaceTab)";
        return;
    }
    this->removeTab(index);
    this->insertTab(index, page, name);
    this->setCurrentIndex(index);
    setTabEnabled(auxTab,true);
    //setTabEnabled(spikeTab,true);
    setTabEnabled(moduleTab,true);
}

void WorkspaceEditor::setWidgetTextPaletteColor(QWidget *widget, QColor newColor) {
    QPalette p = widget->palette();
    p.setColor(QPalette::Active, QPalette::WindowText, newColor);
    widget->setPalette(p);
}

void WorkspaceEditor::setWidgetTextPaletteColor(QCheckBox *widget, QColor newColor){
    widget->setStyleSheet("QCheckBox {border: none;color: " + newColor.name() + ";}");
}

void WorkspaceEditor::setComboBoxFirstItemUnselectable(QComboBox *comboBox) {
    //The purpose of this function is to set the passed combo box's first item to be unselectable by the user
    //This is so the combo box can be set to an 'empty' value;
    QStandardItemModel* model = qobject_cast<QStandardItemModel*>(comboBox->model());
    QModelIndex firstIndex = model->index(0, comboBox->modelColumn(), comboBox->rootModelIndex());
    QStandardItem* firstItem = model->itemFromIndex(firstIndex);
    firstItem->setSelectable(false);
}

// ******************************* Save/Load to XML Functions *******************************
// ******************************************************************************************
void WorkspaceEditor::configChanged() {
    unsavedChanges = true;
    updateTitle();
}

void WorkspaceEditor::updateTitle() {
    QString t;
    if (curSaveFile == "") {
        if (curLoadedFile == "") {
            t = "Untitled";
        } else {
            t = curLoadedFile;
        }
    } else {
        t = curSaveFile;
    }

    if (unsavedChanges) {
        t = t+"*";
    }

    emit newTitle(t);

}

bool WorkspaceEditor::saveToXML(QString filename, bool isTempSave) {

    //NOTE: for embedded editor, this might be a problem for workspace creation...

    if (filename == "" && !isTempSave) {
        //default to curSaveFileName
        if (curSaveFile == "") {
            if (curLoadedFile == "") {
                //if no curSaveFile name, ask user for a file name
                //qDebug() << "no Currently saved file, execute saveAs...";
                curSaveFile = saveAsToXML();
                if (curSaveFile.isEmpty()) {
                    return(false);
                }
                filename = curSaveFile;
                curLoadedFile = curSaveFile;
            } else {
                filename = curLoadedFile;
            }
        } else {
            filename = curSaveFile;
        }
    }
//    printNTrodeSettings();
    prepConfigsForSave();
    QString errorMsg = "";

    if (verifyCurrentWorkspace(&errorMsg)) {
        if (!isTempSave)
            qDebug() << "Saving File: " << filename;

        //writeTrodesConfig(filename);
        loadedWorkspace.writeTrodesConfig(filename);
        unsavedChanges = false;
        curSaveFile = filename;
        curLoadedFile = curSaveFile;
        updateTitle();
        return(true);
    }
    else {
        if (!isTempSave) {
            errorMsg = QString("Error:  Workspace could not be saved for the following reasons:\n%1").arg(errorMsg);
            QMessageBox::critical(this, tr("Error Saving File"), tr(qPrintable(errorMsg)));
        }
        else { //if this is a temp save, it's being used to immediately load a workspace
            errorMsg = QString("Error:  Workspace could not be created for the following reasons:\n%1").arg(errorMsg);
            QMessageBox::critical(this, tr("Error Creating Workspace"), tr(qPrintable(errorMsg)));
        }
        return(false);
    }
//    printNTrodeSettings();
}

QString WorkspaceEditor::saveAsToXML() {





    QString filePath = curSaveFile;
    if (filePath.isEmpty()) {
        filePath = curLoadedFile;
    }
    //if (filePath.isEmpty()) {
        QFileDialog dialog(this, "Save configuration as...");
        dialog.setDefaultSuffix("trodesconf");
        dialog.selectFile(filePath);
        dialog.setFileMode(QFileDialog::AnyFile);
        dialog.setAcceptMode(QFileDialog::AcceptSave);
        dialog.setOption(QFileDialog::DontConfirmOverwrite, false);
        QStringList filenames;
        if (dialog.exec()) {
            filenames = dialog.selectedFiles();
        }
        //QString filename;
        if (filenames.size() == 1)
            filePath = filenames.first();
    //}

    //curSaveFile = filePath;
    //saveToXML(filePath);

    /*if (!filenames.isEmpty()) {
        //CHECK FOR FILE OVERWRITE;

        curSaveFile = filename;
        saveToXML(filename);
    }*/
    return filePath;
}

void WorkspaceEditor::loadDefaultUnconfigured() {
    loadingXML = true;
    nTrodeTree->setLoading(true);
    QString templateWorkspace;
#if defined (__APPLE__)
    templateWorkspace = QString("%1/../../../Resources/SampleWorkspaces/BlankWorkspace.trodesconf").arg(QCoreApplication::applicationDirPath());
#else
    templateWorkspace = QString("%1/Resources/SampleWorkspaces/BlankWorkspace.trodesconf").arg(QCoreApplication::applicationDirPath());
#endif

    loadFromXML(templateWorkspace);
    curLoadedFile="";
    curSaveFile="";
    updateTitle();
}

void WorkspaceEditor::loadFromXML(QString filename) {
    loadingXML = true;
    nTrodeTree->setLoading(true);

    qDebug() << "Loading: " << filename;

    //int retval = nsParseTrodesConfig(filename);

    QString errorString = loadedWorkspace.readTrodesConfig(filename);
    if (!errorString.isEmpty()) {
        qDebug() << "Error: Invalid Config File. (WorkspaceEditor::loadFromXML)." << errorString;
        return;
    }
    curLoadedFile = filename;

    setTabOneToGeneralView(); //if tab one wasn't already set to config view, set it!

    //set all workspaceConfigs to global configs
    //mark: TO DO: delete config objects if they exist before this, we won't need them anymore
    //mark: TO DO: don't copy the pointer, COPY THE SETTINGS, not doing that will cause segmentation faults in Trodes
    globalConfig = &loadedWorkspace.globalConf;
    hardwareConfig = &loadedWorkspace.hardwareConf;
    headerConfig = &loadedWorkspace.headerConf;
    streamConfig = &loadedWorkspace.streamConf;
    spikeConfig = &loadedWorkspace.spikeConf;
    moduleConfig = &loadedWorkspace.moduleConf;
    benchmarkConfig = &loadedWorkspace.benchConfig;

    //set containers to hold read values...
    //device container (this happens in loadHardwareConfigINtoGUI()

    //clear aux GUI list elements
    clearAuxGUI();

    //nTrodeConfig container
    nTrodeConfigs.clear();

    for (int i = 0; i < spikeConfig->ntrodes.length(); i++) {

        SingleSpikeTrodeConf curNTrode = *spikeConfig->ntrodes.at(i);
        if (curNTrode.unconverted_hw_chan.length() > 0) {
            curNTrode.channelSettings.thresh = curNTrode.thresh.at(0);
            curNTrode.channelSettings.maxDisp = curNTrode.maxDisp.at(0);
            curNTrode.channelSettings.triggerOn = curNTrode.triggerOn.at(0);
            curNTrode.channelSettings.streamingChannelLookup = curNTrode.streamingChannelLookup.at(0);
        }
        else {
            qDebug() << "Error: nTrode does not have child, abort loading channel settings. (WorkspaceGui::loadXML)";
        }

        nTrodeConfigs.append(curNTrode);
    }

    //moduleConfigs container
    moduleConfigs.clear();
    for (int i = 0; i < moduleConfig->singleModuleConf.length(); i++) {
        moduleConfigs.append(moduleConfig->singleModuleConf.at(i));
    }
//    printModuleConfigs();


    //load all configuration info into GUI
    loadGlobalConfigIntoGUI();
    loadHardwareConfigIntoGUI();
    loadStreamConfigIntoGUI();
    loadHeaderConfigIntoGUI();
    loadNTrodeConfigIntoGUI();
    loadModuleConfigIntoGUI();
    loadRefGroupsIntoGUI();
    nTrodeTree->setLoading(false);
    loadingXML = false;
    unsavedChanges = false;
    updateTitle();
}

QString WorkspaceEditor::askToSaveUnsavedChanges() {
    if (!unsavedChanges) //exit if there are no unsaved changes
        return "";

    QString filePath = "";// = curSaveFile;
//    if (filePath == "") {
//        filePath = curLoadedFile;
//    }

    QMessageBox *askUser = new QMessageBox(this);
    askUser->setText("Save Configuration?");
    askUser->setInformativeText("There are unsaved changes to the current configuration.  Do you want to save them?");
    QPushButton *cancelButton = askUser->addButton("Cancel", QMessageBox::RejectRole);
    QPushButton *noSaveButton = askUser->addButton("Don't Save", QMessageBox::NoRole);
    QPushButton *saveButton = askUser->addButton("Save", QMessageBox::AcceptRole);
    askUser->exec();
    QAbstractButton *choice = askUser->clickedButton();
    if (choice == saveButton) {
        filePath = saveToXML("");
    } else if (choice == cancelButton) {
        filePath = "---"; //message to cancel close.
    }
    delete askUser; //delete the
    return filePath;
}

//NOTE: not in use, this function still has the following issues:
//  1) Previously selected devices are greyed out
//  2) refNTrode warning are being thrown
//  3) many nTrode settings are not being cleared properly (included cur selected nTrode)
void WorkspaceEditor::clearGUI() {
    //set globalConfig defaults
    globalConfig->filePath = "";
    globalConfig->filePrefix = "";
    globalConfig->saveDisplayedChanOnly = true;
    globalConfig->realTimeMode = true;

    //set hardwareConfig defaults
    hardwareConfig->NCHAN = 32;
    hardwareConfig->sourceSamplingRate = DEFAULT_SAMPLING_RATE;
    hardwareConfig->devices.clear();

    //set streamComfig defaults
    streamConfig->nColumns = 2;
    streamConfig->backgroundColor = QColor("#808080");

    //set headerConfig defaults
    headerConfig->headerChannels.clear();
    clearAuxGUI();

    //set spikeConfig defaults
    nTrodeConfigs.clear();
    /*if (spikeConfig != NULL && spikeConfig != spikeConf)
        delete spikeConfig;*/

    //set moduleConfig defaults
    moduleConfigs.clear();
    /*if (moduleConfig != NULL && moduleConfig != moduleConf)
        delete moduleConfig;*/

    loadGlobalConfigIntoGUI();
    loadHardwareConfigIntoGUI();
    loadStreamConfigIntoGUI();
    loadHeaderConfigIntoGUI();
    loadNTrodeConfigIntoGUI();
    loadModuleConfigIntoGUI();

    curSaveFile = "";
    curLoadedFile = "";
    updateTitle();
}

//api function to set certain GUI elements un-editable, specifically for Recofiguring
void WorkspaceEditor::setForReconfigure() {
    hardwareGroup->setEnabled(false);
}

void WorkspaceEditor::buttonLoadPressed() {

    askToSaveUnsavedChanges();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("paths"));
    QString tempPath = settings.value(QLatin1String("configPath")).toString();

    settings.endGroup();
    QString filename = QFileDialog::getOpenFileName(this, QString("Open configuration file"), tempPath, "Trodes config files (*.trodesconf *.xml)");

    if (!filename.isEmpty())
        loadFromXML(filename);
}



void WorkspaceEditor::prepConfigsForSave() {
    /*if (globalConfig == NULL) {
        globalConfig = new GlobalConfiguration(this);
    }
    if (hardwareConfig == NULL) {
        hardwareConfig = new HardwareConfiguration(this);
    }
    if (headerConfig == NULL) {
        headerConfig = new headerDisplayConfiguration(this);
    }
    if (streamConfig == NULL) {
        streamConfig = new streamConfiguration();
        streamConfig->nColumns = 2;
        streamConfig->backgroundColor = "#808080";
    }
    if (spikeConfig == NULL) {
        spikeConfig = new SpikeConfiguration(this, nTrodeConfigs.length());
    }
    if (moduleConfig == NULL) {
        moduleConfig = new ModuleConfiguration(this);
    }
    if (benchmarkConfig == NULL) {
        benchmarkConfig = new BenchmarkConfig();
    }*/

    //make sure to retrieve all values from config
    setGlobalConfigValues();
    setStreamConfigValues();
    setHardwareConfigValues();
    setNTrodeConfigValues();
    setRefGroupConfigValues();
    setModuleConfigValues();

    //make temp ptrs to delete the global conf objects after reassignment
    /*GlobalConfiguration *tempG = globalConf;
    HardwareConfiguration *tempH = hardwareConf;
    headerDisplayConfiguration *tempA = headerConf;
    streamConfiguration *tempS = streamConf;
    SpikeConfiguration *tempSC = spikeConf;
    ModuleConfiguration *tempM = moduleConf;

    if (tempG != NULL && (tempG != globalConfig))
        delete tempG;
    if (tempH != NULL && (tempH != hardwareConfig))
        delete tempH;
    if (tempA != NULL && (tempA != headerConfig))
        delete tempA;
    if (tempS != NULL && (tempS != streamConfig))
        delete tempS;
    if (tempSC != NULL && (tempSC != spikeConfig))
        delete tempSC;
    if (tempM != NULL && (tempM != moduleConfig))
        delete tempM;

    //reassign global configurations to match GUI configs
    globalConf = globalConfig;
    hardwareConf = hardwareConfig;
    headerConf = headerConfig;
    streamConf = streamConfig;
    spikeConf = spikeConfig;
    moduleConf = moduleConfig;
    benchConfig = benchmarkConfig;*/

}

// ********************** Setting Configuration Value Binding Functions *********************
// ******************************************************************************************

void WorkspaceEditor::setTabOneToGeneralView() {
    replaceTab(0,generalInfoView,"General Settings");
}

void WorkspaceEditor::insertAvailableTag(QString tag, int value) {
    availableTags.insert(tag, value);
    emit updatedAvailTags(availableTags);
}

void WorkspaceEditor::clearAvailableTags() {
    availableTags.clear();
    emit updatedAvailTags(availableTags);
}

/*void WorkspaceEditor::addNTrodesToList(void) {
    //This function is activated in button press, reads how many nTrodes to create, and creates them based on current settings
    qDebug() << "Adding NTrode Group" << nTrodeTree->getNTrodeList().length();

    if (spikeConfig == NULL) {
        spikeConfig = new SpikeConfiguration(this, nTrodeTree->getNTrodeList().length());
    }

    bool valid = false;
    int numNTrodes = 1;
    valid = true;
    //int numNTrodes = 0;
    //numNTrodes = editNumToAdd->text().toInt(&valid);
    if (!valid || numNTrodes <= 0) {
        qDebug() << "Error: Invalid number input. (WorkspaceEditor::AddNTrodesToList)";
        editNumToAdd->setText("1");
        return;
    }
    SingleSpikeTrodeConf curSettings;
//    curSettings.filterOn = enableSpikeFilter->isChecked();
    curSettings.filterOn = dgSpikeFilterGroup->isChecked();
    curSettings.lowFilter = lowFilterBox->currentText().toInt();
    curSettings.highFilter = highFilterBox->currentText().toInt();

    curSettings.refOn = enableDigitalRef->isChecked();
    curSettings.refNTrode = nTrodeBox->currentIndex();
    curSettings.refNTrodeID = nTrodeBox->currentText().toInt();
    curSettings.refChan = channelBox->currentIndex();
    curSettings.refChanID = channelBox->currentText().toInt();

    curSettings.moduleDataChan = LFPChannelsBox->currentText().toInt();
//    curSettings.moduleDataChan = LFPChannelsBox->currentIndex();
    curSettings.moduleDataHighFilter = LFPHighFilterBox->currentText().toInt();
    curSettings.moduleDataOn = 0; //default to false;

    curSettings.channelSettings.thresh = threshSpinBox->value();
    curSettings.channelSettings.maxDisp = maxDispSpinBox->value();
//    curSettings.channelSettings.triggerOn = enableTrigger->isChecked();
    curSettings.channelSettings.triggerOn = true; //TODO: ask mattias about this bool
    curSettings.channelSettings.streamingChannelLookup = DEFAULT_HWCHAN_STREAMINGCHANLOOKUP;

    QList<GroupingTag> tagsToAdd = tagGroupPanel->getGTagList();
    for (int i = 0; i < tagsToAdd.length(); i++) {
        GroupingTag curTag = tagsToAdd.at(i);
        if (!curSettings.gTags.contains(curTag))
            curSettings.gTags.insert(curTag,1);

        if (!availableTags.contains(curTag.tag))
            insertAvailableTag(curTag.tag,1);
    }
//    for (int i = 1; i < (tagSelectionBox->count()-1); i++) { //exclude first and last indexes, dummy values
//        if (!curSettings.tags.contains(tagSelectionBox->itemText(i)))
//            curSettings.tags.insert(tagSelectionBox->itemText(i),1);

//        if (!availableTags.contains(tagSelectionBox->itemText(i)))
//            insertAvailableTag(tagSelectionBox->itemText(i),1);
//    }
   //updateFilterSelector();
    //to do: make an object to store something...

    for (int i = 0; i < numNTrodes; i++) {
        //TO DO: Add random color generator code here
        curSettings.color = colorGenerator.getSequenceColor();
        lastNTrode = nTrodeTree->addNewNTrode(QString("nTrode"), curSettings);
    }

}*/

void WorkspaceEditor::initializeDefaults() {

    //*** Global Defaults ***
    realTimeMode->setChecked(false);
    saveDisplayedChanOnly->setChecked(true);

    //*** Hardware Defaults ***
    editSamplingRate->setText(QString("%1").arg(DEFAULT_SAMPLING_RATE));
    //set device default to include an MCU_IO if it exists
    for (int i = 0; i < deviceSelector->count(); i++) {
        if (deviceSelector->itemText(i) == "MCU_IO") {
            deviceSelector->setCurrentIndex(i);
            addDevice();
            i = deviceSelector->count();
        }
    }

    //*** Stream Defaults ***
    streamColumnComboBox->setCurrentIndex(1); //default to '2'
    streamPagesComboBox->setCurrentIndex(0); //default to '1'
    streamColorButton->setStyleSheet("background-color:black");
    streamConfig->backgroundColor = QColor("black");

    //*** Aux Defaults ***
    //see DEFAULT_AUX defines in workspaceEditor.h

    //*** nTrode(Spike) Defaults ***
    //Default nTrode Settings
//    enableSpikeFilter->setChecked(true);
    //dgSpikeFilterGroup->setChecked(true);
    //lowFilterBox->setCurrentText("600");
    //highFilterBox->setCurrentText("6000");

//    enableTrigger->setChecked(true);
    //threshSpinBox->setValue();
    //maxDispSpinBox->setValue();

    //LFPHighFilterBox->setCurrentText("100");


}

// *********************************** Global Config Slots **********************************
// ******************************************************************************************

void WorkspaceEditor::setGlobalConfigValues() {
    if (globalConfig == NULL)
        return;
    globalConfig->filePath = editFilePath->text();
    globalConfig->filePrefix = editFilePrefix->text();
    globalConfig->saveDisplayedChanOnly = saveDisplayedChanOnly->isChecked();
    globalConfig->realTimeMode = realTimeMode->isChecked();
    globalConfig->MBPerFileChunk = -1; //default value
}

void WorkspaceEditor::loadGlobalConfigIntoGUI() {
    editFilePath->setText(globalConfig->filePath);
    editFilePrefix->setText(globalConfig->filePrefix);
    saveDisplayedChanOnly->setChecked(globalConfig->saveDisplayedChanOnly);
    realTimeMode->setChecked(globalConfig->realTimeMode);

    //MBPerFileChunk value here...
}

void WorkspaceEditor::clickedEditFilePath(QString lineEditText) {
    //clicked the edit file path box, so open a file dialog to choose the folder
    QString dirName = "";

    if (lineEditText != "") { //if a previous save location was specified, open that location
        dirName = QFileDialog::getExistingDirectory(this, QString("Choose default save location"), lineEditText);
    }
    else { //otherwise, open the dialog on the executable's location
        dirName = QFileDialog::getExistingDirectory(this, QString("Choose default save location"), QCoreApplication::applicationDirPath());
    }

    if (dirName != "") { //set the file name only if a directory was specified
        editFilePath->setText(dirName);
        configChanged();
    }
}

// ********************************** Hardware Config Slots *********************************
// ******************************************************************************************


void WorkspaceEditor::setHardwareConfigValues() {
    if (hardwareConfig == NULL)
        return;

    QStringList channelL = channelSelector->currentText().split(" ");
    hardwareConfig->NCHAN = channelL.at(0).toInt();
//    qDebug() << "Channels: " << channelL.at(0).toInt();
    hardwareConfig->sourceSamplingRate = editSamplingRate->text().toInt();

    //add devices to hardwareConfig
    hardwareConfig->devices.clear();
    for (int i = 0; i < deviceInfoConfigs.length(); i++) {
        hardwareConfig->devices.append(deviceInfoConfigs.at(i));
    }

}

void WorkspaceEditor::loadHardwareConfigIntoGUI() {
    for(int i = 0; i < channelSelector->count(); i++) {
        QStringList channelL = channelSelector->itemText(i).split(" ");
        if (hardwareConfig->NCHAN == channelL.at(0).toInt()) {
            channelSelector->setCurrentIndex(i);
            setHardwareChannels(hardwareConfig->NCHAN);
            break;
        }
    }



    editSamplingRate->setText(QString("%1").arg(hardwareConfig->sourceSamplingRate));

    //Load Devices
    deviceList->clear();
    deviceInfoConfigs.clear();
    for (int i = 0; i < hardwareConfig->devices.length(); i++) {
//        for (int j = 1; j < deviceSelector->count(); j++) {
        for(int j = 0; j < availableDevices.length(); ++j){
//            if (hardwareConfig->devices.at(i).wsDisplayName == deviceSelector->itemText(j)) {
            if(hardwareConfig->devices.at(i).name == availableDevices[j].name){
                deviceSelector->setCurrentIndex(j+1);
                addDevice();
                break;
            }

        }
    }
}

void WorkspaceEditor::channelNumberSelected(QString text) {
//    qDebug() << text << " selected...";
//    qDebug() << channelSelector->currentIndex();
    QStringList textElements = text.split(" ");
    if (textElements.length() > 0) {
        QMessageBox *askUser = new QMessageBox(this);
        QString text = "Existing nTrode-channel assignments will be reset.";
        QString infoText = "Proceed?";
        QString overwriteButtonText = "Yes";
        if (textElements.at(0) == "0") {
            text = "Warning: 0 channels option selected.";
            infoText = "Selecting 0 channels will overwrite any current nTrode-Channel assignments, and Trodes will be opened with no stream display.  Do you want to continue?";
            overwriteButtonText = "Yes";
        }
        askUser->setText(text);
        askUser->setInformativeText(infoText);
        /*QPushButton *cancelButton = */askUser->addButton("Cancel", QMessageBox::RejectRole);
        QPushButton *preserveButton;
        /*if (textElements.at(0) != "0")
            preserveButton = askUser->addButton("Preserve", QMessageBox::YesRole);*/
        QPushButton *overwriteButton = askUser->addButton(overwriteButtonText, QMessageBox::AcceptRole);
        askUser->exec();
        QAbstractButton *choice = askUser->clickedButton();

        ButtonOption usrChoice = BO_Cancel;
        if (choice == preserveButton) {
            usrChoice = BO_Preserve;
        }
        else if (choice == overwriteButton) {
            usrChoice = BO_Overwrite;
        }
        delete askUser;
        setHardwareChannels(textElements.at(0).toInt());
    }
    else
        qDebug() << "Error: Invalid text entry passed to function. (WorkspaceEditor::channelNumberSelector)";

}

void WorkspaceEditor::setHardwareChannels(int numChannels) {
    /*if (action == BO_Cancel) {
        //canceled, reset channelSelector to the previous channel count
//        qDebug() << "Find row w/ " << nTrodeTree->getTotalHwChannels() << " channels";
        for (int i = 0; i < this->channelSelector->count(); i++) {
            int rowChan = this->channelSelector->itemText(i).split(" ").first().toInt();
            if (rowChan == this->nTrodeTree->getTotalHwChannels()) {
                this->channelSelector->setCurrentIndex(i);
                break;
            }
        }
        return;
    }*/

    //action is an integer value that determines whether the nTrode tree is overwritten or not and corresponds to QMessageBox::Accept (overwrite nTrode assignments) and QMessageBox::Yes (preserve ntrode assignments)
    /*bool preserveNtrodeAssignments = false; //by default we overwrite nTrode Assignments

    if (action == BO_Preserve) {
        preserveNtrodeAssignments = true;
    }*/

    availHardChanList->clear();
    hardwareChanConfigs.clear();

    /*int channelsToAdd[numChannels];
    QList<int> excludedChannels;

    for (int i = 0; i < numChannels; i++) {
        channelsToAdd[i] = i;
    }

    //get preserved channels
    for (int i = 0; i < nTrodeTree->topLevelItemCount(); i++) {
        QTreeWidgetItem *curTopItem = nTrodeTree->topLevelItem(i);
        for (int j = 0; j < curTopItem->childCount(); j++) {
            QStringList objStrs = curTopItem->child(j)->text(0).split(" ");
            if (objStrs.length() > 0) {
                //qDebug() << curTopItem->child(j)->text(0);
                int chNum = objStrs.at(objStrs.length()-1).toInt();
                excludedChannels.append(objStrs.at(objStrs.length()-1).toInt());
                if (chNum > 0 && chNum <= numChannels && preserveNtrodeAssignments) {
                    channelsToAdd[chNum-1] = -1;
                } //can also put else statement to remove overindexed channels added to nTrode list
                else if (chNum > numChannels || !preserveNtrodeAssignments) { //this means the channel in the ntrode list is no longer supported, remove it
                    nTrodeTree->removeChildFromNTrode(i,j);
                    delete curTopItem->child(j);
                    j--;
                }
            }
            else {
                qDebug() << "Error: Bad array access. (WorkspaceEditor::setHardwareChannels)";
            }
        }
    }

    //now add channels to the hardware channel list if they weren't already assigned previously
    for (int i = 0; i < numChannels; i++) {
        if (channelsToAdd[i] != -1) {
            availHardChanList->addHardwareChannel(QString("Hardware Channel %1").arg(channelsToAdd[i]));
        }

        HardwareChannelSettings curChannel;
        curChannel.ID = i;
        curChannel.maxDisp = DEFAULT_HWCHAN_MAXDISP;
        curChannel.streamingChannelLookup = DEFAULT_HWCHAN_STREAMINGCHANLOOKUP;
        curChannel.thresh = DEFAULT_HWCHAN_THRESH;
        curChannel.triggerOn = DEFAULT_HWCHAN_TRIGGERON;
        hardwareChanConfigs.append(curChannel);

    }
//    channelSettingsGroup->setVisible(false);
    if (guiHeight > 0) {
//        qDebug() << "setting size: " << guiWidth << " - " << guiHeight;
        this->setMaximumWidth(guiWidth);
        this->setMaximumWidth(10000);
//        this->resize(guiWidth,guiHeight);
        //to do: figure out why this code isn't working
    }*/

    channelTable->setNumHardwareChannels(numChannels);

    configChanged();
    emit(hardwareChannelsInitialized(numChannels));
    //channelMapChangedInTable();
}

void WorkspaceEditor::clickedEditSamplingRate(QString samplingRate) {
    QMessageBox::warning(this,tr("Warning"), tr("SpikeGadgets hardware is currently specifically coded to certain frequencies.  It is advised that you do not change this value unless you know what your hardware's specific frequency configuration is."));
}

void WorkspaceEditor::addDevice() {
    int curDevInd = deviceSelector->currentIndex();
    if (curDevInd == 0) {
        return; //don't do anything for the top item, which is info text
    }

    DeviceInfo deviceToAdd = availableDevices.at(curDevInd-1);

    //Add device to list
    deviceList->addItem(deviceToAdd.wsDisplayName);

    //add device to deviceInfoConfigs;
    //calculate byte offset
    int currentOffset = 1;
    for (int i = 0; i < deviceInfoConfigs.length(); i++) {
        currentOffset += deviceInfoConfigs.at(i).numBytes;

    }
    deviceToAdd.byteOffset = currentOffset;
    //now each channels offset
    for (int i = 0; i < deviceToAdd.channels.length(); i++) {
        DeviceChannel curChannel = deviceToAdd.channels.at(i);
        curChannel.startByte += currentOffset;
        if (curChannel.interleavedDataIDByte > -1)
            curChannel.interleavedDataIDByte += currentOffset;

        deviceToAdd.channels.replace(i, curChannel);
    }
    deviceInfoConfigs.append(deviceToAdd);
    addDeviceToAuxChannelList(deviceToAdd);


    deviceSelector->setCurrentIndex(0);
    //deactivate selection option
    deviceSelector->setItemData(curDevInd, QVariant(0), Qt::UserRole-1);
    configChanged();
}

void WorkspaceEditor::removeDevice() {
    int removedIndex = deviceList->currentRow();
    if (deviceList->count() == 0 || removedIndex < 0)
        return;

//    qDebug() << removedIndex;
    //remove device from list
    QString removedDevName = deviceList->currentItem()->text();
    removeDeviceFromAuxChannelList(removedDevName);
    delete deviceList->currentItem(); //removes item
    //find removed item in deviceSelector
    for (int i = 1; i < deviceSelector->count(); i++) {
        if (removedDevName == deviceSelector->itemText(i)) {
            //activate selector item
            deviceSelector->setItemData(i, QVariant(1 | 32), Qt::UserRole-1);
        }
    }
    deviceList->setCurrentRow(-1);

    //remove device from aux header channels
    auxRemoveDevice(deviceInfoConfigs.at(removedIndex).name);

    //remove device from deviceInfoConfigs
    int removedByteOffset = deviceInfoConfigs.at(removedIndex).numBytes;
    deviceInfoConfigs.removeAt(removedIndex);
    for (int i = 0; i < deviceInfoConfigs.length(); i++) {
        DeviceInfo curDev = deviceInfoConfigs.at(i);
        curDev.byteOffset -= removedByteOffset;

        for (int j = 0; j < curDev.channels.length(); j++) {
            DeviceChannel curChannel = curDev.channels.at(j);
            curChannel.startByte -= removedByteOffset;
            if (curChannel.interleavedDataIDByte > -1)
                curChannel.interleavedDataIDByte -= removedByteOffset;

            curDev.channels.replace(j, curChannel);
        }
        deviceInfoConfigs.replace(i, curDev);
    }
    configChanged();
    //printDeviceInfoConfigs();

}

// *********************************** Stream Config Slots **********************************
// ******************************************************************************************

void WorkspaceEditor::setStreamConfigValues() {
    QString bkgColorCode = QString("#%1").arg(streamColorButton->styleSheet().split("#").last());
    streamConfig->backgroundColor = bkgColorCode;

    setStreamDisplayColumns();
}

void WorkspaceEditor::getBackgroundColorDialog() {
    QString colorCode = QString("#%1").arg(streamColorButton->styleSheet().split("#").last());
    QColor newColor = QColorDialog::getColor(colorCode, this, "Select Background Color");
    if (newColor.isValid()) { //when the user presses 'cancel' the returned QColor will be invalid
        streamColorButton->setStyleSheet(QString("background-color:%1").arg(newColor.name()));
        streamConfig->backgroundColor = newColor;
        configChanged();
    }
}

void WorkspaceEditor::loadStreamConfigIntoGUI() {
    streamColumnComboBox->setCurrentIndex((streamConfig->nColumns-1));
    streamPagesComboBox->setCurrentIndex((streamConfig->nTabs-1));
    streamColorButton->setStyleSheet(QString("background-color:%1").arg(streamConfig->backgroundColor.name()));
}

void WorkspaceEditor::setStreamDisplayColumns() {
//    qDebug() << "Columns set to: " << streamColumnComboBox->currentText();
    streamConfig->nColumns = streamColumnComboBox->currentText().toInt();
    configChanged();
}

void WorkspaceEditor::setStreamDisplayPages() {
//    qDebug() << "Pages set to: " << streamPagesComboBox->currentText();
    streamConfig->nTabs = streamPagesComboBox->currentText().toInt();
    configChanged();
}

// ********************************* Auxiliary Config Slots *********************************
// ******************************************************************************************

void WorkspaceEditor::getColorDialog() {
//    QColorDialog::get
    QString colorCode = QString("#%1").arg(buttonColorBox->styleSheet().split("#").last());
    QColor newColor = QColorDialog::getColor(colorCode, this, "Select New Channel Color");
    if (newColor.isValid()) {
        buttonColorBox->setStyleSheet(QString("background-color:%1").arg(newColor.name()));
        auxSaveChannelColor();
//        auxSetApplyButtonsEnabled(true);
    }
}

void WorkspaceEditor::clearAuxGUI() {
    loadAuxInfoChannelIntoPanel("","");
    availableAuxChannelsTree->clear();
    configuredAuxChannelsTree->clear();
}

void WorkspaceEditor::loadHeaderConfigIntoGUI() {
    auxChanRefTable.clear();
    for (int i = 0; i < headerConfig->headerChannels.length(); i++) {
        headerChannel curChan = headerConfig->headerChannels.at(i);
        auxChanRefTable.insert(curChan.idString, curChan);
        addAuxChannelToConfiguredTree(curChan.deviceName, curChan.idString, true);
    }
}

void WorkspaceEditor::selectedAuxChannel(QTreeWidgetItem *selectedItem, QTreeWidgetItem *prevItem) {
//    qDebug() << " selected new channel";
    if (!loadingXML) {
        if (configuredAuxChannelsTree->indexOfTopLevelItem(selectedItem) == -1) { //if you clicked on a child item (aka a aux channel)
//            auxSetApplyButtonsEnabled(false);
            loadAuxInfoChannelIntoPanel(selectedItem->parent()->text(0), selectedItem->text(0).split(" ").first());
        }
        else {
//            auxSetApplyButtonsEnabled(false);
            loadAuxInfoChannelIntoPanel("","");
        }
    }
}

void WorkspaceEditor::clickedAuxChannel(QTreeWidgetItem* clickedItem, int clickedIndex) {
    //NOTE: use of this function has been depreciated by 'selectedAuxChannel'
    if (configuredAuxChannelsTree->indexOfTopLevelItem(clickedItem) == -1) { //if you clicked on a child item (aka a aux channel)
//        auxSetApplyButtonsEnabled(false);
        loadAuxInfoChannelIntoPanel(clickedItem->parent()->text(0), clickedItem->text(0).split(" ").first());
    }
    else {
//        auxSetApplyButtonsEnabled(false);
        loadAuxInfoChannelIntoPanel("","");
    }

}

void WorkspaceEditor::loadAuxInfoChannelIntoPanel(QString deviceName, QString chanID, bool force) {
    //find the channel's settings
//    qDebug() << deviceName << " : " << chanID;
    resetAuxPanelLabelColor();
    QString multiTooltip = "";
    headerChannel firstSelectedChannel;
    if (configuredAuxChannelsTree->selectedItems().length() > 1 && !force) {
        multiTooltip = "Selected: ";
        QString auxChanIDstr = QString("ID: Multiple (hover to see selected)");

        for (int i = 1; i < configuredAuxChannelsTree->selectedItems().length(); i++) {
            if (configuredAuxChannelsTree->selectedItems().at(i)->parent() != NULL) {
                loadAuxInfoChannelIntoPanel(configuredAuxChannelsTree->selectedItems().at(i)->parent()->text(0),configuredAuxChannelsTree->selectedItems().at(i)->text(0).split(" ").first(), true);
                break;
            }
        }
//            loadAuxInfoChannelIntoPanel(configuredAuxChannelsTree->selectedItems().first()->parent()->text(0),configuredAuxChannelsTree->selectedItems().first()->text(0).split(" ").first(), true);
        for (int i = 0; i < configuredAuxChannelsTree->selectedItems().length(); i++) {
            if (configuredAuxChannelsTree->indexOfTopLevelItem(configuredAuxChannelsTree->selectedItems().at(i)) != -1) {
                continue; //if top level item, skip it
            }

            QString curChanID = configuredAuxChannelsTree->selectedItems().at(i)->text(0).split(" ").first();
            multiTooltip = QString("%1%2, ").arg(multiTooltip).arg(configuredAuxChannelsTree->selectedItems().at(i)->text(0).split(" ").first());
//            qDebug() << "checking chan: " << curChanID;
            headerChannel curChan = auxChanRefTable.value(curChanID);
            if (i == 0) {
                firstSelectedChannel = curChan;
            }
            QString mismatchMsg = "";
            if (checkSelectedAuxChannelForConflicts(curChan)) {
                buttonAuxApply->setEnabled(true);
                mismatchMsg = "*Mismatched Channel Settings*";
            }
            labelMismatchAuxSettingsWarning->setText(mismatchMsg);
//            qDebug() << configuredAuxChannelsTree->selectedItems().at(i)->text(0) << " vs " << headerConfig->headerChannels.at(i).idString;
        }
//        qDebug() << "-----------";
        multiTooltip = QString("%1").arg(multiTooltip.left(multiTooltip.length()-2));
        labelAuxChannelID->setText(auxChanIDstr);

//        auxSetApplyButtonsEnabled(true);

    }
    else {
        labelMismatchAuxSettingsWarning->setText("");
        if (deviceName != "") {
            auxChannelGroup->setEnabled(true);
            for (int i = 0; i < headerConfig->headerChannels.length(); i++) {
                if (headerConfig->headerChannels.at(i).idString == chanID) {
                    firstSelectedChannel = auxChanRefTable.value(chanID);
                    auxMaxDispSpinBox->setValue(headerConfig->headerChannels.at(i).maxDisp);
                    buttonColorBox->setStyleSheet(QString("background-color:%1").arg(headerConfig->headerChannels.at(i).color.name()));
                    break;
                }

            }
        }
        else
            auxChannelGroup->setEnabled(false);

        labelAuxChannelID->setText(QString("ID: %1").arg(chanID));
        labelAuxChannelDevice->setText(QString("Device: %1").arg(deviceName));
//        auxSetApplyButtonsEnabled(false);
        buttonAuxApply->setEnabled(false);
    }
    labelAuxChannelID->setToolTip(multiTooltip);
//    qDebug() << "First Selected Channel ID: " << firstSelectedChannel.idString;
    if (firstSelectedChannel.dataType == DeviceChannel::INT16TYPE) { //if the first selected chan is analog
        auxMaxDispSpinBox->setEnabled(true);
    }
    else { //else, it's digital
        auxMaxDispSpinBox->setEnabled(false); //disable the max display box, we don't want the user changing the digital max display value
    }

}

bool WorkspaceEditor::checkAuxChannelsForConflicts() {
    bool conflicts = false;
    QString mismatchMsg = "";
    for (int i = 0; i < configuredAuxChannelsTree->selectedItems().length(); i++) {

        QString curChanID = configuredAuxChannelsTree->selectedItems().at(i)->text(0).split(" ").first();
        headerChannel curChan = auxChanRefTable.value(curChanID);

        if (checkSelectedAuxChannelForConflicts(curChan)) {
            conflicts = true;
            mismatchMsg = "*Mismatched Channel Settings*";
        }

    }
    labelMismatchAuxSettingsWarning->setText(mismatchMsg);
    return(conflicts);
}

bool WorkspaceEditor::checkSelectedAuxChannelForConflicts(headerChannel selectedChan) {
//    qDebug() << "--Checking channel " << selectedChan.idString << " against other selected";
    bool conflicts = false;
    for (int i = 0; i < configuredAuxChannelsTree->selectedItems().length(); i++) {
        if (configuredAuxChannelsTree->indexOfTopLevelItem(configuredAuxChannelsTree->selectedItems().at(i)) != -1) {
            continue; //if there's a top level item in the tree, break;
        }

        QString curChanID = configuredAuxChannelsTree->selectedItems().at(i)->text(0).split(" ").first();
        headerChannel curChan;
        //just makes sure the chanID is in the reference table, if not, fix the table.
        if (auxChanRefTable.contains(curChanID)) {
            curChan = auxChanRefTable.value(curChanID);
        }
        else {
            qDebug() << "Error: aux channel not in reference table. (WorkspaceEditor::checkSelectedAuxChannelForConflicts)";
            return(conflicts);
        }
        //check all channels but not itself, that would be redundant...
        if (curChanID != selectedChan.idString) {
            if (curChan.color.name() != selectedChan.color.name()) {
//                qDebug() << "Conflict (color)";
                setWidgetTextPaletteColor(labelColor, QColor("red"));
                conflicts = true;
            }

            if (curChan.maxDisp != selectedChan.maxDisp) {
//                qDebug() << "Conflict (maxDisp)";
                setWidgetTextPaletteColor(labelAuxMaxDispSpin, QColor("red"));
                conflicts = true;
            }
        }
    }


    return(conflicts);
}

void WorkspaceEditor::resetAuxPanelLabelColor() {
    setWidgetTextPaletteColor(labelColor, QColor("black"));
    setWidgetTextPaletteColor(labelAuxMaxDispSpin, QColor("black"));
    labelMismatchAuxSettingsWarning->setText("");
}

void WorkspaceEditor::addDeviceToAuxChannelList(DeviceInfo deviceToAdd) {
    QTreeWidgetItem *newDevice = new QTreeWidgetItem();
    newDevice->setText(0, QString("%1").arg(deviceToAdd.name));
    for (int i = 0; i < deviceToAdd.channels.length(); i++) {
        QTreeWidgetItem *newChannel = new QTreeWidgetItem();
        newChannel->setText(0,QString("%1").arg(deviceToAdd.channels.at(i).idString));
        newDevice->addChild(newChannel);
    }
    availableAuxChannelsTree->addTopLevelItem(newDevice);
    newDevice->setExpanded(true);
}

void WorkspaceEditor::removeDeviceFromAuxChannelList(QString deviceName) {
    //find the device in the configured tree first
    for (int i = 0; i < configuredAuxChannelsTree->topLevelItemCount(); i++) {
        if (configuredAuxChannelsTree->topLevelItem(i)->text(0) == deviceName) {
            for (int j = 0; j < configuredAuxChannelsTree->topLevelItem(i)->childCount(); j++) {
                removeConfiguredChannel(i, j);
                j--;
            }
            delete configuredAuxChannelsTree->topLevelItem(i);
            break;
        }
    }

    //find the device in the available tree second
    for (int i = 0; i < availableAuxChannelsTree->topLevelItemCount(); i++) {
        if (availableAuxChannelsTree->topLevelItem(i)->text(0) == deviceName) {
            delete availableAuxChannelsTree->topLevelItem(i);
        }
    }
    //to do: add in container deletion here
}

void WorkspaceEditor::addAvailableChannelToList(QString deviceName, QString id) {
    QTreeWidgetItem *newChannel = new QTreeWidgetItem();
    newChannel->setText(0, id);
    for (int i = 0; i < availableAuxChannelsTree->topLevelItemCount(); i++) {
        if (deviceName == availableAuxChannelsTree->topLevelItem(i)->text(0)) {
            availableAuxChannelsTree->topLevelItem(i)->addChild(newChannel);
        }
    }

}

void WorkspaceEditor::removeConfiguredChannel(int parentIndex, int itemIndex) {
    QTreeWidgetItem *channelToRemove = configuredAuxChannelsTree->topLevelItem(parentIndex)->child(itemIndex);
    QString deviceName = channelToRemove->parent()->text(0);
    QStringList channelL = channelToRemove->text(0).split(" ");
    QString channelID = channelL.at(0);
    addAvailableChannelToList(deviceName, channelID);
    delete channelToRemove;

    for (int i = 0; i < headerConfig->headerChannels.length(); i++) {
        if (headerConfig->headerChannels.at(i).idString == channelID) {
//            qDebug() << "chan: " << headerConfig->headerChannels.at(i).idString;
            auxChanRefTable.remove(channelID);
            headerConfig->headerChannels.removeAt(i);
            break;
        }
    }
    if (headerConfig->headerChannels.length() == 0 || configuredAuxChannelsTree->indexOfTopLevelItem(configuredAuxChannelsTree->currentItem()) != -1) {
        loadAuxInfoChannelIntoPanel("","");
    }
    else {
        if (configuredAuxChannelsTree->currentItem() != NULL) {
            loadAuxInfoChannelIntoPanel(configuredAuxChannelsTree->currentItem()->parent()->text(0), configuredAuxChannelsTree->currentItem()->text(0).split(" ").first());
        }
        else
            loadAuxInfoChannelIntoPanel("","");
    }
    configChanged();
    //qDebug() << headerConfig->headerChannels.length();

}

void WorkspaceEditor::addAuxChannelToConfiguredTree(QString deviceName, QString id, bool loadFromXML) {
//    qDebug() << "Adding channel '" << id << "' from device '" << deviceName << "'";
    QTreeWidgetItem *parentDevice = NULL;

    //remove obj from avilable tree
    for (int i = 0; i < availableAuxChannelsTree->topLevelItemCount(); i++) {
        QTreeWidgetItem *curDevice = availableAuxChannelsTree->topLevelItem(i);
        if (curDevice->text(0) == deviceName) {
            for (int j = 0; j < curDevice->childCount(); j++) {
                QTreeWidgetItem *curChan = curDevice->child(j);
                if (curChan->text(0).split(" ").first() == id) {
                    delete curChan;
                    break;
                }
            }
            break;
        }
    }

    //check to see if the added channel's device is already in the configured tree
    for (int i = 0; i < configuredAuxChannelsTree->topLevelItemCount(); i++) {
        if (configuredAuxChannelsTree->topLevelItem(i)->text(0) == deviceName) {
            parentDevice = configuredAuxChannelsTree->topLevelItem(i);
            break;
        }
    }
    //if the device hasn't been added already, add it
    if (parentDevice == NULL) {
        //add the device to the tree
        parentDevice = new QTreeWidgetItem();
        parentDevice->setText(0, QString("%1").arg(deviceName));
        configuredAuxChannelsTree->addTopLevelItem(parentDevice);
    }

    QTreeWidgetItem *channelToAdd = new QTreeWidgetItem();
    channelToAdd->setText(0, QString("%1 -").arg(id));
    parentDevice->addChild(channelToAdd);
    parentDevice->setExpanded(true);

    //to do: maybe put in sorting function
//    headerChannel addedChan;
//    addedChan.idString = id;
//    addedChan.deviceName = deviceName;
    if (!loadFromXML) {
        //first, find the device channel settings in question
        //use this code for loading the settings...
        DeviceInfo deviceObj;
//        deviceObj.name = "";
        for (int i = 0; i < deviceInfoConfigs.length(); i++) {
            if (deviceInfoConfigs.at(i).name == deviceName) {
//                qDebug() << "Found " << deviceName;
                deviceObj = deviceInfoConfigs.at(i);
                break;
            }
        }
        headerChannel addedChan;
        if (deviceObj.name != "") {

            for (int i = 0; i < deviceObj.channels.length(); i++) {
                if (deviceObj.channels.at(i).idString == id) {
//                    qDebug() << "   Found chan " << id;
                    DeviceChannel devChan = deviceObj.channels.at(i);
                    addedChan.idString = id;
                    addedChan.deviceName = deviceObj.name;
                    QColor color;
                    int maxDisplay = 0;
                    if (devChan.dataType == DeviceChannel::INT16TYPE) { //if analog
                        color.setNamedColor(DEFAULT_AUX_ANALOG_COLOR);
                        maxDisplay = DEFAULT_AUX_ANALOG_MAXDISP;
//                        color("#80c342");
                    }
                    else { //else, it's digital
                        color.setNamedColor(DEFAULT_AUX_DIG_COLOR);
                        maxDisplay = DEFAULT_AUX_DIG_MAXDISP;
                    }
                    addedChan.color = color;
                    addedChan.maxDisp = maxDisplay;
                    addedChan.startByte = devChan.startByte;
                    addedChan.digitalBit = devChan.digitalBit;
                    addedChan.dataType = devChan.dataType;
                    headerConfig->headerChannels.append(addedChan);
                    auxChanRefTable.insert(id, addedChan);
    //                addedChan.dataType = devChan->dataType;
    //                addedChan.

                }
            }

        }
    }
    configChanged();
}

void WorkspaceEditor::setConfiguredChannelValues(QString chanID) {
//    qDebug() << "Applying values to " << chanID;
    for (int i = 0; i < headerConfig->headerChannels.length(); i++) {
        headerChannel curChan = headerConfig->headerChannels.at(i);
        if (curChan.idString == chanID) {
            int newMaxDisplayVal = auxMaxDispSpinBox->value();

            if (curChan.dataType == DeviceChannel::INT16TYPE) { //if analog
            }
            else { //else, it's digital
                /*  This code used to prevent changing the max display value above the default, now we prevent it from changing at all
                if (newMaxDisplayVal > DEFAULT_AUX_DIG_MAXDISP) {
                    newMaxDisplayVal = DEFAULT_AUX_DIG_MAXDISP;
                    QMessageBox::critical(0,tr("Error"), qPrintable(QString("Digital auxiliary channels' max display value cannot be set higher than %1.  Auto setting %2's max display value to %3").arg(DEFAULT_AUX_DIG_MAXDISP).arg(curChan.idString).arg(DEFAULT_AUX_DIG_MAXDISP)));
                }*/
                newMaxDisplayVal = DEFAULT_AUX_DIG_MAXDISP;
            }
            curChan.maxDisp = newMaxDisplayVal;
            QString colorCode = QString("#%1").arg(buttonColorBox->styleSheet().split("#").last());
            curChan.color = QColor(colorCode);
            headerConfig->headerChannels.replace(i, curChan);
            auxChanRefTable.remove(chanID);
            auxChanRefTable.insert(chanID, curChan);
            return;
        }
    }
}

void WorkspaceEditor::auxSaveMaxDisplay() {
//    qDebug() << "Save max display";
    resetAuxPanelLabelColor();
    configChanged();

    //apply settings to all selected channels
    for (int i = 0; i < configuredAuxChannelsTree->selectedItems().length(); i++) {
        QTreeWidgetItem *curItem = configuredAuxChannelsTree->selectedItems().at(i);
        if (configuredAuxChannelsTree->indexOfTopLevelItem(curItem) == -1) { //exclude any top level items (devices)
            QString chanID = curItem->text(0).split(" ").first();
            for (int i = 0; i < headerConfig->headerChannels.length(); i++) {
                headerChannel curChan = headerConfig->headerChannels.at(i);
                if (curChan.idString == chanID) {

                    int newMaxDisplayVal = auxMaxDispSpinBox->value();

                    if (curChan.dataType == DeviceChannel::INT16TYPE) { //if analog
                    }
                    else { //else, it's digital
                        /*  This code used to prevent changing the max display value above the default, now we prevent it from changing at all
                        if (newMaxDisplayVal > DEFAULT_AUX_DIG_MAXDISP) {
                            newMaxDisplayVal = DEFAULT_AUX_DIG_MAXDISP;
                            QMessageBox::critical(0,tr("Error"), qPrintable(QString("Digital auxiliary channels' max display value cannot be set higher than %1.  Auto setting %2's max display value to %3").arg(DEFAULT_AUX_DIG_MAXDISP).arg(curChan.idString).arg(DEFAULT_AUX_DIG_MAXDISP)));
                        }*/
                        newMaxDisplayVal = DEFAULT_AUX_DIG_MAXDISP;
                    }
                    curChan.maxDisp = newMaxDisplayVal;
                    headerConfig->headerChannels.replace(i, curChan);
                    auxChanRefTable.remove(chanID);
                    auxChanRefTable.insert(chanID, curChan);
                    break;
                }
            }

        }
    }

    if (configuredAuxChannelsTree->selectedItems().length() > 1)
        checkAuxChannelsForConflicts();
}

void WorkspaceEditor::auxSaveChannelColor() {
//    qDebug() << "Save chan color";
    resetAuxPanelLabelColor();
    configChanged();
    //apply settings to all selected channels
    for (int i = 0; i < configuredAuxChannelsTree->selectedItems().length(); i++) {
        QTreeWidgetItem *curItem = configuredAuxChannelsTree->selectedItems().at(i);
        if (configuredAuxChannelsTree->indexOfTopLevelItem(curItem) == -1) { //exclude any top level items (devices)
            QString chanID = curItem->text(0).split(" ").first();
            //very inefficient, possibly replace w/ hash access
            for (int i = 0; i < headerConfig->headerChannels.length(); i++) {
                headerChannel curChan = headerConfig->headerChannels.at(i);
                if (curChan.idString == chanID) {

                    QString colorCode = QString("#%1").arg(buttonColorBox->styleSheet().split("#").last());
                    curChan.color = QColor(colorCode);
                    headerConfig->headerChannels.replace(i, curChan);
                    auxChanRefTable.remove(chanID);
                    auxChanRefTable.insert(chanID, curChan);
                    break;
                }
            }

        }
    }

    if (configuredAuxChannelsTree->selectedItems().length() > 1)
        checkAuxChannelsForConflicts();
}

void WorkspaceEditor::auxSetApplyButtonsEnabled(bool enabled) {
    buttonAuxApply->setEnabled(enabled);
    buttonAuxApplyToAll->setEnabled(enabled);
}

//FUNCTION DEPRECIATED
void WorkspaceEditor::auxCancelButtonPressed() {

    if (configuredAuxChannelsTree->topLevelItemCount() > 0) {
        if (configuredAuxChannelsTree->currentItem() != NULL && configuredAuxChannelsTree->indexOfTopLevelItem(configuredAuxChannelsTree->currentItem()) == -1) {
            loadAuxInfoChannelIntoPanel(configuredAuxChannelsTree->currentItem()->parent()->text(0), configuredAuxChannelsTree->currentItem()->text(0).split(" ").first());
            return;
        }

    }
    loadAuxInfoChannelIntoPanel("","");
}

void WorkspaceEditor::auxApplyButtonPressed() {
//    qDebug() << "Applying settings to the following channels";

    //apply settings to all selected channels
    for (int i = 0; i < configuredAuxChannelsTree->selectedItems().length(); i++) {
        QTreeWidgetItem *curItem = configuredAuxChannelsTree->selectedItems().at(i);
        if (configuredAuxChannelsTree->indexOfTopLevelItem(curItem) == -1) { //exclude any top level items (devices)
            setConfiguredChannelValues(curItem->text(0).split(" ").first());
//            qDebug() << "    -" << curItem->text(0).split(" ").first();
        }
    }
//    qDebug() << " ";

//    setConfiguredChannelValues(configuredAuxChannelsTree->currentItem()->text(0).split(" ").first());
    resetAuxPanelLabelColor();
//    auxSetApplyButtonsEnabled(false);
    buttonAuxApply->setEnabled(false);
}

void WorkspaceEditor::auxApplyToAllButtonPressed() {
    for (int i = 0; i < configuredAuxChannelsTree->currentItem()->parent()->childCount(); i++) {
        setConfiguredChannelValues(configuredAuxChannelsTree->currentItem()->parent()->child(i)->text(0).split(" ").first());
    }
    resetAuxPanelLabelColor();
//    auxSetApplyButtonsEnabled(false);
    buttonAuxApply->setEnabled(false);
}

void WorkspaceEditor::auxAddChannelButtonPressed() {
//    if (availableAuxChannelsTree)
    for (int i = 0; i < availableAuxChannelsTree->selectedItems().length(); i++) {
        QTreeWidgetItem *curChan = availableAuxChannelsTree->selectedItems().at(i);
        if (curChan != NULL) {
            if (availableAuxChannelsTree->indexOfTopLevelItem(curChan) == -1) {
                addAuxChannelToConfiguredTree(curChan->parent()->text(0), curChan->text(0));
                i--;
                //            delete curChan; //remove the added channel from the available channel list
            }
        }
    }
    availableAuxChannelsTree->setPrevSelectedItem(NULL); //make sure the item sent over is still not being considered in the tree
}

void WorkspaceEditor::auxRemoveChannelButtonPressed() {
//    qDebug() << "Remove channel";
    if (configuredAuxChannelsTree->topLevelItemCount() == 0)
        return;

    QTreeWidgetItem *curChan = configuredAuxChannelsTree->currentItem();
    if (curChan != NULL && (configuredAuxChannelsTree->indexOfTopLevelItem(curChan) != -1)) { //if a device is to be deleted
        int parentIndex = configuredAuxChannelsTree->indexOfTopLevelItem(curChan);
        for (int i = 0; i < curChan->childCount(); i++) {
            int itemIndex = i;
            removeConfiguredChannel(parentIndex, itemIndex);
            i--;
        }
        delete curChan; //delete the device
    }

    for (int i = 0; i < configuredAuxChannelsTree->selectedItems().length(); i++) { //delete all selected channels
        QTreeWidgetItem *curItem = configuredAuxChannelsTree->selectedItems().at(i);
        if (configuredAuxChannelsTree->indexOfTopLevelItem(curItem) == -1) {// only run deletion protocol for children
            int parentIndex = configuredAuxChannelsTree->indexOfTopLevelItem(curItem->parent());
            int childIndex = curItem->parent()->indexOfChild(curItem);
            removeConfiguredChannel(parentIndex, childIndex);
            i--;
        }
    }

    for (int i = 0; i < configuredAuxChannelsTree->topLevelItemCount(); i++) { //delete any empty devices
        if (configuredAuxChannelsTree->topLevelItem(i)->childCount() <= 0) {
            delete configuredAuxChannelsTree->topLevelItem(i);
            i--;
        }
    }
    configuredAuxChannelsTree->setPrevSelectedItem(NULL); //make sure the item sent over is still not being considered in the tree
}

void WorkspaceEditor::auxRemoveDevice(QString devicename)
{
    auto items = configuredAuxChannelsTree->findItems(devicename, Qt::MatchExactly);
    for(auto item : items){
        int parentIndex = configuredAuxChannelsTree->indexOfTopLevelItem(item);
        for(int i = 0; i < item->childCount(); ++i){
            int itemIndex = i;
            removeConfiguredChannel(parentIndex, itemIndex);
            i--;
        }
    }
    for (int i = 0; i < configuredAuxChannelsTree->topLevelItemCount(); i++) { //delete any empty devices
        if (configuredAuxChannelsTree->topLevelItem(i)->childCount() <= 0) {
            delete configuredAuxChannelsTree->topLevelItem(i);
            i--;
        }
    }
    configuredAuxChannelsTree->setPrevSelectedItem(NULL); //make sure the item sent over is still not being considered in the tree
}


// *********************************** Spike Config Slots ***********************************
// ******************************************************************************************

void WorkspaceEditor::setNTrodeConfigValues() {
    if (spikeConfig == NULL) {
        spikeConfig = new SpikeConfiguration(this, nTrodeConfigs.length());
    }
    //nTrodeConfigs.clear();
    spikeConfig->ntrodes.clear();

    for (int i=0; i<nTrodeConfigs.length(); i++) {


        spikeConfig->ntrodes.append(nTrodeConfigs.at(i).nTrodeId,&nTrodeConfigs[i]);
        spikeConfig->ntrodes[i]->maxDisp.clear();
        spikeConfig->ntrodes[i]->thresh.clear();
        /*if(i % 8 == 0){
            spikeConfig->ntrodes[i]->color =  QColor("#fafafa");
        }
        if(i % 8 == 1){
            spikeConfig->ntrodes[i]->color = QColor("#fa3232");
        }
        if(i % 8 == 2){
            spikeConfig->ntrodes[i]->color = QColor("#14fa14");
        }
        if(i % 8 == 3){
            spikeConfig->ntrodes[i]->color = QColor("#1414fa");
        }
        if(i % 8 == 4){
            spikeConfig->ntrodes[i]->color = QColor("#ff00ff");
        }
        if(i % 8 == 5){
            spikeConfig->ntrodes[i]->color = QColor("#e9b96e");
        }
        if(i % 8 == 6){
            spikeConfig->ntrodes[i]->color = QColor("#729fcf");
        }
        if(i % 8 == 7){
            spikeConfig->ntrodes[i]->color = QColor("#2eff94");
        }*/

        for (int j=0;j<spikeConfig->ntrodes[i]->hw_chan.length();j++) {
            spikeConfig->ntrodes[i]->maxDisp.push_back(spikeConfig->ntrodes[i]->channelSettings.maxDisp);
            spikeConfig->ntrodes[i]->thresh.push_back(spikeConfig->ntrodes[i]->channelSettings.thresh);
        }



    }




    /*QList<NTrodeSettings> curNTrodeSettings = nTrodeTree->getNTrodeList();

    for (int i = 0; i < curNTrodeSettings.length(); i++) {
        SingleSpikeTrodeConf *curNtrodeConfig = (SingleSpikeTrodeConf*)&curNTrodeSettings.at(i).settings;

        //replace GUI ref defaults with expected defaults in configuration.cpp
        if (curNtrodeConfig->refNTrode == -1)
            curNtrodeConfig->refNTrode = 0;
        if (curNtrodeConfig->refChan == -1)
            curNtrodeConfig->refChan = 0;


        //for all children of curNTrodeConfig.settings, assign hardware channel values;
        for (int j = 0; j < curNtrodeConfig->unconverted_hw_chan.length(); j++) {

            curNtrodeConfig->thresh.replace(j,curNtrodeConfig->channelSettings.thresh);
            curNtrodeConfig->maxDisp.replace(j,curNtrodeConfig->channelSettings.maxDisp);
            curNtrodeConfig->streamingChannelLookup.replace(j,curNtrodeConfig->channelSettings.streamingChannelLookup);
            curNtrodeConfig->triggerOn.replace(j,curNtrodeConfig->channelSettings.triggerOn);

//            curNtrodeConfig->thresh.replace(j,hardwareChanConfigs.at(curNtrodeConfig->unconverted_hw_chan.at(j)).thresh);
//            curNtrodeConfig->maxDisp.replace(j,hardwareChanConfigs.at(curNtrodeConfig->unconverted_hw_chan.at(j)).maxDisp);
//            curNtrodeConfig->streamingChannelLookup.replace(j,hardwareChanConfigs.at(curNtrodeConfig->unconverted_hw_chan.at(j)).streamingChannelLookup);
//            curNtrodeConfig->triggerOn.replace(j,hardwareChanConfigs.at(curNtrodeConfig->unconverted_hw_chan.at(j)).triggerOn);
        }

        nTrodeConfigs.append(*curNtrodeConfig);
        spikeConfig->ntrodes.append(curNtrodeConfig->nTrodeId, curNtrodeConfig);
    }*/

    convertNTrodeIDsToRef();
}

void WorkspaceEditor::loadNTrodeConfigIntoGUI() {
    //qDebug() << nTrodeConfigs.length();
    //hardwareChanConfigs.clear();
//    nTrodeTree->setLoading(true);
    //nTrodeTree->allClear();
    //nTrodeBox->clear();

    nTrodeTree->updateMapping(&nTrodeConfigs);

    /*clearAvailableTags();
    activeFilters.clear();
    convertNTrodeRefToIDs();
    for (int i = 0; i < nTrodeConfigs.length(); i++) {
        //note that all children are automatically already in each added nTrode, so call separate fun to load it into the tree
        QTreeWidgetItem *addedItem = nTrodeTree->addNewNTrode("nTrode", nTrodeConfigs.at(i), true);
        int index = nTrodeTree->indexOfTopLevelItem(addedItem);
        SingleSpikeTrodeConf temp = nTrodeConfigs.at(i);
        temp.channelSettings.thresh = temp.thresh.at(0);
        temp.channelSettings.maxDisp = temp.maxDisp.at(0);
        temp.channelSettings.streamingChannelLookup = temp.streamingChannelLookup.at(0);
        temp.channelSettings.triggerOn = temp.triggerOn.at(0);

        QHashIterator<QString, int> iter(temp.tags);
        while(iter.hasNext()) {
            iter.next();
            if (!availableTags.contains(iter.key()))
                insertAvailableTag(iter.key(),1);
        }

        nTrodeConfigs.replace(i,temp);
        for (int j = 0; j < nTrodeConfigs.at(i).unconverted_hw_chan.length(); j++) {
            int hwChanId = nTrodeConfigs.at(i).unconverted_hw_chan.at(j);
            QString hwChan = QString("HW %1").arg(hwChanId);
            nTrodeTree->loadChildIntoTree(index,hwChan);
            HardwareChannelSettings curChan = hardwareChanConfigs.at(hwChanId);
            curChan.thresh = nTrodeConfigs.at(i).channelSettings.thresh;
            curChan.maxDisp = nTrodeConfigs.at(i).channelSettings.maxDisp;
            curChan.streamingChannelLookup = nTrodeConfigs.at(i).channelSettings.streamingChannelLookup;
            curChan.triggerOn = nTrodeConfigs.at(i).channelSettings.triggerOn;
            hardwareChanConfigs.replace(hwChanId, curChan);

            emit (nTrodeTree->sig_removeChildFromList(nTrodeConfigs.at(i).unconverted_hw_chan.at(j)));
        }
    }
    tagGroupPanel->updateDict(spikeConfig->groupingDict);
   //updateFilterSelector();

    delete nTrodeDelegate;
    nTrodeDelegate = new NTrodeTreeWidgetDelegate(this, nTrodeTree);
    nTrodeTree->setItemDelegate(nTrodeDelegate);
//    nTrodeTree->setLoading(false);
//    nTrodeTree->printNTrodeList();
*/
    channelMapChangedInList();
}

void WorkspaceEditor::convertNTrodeRefToIDs() {
    for (int i = 0; i < nTrodeConfigs.length(); i++) {
        SingleSpikeTrodeConf curNTrode = nTrodeConfigs.at(i);
        if (curNTrode.refNTrodeID == -1) {
            int relativeRefNTrode = curNTrode.refNTrode;
            SingleSpikeTrodeConf refNTrode = nTrodeConfigs.at(relativeRefNTrode);
            curNTrode.refNTrodeID = refNTrode.nTrodeId;

            nTrodeConfigs.replace(i,curNTrode);
        }
    }
}

//NOTE: this function must always come directly after all nTrodes are appended to spikeConfig for the nTrodeIndex to be guarenteed to be correct
void WorkspaceEditor::convertNTrodeIDsToRef() {
    //for all nTrodes convert the refNTrodeID to refNTrode
    for (int i = 0; i < spikeConfig->ntrodes.length(); i++) {
        SingleSpikeTrodeConf *curNTrode = spikeConfig->ntrodes.at(i);
        SingleSpikeTrodeConf *refNTrode = spikeConfig->ntrodes.ID(curNTrode->refNTrodeID);
        if (refNTrode != NULL)
            curNTrode->refNTrode = refNTrode->nTrodeIndex; //don't add one here b/c SpikeConfiguration::saveToXML does that automatically
    }
}

/*void WorkspaceEditor::addedNTrode(int ID, bool loadedFromXML) {
//    qDebug() << "added " << ID;
    configChanged();
    for (int i = 0; i < nTrodeBox->count(); i++) {
        if (ID < nTrodeBox->itemText(i).toInt()) {
            nTrodeBox->insertItem(i, QString("%1").arg(ID));
            return;
        }
    }
    nTrodeBox->addItem(QString("%1").arg(ID));
    if (!loadedFromXML)
        setNTrodeConfigValues();
}*/

/*void WorkspaceEditor::removedNTrode(int index) {
    nTrodeBox->removeItem(index);
}*/

void WorkspaceEditor::autopopulateButtonPressed(){
//    availHardChanList
    int chans = availHardChanList->findItems("Hardware Channel", Qt::MatchStartsWith).length();
//    while((chan=availHardChanList->takeTopLevelItem(0))){
    for(int i = 0; i < chans; i++){
        nTrodeAddbuttonPressed();
//        addChannelsToNTrode(aquireSelectedChannels(), lastNTrode);
    }
}

void WorkspaceEditor::addListItemsButtonPressed() {
    //Add selected channels to the destination nTrode
    //addChannelsToNTrode(aquireSelectedChannels(),aquireDestinationNTrode());
}

QStringList WorkspaceEditor::aquireSelectedChannels() {
    QList<QTreeWidgetItem*> selectedChannels = availHardChanList->getSelectedChannels();
    if (selectedChannels.length() < 1) {
        QTreeWidgetItem* availChan = availHardChanList->getFirstAvailChan();
        if (availChan != NULL)
            selectedChannels.append(availHardChanList->getFirstAvailChan());
    }
    QStringList channels;
    for (int i = 0; i < selectedChannels.length(); i++) {
        channels.append(selectedChannels.at(i)->text(0));
    }
    qDebug() << channels;
    return(channels);
}

QTreeWidgetItem* WorkspaceEditor::aquireDestinationNTrode() {
    QTreeWidgetItem* destNTrode = nTrodeTree->getFirstSelectedNTrode();
    if (destNTrode == NULL) { //No nTrodes exist, make a new one
        nTrodeAddbuttonPressed();
        destNTrode = nTrodeTree->getFirstSelectedNTrode(); //get the created nTrode
    }
    return(destNTrode);
}

/*void WorkspaceEditor::addChannelsToNTrode(QStringList channels, QTreeWidgetItem *destNTrode) {
    if (channels.length() == 0 || destNTrode == NULL) //no available channels or nTrode exist, exit function
        return;
    for (int i = 0; i < channels.length(); i++) {
        if (nTrodeTree->addChildToNTrode(nTrodeTree->indexOfTopLevelItem(destNTrode),destNTrode->childCount(), channels.at(i)) != NULL)
            availHardChanList->removeHardwareChannel(channels.at(i).split(" ").last().toInt());
    }
    channelMapChangedInList();
}*/

void WorkspaceEditor::setFocusedWiget(int focusTarget) {
    switch (focusTarget) {
    case FT_nTrodeTree: { //set nTrode Tree focused
        nTrodeTree->setFocus();
        break;
    }
    case FT_ChannelTree: { //set channel tree focused
        availHardChanList->setFocus();
        break;
    }
    case FT_ConfiguredAux: { //set configured aux channel tree focused
        configuredAuxChannelsTree->setFocus();
        break;
    }
    case FT_AvailAux: { //set available aux channel tree focused
        availableAuxChannelsTree->setFocus();
        break;
    }
    default: {
        qDebug() << "Error: Invalid focus target. (WorkspaceEditor::setFocusedWidget)";
        break;
    }
    }
}

/*void WorkspaceEditor::resetNTrodePanelLabelColor() {
    //this function resets all text labels to black in the NTrode settings panel.  TO be used in conjunction with multi highlighting to reset red indicator text
    labelMismatchSettingsWarning->setText("");

    //spike filter labels
//    setWidgetTextPaletteColor(enableSpikeFilter, QColor("black"));
    setWidgetTextPaletteColor(dgSpikeFilterGroup, QColor("black"));
    setWidgetTextPaletteColor(labelLowFilter, QColor("black"));
    setWidgetTextPaletteColor(labelHighFilterFilter, QColor("black"));
    //referencing labels
    setWidgetTextPaletteColor(enableDigitalRef, QColor("black"));
    setWidgetTextPaletteColor(enableLFPRef, QColor("black"));
    setWidgetTextPaletteColor(labelNTrode, QColor("black"));
    setWidgetTextPaletteColor(labelChannel, QColor("black"));
    //LFP labels
    setWidgetTextPaletteColor(dgLFPsettingsGroup, QColor("black"));
    setWidgetTextPaletteColor(labelLFPChannelsFilter, QColor("black"));
    setWidgetTextPaletteColor(labelLFPHighFilter, QColor("black"));
    //spike trigger labels
//    setWidgetTextPaletteColor(enableTrigger, QColor("black"));
    setWidgetTextPaletteColor(labelThreshSpin, QColor("black"));
    //Display Settings labels
    setWidgetTextPaletteColor(labelMaxDispSpin, QColor("black"));
    setWidgetTextPaletteColor(labelChannel, QColor("black"));
    //tag label
    setWidgetTextPaletteColor(tagGroupPanel, QColor("black"));
}*/

//Loads a specified nTrode at nTrodeIndex into the settings panel.
//The 'force' modifier disregards multiSelection and forces the gui to load the sepcified nTrode.
/*void WorkspaceEditor::loadNTrodeSettingsIntoPanel(int nTrodeIndex, bool force) {
//    qDebug() << "Load Ntrode index " << nTrodeIndex;
//    qDebug() << "spikeConfig = " << spikeConfig;
    resetNTrodePanelLabelColor();
    QHash<QString, int> ntrodeHash = getTopLevelHash(nTrodeTree); //finds how many nTrodes in the tree were selected and stores each selected ntrode in the hash table

    QString multiTooltip = "";
    if ((ntrodeHash.count() > 1) && !force) { //if multiple nTrodes selected
        //find the firstNTrode selected, this nTrode's settings will be the default settings
        QTreeWidgetItem *firstNTrode = nTrodeTree->selectedItems().first();
        if (nTrodeTree->indexOfTopLevelItem(firstNTrode) == -1) {
            firstNTrode = firstNTrode->parent();
        }
//        qDebug() << "First selected nTrode:F " << firstNTrode->text(0);
        //force load the first nTrode's settings into the pannel
        loadNTrodeSettingsIntoPanel(nTrodeTree->indexOfTopLevelItem(firstNTrode), true); //uncomment this to enable force loading of the first selected nTrode

        //turn the apply to all button off, we don't want it enabled while multi selecting
//        nTrodeSetApplyToAllButtonEnabled(false);

        //now iterate through the hash table of selected nTrodes
        QHashIterator<QString, int> i(ntrodeHash);
        QString defaultGroupTitle = QString("nTrode Settings     |    Selected: Multiple nTrodes (hover for details)");
        multiTooltip = "Selected nTrodes: ";
        QString modifiedNtrodesMsg = "";

        QList<int> selectedIDs;
        QList<SingleSpikeTrodeConf> selectedNTrodes;
        while(i.hasNext()) {
            i.next();
            selectedIDs.append(i.key().split(" ").last().toInt());
//            qDebug() << "  - " << i.key() << " at " << i.value();
            SingleSpikeTrodeConf curNTrode = nTrodeTree->getNTrode(i.value()).settings;
            selectedNTrodes.append(curNTrode);
//            multiTooltip = QString("%1%2, ").arg(multiTooltip).arg(i.key().split(" ").last());
            if (checkSelectedNTrodesForConflicts(nTrodeTree->getNTrode(i.value()).settings)) {
//                qDebug() << "Conflict detected between nTrodes";
                modifiedNtrodesMsg = "  *Mismatched nTrode Settings*";
                buttonNTrodeApply->setEnabled(true);
            }
        }
        std::sort(selectedIDs.begin(),selectedIDs.end()); //sort the ID list
        for (int i = 0; i < selectedIDs.length(); i++) { //add the selected ID's to the tooltip in ascending order
            multiTooltip = QString("%1%2, ").arg(multiTooltip).arg(selectedIDs.at(i));
        }

        tagGroupPanel->loadMultipleNTrodeTags(selectedNTrodes);

//        defaultGroupTitle = QString("<span style=" color:#000000;">%1)</span> <span style=" color:#ff0000;"> TESTING RED </span>").arg(defaultGroupTitle.left(defaultGroupTitle.length()-2));
        labelMismatchSettingsWarning->setText(modifiedNtrodesMsg);
        QString toolTipMsg = "Settings across selected nTrodes differ.  Clicking 'Apply' will set the first selected nTrode's settings to all others.";
        labelMismatchSettingsWarning->setToolTip(toolTipMsg);
        multiTooltip = QString("%1").arg(multiTooltip.left(multiTooltip.length()-2));
        //defaultGroupTitle = QString("%1)%2").arg(defaultGroupTitle.left(defaultGroupTitle.length()-2)).arg(modifiedNtrodesMsg);
        defaultGroup->setTitle(defaultGroupTitle);
//        buttonNTrodeApply->setEnabled(true);

    }
    else { //else, only one nTrode was selected...
        labelMismatchSettingsWarning->setText("");
        NTrodeSettings curNTrode = nTrodeTree->getNTrode(nTrodeIndex);
        defaultGroup->setTitle(QString("nTrode Settings     |    Selected: nTrode %1").arg(curNTrode.settings.nTrodeId));

        //TODO: channel color stuff here
//        enableSpikeFilter->setChecked(curNTrode.settings.filterOn);
        dgSpikeFilterGroup->setChecked(curNTrode.settings.filterOn);
        lowFilterBox->setCurrentText(QString("%1").arg(curNTrode.settings.lowFilter));
        highFilterBox->setCurrentText(QString("%1").arg(curNTrode.settings.highFilter));

        enableDigitalRef->setChecked(curNTrode.settings.refOn);
        enableLFPRef->setChecked(curNTrode.settings.lfpRefOn);
        dgLFPsettingsGroup->setChecked(curNTrode.settings.lfpFilterOn);

    //    qDebug() << "Loading refNTrode " << curNTrode.settings.refNTrodeID << " for nTrode at index " << nTrodeIndex;
        if (curNTrode.settings.refNTrodeID != -1) {
            nTrodeBox->setCurrentText(QString("%1").arg(curNTrode.settings.refNTrodeID));

            for (int i = 0; i < nTrodeTree->topLevelItemCount(); i++) {
                if (nTrodeTree->topLevelItem(i)->text(0).split(" ").last().toInt() == curNTrode.settings.refNTrodeID) {
                    setDigChannelBoxNum(i);
                    break;
                }
            }

            if (curNTrode.settings.refChan >= 0) {
                channelBox->setCurrentIndex(curNTrode.settings.refChan);
            }
        }
        else {
            nTrodeBox->setCurrentIndex(0);
            setDigChannelBoxNum(0);
            channelBox->setCurrentIndex(0);
        }

        loadLFPSettingsIntoPanel(nTrodeIndex);

//        enableTrigger->setChecked(curNTrode.settings.channelSettings.triggerOn);
        threshSpinBox->setValue(curNTrode.settings.channelSettings.thresh);

        maxDispSpinBox->setValue(curNTrode.settings.channelSettings.maxDisp);
        buttonChannelColorBox->setStyleSheet(QString("background-color:%1").arg(curNTrode.settings.color.name()));

        //load tags...
        if (!force) {
            tagGroupPanel->loadNTrodeTags(curNTrode.settings.gTags);
        }
        tagGroupPanel->loadCurrentCategoryTags();
//        tagSelectionBox->clear();
//        tagSelectionBox->addItem("");
//        QHashIterator<QString, int> i(curNTrode.settings.tags);
//        while(i.hasNext()) {
//            i.next();
//            tagSelectionBox->addItem(i.key());
//        }
////        for (int i = 0; i < curNTrode.settings.tags.length(); i++) {
////            //add new tags to the current nTrode
////            tagSelectionBox->addItem(curNTrode.settings.tags.at(i));
////        }
//        tagSelectionBox->addItem("+Add/-Remove Tag");
//        setComboBoxFirstItemUnselectable(tagSelectionBox);
//        tagSelectionBox->setCurrentIndex(tagSelectionBox->count()-2);

        buttonNTrodeApply->setEnabled(false);
//        buttonNTrodeApplyToAll->setEnabled(false);
    }
    defaultGroup->setToolTip(multiTooltip);
}*/

/*void WorkspaceEditor::saveNTrodeSettings(int nTrodeIndex, QString *LFPerrorMessage) {

    //    qDebug() << "Saving nTrode: " << nTrodeTree->getNTrode(nTrodeIndex).settings.nTrodeId << " at indi: " << nTrodeIndex;
    SingleSpikeTrodeConf newSettings = nTrodeTree->getNTrode(nTrodeIndex).settings;
//    newSettings.filterOn = enableSpikeFilter->isChecked();
    newSettings.filterOn = dgSpikeFilterGroup->isChecked();
    newSettings.lowFilter = lowFilterBox->currentText().toInt();
    newSettings.highFilter = highFilterBox->currentText().toInt();

    newSettings.refOn = enableDigitalRef->isChecked();
//    newSettings.refNTrode = nTrodeBox->currentIndex();

    int relativeRefChan = 0; //find and calculate the relativeRefChan
    SingleSpikeTrodeConf refNTrode;
    bool setDigRefDefaults = false;

    if (nTrodeBox->currentText().isEmpty() || channelBox->currentText().isEmpty()) {
        setDigRefDefaults = true;
    }

    if (!setDigRefDefaults) {
        for (int i = 0; i < nTrodeTree->topLevelItemCount(); i++) {
            if (nTrodeTree->getNTrode(i).settings.nTrodeId == nTrodeBox->currentText().toInt()) {
                refNTrode = nTrodeTree->getNTrode(i).settings;
                break;
            }
        }
        for (int i = 0; i < refNTrode.unconverted_hw_chan.length(); i++) {
            if (refNTrode.unconverted_hw_chan.at(i) == channelBox->currentText().toInt()) {
                relativeRefChan = i;
                break;
            }
        }

        newSettings.refNTrodeID = nTrodeBox->currentText().toInt();
        newSettings.refChan = relativeRefChan;
        newSettings.refChanID = channelBox->currentText().toInt();
    }
    else {
        newSettings.refNTrodeID = -1;
        newSettings.refChan = -1;
        newSettings.refChanID = -1;
    }

    if (newSettings.unconverted_hw_chan.length() > 0) {
        if (LFPChannelsBox->currentIndex() < newSettings.unconverted_hw_chan.length()) {
                newSettings.moduleDataChan = LFPChannelsBox->currentIndex();
        }
        else {
            newSettings.moduleDataChan = newSettings.unconverted_hw_chan.length()-1;
        }
    }
    else {

        if (LFPerrorMessage != 0)
            *LFPerrorMessage = QString("%1   -nTrode %2\n").arg(*LFPerrorMessage).arg(newSettings.nTrodeId);
        newSettings.moduleDataChan = -1;
    }

    newSettings.moduleDataHighFilter = LFPHighFilterBox->currentText().toInt();
    newSettings.moduleDataOn = 0; //default to false;

    newSettings.channelSettings.thresh = threshSpinBox->value();
    newSettings.channelSettings.maxDisp = maxDispSpinBox->value();
//    newSettings.channelSettings.triggerOn = enableTrigger->isChecked();
    newSettings.channelSettings.triggerOn = true; //TODO: ask mattias about this value
    newSettings.channelSettings.streamingChannelLookup = DEFAULT_HWCHAN_STREAMINGCHANLOOKUP;

    //set tag valuse
    newSettings.tags.clear();
    QList<GroupingTag> tagsToAdd = tagGroupPanel->getGTagList();
    for (int i = 0; i < tagsToAdd.length(); i++) {
        GroupingTag curTag = tagsToAdd.at(i);
        if (!newSettings.gTags.contains(curTag))
            newSettings.gTags.insert(curTag,1);

        if (!availableTags.contains(curTag.tag))
            insertAvailableTag(curTag.tag,1);
    }



    //nTrodeTree->setNTrodeSettings(nTrodeIndex, newSettings);
}*/


/*void WorkspaceEditor::nTrodeSaveSpikeFilter() {
    qDebug() << "Save Spike Filter";
    resetNTrodePanelLabelColor();
    configChanged();
    QHash<QString, int> ntrodeHash = getTopLevelHash(nTrodeTree);
    QHashIterator<QString, int> i(ntrodeHash);
    int numNTrodes = 0;
    while(i.hasNext()) {
        i.next();
        int nTrodeIndex = i.value();
        SingleSpikeTrodeConf newSettings = nTrodeTree->getNTrode(nTrodeIndex).settings;

//        newSettings.filterOn = enableSpikeFilter->isChecked();
        newSettings.filterOn = dgSpikeFilterGroup->isChecked();

        nTrodeTree->setNTrodeSettings(nTrodeIndex, newSettings);
        numNTrodes++;
    }

    if (numNTrodes > 1)
        checkNTrodesForConflicts();
}

void WorkspaceEditor::nTrodeSaveLowFilter() {
//    qDebug() << "Save Low Filter";/
    resetNTrodePanelLabelColor();
    configChanged();
    QHash<QString, int> ntrodeHash = getTopLevelHash(nTrodeTree);
    QHashIterator<QString, int> i(ntrodeHash);
    int numNTrodes = 0;
    while(i.hasNext()) {
        i.next();
        int nTrodeIndex = i.value();
        SingleSpikeTrodeConf newSettings = nTrodeTree->getNTrode(nTrodeIndex).settings;

        newSettings.lowFilter = lowFilterBox->currentText().toInt();

        nTrodeTree->setNTrodeSettings(nTrodeIndex, newSettings);
        numNTrodes++;
    }
    if (numNTrodes > 1)
        checkNTrodesForConflicts();
}

void WorkspaceEditor::nTrodeSaveHighFilter() {
//    qDebug() << "Save High Filter";
    resetNTrodePanelLabelColor();
    configChanged();
    QHash<QString, int> ntrodeHash = getTopLevelHash(nTrodeTree);
    QHashIterator<QString, int> i(ntrodeHash);
    int numNTrodes = 0;
    while(i.hasNext()) {
        i.next();
        int nTrodeIndex = i.value();
        SingleSpikeTrodeConf newSettings = nTrodeTree->getNTrode(nTrodeIndex).settings;

        newSettings.highFilter = highFilterBox->currentText().toInt();

        nTrodeTree->setNTrodeSettings(nTrodeIndex, newSettings);
        numNTrodes++;
    }
    if (numNTrodes > 1)
        checkNTrodesForConflicts();
}

void WorkspaceEditor::nTrodeSaveDigitalRef() {
//    qDebug() << "Save Dig Ref";
    resetNTrodePanelLabelColor();
    configChanged();
    QHash<QString, int> ntrodeHash = getTopLevelHash(nTrodeTree);
    QHashIterator<QString, int> i(ntrodeHash);
    int numNTrodes = 0;
    while(i.hasNext()) {
        i.next();
        int nTrodeIndex = i.value();
        SingleSpikeTrodeConf newSettings = nTrodeTree->getNTrode(nTrodeIndex).settings;

        newSettings.refOn = enableDigitalRef->isChecked();
        //check if we should set defaults
        int relativeRefChan = 0; //find and calculate the relativeRefChan
        SingleSpikeTrodeConf refNTrode;
        if (nTrodeBox->currentText().isEmpty() || channelBox->currentText().isEmpty()) {
            newSettings.refNTrodeID = -1;
            newSettings.refChan = -1;
            newSettings.refChanID = -1;
        }

        nTrodeTree->setNTrodeSettings(nTrodeIndex, newSettings);
        numNTrodes++;
    }
    if (numNTrodes > 1)
        checkNTrodesForConflicts();
}

void WorkspaceEditor::nTrodeSaveLFPRef() {
//    qDebug() << "Save LFP Ref";
    resetNTrodePanelLabelColor();
    configChanged();
    QHash<QString, int> ntrodeHash = getTopLevelHash(nTrodeTree);
    QHashIterator<QString, int> i(ntrodeHash);
    int numNTrodes = 0;
    while(i.hasNext()) {
        i.next();
        int nTrodeIndex = i.value();
        SingleSpikeTrodeConf newSettings = nTrodeTree->getNTrode(nTrodeIndex).settings;

        newSettings.lfpRefOn = enableLFPRef->isChecked();

        nTrodeTree->setNTrodeSettings(nTrodeIndex, newSettings);
        numNTrodes++;
    }
    if (numNTrodes > 1)
        checkNTrodesForConflicts();
}

void WorkspaceEditor::nTrodeSaveRefNTrodeID() {
//    qDebug() << "Save Ref NTrode ID";
    resetNTrodePanelLabelColor();
    configChanged();
    QHash<QString, int> ntrodeHash = getTopLevelHash(nTrodeTree);
    QHashIterator<QString, int> i(ntrodeHash);
    int numNTrodes = 0;
    while(i.hasNext()) {
        i.next();
        int nTrodeIndex = i.value();
        SingleSpikeTrodeConf newSettings = nTrodeTree->getNTrode(nTrodeIndex).settings;

        newSettings.refNTrodeID = nTrodeBox->currentText().toInt();

        nTrodeTree->setNTrodeSettings(nTrodeIndex, newSettings);
        numNTrodes++;
    }
    if (numNTrodes > 1)
        checkNTrodesForConflicts();
}

void WorkspaceEditor::nTrodeSaveRefNTrodeChan() {
//    qDebug() << "Save Ref NTrode Chan";
    resetNTrodePanelLabelColor();
    configChanged();
    QHash<QString, int> ntrodeHash = getTopLevelHash(nTrodeTree);
    QHashIterator<QString, int> i(ntrodeHash);
    int numNTrodes = 0;
    while(i.hasNext()) {
        i.next();
        int nTrodeIndex = i.value();
        SingleSpikeTrodeConf newSettings = nTrodeTree->getNTrode(nTrodeIndex).settings;

        //calc the relative channel's index (NOT one based yet, that happens at the parser level in configuration.cpp/.h
        int relativeRefChan = 0; //find and calculate the relativeRefChan
        SingleSpikeTrodeConf refNTrode;
        for (int j = 0; j < nTrodeTree->topLevelItemCount(); j++) { //find the referenced NTrode
            if (nTrodeTree->getNTrode(j).settings.nTrodeId == nTrodeBox->currentText().toInt()) {
                refNTrode = nTrodeTree->getNTrode(j).settings;
                break;
            }
        }
        for (int j = 0; j < refNTrode.unconverted_hw_chan.length(); j++) { //find the index of the referenced channel
            if (refNTrode.unconverted_hw_chan.at(j) == channelBox->currentText().toInt()) {
                relativeRefChan = j;
                break;
            }
        }

        newSettings.refChan = relativeRefChan;
        newSettings.refChanID = channelBox->currentText().toInt();

        nTrodeTree->setNTrodeSettings(nTrodeIndex, newSettings);
        numNTrodes++;
    }
    if (numNTrodes > 1)
        checkNTrodesForConflicts();
}

void WorkspaceEditor::nTrodeSaveLFPChannel() {
//    qDebug() << "Save nTrodeSaveLFPChan";
    resetNTrodePanelLabelColor();
    configChanged();
    QHash<QString, int> ntrodeHash = getTopLevelHash(nTrodeTree);
    QHashIterator<QString, int> i(ntrodeHash);
    int numNTrodes = 0;

    QString lfpErrorMessage = "Warning: LFP settings could not be applied to\n";
    while(i.hasNext()) {
        i.next();
        int nTrodeIndex = i.value();
        SingleSpikeTrodeConf newSettings = nTrodeTree->getNTrode(nTrodeIndex).settings;

        if (newSettings.unconverted_hw_chan.length() > 0) {
            if (LFPChannelsBox->currentIndex() < newSettings.unconverted_hw_chan.length()) {
                    newSettings.moduleDataChan = LFPChannelsBox->currentIndex();
            }
            else {
                newSettings.moduleDataChan = newSettings.unconverted_hw_chan.length()-1;
            }
        }
        else {
            lfpErrorMessage = QString("%1   -nTrode %2\n").arg(lfpErrorMessage).arg(newSettings.nTrodeId);
        }

        nTrodeTree->setNTrodeSettings(nTrodeIndex, newSettings);
        numNTrodes++;
    }

    if (lfpErrorMessage != "Warning: LFP settings could not be applied to\n") {
        lfpErrorMessage = QString("%1 You will have to manually apply the correct LFP settings to these nTrodes.").arg(lfpErrorMessage);
        QMessageBox::warning(this,tr("LFP Settings Not Applied"), tr(qPrintable(lfpErrorMessage)));

    }

    if (numNTrodes > 1)
        checkNTrodesForConflicts();


}

void WorkspaceEditor::nTrodeSaveLFPHighFilter() {
//    qDebug() << "Save LFP High Filter";
    resetNTrodePanelLabelColor();
    configChanged();
    QHash<QString, int> ntrodeHash = getTopLevelHash(nTrodeTree);
    QHashIterator<QString, int> i(ntrodeHash);
    int numNTrodes = 0;
    while(i.hasNext()) {
        i.next();
        int nTrodeIndex = i.value();
        SingleSpikeTrodeConf newSettings = nTrodeTree->getNTrode(nTrodeIndex).settings;

        newSettings.moduleDataHighFilter = LFPHighFilterBox->currentText().toInt();

        nTrodeTree->setNTrodeSettings(nTrodeIndex, newSettings);
        numNTrodes++;
    }
    if (numNTrodes > 1)
        checkNTrodesForConflicts();
}

void WorkspaceEditor::nTrodeSaveLFPFilter() {
    qDebug() << "Save LFP Filter (bool)";
    resetNTrodePanelLabelColor();
    configChanged();
    QHash<QString, int> ntrodeHash = getTopLevelHash(nTrodeTree);
    QHashIterator<QString, int> i(ntrodeHash);
    int numNTrodes = 0;
    while(i.hasNext()) {
        i.next();
        int nTrodeIndex = i.value();
        SingleSpikeTrodeConf newSettings = nTrodeTree->getNTrode(nTrodeIndex).settings;

        newSettings.lfpFilterOn = dgLFPsettingsGroup->isChecked();

        nTrodeTree->setNTrodeSettings(nTrodeIndex, newSettings);
        numNTrodes++;
    }
    if (numNTrodes > 1)
        checkNTrodesForConflicts();
}

void WorkspaceEditor::nTrodeSaveThresh() {
//    qDebug() << "Save Thresh";
    resetNTrodePanelLabelColor();
    configChanged();
    QHash<QString, int> ntrodeHash = getTopLevelHash(nTrodeTree);
    QHashIterator<QString, int> i(ntrodeHash);
    int numNTrodes = 0;
    while(i.hasNext()) {
        i.next();
        int nTrodeIndex = i.value();
        SingleSpikeTrodeConf newSettings = nTrodeTree->getNTrode(nTrodeIndex).settings;

        newSettings.channelSettings.thresh = threshSpinBox->value();

        nTrodeTree->setNTrodeSettings(nTrodeIndex, newSettings);
        numNTrodes++;
    }
    if (numNTrodes > 1)
        checkNTrodesForConflicts();
}

void WorkspaceEditor::nTrodeSaveMaxDisp() {
//    qDebug() << "Save Max Display";
    resetNTrodePanelLabelColor();
    configChanged();
    QHash<QString, int> ntrodeHash = getTopLevelHash(nTrodeTree);
    QHashIterator<QString, int> i(ntrodeHash);
    int numNTrodes = 0;
    while(i.hasNext()) {
        i.next();
        int nTrodeIndex = i.value();
        SingleSpikeTrodeConf newSettings = nTrodeTree->getNTrode(nTrodeIndex).settings;

        newSettings.channelSettings.maxDisp = maxDispSpinBox->value();

        nTrodeTree->setNTrodeSettings(nTrodeIndex, newSettings);
        numNTrodes++;
    }
    if (numNTrodes > 1)
        checkNTrodesForConflicts();
}

void WorkspaceEditor::nTrodeSaveChanColor() {
        qDebug() << "Save Channel Color";
        resetNTrodePanelLabelColor();
        configChanged();
        QHash<QString, int> ntrodeHash = getTopLevelHash(nTrodeTree);
        QHashIterator<QString, int> i(ntrodeHash);
        int numNTrodes = 0;
        while(i.hasNext()) {
            i.next();
            int nTrodeIndex = i.value();
            SingleSpikeTrodeConf newSettings = nTrodeTree->getNTrode(nTrodeIndex).settings;


            QString colorCode = QString("#%1").arg(buttonChannelColorBox->styleSheet().split("#").last());
            newSettings.color = QColor(colorCode);

            nTrodeTree->setNTrodeSettings(nTrodeIndex, newSettings);
            numNTrodes++;
        }
        if (numNTrodes > 1)
            checkNTrodesForConflicts();
}

void WorkspaceEditor::nTrodeSaveEnableTrigger() {
//    qDebug() << "Save Enable Trigger";
    resetNTrodePanelLabelColor();
    configChanged();
    QHash<QString, int> ntrodeHash = getTopLevelHash(nTrodeTree);
    QHashIterator<QString, int> i(ntrodeHash);
    int numNTrodes = 0;
    while(i.hasNext()) {
        i.next();
        int nTrodeIndex = i.value();
        SingleSpikeTrodeConf newSettings = nTrodeTree->getNTrode(nTrodeIndex).settings;

//        newSettings.channelSettings.triggerOn = enableTrigger->isChecked();

        nTrodeTree->setNTrodeSettings(nTrodeIndex, newSettings);
        numNTrodes++;
    }
    if (numNTrodes > 1)
        checkNTrodesForConflicts();
}

void WorkspaceEditor::nTrodeSaveTags() {
//    qDebug() << "Save Tags";
    resetNTrodePanelLabelColor();
    configChanged();
    QHash<QString, int> ntrodeHash = getTopLevelHash(nTrodeTree);
    QHashIterator<QString, int> i(ntrodeHash);
    int numNTrodes = 0;

    QList<GroupingTag> tagsToAdd = tagGroupPanel->getAddedTags();
    QList<GroupingTag> tagsToRemove = tagGroupPanel->getRemovedTags();

    while(i.hasNext()) {
        i.next();
        int nTrodeIndex = i.value();
        SingleSpikeTrodeConf newSettings = nTrodeTree->getNTrode(nTrodeIndex).settings;

        //set tag valuse
        newSettings.tags.clear();
//        QList<GroupingTag> tagsToAdd = tagGroupPanel->getGTagList();
//        for (int i = 0; i < tagsToAdd.length(); i++) {
//            GroupingTag curTag = tagsToAdd.at(i);
//            if (!newSettings.gTags.contains(curTag))
//                newSettings.gTags.insert(curTag,1);

//            if (!availableTags.contains(curTag.tag))
//                insertAvailableTag(curTag.tag,1);
//        }

        for (int j = 0; j < tagsToAdd.length(); j++) {
            if (!newSettings.gTags.contains(tagsToAdd.at(j))) { //Add the tag to the nTrode if it doesn't already exist
                newSettings.gTags.insert(tagsToAdd.at(j),1);
            }
        }
        for (int j = 0; j < tagsToRemove.length(); j++) {
            if (newSettings.gTags.contains(tagsToRemove.at(j))) { //remove the tag from the nTrode if it exists in the nTrode
                newSettings.gTags.remove(tagsToRemove.at(j));
            }
        }
       //updateFilterSelector();

        nTrodeTree->setNTrodeSettings(nTrodeIndex, newSettings);
        numNTrodes++;
    }
    tagGroupPanel->clearAddedTagList();
    tagGroupPanel->clearRemovedTagList();

   //applyFilters();
    if (numNTrodes > 1)
        checkNTrodesForConflicts();
}

void WorkspaceEditor::loadLFPSettingsIntoPanel(int nTrodeIndex) {
    NTrodeSettings curNTrode = nTrodeTree->getNTrode(nTrodeIndex);

    //first load the populate the channels box with the current nTrode's channel count
//    qDebug() << " num chans: " << curNTrode.settings.unconverted_hw_chan.length();
//    qDebug() << " LFP Chan: " << (curNTrode.settings.moduleDataChan+1);
//    qDebug() << " LFP High Filter: " << curNTrode.settings.moduleDataHighFilter;
    LFPChannelsBox->clear();
    for (int i = 0; i < curNTrode.settings.unconverted_hw_chan.length(); i++) {
        LFPChannelsBox->addItem(QString("%1").arg(i+1));
    }


    LFPChannelsBox->setCurrentIndex(curNTrode.settings.moduleDataChan);
    LFPHighFilterBox->setCurrentText(QString("%1").arg(curNTrode.settings.moduleDataHighFilter));
}

void WorkspaceEditor::getChanColorDialog() {
    QString colorCode = QString("#%1").arg(buttonChannelColorBox->styleSheet().split("#").last());
    QColor newColor = QColorDialog::getColor(colorCode, this, "Select New Channel Color");
    if (newColor.isValid()) {
        buttonChannelColorBox->setStyleSheet(QString("background-color:%1").arg(newColor.name()));
        nTrodeSaveChanColor();
    }
}

bool WorkspaceEditor::checkNTrodesForConflicts() {
    bool conflict = false;
    QHash<QString, int> ntrodeHash = getTopLevelHash(nTrodeTree);
    QHashIterator<QString, int> i(ntrodeHash);
    QString modifiedNtrodesMsg = "";
    while(i.hasNext()) {
        i.next();
        if (checkSelectedNTrodesForConflicts(nTrodeTree->getNTrode(i.value()).settings)) {
            conflict = true;
            modifiedNtrodesMsg = "  *Mismatched nTrode Settings*";
        }
    }
    labelMismatchSettingsWarning->setText(modifiedNtrodesMsg);
    QString toolTipMsg = "Settings across selected nTrodes differ.  Clicking 'Apply' will set the first selected nTrode's settings to all others.";
    labelMismatchSettingsWarning->setToolTip(toolTipMsg);
    return(conflict);
}

bool WorkspaceEditor::checkSelectedNTrodesForConflicts(SingleSpikeTrodeConf checkedNTrode) {
    bool conflict = false;
//    qDebug() << "For nTrode " << checkedNTrode.nTrodeId;
    QHash<QString, int> ntrodeHash = getTopLevelHash(nTrodeTree);
    QHashIterator<QString, int> i(ntrodeHash);

    while(i.hasNext()) {
        i.next();
        SingleSpikeTrodeConf curNTrode = nTrodeTree->getNTrode(i.value()).settings;

        if (checkedNTrode.filterOn != curNTrode.filterOn) {
//            qDebug() << "Conflict (spike filter bool) ";
//            setWidgetTextPaletteColor(enableSpikeFilter, QColor("red"));
            setWidgetTextPaletteColor(dgSpikeFilterGroup, QColor("red"));
            conflict = true;
        }

        if (checkedNTrode.lowFilter != curNTrode.lowFilter) {
//            qDebug() << "Conflict (spike low) ";
            setWidgetTextPaletteColor(labelLowFilter, QColor("red"));
            conflict = true;
        }

        if (checkedNTrode.highFilter != curNTrode.highFilter) {
//            qDebug() << "Conflict (spike high) ";
            setWidgetTextPaletteColor(labelHighFilterFilter, QColor("red"));
            conflict = true;
        }

        if (checkedNTrode.refOn != curNTrode.refOn) {
//            qDebug() << "Conflict (dig ref bool) ";
            setWidgetTextPaletteColor(enableDigitalRef, QColor("red"));
            conflict = true;
        }

        if (checkedNTrode.lfpRefOn != curNTrode.lfpRefOn) { //lfp ref check
            setWidgetTextPaletteColor(enableLFPRef, QColor("red"));
            conflict = true;
        }

        if (checkedNTrode.lfpFilterOn != curNTrode.lfpFilterOn) { //lfp filter check
            setWidgetTextPaletteColor(dgLFPsettingsGroup, QColor("red"));
            conflict = true;
        }

        if (checkedNTrode.refNTrodeID != curNTrode.refNTrodeID) {
//            qDebug() << "Conflict (dig ref nTrode ID)";
            setWidgetTextPaletteColor(labelNTrode, QColor("red"));
            conflict = true;
        }

        if (checkedNTrode.refChan != curNTrode.refChan) {
//            qDebug() << "Conflict (dig ref channel)";
            setWidgetTextPaletteColor(labelChannel, QColor("red"));
            conflict = true;
        }

        if (checkedNTrode.moduleDataChan != curNTrode.moduleDataChan) {
//            qDebug() << "Conflict (LFP Channel)";
            setWidgetTextPaletteColor(labelLFPChannelsFilter, QColor("red"));
            conflict = true;
        }

        if (checkedNTrode.moduleDataHighFilter != curNTrode.moduleDataHighFilter) {
//            qDebug() << "Conflict (LFP high filter)";
            setWidgetTextPaletteColor(labelLFPHighFilter, QColor("red"));
            conflict = true;
        }

//        if (checkedNTrode.channelSettings.triggerOn != curNTrode.channelSettings.triggerOn) {
//            qDebug() << "Conflict (enable trigger)";
//            setWidgetTextPaletteColor(enableTrigger, QColor("red"));
//            conflict = true;
//        }

        if (checkedNTrode.channelSettings.thresh != curNTrode.channelSettings.thresh) {
//            qDebug() << "Conflict (Thresh value)";
            setWidgetTextPaletteColor(labelThreshSpin, QColor("red"));
            conflict = true;
        }

        if (checkedNTrode.channelSettings.maxDisp != curNTrode.channelSettings.maxDisp) {
//            qDebug() << "Conflict (Max Display)";
            setWidgetTextPaletteColor(labelMaxDispSpin, QColor("red"));
            conflict = true;
        }

        if (checkedNTrode.color != curNTrode.color) { //Channel color
//            qDebug() << "Conflict (color)";
            setWidgetTextPaletteColor(labelChannel, QColor("red"));
            conflict = true;
        }

        //check tags
        QHashIterator<QString, int> iter(curNTrode.tags);
        while (iter.hasNext()) {
            iter.next();
            if (!checkedNTrode.tags.contains(iter.key())) {
                setWidgetTextPaletteColor(tagGroupPanel, QColor("red"));
                conflict = true;
            }
        }

    }

    return(conflict);
}

void WorkspaceEditor::checkDigitalRefs(int removedNTrode) {
    qDebug() << "ntrode " << removedNTrode << " removed";
    //check all dig reference IDs

}

void WorkspaceEditor::checkDigitalRefs(int originNTrodeID, int removedChanID) {
    qDebug() << "chan " << removedChanID << " removed from nTrode " << originNTrodeID;
}

void WorkspaceEditor::setDigChannelBoxNum(int nTrodeIndex) {
    NTrodeSettings curNTrode = nTrodeTree->getNTrode(nTrodeIndex);
//    qDebug() << "setting digital channel box for nTrode ID: " << curNTrode.settings.nTrodeId;

    channelBox->clear();
    for (int i = 0; i < curNTrode.settings.unconverted_hw_chan.length(); i++) {
        channelBox->addItem(QString("%1").arg(curNTrode.settings.unconverted_hw_chan.at(i)));
//        qDebug() << "setting digital ntrode at index " << nTrodeIndex << " to ref channel " << curNTrode.settings.unconverted_hw_chan.at(i);
    }
}

void WorkspaceEditor::addCategoryToDict(QString newCategory) {
    if (spikeConfig == NULL) {
        qDebug() << "Error: SpikeConfig object is NULL ptr. (WorkspaceEditor::addCategoryToDict)";
        return;
    }
    spikeConfig->groupingDict.addCategory(newCategory);
}

void WorkspaceEditor::addTagToDict(QString category, QString newTag) {
    if (spikeConfig == NULL) {
        qDebug() << "Error: SpikeConfig object is NULL ptr. (WorkspaceEditor::addTagToDict)";
        return;
    }
    spikeConfig->groupingDict.addTagToCategory(category,newTag);
}

void WorkspaceEditor::processTagSelectionBox(int selectedIndex) {
    //DEPRECIATED: replaced by TagGroupingPanel object
    if (selectedIndex != tagSelectionBox->count()-1)
        return;

    QList<QString> curTags;
//    qDebug() << "Cur Items:";
    for (int i = 1; i < tagSelectionBox->count()-1; i++) {
//        qDebug() << tagSelectionBox->itemText(i);
        curTags.append(tagSelectionBox->itemText(i));
    }

    EditTagDialog *askUser = new EditTagDialog(curTags, availableTags);
    int retVal = askUser->exec();

    if (retVal == QDialog::Accepted) {
        QList<QString> newTagList = askUser->getTags();

        tagSelectionBox->clear();
        tagSelectionBox->addItem("");
        for (int i = 0; i < newTagList.length(); i++) {
            //add new tags to the current nTrode
            tagSelectionBox->addItem(newTagList.at(i));
        }
        tagSelectionBox->addItem("+Add/-Remove Tag");
        setComboBoxFirstItemUnselectable(tagSelectionBox);

        nTrodeSetApplyButtonsEnabled();

    }

    delete askUser;
    tagSelectionBox->setCurrentIndex(tagSelectionBox->count()-2);
}

//DEPRECIATED: New grouping tag system makes this function obsolete
void WorkspaceEditor::processFilterSelector() {
//    qDebug() << "Apply Filters";
    if (filterSelector->currentIndex() == 0) {
        //clearFilters();
    }
    else {
        filterSelector->setItemData(0,Qt::Unchecked,Qt::CheckStateRole); //set no filters off
        if (filterSelector->itemData(filterSelector->currentIndex(), Qt::CheckStateRole) == Qt::Checked) {
            //deactivate filter
            if (activeFilters.contains(filterSelector->currentText())) {
    //            qDebug() << "deactivating filter " << filterSelector->currentText();
                activeFilters.remove(filterSelector->currentText());
            }
            filterSelector->setItemData(filterSelector->currentIndex(),Qt::Unchecked,Qt::CheckStateRole);
        }
        else {
            //activate filter
            if (!activeFilters.contains(filterSelector->currentText())) {
    //            qDebug() << "activating filter " << filterSelector->currentText();
                activeFilters.insert(filterSelector->currentText(), 1);
            }
            filterSelector->setItemData(filterSelector->currentIndex(),Qt::Checked,Qt::CheckStateRole);
        }
       //applyFilters();
    }
}

//DEPRECIATED: New grouping tag system makes this function obsolete
void WorkspaceEditor::applyFilters() {
//    qDebug() << "Applying Filters: ";
    //find out what kind of filter operation is happening
    bool isOrOperation;
    if (filterOperationSelector->currentIndex() == 0) {
        isOrOperation = true;
    }
    else
        isOrOperation = false; //this means it's an AND operation

    QString activeFilterStr;
    QList<NTrodeSettings> nTrodes = nTrodeTree->getNTrodeList();
    //check all nTrodes and set the ones without the filter tags to be invisible
    for (int i = 0; i < nTrodes.length(); i++) {
        NTrodeSettings curNTrode = nTrodes.at(i);
        activeFilterStr = "Active Filters: ";
        bool hideNTrode;
        if (isOrOperation)
            hideNTrode = true; //change this to false to make AND operation
        else
            hideNTrode = false;

        QHashIterator<QString, int> iter(activeFilters);
        while(iter.hasNext()) {
            iter.next();
//            qDebug() << "   -" << iter.key();
            activeFilterStr = QString("%1 %2,").arg(activeFilterStr).arg(iter.key());

            //for AND operation
            if (!isOrOperation && !curNTrode.settings.tags.contains(iter.key()))
                hideNTrode = true;

            //for OR operation
            if (isOrOperation && curNTrode.settings.tags.contains(iter.key()))
                hideNTrode = false;
        }

        if (activeFilters.empty())
            hideNTrode = false;

        curNTrode.treeItem->setHidden(hideNTrode);


    }

    labelActiveFilters->setText((activeFilterStr.left(activeFilterStr.length()-1)));


}

//DEPRECIATED: New grouping tag system makes this function obsolete
void WorkspaceEditor::clearFilters() {
//    qDebug() << "Clear Filter button pressed";
    filterSelector->setItemData(0,Qt::Checked,Qt::CheckStateRole); //set no filter checked
    for (int i = 1; i < filterSelector->count(); i++) {
        filterSelector->setItemData(i,Qt::Unchecked,Qt::CheckStateRole);
    }
    activeFilters.clear();
//    if (nTrodeTree->topLevelItem(0)->isHidden())
//        nTrodeTree->topLevelItem(0)->setHidden(false);
//    else
//        nTrodeTree->topLevelItem(0)->setHidden(true);
   //applyFilters();
    filterSelector->setCurrentIndex(0);
}

//DEPRECIATED: New grouping tag system makes this function obsolete
void WorkspaceEditor::updateFilterSelector() {
    filterSelector->clear();
    //create the first 'no filter' item
    QStandardItemModel *filterModel = new QStandardItemModel();
    QStandardItem *firstItem = new QStandardItem();
    firstItem->setText("No Filter");
    firstItem->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    firstItem->setData(Qt::Checked, Qt::CheckStateRole);
    filterModel->insertRow(0, firstItem);
    //add all available tags as filters
    QHashIterator<QString, int> iter(availableTags);
    bool hasActiveFilters = false;
    int lastActiveFilterIndex = 0;
    while(iter.hasNext()) {
        iter.next();
        QStandardItem *newFilter = new QStandardItem();
//        qDebug() << iter.key();
        newFilter->setText(iter.key());
        newFilter->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        //to do: check if this filter is active
        if (activeFilters.contains(iter.key())) {
            newFilter->setData(Qt::Checked, Qt::CheckStateRole);
//            qDebug() << "Active filter detected";
            lastActiveFilterIndex = filterModel->rowCount();
            hasActiveFilters = true;
        }
        else
            newFilter->setData(Qt::Unchecked, Qt::CheckStateRole);
        filterModel->insertRow(filterModel->rowCount(), newFilter);
    }
    if (hasActiveFilters) {
        firstItem->setData(Qt::Unchecked, Qt::CheckStateRole);
    }
    filterSelector->setModel(filterModel);
    if (hasActiveFilters)
        filterSelector->setCurrentIndex(lastActiveFilterIndex);
//    setComboBoxFirstItemUnselectable(filterSelector);
}


void WorkspaceEditor::setHardwareEditViewToChannel(int chanIndex) {
    qDebug() << " chan: " << chanIndex;
    channelSettingsGroup->setVisible(true);
    loadChannelSettingsIntoPanel(chanIndex);
}

void WorkspaceEditor::loadChannelSettingsIntoPanel(int chanIndex) {
    HardwareChannelSettings hwChannel = hardwareChanConfigs.at(chanIndex);
//    labelChannelID->setText(QString("Selected Channel: %1").arg(hwChannel.ID));
    enableTrigger->setChecked(hwChannel.triggerOn);
    threshSpinBox->setValue(hwChannel.thresh);
    maxDispSpinBox->setValue(hwChannel.maxDisp);
    buttonNTrodeApply->setEnabled(false);
    buttonNTrodeApplyToAll->setEnabled(false);
}

void WorkspaceEditor::saveChannelPannelSettings(int chanIndex) {
    qDebug() << "Saving settings to channel " << chanIndex;
    HardwareChannelSettings hwChannel = hardwareChanConfigs.at(chanIndex);
    hwChannel.triggerOn = enableTrigger->isChecked();
    hwChannel.thresh = threshSpinBox->value();
    hwChannel.maxDisp = maxDispSpinBox->value();
    hardwareChanConfigs.replace(chanIndex, hwChannel);
} **/

void WorkspaceEditor::nTrodeSetApplyToAllButtonEnabled(bool enabled) {
    buttonNTrodeApplyToAll->setEnabled(enabled);
}

void WorkspaceEditor::nTrodeSetApplyButtonsEnabled() {
    QHash<QString, int> selectedNTrodes = getTopLevelHash(nTrodeTree);
    buttonNTrodeApply->setEnabled(true);
    if (selectedNTrodes.count() > 1)
        buttonNTrodeApplyToAll->setEnabled(false);
    else
        buttonNTrodeApplyToAll->setEnabled(true);
}

//DEPRECIATED no longer in use
void WorkspaceEditor::nTrodeCancelButtonPressed() {
//    QStringList channelL = labelChannelID->text().split(" ");
//    int chanIndex = channelL.at(channelL.length()-1).toInt();
//    loadChannelSettingsIntoPanel(chanIndex);
    buttonNTrodeApply->setEnabled(false);
    buttonNTrodeApplyToAll->setEnabled(false);

}

void WorkspaceEditor::nTrodeApplyButtonPressed() {
    /*resetNTrodePanelLabelColor();
    QHash<QString, int> ntrodeHash = getTopLevelHash(nTrodeTree);
    QHashIterator<QString, int> i(ntrodeHash);
    while(i.hasNext()) {
        i.next();
        saveNTrodeSettings(i.value());
    }
   //applyFilters();
    buttonNTrodeApply->setEnabled(false);
//    buttonNTrodeApplyToAll->setEnabled(false);*/

}

void WorkspaceEditor::nTrodeApplyToAllButtonPressed() {
    /*resetNTrodePanelLabelColor();
    QString lfpErrorMessage = "Warning: LFP settings could not be applied to\n";
    for (int i = 0; i < nTrodeTree->topLevelItemCount(); i++) {
        saveNTrodeSettings(i, &lfpErrorMessage);
    }

    if (lfpErrorMessage != "Warning: LFP settings could not be applied to\n") {
//        qDebug() << qPrintable(lfpErrorMessage);
        lfpErrorMessage = QString("%1 You will have to manually apply the correct LFP settings to these nTrodes.").arg(lfpErrorMessage);
        QMessageBox::warning(this,tr("LFP Settings Not Applied"), tr(qPrintable(lfpErrorMessage)));

    }
   //applyFilters();
    buttonNTrodeApply->setEnabled(false);
//    buttonNTrodeApplyToAll->setEnabled(false);*/
}

void WorkspaceEditor::nTrodeAddbuttonPressed() {
    //addNTrodesToList();
   //applyFilters();
}

// ************************************ Module Tab Slots ************************************
// ******************************************************************************************

void WorkspaceEditor::setModuleConfigValues() {
    moduleConfig->singleModuleConf.clear();
    for (int i = 0; i < moduleConfigs.length(); i++) {
        moduleConfig->singleModuleConf.append(moduleConfigs.at(i));
    }
}

void WorkspaceEditor::loadModuleConfigIntoGUI() {
    moduleArgumentTree->clear();
    for (int i = 0; i < moduleConfigs.length(); i++) {
        QTreeWidgetItem *modItem = addModuleToTree(moduleConfigs.at(i));
        for (int j = 0; j < moduleConfigs.at(i).moduleArguments.length(); j++) {
            QString flag = moduleConfigs.at(i).moduleArguments.at(j);
            //flag.remove(0,1)
            j += 1;
            if (j == moduleConfigs.at(i).moduleArguments.length()) {
                qDebug() << "Error: Incorrectly formated arguments. (WorkspaceEditor::loadModuleConfigIntoGUI)";
                return;
            }
            QString value = moduleConfigs.at(i).moduleArguments.at(j);
            addArgumentToTree(modItem,flag,value);

        }
    }

}

void WorkspaceEditor::processModulePullDownMenuIndex(int index) {
    editingModule = false;
    int moduleConfigsIndex = -1;
    if (index == (modulePullDownMenu->count()-1)) {
        AddModuleDialog *askUser = new AddModuleDialog();
        int retVal = askUser->exec();

        if (retVal == QDialog::Accepted) {
            QString newMod = askUser->getText();
            if (!newMod.isEmpty()) {
                modulePullDownMenu->insertItem((modulePullDownMenu->count()-1),newMod);
                modulePullDownMenu->setCurrentIndex((modulePullDownMenu->count()-2));
                enableSendNetworkInfo->setChecked(true);
                enableSendTrodesConfig->setChecked(true);
            }
            else {
                modulePullDownMenu->setCurrentIndex(0);
                enableSendNetworkInfo->setChecked(true);
                enableSendTrodesConfig->setChecked(true);
            }
        }
        else {
            modulePullDownMenu->setCurrentIndex(0);
            enableSendNetworkInfo->setChecked(true);
            enableSendTrodesConfig->setChecked(true);
        }

        delete askUser;
    }
    else if (index > 0) {
        //check if the item exists in the tree/moduleConfigs
        QString selectedItem = modulePullDownMenu->itemText(index);
        //qDebug() << "Selected: " << selectedItem;
        for (int i = 0; i < moduleConfigs.length(); i++) {
            if (QString::compare(selectedItem, moduleConfigs.at(i).moduleName, Qt::CaseInsensitive) == 0) {
                moduleConfigsIndex = i;
                editingModule = true;
                break;
            }
        }
    }

    if (editingModule) {
        enableSendNetworkInfo->setChecked(moduleConfigs.at(moduleConfigsIndex).sendNetworkInfo);
        enableSendTrodesConfig->setChecked(moduleConfigs.at(moduleConfigsIndex).sendTrodesConfig);

        buttonAddModule->setText("Apply");
        amGroup->setTitle(tr("Edit Module"));
    }
    else {
        enableSendNetworkInfo->setChecked(true);
        enableSendTrodesConfig->setChecked(true);

        buttonAddModule->setText("Add");
        amGroup->setTitle(tr("Add Module"));
    }
}

void WorkspaceEditor::addEditModuleToConfig() {
    //qDebug() << "Add/Edit Module [" << modulePullDownMenu->itemText(modulePullDownMenu->currentIndex()) << "] to config";
    //Add To ModuleConfigs
    if (modulePullDownMenu->currentIndex() == 0)
        return;

    QString name = modulePullDownMenu->itemText(modulePullDownMenu->currentIndex());
    bool sendNInfo = enableSendNetworkInfo->isChecked();
    bool sendTCInfo = enableSendTrodesConfig->isChecked();

    if (editingModule) {
        //find index of Edited module
        int index = -1;
        for (int i = 0; i < moduleConfigs.length(); i++) {
            if (QString::compare(name, moduleConfigs.at(i).moduleName, Qt::CaseInsensitive) == 0) {
                index = i;
                break;
            }
        }

        //edit module in tree
        if (index >= 0) { //if the index exists
            moduleArgumentTree->topLevelItem(index)->setText(0,QString("module: %1 - sendNetworkInfo: %2 - sendTrodesConfig: %3").arg(name).arg(sendNInfo).arg(sendTCInfo));

            //edit module in moduleConfigs
            SingleModuleConf editedModule = moduleConfigs.at(index);
            editedModule.sendNetworkInfo = sendNInfo;
            editedModule.sendTrodesConfig = sendTCInfo;
            moduleConfigs.replace(index, editedModule);
        }
        else {
            qDebug() << "Error: Bad Index Access Prevented. (WorkspaceEditor::addEditModuleToConfig)";
        }

    }
    else {

        SingleModuleConf newModule;
        newModule.moduleName = name;
        newModule.sendNetworkInfo = sendNInfo;
        newModule.sendTrodesConfig = sendTCInfo;
        moduleConfigs.append(newModule);

        addModuleToTree(newModule);
    }

    configChanged();
    modulePullDownMenu->setCurrentIndex(0);
    emit (modulePullDownMenu->activated(0));
}

QTreeWidgetItem* WorkspaceEditor::addModuleToTree(SingleModuleConf module) {
    QTreeWidgetItem *newMod = new QTreeWidgetItem();
    newMod->setText(0,QString("module: %1 - sendNetworkInfo: %2 - sendTrodesConfig: %3").arg(module.moduleName).arg(module.sendNetworkInfo).arg(module.sendTrodesConfig));
    moduleArgumentTree->addTopLevelItem(newMod);
    moduleArgumentTree->setCurrentItem(newMod);
    return(newMod);
}

void WorkspaceEditor::addArgument() {
    if (moduleArgumentTree->topLevelItemCount() <= 0)
        return;
    QTreeWidgetItem *parentTopLevelItem = moduleArgumentTree->currentItem();
    if(parentTopLevelItem == nullptr){
        return;
    }
    bool isTopLevelItem = false;
    for (int i = 0; i < moduleArgumentTree->topLevelItemCount(); i++) {
        if (parentTopLevelItem == moduleArgumentTree->topLevelItem(i)) {
            isTopLevelItem = true;
            break;
        }
    }

    if (!isTopLevelItem) {
        //if the curItem was a argument, set the parent to that arg's parent item
        parentTopLevelItem = parentTopLevelItem->parent();
    }

    //add argument to tree (need AddArgument dialog here)
    AddArgumentDialog *askUser = new AddArgumentDialog();
    int retVal = askUser->exec();

    if (retVal == QDialog::Accepted) {

//        QTreeWidgetItem *argument = new QTreeWidgetItem();
//        QString argStr = askUser->getArg();
        QString flag = askUser->getFlag();
        QString value = askUser->getValue();
//        argument->setText(0,argStr);
        addArgumentToTree(parentTopLevelItem, flag, value);
//        parentTopLevelItem->addChild(argument);
//        parentTopLevelItem->setExpanded(true);

        int itemIndex = moduleArgumentTree->indexOfTopLevelItem(parentTopLevelItem);
        //qDebug() << "adding arg to item indi: " << itemIndex;
        SingleModuleConf curModConf = moduleConfigs.at(itemIndex);
        curModConf.moduleArguments.append(askUser->getFlag());
        curModConf.moduleArguments.append(askUser->getValue());
        moduleConfigs.replace(itemIndex, curModConf);
        configChanged();

    }
    delete askUser;
}

void WorkspaceEditor::addArgumentToTree(QTreeWidgetItem *parentItem, QString flag, QString value) {
    flag.remove(0,1); //remove the '-' from the front of this noise
    QString argument = QString("Argument -flag=\"%1\" -value=\"%2\"").arg(flag).arg(value);
    QTreeWidgetItem *argItem = new QTreeWidgetItem();
    argItem->setText(0,argument);
    parentItem->addChild(argItem);
    parentItem->setExpanded(true);
}

void WorkspaceEditor::editModuleArgument() {
    if (moduleArgumentTree->topLevelItemCount() <= 0)
        return;

    QTreeWidgetItem *editedItem = moduleArgumentTree->currentItem();

    bool isTopLevelItem = false;
    int indexTopLevel = -1;
    for (int i = 0; i < moduleArgumentTree->topLevelItemCount(); i++) {
        if (editedItem == moduleArgumentTree->topLevelItem(i)) {
            isTopLevelItem = true;
            indexTopLevel = i;
            break;
        }
    }

    if (isTopLevelItem) {
        //Edit top level tree item
        QString moduleName = moduleConfigs.at(indexTopLevel).moduleName;
        for (int i = 0; i < modulePullDownMenu->count(); i++) {
            if (QString::compare(moduleName, modulePullDownMenu->itemText(i)) == 0) {
                modulePullDownMenu->setCurrentIndex(i);
                emit (modulePullDownMenu->activated(i));
                break;
                //See function 'processModulePullDownMenuIndex' for editing behavior
            }
        }

    }
    else {
        QTreeWidgetItem *parentTopLevelItem = editedItem->parent();
        AddArgumentDialog *askUser = new AddArgumentDialog();
        askUser->setMainText("Edit Argument");
        QString curArg = qPrintable(editedItem->text(0));
        QStringList curArgL = curArg.split("\"");

        askUser->populateEditFields(curArgL.at(1),curArgL.at(3));

        int retVal = askUser->exec();

        if (retVal == QDialog::Accepted) {
            QString argStr = askUser->getArg();
            //Edit argument tree item
            editedItem->setText(0,argStr);

            //Edit argument to moduleConfigs
            //find parentTopLevelItem index;
            int parentItemIndex = moduleArgumentTree->indexOfTopLevelItem(parentTopLevelItem);
            int itemIndex = parentTopLevelItem->indexOfChild(editedItem);
            SingleModuleConf curModConf = moduleConfigs.at(parentItemIndex);
            int argIndex = itemIndex*2;

            //arguments are laid out like so: [0]flg1 [1]val1 [2]flg2 [3]val2 [4]flg3 [5]val3
            curModConf.moduleArguments.replace(argIndex, askUser->getFlag());
            argIndex++;
            curModConf.moduleArguments.replace(argIndex, askUser->getValue());
            moduleConfigs.replace(parentItemIndex, curModConf);
            configChanged();
        }
    }
    //printModuleConfigs();

}

void WorkspaceEditor::removeModuleArgument() {
    if (moduleArgumentTree->topLevelItemCount() <= 0)
        return;

    QTreeWidgetItem *removedItem = moduleArgumentTree->currentItem();
    //qDebug() << "Removing: " << removedItem->text(0);

    bool isTopLevelItem = false;
    int itemIndex = -1;
    for (int i = 0; i < moduleArgumentTree->topLevelItemCount(); i++) {
        if (removedItem == moduleArgumentTree->topLevelItem(i)) {
            isTopLevelItem = true;
            itemIndex = i;
            break;
        }
    }

    if (isTopLevelItem) {
        //qDebug() << "Top level item...";
        //remove topLevelItem from Tree

        //remove all argument children
        for (int i = 0; i < removedItem->childCount(); i++) {
            //remove children from tree, this will happen automatically in moduleConfigs when the the module is removed
            removedItem->removeChild(removedItem->child(i));
            i--;
        }
        //remove module from moduleConfigs
        moduleConfigs.removeAt(itemIndex); //this only works if the tree index is equal to the moduleConfigs index
    }
    else {
        //remove argument from moduleConfigs
        int argIndex = removedItem->parent()->indexOfChild(removedItem);
        int parentIndex = moduleArgumentTree->indexOfTopLevelItem(removedItem->parent());
        //qDebug() << "removeing p: " << parentIndex << " a: " << argIndex;
        //moduleConfigs.at(parentIndex).moduleArguments.removeAt(argIndex);
        SingleModuleConf curModConf = moduleConfigs.at(parentIndex);
        curModConf.moduleArguments.removeAt(argIndex);
        moduleConfigs.replace(parentIndex, curModConf);
    }
    //remove module/argument from Tree;
    configChanged();
    delete removedItem;
    //printModuleConfigs();
}

bool WorkspaceEditor::verifyCurrentWorkspace(QString *errorMsg) {
    bool isValid = true;
//    bool recordErrors = false; //recordErrors unused

//    if (errorMsg != 0)
//        recordErrors = true;

    if(!verifyHardwareConfiguration(errorMsg))
        isValid = false;

    if(!verifyNTrodeConfiguration(errorMsg))
        isValid = false;

    if(!verifyRefGroupConfiguration(errorMsg))
        isValid = false;

//    if (recordErrors && *errorMsg != "") {
//        *errorMsg = QString()
//    }

    return(isValid);
}

bool WorkspaceEditor::verifyHardwareConfiguration(QString *errorMsg) {
    bool isValid = true;
    bool recordErrors = false;

    if (errorMsg != 0)
        recordErrors = true;

    //verify an MCU_IO is included in the hardware configuration
    if (hardwareConfig->devices.length() == 0) {
        isValid = false;
        qDebug() << "Invalid Device Configuration: An MCU must be present.";
        if (recordErrors)
            *errorMsg = QString("%1     -Invalid Device Configuration: An MCU must be present.\n").arg(*errorMsg);
    }

    //verify device setup (basically MCU is present if any devices attached)
    if (hardwareConfig->devices.length() > 0) {
        bool mcuPresent = false;
        bool dockPresent = false;
        for (int i = 0; i < hardwareConfig->devices.length(); i++) {
            if (hardwareConfig->devices.at(i).name == "MCU_IO") {
                mcuPresent = true;
            }
            if( hardwareConfig->devices.at(i).name == "Dock_IO"){
                dockPresent = true;
            }
        }
        if (!mcuPresent && !dockPresent) {
            isValid = false;
            qDebug() << "Invalid Device Configuration: An MCU or DockingStation must be present to stream.";
            if (recordErrors)
                *errorMsg = QString("%1     -Invalid Device Configuration: An MCU or DockingStation must be present.\n").arg(*errorMsg);
        }
        if (mcuPresent && dockPresent) {
            isValid = false;
            qDebug() << "Invalid Device Configuration: Both an MCU and DockingStation cannot be connected at the same time.";
            if (recordErrors)
                *errorMsg = QString("%1     -Invalid Device Configuration: Both an MCU and DockingStation cannot be connected at the same time.\n").arg(*errorMsg);
        }
    }


    return(isValid);
}

bool WorkspaceEditor::verifyNTrodeConfiguration(QString *errorMsg) {
    bool isValid = true;
    bool recordErrors = false;

    if (errorMsg != 0)
        recordErrors = true;

    //verify that at least one nTrode exists
    if (spikeConfig->ntrodes.length() == 0 && hardwareConfig->NCHAN > 0) {
        qDebug() << "No nTrodes added.  You must add at least one nTrode to the configuration if you have hardware channels connected.";
        isValid = false;
        if (recordErrors)
            *errorMsg =  QString("%1     -No nTrodes added.  You must add at least one nTrode to the configuration if you have hardware channels connected.\n").arg(*errorMsg);

    }

    //verify that the number of display columns is less than or equal to the number of nTrodes
    if (streamConfig->nColumns*streamConfig->nTabs > spikeConfig->ntrodes.length() && spikeConfig->ntrodes.length() != 0) {
        qDebug() << "The number of nTrodes cannot be less than the total number of display columns across all pages.";
        isValid = false;
        if (recordErrors)
            *errorMsg = QString("%1     -The number of nTrodes (%2) cannot be less than the total number of display columns across all pages (%3).\n").arg(*errorMsg).arg(spikeConfig->ntrodes.length()).arg(streamConfig->nColumns*streamConfig->nTabs);
    }

    for (int i = 0; i <spikeConfig->ntrodes.length(); i++) {
        SingleSpikeTrodeConf *curNTrode = spikeConfig->ntrodes.at(i);

        //verify all nTrodes have channels assigned to them
        if (curNTrode->unconverted_hw_chan.length() == 0) {
            qDebug() << "Empty nTrode. (nTrode " << curNTrode->nTrodeId << ")";
            isValid = false;
            if (recordErrors)
                *errorMsg = QString("%1     -Empty nTrode. (nTrode %2)\n").arg(*errorMsg).arg(curNTrode->nTrodeId);
        }

        //verify digital referencing
        if (curNTrode->refOn) {
            if (curNTrode->refNTrodeID == -1) {
                qDebug() << "Invalid refNTrodeID. (nTrode " << curNTrode->nTrodeId << ")";
                isValid = false;
                if (recordErrors)
                    *errorMsg = QString("%1     -Invalid refNTrodeID. (nTrode %2)\n").arg(*errorMsg).arg(curNTrode->nTrodeId);
            }
            else {
                //to do: add this line when you merge the QHash code
//                SingleSpikeTrodeConf refNTrode = spikeConfig->ntrodes.find(curNTrode->refNTrodeID);
                //to do: check that the refNTrodeID exists
            }

            if (curNTrode->refChan == -1) {
                qDebug() << "Invalid refChan (nTrode " << curNTrode->nTrodeId << ")";
                isValid = false;
                if (recordErrors)
                    *errorMsg = QString("%1     -Invalid refChan assignment. (nTrode %2)\n").arg(*errorMsg).arg(curNTrode->nTrodeId);
                //to do: also make sure to check that refChan index doesn't exceed the total number of chan's in the refNtrode
            }

        }

        //verify LFP settings
        if (curNTrode->moduleDataChan == -1) {
            qDebug() << "Invalid LFP Settings (nTrode " << curNTrode->nTrodeId << ")";
            isValid = false;
            if (recordErrors)
                *errorMsg = QString("%1     -Invalid LFP Settings. (nTrode %2)\n").arg(*errorMsg).arg(curNTrode->nTrodeId);
        }
        if (curNTrode->moduleDataChan >= curNTrode->unconverted_hw_chan.length()) {
            qDebug() << "Invalid LFP Settings (nTrode " << curNTrode->nTrodeId << ")";
            isValid = false;
            if (recordErrors)
                *errorMsg = QString("%1     -Invalid LFP Settings. (nTrode %2)\n").arg(*errorMsg).arg(curNTrode->nTrodeId);
        }
    }

    return(isValid);
}

bool WorkspaceEditor::verifyRefGroupConfiguration(QString *errorMsg){
    bool isValid = true;
    for(auto const group : spikeConfig->carGroups){
        for(auto const carchan : group.chans){
            if(!spikeConfig->ntrodes.exists(carchan.ntrodeid)){
                //ntrode doesn't exist
                isValid = false;
                *errorMsg = QString("%1     -Invalid Reference Group %2: ( NTrode %3 doesn't exist )\n").arg(*errorMsg).arg(group.description).arg(carchan.ntrodeid);
                continue;
            }
            if(!(carchan.chan > 0 && carchan.chan <= spikeConfig->ntrodes.ID(carchan.ntrodeid)->hw_chan.length())){
                //channel doesn't exist within ntrode
                isValid = false;
                *errorMsg = QString("%1     -Invalid Reference Group %2: ( Chan %3 of NTrode %4 doesn't exist )\n").arg(*errorMsg).arg(group.description).arg(carchan.chan).arg(carchan.ntrodeid);
                continue;
            }
        }
    }
    return isValid;
}

void WorkspaceEditor::printModuleConfigs() {
    qDebug() << " -- Module Configs --";
    for (int i = 0; i < moduleConfigs.length(); i++) {
        qDebug() << " -" << moduleConfigs.at(i).moduleName;
        for (int j = 0; j < moduleConfigs.at(i).moduleArguments.length(); j++) {
            qDebug() << "    +" << moduleConfigs.at(i).moduleArguments.at(j);

        }
    }
}

void WorkspaceEditor::printDeviceInfoConfigs() {
    for (int i = 0; i < deviceInfoConfigs.length(); i++) {
        qDebug() << "-Device Info:";
        qDebug() << "  -Name: " << deviceInfoConfigs.at(i).name;
        qDebug() << "  -packetOrderPref: " << deviceInfoConfigs.at(i).packetOrderPreference;
        qDebug() << "  -numBytes: " << deviceInfoConfigs.at(i).numBytes;
        qDebug() << "  -available: " << deviceInfoConfigs.at(i).available;
        qDebug() << "  -byteOffset: " << deviceInfoConfigs.at(i).byteOffset;
        for (int j = 0; j < deviceInfoConfigs.at(i).channels.length(); j++) {
            qDebug() << "         -Channel: " << deviceInfoConfigs.at(i).channels.at(j).idString;
            qDebug() << "         -dataTyp: " << (int)deviceInfoConfigs.at(i).channels.at(j).dataType;
            qDebug() << "         -startByte: " << deviceInfoConfigs.at(i).channels.at(j).startByte;
            qDebug() << "         -digitalBit: " << deviceInfoConfigs.at(i).channels.at(j).digitalBit;
            qDebug() << "         -port: " << deviceInfoConfigs.at(i).channels.at(j).port;
            qDebug() << "         -interleavedDataIDByte: " << deviceInfoConfigs.at(i).channels.at(j).interleavedDataIDByte;
            qDebug() << "         -interleavedDataIDBit: " << deviceInfoConfigs.at(i).channels.at(j).interleavedDataIDBit;
            qDebug() << "         -input: " << deviceInfoConfigs.at(i).channels.at(j).input;
            qDebug() << "         -------------------------------------------";
        }
        qDebug() << "-----------------------------------------------------------------";
        qDebug() << " ";
    }
}

void WorkspaceEditor::printNTrodeSettings(bool printHWChannels) {
    for (int i = 0; i < nTrodeConfigs.length(); i++) {
        SingleSpikeTrodeConf curNTrode = nTrodeConfigs.at(i);
        qDebug() << "NTrode " << curNTrode.nTrodeId;
        qDebug() << " -refNTrode: " << curNTrode.refNTrode;
        qDebug() << " -refNTrodeID: " << curNTrode.refNTrodeID;
        qDebug() << " -refChan: " << curNTrode.refChan;
        qDebug() << " -refChanID: " << curNTrode.refChanID;
        qDebug() << " -lowFilter: " << curNTrode.lowFilter;
        qDebug() << " -highFilter: " << curNTrode.highFilter;
        qDebug() << " -moduleDataChan: " << curNTrode.moduleDataChan;
        qDebug() << " -moduleDataHighFilter: " << curNTrode.moduleDataHighFilter;
        qDebug() << " -refOn: " << curNTrode.refOn;
        qDebug() << " -filterOn: " << curNTrode.filterOn;
        qDebug() << " -moduleDataOn: " << curNTrode.moduleDataOn;

        if (printHWChannels) {
            for (int j = 0; j < curNTrode.unconverted_hw_chan.length(); j++) {
                qDebug() << "    --hw_chan: " << curNTrode.unconverted_hw_chan.at(j);
                qDebug() << "       --converted_hw_chan: " << curNTrode.hw_chan.at(j);
                qDebug() << "       --maxDisp: " << curNTrode.maxDisp.at(j);
                qDebug() << "       --thresh: " <<curNTrode.thresh.at(j);
                qDebug() << "       --thresh_rangeconvert: " << curNTrode.thresh_rangeconvert.at(j);
                qDebug() << "       --streamingChannelLookup: " << curNTrode.streamingChannelLookup.at(j);
                qDebug() << "       --triggerOn: " << curNTrode.triggerOn.at(j);
                qDebug() << " ";
            }
        }
        qDebug() << " ";
    }
}

void WorkspaceEditor::printAvailableTags() {

}

void WorkspaceEditor::addECU(){
//    deviceSelector->setCurrentText("ECU");
    deviceSelector->setCurrentIndex(ECUIndex);
    addDevice();
}

void WorkspaceEditor::addHSSensors(){
//    deviceSelector->setCurrentText("headstageSensor");
    deviceSelector->setCurrentIndex(HSIndex);
    addDevice();
}

void WorkspaceEditor::addRF(){
//    deviceSelector->setCurrentText("RF");
    deviceSelector->setCurrentIndex(RFIndex);
    addDevice();
}

void WorkspaceEditor::removeSysClock(){
//    deviceList->setCurrentItem()
    for(int i = 0; i < deviceList->count(); ++i){
        auto item = deviceList->item(i);
        if(item->text() == "SysClock"){
            deviceList->setCurrentItem(item);
            break;
        }
    }
    removeDevice();
}

void WorkspaceEditor::displayAllAux(){
    //Aux channels tree only has 2 levels: device and channel
    //Iterate through each top level item (device), and each of their children (channels)
    for(int i = 0; i < availableAuxChannelsTree->topLevelItemCount(); ++i){
        availableAuxChannelsTree->topLevelItem(i)->setSelected(true);
        for(int j = 0; j < availableAuxChannelsTree->topLevelItem(i)->childCount(); ++j){
            availableAuxChannelsTree->topLevelItem(i)->child(j)->setSelected(true);
        }
    }
    //Set to display all channels
    auxAddChannelButtonPressed();
}

void WorkspaceEditor::setNumChannels(int numchans){
    channelSelector->setCurrentText(QString("%1 Channels").arg(numchans));
    channelTable->setNumHardwareChannels(numchans);
}

void WorkspaceEditor::configureDisplay(int columns, int pages){
    streamColumnComboBox->setCurrentText(QString::number(columns));
    streamPagesComboBox->setCurrentText(QString::number(pages));
}

void WorkspaceEditor::setSamplingRate(int samplingrate){
    editSamplingRate->setText(QString::number(samplingrate));
}
