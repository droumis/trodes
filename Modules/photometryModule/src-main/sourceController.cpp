/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "sourceController.h"
#include "globalObjects.h"

//The SourceController object acts as a wrapper for all threads that aquire data from a data stream.
//The rest of the code can interact with this wrapper regardless of which source is currenly active.

uint32_t currentTimeStamp = 0;

eegDataBuffer           rawData;
quint64                 rawDataWritten = 0; //global variable in globalObjects.h
QList <QSemaphore *>    rawDataAvailable;
QMutex                  rawDataAvailableMutex;
QString                 playbackFile;
bool                    playbackFileOpen = false;
int                     fileDataPos;
int                     playbackFileSize;
int                     playbackFileCurrentLocation;
int                     filePlaybackSpeed;  // 1 is actual speed, 2 is 2X, etc.


//---------------------------------------
//SourceController

SourceController::SourceController(QObject *){
  state = 0;
  currentSource = SourceNone;
  fileSource = NULL;
  currentSourceObj = NULL;
  currentOutputObj = NULL;
}

void SourceController::clearBuffers() {


    for (int i=0;i < EEG_BUFFER_SIZE*6*3; i++) {
        rawData.data_f[i] = 0;
    }
    for (int i=0;i < EEG_BUFFER_SIZE*8; i++) {
        rawData.digitalInfo[i] = 0;
    }

    for (int i=0;i < EEG_BUFFER_SIZE; i++) {
        rawData.timestamps[i] = 0;
        rawData.dTime[i] = 0;
    }

    rawData.writeIdx = 0;

}

void SourceController::disconnectFromSource() {

    if (currentSourceObj != NULL) {
        currentSourceObj->StopAcquisition();
    }
    if (currentOutputObj != NULL) {
        currentOutputObj->StopAcquisition();
    }

    rawDataWritten = 0; //for sound

    //There might be available semaphores, so we aquire them up

    for (int a = 0; a < rawDataAvailable.length(); a++) {
        while (rawDataAvailable[a]->available() > 0) {
            rawDataAvailable[a]->tryAcquire();
        }
    }

    //Make sure the current time is reset
    currentTimeStamp = 0;
    clearBuffers();

}

void SourceController::connectToSource() {

    numConnectionTries = 0;

    if (currentOutputObj != NULL) {
        currentOutputObj->StartAcquisition();
    }

    if (currentSourceObj != NULL) {
        currentSourceObj->StartAcquisition();
    }

}

void SourceController::setSource(DataSource source) {
    //first we kill the current source thread
    if (currentSourceObj != NULL) {
        disconnect(this,SLOT(setSourceState(int)));
        currentSourceObj->CloseInterface();
        delete currentSourceObj;
    }

    if (currentOutputObj != NULL) {
        currentOutputObj->CloseInterface();
        delete currentOutputObj;
    }

    rawDataWritten = 0;
    currentSource = source;
    //then we start the new one
    switch (source) {
    case SourceNone:
        //No Source
        currentSourceObj = NULL;
        currentOutputObj = NULL;
        emit stateChanged(SOURCE_STATE_NOT_CONNECTED);
        break;

    case SourceFile:
        //stream from file
        fileSource = new fileSourceInterface(NULL);
        currentSourceObj = fileSource;
        break;

    case SourceEthernet:
        currentSourceObj = new NIUSBDAQInterface(NULL);
        currentOutputObj = new carrierInterface(NULL);
        connect(this, SIGNAL(startRecording()), currentSourceObj, SLOT(startRecord()));
        connect(this, SIGNAL(stopRecording()), currentSourceObj, SLOT(stopRecord()));
        break;

    case SourceRhythm:
        currentSourceObj = new NIUSBDAQInterface(NULL);
        currentOutputObj = new carrierInterface(NULL);
        connect(this, SIGNAL(startRecording()), currentSourceObj, SLOT(startRecord()));
        connect(this, SIGNAL(stopRecording()), currentSourceObj, SLOT(stopRecord()));
        break;
    }

    if (source != SourceNone) {
        connect(currentSourceObj,SIGNAL(stateChanged(int)),this,SLOT(setSourceState(int)));

        currentSourceObj->InitInterface();
        connect(currentSourceObj,SIGNAL(acquisitionStarted()),this,SLOT(StartAcquisition()));
        connect(currentSourceObj,SIGNAL(acquisitionStopped()),this,SLOT(StopAcquisition()));
        currentOutputObj->InitInterface();

    }

}

void SourceController::pauseSource() {

    if (currentSource == SourceFile) {
        fileSource->PauseAcquisition();
    }
    setSourceState(SOURCE_STATE_PAUSED);

}

void SourceController::StartAcquisition(void) {
    emit acquisitionStarted(); //tells display thread to start streaming
}

void SourceController::PauseAcquisition(void) {

    rawDataWritten = 0; //for sound
    emit acquisitionPaused();

}

void SourceController::StopAcquisition(void) {

    rawDataWritten = 0; //for sound
    currentTimeStamp = 0;
    emit acquisitionStopped(); //tells display thread to stop streaming

}

void SourceController::startRecord(void) {
    emit startRecording(); //tells display thread to start streaming
}

void SourceController::stopRecord(void) {
    emit stopRecording();
}

void SourceController::dataError() {

    numConnectionTries++;

    if (numConnectionTries < 3) {
        StopAcquisition();
        qDebug() << "Data coming in wrong.... retrying connection.";
        QThread::msleep(100);
        StartAcquisition();
    } else {
        StopAcquisition();
    }

}

void SourceController::noDataComing() {
    if (currentSource != SourceFile) {
        disconnectFromSource();
    }
}

void SourceController::setSourceState(int state) {
    //pass the state along to the mainWindow
    emit stateChanged(state);
}

void SourceController::waitForThreads() {
    if (currentSource == 2) {
        //The source is a file
        fileSource->waitForThreads();
    }
}

void SourceController::SendTimeRequest(){
    emit newTimeStamp();
}

