#include "pythoncamerainterface.h"




//------------------------------------------------------------------------
PythonDataInterface::PythonDataInterface(PythonQtObjectPtr *mainContextInput):
    QObject(NULL),
    //bufferSize(200),
    //writeHead(0),
    //readHead(0),
    //totalItemsWritten(0),
    //totalItemsRead(0),
    //continuous(false),
    hasCallback(false),
    mainContext(mainContextInput)

{
    //buffer.resize(bufferSize);
    averageFramePeriod = -1.0;

}

void PythonDataInterface::setNewDataCallback(const QString &constructorCall) {
    //mainContext->evalFile(fileName);

    //mainContext->evalScript("import " + fileName + "\n");
    newDataCallbackTag = mainContext->evalScript(constructorCall, Py_eval_input);
    hasCallback = true;
}


void PythonDataInterface::writeDataPoint(const QVariant &p) {

    _currentPos = p.toPoint();
    qint64 tElapsed = timer.elapsed();
    timer.restart();


    if (tElapsed < 200) {
        if (averageFramePeriod > 0) {

            //calculate a moving average for the frame rate
            averageFramePeriod -= (averageFramePeriod/10);
            averageFramePeriod += (tElapsed/1000.0)/10;

            //send position data to python if a callback has been set up
            if (hasCallback) {
                QVariant velocity = newDataCallbackTag.call("newDataPoint", QVariantList() << p);
                emit newVelocity(velocity.toDouble());
            }

        } else {
            averageFramePeriod = 0.033; //start with a 30 fps rate (actual rate will be calculated with a moving average)
        }
    }

    //qDebug() << 1/averageFramePeriod;

}

QPoint PythonDataInterface::currentPos() {
    return _currentPos;
}

void PythonDataInterface::setSpeed(qreal s) {
    //This function is called from python after it has calculated current distance traveled (in pixels) per frame

    //qDebug() << "Speed: " << s/(7.5*averageFramePeriod);

}


//------------------------------------------------

QtToPythonCircularBuffer::QtToPythonCircularBuffer() : QObject(NULL) {

}

void QtToPythonCircularBuffer::setContext(PythonQtObjectPtr *mainContextInput) {
    dataBuffer = new PythonDataInterface(mainContextInput);
    connect(dataBuffer,SIGNAL(newVelocity(double)),this,SIGNAL(sendVelocity(double)));
}

void QtToPythonCircularBuffer::writePoint(QPoint p) {
    dataBuffer->writeDataPoint(QVariant(p));
}

void QtToPythonCircularBuffer::writePoint(QPoint pf, QPoint pb, QPoint mid) {
    dataBuffer->writeDataPoint(QVariant(mid));

    //mainContext.evalScript("import os");
}



