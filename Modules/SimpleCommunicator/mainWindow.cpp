#include "mainWindow.h"
#include "ui_mainWindow.h"
//#include "trodesDataInterface.h"


MainWindow::MainWindow(QStringList arguments, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
     qDebug().noquote() << "SimpleCommunicator Version Info:\n" << GlobalConfiguration::getVersionInfo(false); //print version info to debug log

    ui->setupUi(this);
    ui->statusbar->showMessage("Processing arguments.");

    QString serverAddress, serverPort, configFilename;
    int optionInd = 1;



    while (optionInd < arguments.length()) {

        qDebug() << "Option: " << arguments.at(optionInd);
        if (arguments.length() > optionInd+1 )
          qDebug() << "Option(+1): " << arguments.at(optionInd+1);
        if ((arguments.at(optionInd).compare("-serverAddress",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            serverAddress = arguments.at(optionInd+1);
            optionInd++;
        } else if ((arguments.at(optionInd).compare("-serverPort",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            serverPort = arguments.at(optionInd+1);
            optionInd++;
        }
        else if ((arguments.at(optionInd).compare("-trodesConfig",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            configFilename = arguments.at(optionInd+1);
            optionInd++;
        }
        optionInd++;
    }

    ui->statusbar->showMessage("Establishing Trodes interface.");

    //TrodesMessageInterface *trodesMessages = new TrodesMessageInterface(configFilename, serverAddress, serverPort);
    trodesMessages = new TrodesMessageInterface(configFilename, serverAddress, serverPort);

    QThread *msgThread = new QThread();
    trodesMessages->moveToThread(msgThread);
    connect(msgThread, &QThread::started, trodesMessages, &TrodesMessageInterface::setup);
    connect(trodesMessages, &TrodesMessageInterface::quitReceived, this, &MainWindow::close);
    connect(trodesMessages, &TrodesMessageInterface::statusMessage, this, &MainWindow::changeStatusBarMessage);
    connect(trodesMessages, &TrodesMessageInterface::newTimestamp, this, &MainWindow::changeTimestamp);
    connect(trodesMessages, &TrodesMessageInterface::newContinuousMessage, this, &MainWindow::changeContinuousMessage);
    //connect(trodesMessages, SIGNAL(eventListReceived(QVector<TrodesEvent>)), this, SLOT(printEventList(QVector<TrodesEvent>)));
    //connect(trodesMessages, SIGNAL(eventListReceived(QVector<TrodesEvent>)), this, SLOT(updateEventList(QVector<TrodesEvent>)));

    //MARK: Event
    connect(ui->reqEventListButton,SIGNAL(pressed()),this,SLOT(pressedTestButton()));

    //connect(trodesMessages->trodesModuleInterface->trodesClient,SIGNAL(currentTimeReceived(quint32)),this,SLOT(recievedTimestamp(quint32)));
    if (SHOW_LATENCY_TRACKING) {
        connect(trodesMessages,SIGNAL(numPacketsSent(int)),this,SLOT(recievedNumPacketsSent(int))); //DEBUGGING CONNECTION
    }
    connect(trodesMessages,SIGNAL(timeRecieved(quint32)),this,SLOT(recievedTimestamp(quint32)));
    connect(trodesMessages, SIGNAL(newPosition(qint16,qint16,qint16,qreal)), this, SLOT(changePosition(qint16,qint16,qint16,qreal)));
    connect(trodesMessages,SIGNAL(newVelocity(qreal)),this,SLOT(changeVelocity(qreal)));
    msgThread->start();
    changeTimestampLabel(ui->continuousClock, 0);
    changeTimestampLabel(ui->blockContClock, 0);
    changeTimestampLabel(ui->digitalIOClock, 0);

    changeTimestampLabel(ui->positionClock, 0);
    changeTimestampLabel(ui->spikesClock, 0);

    ui->positionLabel->setText("(0,0)");
    ui->velocityLabel->setText("0");
    ui->linearPosLabel->setText("");
    ui->linearSegLabel->setText("");
    packetsRecieved = 0;

    //eventHandler stuff
    iniActionList();
    eventCounter = 0;
    ui->eventNameLabel->setText("");

    eventHandler = new EventHandler(actionList, this);
    eventHandler->setUpConnections(trodesMessages->trodesModuleInterface);
    connect(eventHandler, SIGNAL(sig_executeAction(int,TrodesEvent)), this, SLOT(callMethod(int,TrodesEvent)));

    /* ***ABOUT MENU*** */
    menuHelp = new QMenu;
    menuHelp->setTitle("Help");
    actionAbout = new QAction(this);
    actionAbout->setText("About");
    actionAbout->setMenuRole(QAction::AboutRole);
    menuHelp->addAction(actionAbout);
    menuBar()->addAction(menuHelp->menuAction());
    connect(actionAbout, SIGNAL(triggered()), this, SLOT(about()));


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeTimestampLabel(QLabel *widget, quint32 time)
{
    uint s_cur, m_cur, h_cur;
    h_cur = 0;
    m_cur = 0;
    s_cur = 0;
    if (time != 0) {
        QString extract = widget->text();
        QStringList curTime = extract.split(":");
        h_cur = curTime.at(0).toInt();
        m_cur = curTime.at(1).toInt();
        s_cur = curTime.at(2).toInt();
    }

    uint seconds, minutes, hours;
    if (hardwareConf == NULL) {
        seconds = 0; minutes = 0; hours = 0;
    }
    else {
        seconds = time / (hardwareConf->sourceSamplingRate);
        minutes = (seconds / (60));
        hours = minutes / 60;
        minutes = minutes % 60;
        seconds = seconds % 60;
    }

    if ((hours > h_cur)) {
        h_cur = hours;
    }
    if ((minutes > m_cur) || (m_cur == 59 && minutes < 5) ) {
        m_cur = minutes;
    }
    if ((seconds > s_cur) || (s_cur == 59 && seconds < 5) ) {
        s_cur = seconds;
    }
    /**
    widget->setText(QString("%1:%2:%3").arg(h_cur)
                   .arg((uint)m_cur,2,10,QChar('0'))
                   .arg((uint)s_cur,2,10,QChar('0')));
                   **/
    //qDebug() << "data recieved for time index: (" << hours << ":" << minutes << ":" << seconds << ") -- " << time;
    widget->setText(QString("%1:%2:%3").arg(hours)
                   .arg((uint)minutes,2,10,QChar('0'))
                   .arg((uint)seconds,2,10,QChar('0')));
}

void MainWindow::changeStatusBarMessage(QString msg)
{
    ui->statusbar->showMessage(msg);
}

void MainWindow::changeTimestamp(quint8 dataType, quint32 time)
{
    switch (dataType) {
    case TRODESDATATYPE_CONTINUOUS: {
        changeTimestampLabel(ui->continuousClock, time);
        break;
    }
    case TRODESDATATYPE_DIGITALIO: {
        changeTimestampLabel(ui->digitalIOClock, time);
        break;
    }
    case TRODESDATATYPE_POSITION: {
        quint32 diff = 0;
        quint32 scale = 30; // 30 frames / ms
        if (time >= currentTime) {
            diff = time - currentTime;
        }
        else
            diff = currentTime - time;

        if (time > 0 && currentTime > 0 && SHOW_LATENCY_TRACKING) {
            latencyAvg.insert((diff/scale));
            qDebug() << "Position Data for time[" << time << "] recieved at time[" << currentTime << "] -Latency [" << (diff/scale) << "ms] - avg[" << latencyAvg.average() << "]";
        }

        changeTimestampLabel(ui->positionClock, time);
        break;
    }
    case TRODESDATATYPE_BLOCK_CONTINUOUS: {
        changeTimestampLabel(ui->blockContClock, time);
        break;
    }
    case TRODESDATATYPE_SPIKES: {
        changeTimestampLabel(ui->spikesClock, time);
        break;
    }
    }
}

void MainWindow::changeContinuousMessage(QString msg)
{
    ui->continuousMsg->setText(msg);
}

void MainWindow::changePosition(qint16 x, qint16 y, qint16 linSeg, qreal linPos) {
    if (x != 0 || y != 0) {
        QString newLabel, x1, y1;
        x1 = QVariant(x).toString();
        y1 = QVariant(y).toString();
        newLabel = "(" + x1 + "," + y1 + ")";
        ui->positionLabel->setText(newLabel);
    }
    if (linSeg > -1) {
        QString segment = QVariant(linSeg).toString();
        ui->linearSegLabel->setText(segment);
    }
    if (linPos >= 0.f) {
        QString linPosition = QVariant(linPos).toString();
        ui->linearPosLabel->setText(linPosition);
    }
}

void MainWindow::changeVelocity(qreal velocity) {
    QString vel;
    vel = QVariant(velocity).toString();
    ui->velocityLabel->setText(vel);
}

void MainWindow::recievedTimestamp(quint32 time) {
    currentTime = time;

}

void MainWindow::recievedNumPacketsSent(int num) {

    int difference = num-packetsRecieved;
    float percentLoss = 0.0;
    if (num > 0)
        percentLoss = ((((float)difference)/((float)num))*100);
    qDebug() << "Packets received [" << packetsRecieved << "] - Packets Sent [" << num << "] || Difference [" << difference << "] - Percent of Data Lost [" << percentLoss << "%]";
    packetsRecieved++;
}

void MainWindow::updateEventList(QVector<TrodesEvent> evList) {
    eventList.clear();
    eventList = evList;
}

void MainWindow::printEventList(QVector<TrodesEvent> evList) {
    qDebug() << "Event list (size " << evList.length() << ") received, printing:";

    for (int i = 0; i < evList.length(); i++)
        evList.at(i).printEventInfo();
}

void MainWindow::pressedTestButton() {
    qDebug() << "Pressed Button...";
    trodesMessages->trodesModuleInterface->sendEventListRequest();
    eventHandler->promptEventConnectionMenu();
    //eventConnectionMenu->show();
    //eventMenu->show();

}

void MainWindow::subscribeToEvent(int eventInd) {
    qDebug() << "subscribing to event " << eventInd << " From module " << trodesMessages->trodesModuleInterface->moduleID;
    trodesMessages->trodesModuleInterface->sendEventSubscribe(eventInd);
}

void MainWindow::printEvent(TrodesEvent ev) {
    qDebug() << "Watched Event Received: ";
    ev.printEventInfo();
}

void MainWindow::about() {
    QMessageBox::about(this, tr("About SimpleCommunicator"), tr(qPrintable(GlobalConfiguration::getVersionInfo())));
}

void MainWindow::iniActionList() {
    actionList.append("Iterate Event Counter");
    actionList.append("Show Received Event");
    actionList.append("Print Event");
}

void MainWindow::callMethod(int actionIndex, TrodesEvent event) {
    if (actionIndex > -1 && actionIndex < actionList.length()) {
        //qDebug() << "Calling Method[" << actionList.at(actionIndex) << "]";
        //Be carefull making/appending this switch statement.  Each case integer should
        //correspond to the correct action index in actionList.
        switch (actionIndex) {
        case 0:
        {
            eventCounter++;
            QString num;
            num = QVariant(eventCounter).toString();
            ui->eventCounterLabel->setText(num);
            break;
        }
        case 1:
        {
            QString eventString = QString("[%1] [%2]").arg(event.getParentModule()).arg(event.getEventName());
            ui->eventNameLabel->setText(eventString);
            break;
        }
        case 2:
        {
            printEvent(event);
            break;
        }
        default:
            qDebug() << "Error: Invalid function called. (MainWindow::callMethod)";
            break;
        }


    }
}
