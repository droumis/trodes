#include "ntrodechannelmappingwindow.h"

NTrodeChannelMappingWindow::NTrodeChannelMappingWindow(int numchans, QWidget *parent)
    : QDialog (parent)
{
    table = new NTrodeChannelMappingTable;
    table->setNumHardwareChannels(numchans);

    QDialogButtonBox *buttons = new QDialogButtonBox(QDialogButtonBox::Ok|
                                                     QDialogButtonBox::Cancel |
                                                     QDialogButtonBox::Help);
    QHBoxLayout *tableslayout = new QHBoxLayout;
    tableslayout->addWidget(table);

    QHBoxLayout *buttonslayout = new QHBoxLayout;
    buttonslayout->addWidget(buttons);
    connect(buttons, &QDialogButtonBox::accepted, this, &NTrodeChannelMappingWindow::okpressed);
    connect(buttons, &QDialogButtonBox::rejected, this, &QDialog::reject);
    connect(buttons, &QDialogButtonBox::helpRequested, this, [this](){
        QMessageBox::information(this, "Help", "Assign each hardware channel to an nTrode. Each ntrode can contain any number of channels (such as singles or tetrodes). You can also copy paste from an Excel sheet. ");
    });

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addLayout(tableslayout);
    layout->addLayout(buttonslayout);
    setLayout(layout);
    setWindowFlag(Qt::Dialog);
    setWindowModality(Qt::WindowModal);
    setMinimumWidth(300);
    setMinimumHeight(500);
}

QList<int> NTrodeChannelMappingWindow::getChannelMappings()
{
    return table->getChannelAssignments();
}

void NTrodeChannelMappingWindow::okpressed()
{
    QList<int> chans = table->getChannelAssignments();
    QString emptychans;
    for(int i=0; i < chans.length(); ++i){
        if(chans[i] == -1){
            emptychans.append(QString::number(i)+", ");
        }
    }
    if(emptychans.length()){
        QMessageBox::warning(this, "Unassigned channels", QString("Channels [%1] are unassigned!").arg(emptychans));
        return;
    }
    else{
        accept();
    }
}
