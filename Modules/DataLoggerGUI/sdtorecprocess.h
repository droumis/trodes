#ifndef SDTORECPROCESS_H
#define SDTORECPROCESS_H

#include "abstractprocess.h"

class SdtorecProcess : public AbstractProcess
{
    Q_OBJECT
public:
    explicit SdtorecProcess(QString datfile, QString trodesconf, QString outfile, int numchan, QWidget *parent=0);
    void start();
private:
    QString datFile;
    QString trodesConf;
    QString outFile;
    int numChan;
    QString temppath;
    void start_sdtorec();
    void moveFile(int code);
};

#endif // SDTORECPROCESS_H
