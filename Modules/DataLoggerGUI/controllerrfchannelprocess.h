#ifndef CONTROLLERRFCHANNELPROCESS_H
#define CONTROLLERRFCHANNELPROCESS_H

#include "abstractprocess.h"

class ControllerRFChannelProcess: public AbstractProcess
{
    Q_OBJECT
public:
    ControllerRFChannelProcess(QWidget *parent=nullptr);//get
    ControllerRFChannelProcess(uint8_t rfchan, QWidget *parent=nullptr);//get and set
    void start();

    bool mcu_success;
    bool dock_success;
private:
    bool settingrfchan;
    uint8_t input_rfchan;
    void mcu_start_rfchan();
    void dock_start_rfchan();
protected:
    void customReadOutput(const QString &line);
};

#endif // CONTROLLERRFCHANNELPROCESS_H
