#ifndef PCHECKPROCESS_H
#define PCHECKPROCESS_H

#include "abstractprocess.h"

class PcheckProcess : public AbstractProcess
{
    Q_OBJECT
public:
    explicit PcheckProcess(QString device, QWidget *parent = nullptr);
    void start();
signals:
public slots:
private:
    QString device;
//    QProcess *pcheckprocess;
//    QTextEdit *console;
//    void readOutput();
    void win_start_pcheck();
    void lin_start_pcheck();
    void mac_start_pcheck();
    void docking_pcheck();
};

#endif // PCHECKPROCESS_H
