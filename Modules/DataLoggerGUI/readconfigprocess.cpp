#include "readconfigprocess.h"

ReadConfigProcess::ReadConfigProcess(QString device, QWidget *parent)
    : AbstractProcess ("ReadConfig", parent), device(device)
{
}

void ReadConfigProcess::start(){
    if(device == "Docking Station"){
        docking_readconfig();
    }
    else{
        sd_readconfig();
    }
}

void ReadConfigProcess::sd_readconfig(){
#if defined(Q_OS_WIN)
    QRegExp rex("(.*)([0-9]{1,3})"); //Takes the last digits of device string (PhysicalDrive1) -> 1
    rex.indexIn(device);
    QStringList args;
    args << rex.cap(2);

    process->start(".\\windows_sd_util\\readConfig.exe", args);
#elif defined(Q_OS_MAC)
    process->start("./macos_sd_util/readConfig", {device});
#elif defined(Q_OS_LINUX)
    process->start("./linux_sd_util/readConfig", {device});
#else
#error "OS not supported!"
#endif
}

void ReadConfigProcess::customReadOutput(const QString &line){
}

void ReadConfigProcess::docking_readconfig(){
    process->start(DOCKINGPATH, {"-r"});
}
