#include "sdtorecprocess.h"

SdtorecProcess::SdtorecProcess(QString datfile, QString trodesconf, QString outfile, int numchan, QWidget *parent)
    : AbstractProcess("SD to Rec", parent), datFile(datfile), trodesConf(trodesconf), outFile(outfile), numChan(numchan)
{
//    connect(this, &SdtorecProcess::processfinished, this, &SdtorecProcess::moveFile);
}

void SdtorecProcess::start(){
    start_sdtorec();
}

void SdtorecProcess::start_sdtorec(){
    QStringList args;
    args << "-sd" << QFileInfo(datFile).filePath();
//    QString tempname = "temp_"+QDate::currentDate().toString("ddMMyy") + "_" + QTime::currentTime().toString("hhmmss");
//    temppath = QFileInfo(datFile).dir().absolutePath() + QDir::separator() + tempname + ".rec";
//    qDebug() << temppath;
    args << "-numchan" << QString::number(numChan);
    args << "-mergeconf" << QFileInfo(trodesConf).filePath();
    args << "-output" << QFileInfo(outFile).baseName();
    args << "-outputdirectory" << QFileInfo(outFile).absoluteDir().absolutePath() + "/";
//    qDebug() << QString(".")+QDir::separator() + "sdtorec" << args;
    emit newOutputLine(QString(".") + QDir::separator() + "sdtorec "+args.join(" "));
    process->start(QString(".")+QDir::separator() + "sdtorec", args);
}

void SdtorecProcess::moveFile(int code){
//    if(code == 0){
//        QDir dummy;
//        QFile out(outFile);
//        if(out.exists()){
//            out.remove();
//        }
//        if(!dummy.rename(temppath, outFile)){
//            emit processfinished(-1);
//        }
//    }
}
