#include "filebrowser.h"

FileBrowser::FileBrowser(const QString name, const QString baseDir, bool showfiles, QWidget *parent)
    : QWidget(parent),
      myName(name),
      basePath(baseDir),
      selectedPath(baseDir)
{
    selectedDirLabel = new QLabel();
    dirmodel = new QFileSystemModel();
//    if(showfiles){
//        dirmodel->setFilter(QDir::AllEntries|QDir::NoDot|QDir::AllDirs);
//    }
//    else{
//        dirmodel->setFilter(QDir::Dirs|QDir::Drives|QDir::NoDot|QDir::AllDirs);
//    }
    fileBrowser = new QTreeView();
    fileBrowser->setModel(dirmodel);
    fileBrowser->setRootIndex(dirmodel->index(basePath));
//    fileBrowser->setSelectionMode(QAbstractItemView::SingleSelection);
//    fileBrowser->setColumnHidden(1, true);
//    fileBrowser->setColumnHidden(2, true);
//    fileBrowser->setStyleSheet( "QTreeView::branch {  border-image: url(none.png); }" );
//    fileBrowser->setItemsExpandable(false);
    connect(fileBrowser, &QTreeView::clicked, this, &FileBrowser::rowClicked);
    selectedDirLabel->setText(QString(myName + ": %1").arg(basePath));

//    connect(fileBrowser, &QTreeView::doubleClicked, this, &FileBrowser::rowDoubleClicked);
    layout = new QVBoxLayout();
    layout->addWidget(selectedDirLabel);
    layout->addWidget(fileBrowser);

    //Set sortable, but ideally done after directory is loaded in
    dirmodel->setRootPath(basePath);
    connect(dirmodel, &QFileSystemModel::directoryLoaded, this, [this](const QString &){
        fileBrowser->setSortingEnabled(true);
        fileBrowser->sortByColumn(0, Qt::AscendingOrder);
    });
    setLayout(layout);
}

QString FileBrowser::getBasePath() const{
    return basePath;
}

void FileBrowser::setBasePath(const QString &value){
    basePath = value;
}

QString FileBrowser::getSelectedPath() const{
    return selectedPath;
}

void FileBrowser::setSelectedPath(const QString &value){
    selectedPath = value;
}

QString FileBrowser::resolvePath(const QString &path){
    QRegExp regex("(.*\\/)\\.\\.");
    if(regex.exactMatch(path)){
        QDir dir(regex.capturedTexts().first());
        if(dir == QDir::root()){
            return QDir::root().absolutePath();
        }
        else{
            return dir.absolutePath();
        }
    }
    return path;
}

void FileBrowser::navigateToPath(const QString &path){
    QFileInfo file(path);
    QDir dir = file.dir();
    if(file.exists()){
        do{
            fileBrowser->expand(dirmodel->index(dir.path()));
        } while(dir.cdUp());
        fileBrowser->setCurrentIndex(dirmodel->index(path));
        rowClicked(dirmodel->index(path));
    }
    else{
        qDebug() << "COULD NOT NAVIGATE TO" << path;
    }
}

void FileBrowser::rowClicked(const QModelIndex &index){
    selectedPath = resolvePath(dirmodel->filePath(index));
    selectedDirLabel->setText(QString(myName + ": %1").arg(selectedPath));
    emit pathSelected(selectedPath);
}

void FileBrowser::rowDoubleClicked(const QModelIndex &index){
    QString path = resolvePath(dirmodel->filePath(index));
    rowClicked(dirmodel->index(path));
    if(!QFileInfo(path).isDir()){
        path = QFileInfo(path).absoluteDir().path();
    }
    fileBrowser->setRootIndex(dirmodel->index(path));
    fileBrowser->sortByColumn(0, Qt::AscendingOrder);
}

int FileBrowser::getColumnWidth() const{
    return fileBrowser->columnWidth(0);
}

void FileBrowser::setColumnWidth(int w){
    fileBrowser->setColumnWidth(0, w);
}
