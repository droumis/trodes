#include "dockingquickstart.h"
#include "quicksetup.h"
#include "workspaceeditordialog.h"

DockingSettings::DockingSettings(LoggerRecording rec, QWidget *parent)
    : QWidget (parent)
{
    //==============================================
    // Headstage settings
    //==============================================
    QGroupBox *hsbox = new QGroupBox("Headstage Settings");
    HardwareSettingsDisplay *disp = new HardwareSettingsDisplay(HardwareControllerSettings(), rec.settings);
    hsbox->setLayout(disp->hssettingsbox->layout());
    disp->mcusettingsbox->hide();

    //==============================================
    // Recording settings
    //==============================================
    QGroupBox *recDisp = new QGroupBox("Recording data");
    QGridLayout *recLayout = new QGridLayout;
    recDisp->setLayout(recLayout);

    QLabel *maxpacketscapTime = new QLabel("Max capacity (time): ");
    QLabel *MaxPacketsLabelTime = new QLabel((!rec.settings.valid) ? "-" :
                QTime::fromMSecsSinceStartOfDay(1000.0*rec.maxPackets/(double)rec.settings.samplingRate).toString("h'h' m'm' ss.zzz's'"));
    recLayout->addWidget(maxpacketscapTime, 0, 0);
    recLayout->addWidget(MaxPacketsLabelTime, 0, 1, Qt::AlignRight);

    QLabel *recordedpacketsTime = new QLabel("Recorded data (time): ");
    QLabel *RecordedPacketsLabelTime = new QLabel((!rec.settings.valid) ? "-" :
                QTime::fromMSecsSinceStartOfDay(1000.0*rec.recordedPackets/(double)rec.settings.samplingRate).toString("h'h' m'm' ss.zzz's'"));
    recLayout->addWidget(recordedpacketsTime, 1, 0);
    recLayout->addWidget(RecordedPacketsLabelTime, 1, 1, Qt::AlignRight);

    QLabel *droppedpacketsTime = new QLabel("Dropped data (time): ");
    QLabel *DroppedPacketsLabelTime = new QLabel((!rec.settings.valid) ? "-" :
                QTime::fromMSecsSinceStartOfDay(1000.0*rec.droppedPackets/(double)rec.settings.samplingRate).toString("h'h' m'm' ss.zzz's'"));
    recLayout->addWidget(droppedpacketsTime, 2, 0);
    recLayout->addWidget(DroppedPacketsLabelTime, 2, 1, Qt::AlignRight);

    //spacer
    recLayout->addWidget(new QLabel("\t\t"), 3, 0);

    QLabel *maxpacketscap = new QLabel("Max capacity (packets): ");
    QLabel *MaxPacketsLabel = new QLabel(QString::number(rec.maxPackets));
    recLayout->addWidget(maxpacketscap, 4, 0);
    recLayout->addWidget(MaxPacketsLabel, 4, 1, Qt::AlignRight);

    QLabel *recordedpackets = new QLabel("Recorded data (packets): ");
    QLabel *RecordedPacketsLabel = new QLabel(QString::number(rec.recordedPackets));
    recLayout->addWidget(recordedpackets, 5, 0);
    recLayout->addWidget(RecordedPacketsLabel, 5, 1, Qt::AlignRight);

    QLabel *droppedpackets = new QLabel("Dropped data (packets): ");
    QLabel *DroppedPacketsLabel = new QLabel(QString::number(rec.droppedPackets));
    recLayout->addWidget(droppedpackets, 6, 0);
    recLayout->addWidget(DroppedPacketsLabel, 6, 1, Qt::AlignRight);

    //spacer
    recLayout->addWidget(new QLabel("\t\t"), 7, 0);

    QLabel *datetime = new QLabel("Recording Date/Time: ");
    QLabel *DateTimeLabel = new QLabel(rec.recordingDateTime);
    recLayout->addWidget(datetime, 8, 0);
    recLayout->addWidget(DateTimeLabel, 8, 1, Qt::AlignRight);

    QVBoxLayout *mainlayout = new QVBoxLayout;
    setLayout(mainlayout);
    mainlayout->addWidget(hsbox);
    mainlayout->addWidget(recDisp);
}

DockingQuickstart::DockingQuickstart(LoggerRecording rec, QWidget *parent) : QWidget(parent), rec(rec)
{
    DockingSettings *settings = new DockingSettings(rec);


    //==============================================
    // User input buttons/file entry
    //==============================================
    QPushButton *browsemapping = new QPushButton("Browse mapping...");
    chanmapLineEdit = new QLineEdit();
    QPushButton *manualChanMap = new QPushButton("Configure manually");

    QPushButton *saveasButton = new QPushButton("Save As...");
    fileLineEdit = new QLineEdit(QApplication::applicationDirPath() + QString("/temp_%1.rec").arg(QDateTime::currentDateTime().toString("MMddyyyy_hhmm")));
    connect(saveasButton, &QPushButton::released, this, &DockingQuickstart::SelectRecPath);

    QPushButton *cancelButton = new QPushButton("Cancel");
    QPushButton *acceptButton = new QPushButton("Ok");
    connect(cancelButton, &QPushButton::released, this, &DockingQuickstart::close);
    connect(acceptButton, &QPushButton::released, this, &DockingQuickstart::AcceptAndStart);

    QGridLayout *chanmaplayout = new QGridLayout;
    chanmaplayout->addWidget(browsemapping, 1, 0, Qt::AlignLeft);
    chanmaplayout->addWidget(chanmapLineEdit, 1, 1);
    chanmaplayout->addWidget(manualChanMap, 2, 0);
    QGroupBox *channelMappingGroup = new QGroupBox("Channel Mapping");
    channelMappingGroup->setLayout(chanmaplayout);

    QHBoxLayout *filelayout = new QHBoxLayout;
    filelayout->addWidget(saveasButton, 0, Qt::AlignLeft);
    filelayout->addWidget(fileLineEdit, 1);
    QGroupBox *extractGroup = new QGroupBox("Extract Data");
    extractGroup->setLayout(filelayout);

    QHBoxLayout *btnlayout = new QHBoxLayout;
    btnlayout->addWidget(cancelButton);
    btnlayout->addWidget(acceptButton);

    //==============================================
    // Overall Layout
    //==============================================

    QVBoxLayout *mainlayout = new QVBoxLayout;

    mainlayout->addWidget(settings);
    mainlayout->addWidget(channelMappingGroup);
    mainlayout->addWidget(extractGroup);
    mainlayout->addLayout(btnlayout);

    setLayout(mainlayout);

    if(parent == nullptr){
        setAttribute(Qt::WA_DeleteOnClose);
        setWindowModality(Qt::ApplicationModal);
    }

    loadSettings();
    show();
}

DockingQuickstart::~DockingQuickstart(){
    saveSettings();
}

void DockingQuickstart::loadSettings(){
    QSettings settings("SpikeGadgets", "DataLoggerGUI");
    settings.beginGroup("Dockingquickstart");

    settings.endGroup();
}

void DockingQuickstart::saveSettings(){
    QSettings settings("SpikeGadgets", "DataLoggerGUI");
    settings.beginGroup("Dockingquickstart");

    settings.endGroup();
}

void DockingQuickstart::AcceptAndStart(){
    QString tempRootName = fileLineEdit->text().remove(".rec");
    WorkspaceEditorDialog *d = new WorkspaceEditorDialog();
    d->fillInWorkspace(rec.settings, false, false, 4, rec.settings.packetSize, false);
    d->workspaceGui->saveToXML(tempRootName+".trodesconf");
    emit okPressed(tempRootName);
    close();
}

void DockingQuickstart::SelectRecPath(){
    QString selectedpath = QFileDialog::getSaveFileName(this, "Select Trodes .rec to merge with",
                                                        "",
                                                        "Recording files (*.rec)");
    if(!selectedpath.endsWith(".rec")){
        selectedpath.append(".rec");
    }

    if(!selectedpath.remove(".rec").isEmpty()){
        fileLineEdit->setText(selectedpath);
    }
}
