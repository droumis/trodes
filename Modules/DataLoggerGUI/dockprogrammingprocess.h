#ifndef DOCKPROGRAMMINGPROCESS_H
#define DOCKPROGRAMMINGPROCESS_H

#include "abstractprocess.h"

class DockProgrammingProcess : public AbstractProcess{
    Q_OBJECT
public:
    DockProgrammingProcess(QString firmware);
    void start();

private:
    QString device;
    QString firmwarefile;
    void win_start_program();
    void lin_start_program();
    void mac_start_program();
};

#endif // DOCKPROGRAMMINGPROCESS_H
