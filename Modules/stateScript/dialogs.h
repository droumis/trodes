/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef DIALOGS_H
#define DIALOGS_H

#include <QDialog>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QComboBox>
#include <QtSerialPort/QSerialPort>
#include <QFileDialog>
#include "trodesSocket.h"
#include "sharedtrodesstyles.h"


//SocketDialog is used to connect to a TCP server
class CameraDialog : public QWidget {

Q_OBJECT

public:
    CameraDialog(bool connected, QString programPath, QStringList arguments, QWidget *parent = 0);


private:

    QLineEdit* pathEdit;
    QLineEdit* argumentsEdit;

    QPushButton* endButton;
    QPushButton* startButton;

    QLabel* pathDisplay;
    QLabel* argumentsDisplay;

    QString currentPath;
    QStringList currentArguments;

private slots:

    void startButtonPressed();
    void endButtonPressed();
    //void updateValues(int);

public slots:
    void connectionSuccessfull();
    //void disconnectButtonPressed();

protected:
    void closeEvent(QCloseEvent* event);

signals:

    void windowOpenState(bool);
    void cameraStart(QString path, QStringList arguments);
    void cameraStop();
    void windowClosed();

};

//SocketDialog is used to connect to a TCP server
class SocketDialog : public QWidget {

Q_OBJECT

public:
    SocketDialog(TrodesClient* client ,bool connected, QWidget *parent = 0);


private:

    QLineEdit* addressEdit;
    QLineEdit* portEdit;

    QComboBox* addressSelector;
    QPushButton* disconnectButton;
    QPushButton* connectButton;
    QLabel* addressDisplay;
    QLabel* portDisplay;
    QString currentAddress;
    quint16 currentPort;

    QStringList trodesHosts;

private slots:

    void connectButtonPressed();
    void updateValues(int);

public slots:
    void connectionSuccessfull();
    void disconnectButtonPressed();

protected:
    void closeEvent(QCloseEvent* event);

signals:

    void windowOpenState(bool);
    void tcpConnect(QString, quint16);
    void disconnect();
    void windowClosed();

};

//---------------------------------------------------------------
//ConnectDialog is used to conenct to a serial port
//This is how we communicate with the stateScript hardware

class ConnectDialog : public QWidget {

Q_OBJECT

public:
    ConnectDialog(QString currentPortIn,bool connected, QWidget *parent = 0);


private:

    //PortComboBox*   portSelect;
    QComboBox*   portSelect;
    QPushButton* disconnectButton;
    QPushButton* connectButton;
    QLabel* portDisplay;
    QString currentPort;

private slots:

    void connectButtonPressed();
    void disconnectButtonPressed();
    void  fillPortsInfo();

public slots:
    void connectionSuccessfull();

protected:
    void closeEvent(QCloseEvent* event);

signals:

    void windowOpenState(bool);
    void connectButtonPressed(QString);
    void disconnect();
    void windowClosed();

};


//SocketDialog is used to connect to a TCP server
class LanguageDialog : public QWidget {

Q_OBJECT

public:
    LanguageDialog(QString currentInitSelection, QWidget *parent = 0);


private:

    QLineEdit* pathEdit;


    QComboBox* languageSelector;

    QPushButton* disconnectButton;
    QPushButton* connectButton;

    QPushButton* okButton;
    QPushButton* cancelButton;

    QPushButton* selectPathButton;

    QLabel* pathDisplay;

    QString currentInitSelection;
    QString currentPath;

    QStringList availableLanguages;
    QStringList languagePaths;

private slots:

    void connectButtonPressed();
    void okButtonPressed();
    void cancelButtonPressed();
    void disconnectButtonPressed();
    void openFileDialog();
    void updateValues(int);
    void saveCurrentPath();

public slots:
    void connectionSuccessfull();

protected:
    void closeEvent(QCloseEvent* event);

signals:

    void startEngine(QString language, QString path, QString initCommand);
    void setLanguage(QString language, QString path);
    void windowOpenState(bool);
    void disconnect();
    void windowClosed();

};

/*class TrodesButton : public QPushButton {

Q_OBJECT

public:
    TrodesButton(QWidget *parent = 0);

};

*/


#define NUM_MACROS 12
class macroDialog : public QDialog
{
public:
    macroDialog(QWidget *parent = 0);

    QGridLayout *macroLayout;
    QLabel      *descriptionLabel,*codeLabel;
    QLineEdit   *descriptionEdits[NUM_MACROS],*codeEdits[NUM_MACROS];

};



#endif // DIALOGS_H
