#ifndef HARDWARESETTINGS_H
#define HARDWARESETTINGS_H

#include <stdint.h>
#include <QtGlobal>
class HeadstageSettings {

public:
    bool valid;

    quint8 minorVersion;
    quint8 majorVersion;

    uint16_t numberOfChannels;
    uint16_t hsTypeCode;
    uint16_t hsSerialNumber;
    uint16_t packetSize;

    quint8 rfChannel;
//    quint8 statusCode2;
//    quint8 statusCode3;
    quint16 samplingRate;
//    quint8 statusCode4;
    quint8 auxbytes;
//    quint8 statusCode5;
//    quint8 statusCode6;
    quint8 sensorboardVersion;
//    quint8 statusCode7;
    quint8 statusCode8;

    bool sample12bitAvailable;
    bool sample20khzAvailable;
    bool sample12bitOn;
    bool sample20khzOn;

    bool autoSettleOn;
    uint16_t percentChannelsForSettle;
    uint16_t threshForSettle;

    bool accelSensorOn;
    bool gyroSensorOn;
    bool magSensorOn;

    bool smartRefOn;

    bool smartRefAvailable;
    bool autosettleAvailable;
    bool accelSensorAvailable;
    bool gyroSensorAvailable;
    bool magSensorAvailable;

    bool rfAvailable;

    bool waitForStartOverride;
    bool cardEnableCheckOverride;

    HeadstageSettings() {
        valid = false; //Indicates if values have been filled

        minorVersion=0;
        majorVersion=0;

        numberOfChannels=0;
        hsTypeCode=0;
        hsSerialNumber=0;
        packetSize=0;


        autoSettleOn=false;
        percentChannelsForSettle=0;
        threshForSettle=0;

        accelSensorOn=false;
        gyroSensorOn=false;
        magSensorOn=false;

        smartRefOn=false;

        smartRefAvailable=false;
        autosettleAvailable=false;
        accelSensorAvailable=false;
        gyroSensorAvailable=false;
        magSensorAvailable=false;
        rfAvailable=false;
        waitForStartOverride=false;
        cardEnableCheckOverride=false;

        rfChannel=0;
        samplingRate = 0;
        sample12bitOn = false;
        sample12bitAvailable = false;
        sample20khzOn = false;
        sample20khzAvailable = false;
        auxbytes = 0;
//        statusCode2=0;
//        statusCode3=0;
//        statusCode4=0;
//        statusCode5=0;
//        statusCode6=0;
//        statusCode7=0;
        statusCode8=0;
    }
    //Overload == to comapare the following fields:
    bool operator==(const HeadstageSettings& b) {
        return ( (this->autoSettleOn==b.autoSettleOn) &&
                 (this->percentChannelsForSettle==b.percentChannelsForSettle) &&
                 (this->threshForSettle==b.threshForSettle) &&
                 (this->accelSensorOn==b.accelSensorOn) &&
                 (this->gyroSensorOn==b.gyroSensorOn) &&
                 (this->magSensorOn==b.magSensorOn) &&
                 (this->smartRefOn==b.smartRefOn) &&
                 (this->smartRefAvailable==b.smartRefAvailable) &&
                 (this->autosettleAvailable==b.autosettleAvailable) &&
                 (this->accelSensorAvailable==b.accelSensorAvailable) &&
                 (this->gyroSensorAvailable==b.gyroSensorAvailable) &&
                 (this->magSensorAvailable==b.magSensorAvailable) &&
                 (this->rfAvailable==b.rfAvailable) &&
                 (this->waitForStartOverride==b.waitForStartOverride) &&
                 (this->cardEnableCheckOverride==b.cardEnableCheckOverride));

    }
};

class HardwareControllerSettings {

public:
    bool valid; //Indicates if values have been filled

    quint8 minorVersion;
    quint8 majorVersion;
    uint16_t modelNumber;
    uint16_t serialNumber;
    uint16_t packetSize; //estimated automatically based on connected devices
    quint8 rfChannel;
    quint8 samplingRateKhz;
    quint8 hardwareDetect;
    quint8 statusCode4;
    quint8 statusCode5;
    quint8 statusCode6;
    quint8 statusCode7;
    quint8 statusCode8;

    bool ECUDetected;
    bool RFDetected;

    HardwareControllerSettings() {
      valid = false;
      minorVersion=0;
      majorVersion=0;
      modelNumber=0;
      serialNumber=0;
      packetSize=0;
      rfChannel=0;
      samplingRateKhz=0;
      hardwareDetect=0;
      ECUDetected = false;
      RFDetected = false;
      statusCode4=0;
      statusCode5=0;
      statusCode6=0;
      statusCode7=0;
      statusCode8=0;
    }

};


#endif // HARDWARESETTINGS_H
