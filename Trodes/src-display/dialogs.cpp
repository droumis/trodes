/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "globalObjects.h"
#include "dialogs.h"

#include <cmath>
#include <cfloat>
#include <QtGui>
#include <QGridLayout>
#include <QtAlgorithms>


//Dialog used to show export progress
ExportProgressDialog::ExportProgressDialog(QString exportProgram, QStringList argumentList, QWidget *) {
    QGridLayout *widgetLayout = new QGridLayout();
    console = new QTextEdit();
    console->setReadOnly(true);
    widgetLayout->addWidget(console,0,0);

    QGridLayout *buttonLayout = new QGridLayout();
    abortButton = new QPushButton("Abort");
    buttonLayout->addWidget(abortButton,0,1);
    buttonLayout->setColumnStretch(0,1);
    widgetLayout->addLayout(buttonLayout,1,0);

    setLayout(widgetLayout);
    setWindowTitle(tr("Export progress"));

    //Exporting is done by starting a standalone program
    exportProcess = new QProcess(this);

    connect(exportProcess,SIGNAL(readyReadStandardOutput()),this,SLOT(readDataFromProcess()));
    connect(exportProcess,SIGNAL(readyReadStandardError()),this,SLOT(readDataFromProcess()));
    connect(exportProcess,SIGNAL(finished(int)),this,SLOT(programFinished(int)));
    connect(abortButton,SIGNAL(released()),this,SLOT(abortButtonPushed()));


    //connect(exportProcess,SIGNAL(started()),this,SLOT(exportProgramStarted()));



    //qDebug() << "Starting export program" << exportProgram;
    exportProcess->start(exportProgram,argumentList);

}

void ExportProgressDialog::readDataFromProcess() {

    QByteArray tmpProcessOutput;
    QByteArray processErrorOutput;
    processErrorOutput.append(exportProcess->readAllStandardError());
    tmpProcessOutput.append(exportProcess->readAllStandardOutput());

    QTextCursor cursor(console->textCursor());
    cursor.movePosition(QTextCursor::End);
    console->setTextCursor(cursor);
    console->insertPlainText(QString(processErrorOutput));
    console->insertPlainText(QString(tmpProcessOutput));

    QScrollBar *bar = console->verticalScrollBar();
    bar->setValue(bar->maximum());

}

void ExportProgressDialog::programFinished(int exitCode) {
    abortButton->setText("Close");
}

void ExportProgressDialog::abortButtonPushed() {
    exportProcess->terminate();
    close();
}


ExportDialog::ExportDialog(QWidget *) {

    QGridLayout *widgetLayout = new QGridLayout();
    QFont labelFont;
    labelFont.setPixelSize(12);
    labelFont.setFamily("Console");

    //Create the spike export controls
    spikeBox = new QGroupBox(tr("Spikes"),this);
    triggerSelector = new QComboBox();
    triggerSelector->setFont(labelFont);
    triggerSelector->addItem("Current thresholds");
    //triggerSelector->addItem("Standard deviation: 3");
    //triggerSelector->addItem("Standard deviation: 4");
    //triggerSelector->addItem("Standard deviation: 5");

    noiseRemoveSelector = new QComboBox();
    noiseRemoveSelector->setFont(labelFont);
    noiseRemoveSelector->addItem("None");
    //noiseRemoveSelector->addItem("Common events");

    triggerModeLabel = new QLabel("Trigger mode");
    triggerModeLabel->setFont(labelFont);
    noiseLabel = new QLabel("Noise exclusion");
    noiseLabel->setFont(labelFont);
    QGridLayout *spikeBoxLayout = new QGridLayout();
    spikeBoxLayout->addWidget(triggerModeLabel,0,0);
    spikeBoxLayout->addWidget(noiseLabel,0,1);
    spikeBoxLayout->addWidget(triggerSelector,1,0);
    spikeBoxLayout->addWidget(noiseRemoveSelector,1,1);
    spikeBox->setLayout(spikeBoxLayout);
    spikeBox->setCheckable(true);
    spikeBox->setChecked(true);
    //spikeBox->setFixedHeight();
    widgetLayout->addWidget(spikeBox,0,0);

    //Create the ModuleData export controls
    ModuleDataBox = new QGroupBox(tr("ModuleData"),this);
    ModuleDataChannelSelector = new QComboBox();
    ModuleDataChannelSelector->setFont(labelFont);
    ModuleDataChannelSelector->addItem("One per nTrode");
    ModuleDataChannelSelector->addItem("All channels");
    ModuleDataFilterSelector = new QComboBox();
    ModuleDataFilterSelector->setFont(labelFont);
    ModuleDataFilterSelector->setEnabled(false);
    ModuleDataChannelLabel = new QLabel("Channels");
    ModuleDataChannelLabel->setFont(labelFont);
    ModuleDataFilterLabel = new QLabel("Filter");
    ModuleDataFilterLabel->setFont(labelFont);
    QGridLayout *ModuleDataBoxLayout = new QGridLayout();
    ModuleDataBoxLayout->addWidget(ModuleDataChannelLabel,0,0);
    ModuleDataBoxLayout->addWidget(ModuleDataFilterLabel,0,1);
    ModuleDataBoxLayout->addWidget(ModuleDataChannelSelector,1,0);
    ModuleDataBoxLayout->addWidget(ModuleDataFilterSelector,1,1);
    ModuleDataBox->setLayout(ModuleDataBoxLayout);
    ModuleDataBox->setCheckable(true);
    ModuleDataBox->setChecked(false);
    widgetLayout->addWidget(ModuleDataBox,1,0);
    ModuleDataBox->setEnabled(false);

    //Add the buttons
    QGridLayout *buttonLayout = new QGridLayout();
    cancelButton = new QPushButton("Cancel");
    exportButton = new QPushButton("Export");
    buttonLayout->addWidget(cancelButton,0,1);
    buttonLayout->addWidget(exportButton,0,2);
    buttonLayout->setColumnStretch(0,1);
    widgetLayout->addLayout(buttonLayout,2,0);

    //Add the progressbar
    progressBar = new QProgressBar();
    progressBar->setVisible(false);
    widgetLayout->addWidget(progressBar,3,0);

    connect(cancelButton,SIGNAL(clicked()),this,SLOT(cancelButtonPushed()));
    connect(exportButton,SIGNAL(clicked()),this,SLOT(exportButtonBushed()));


    setLayout(widgetLayout);
    setWindowTitle(tr("Export Settings"));

}

void ExportDialog::cancelButtonPushed() {
    emit exportCancelled();
    emit closing();
    this->close();
}

void ExportDialog::exportButtonBushed() {
    progressBar->setVisible(true);
    progressBar->setMinimum(0);
    progressBar->setMaximum(playbackFileSize);
    progressBar->setValue(playbackFileCurrentLocation);
    progressCheckTimer = new QTimer(this);
    progressCheckTimer->setInterval(2000);
    connect(progressCheckTimer,SIGNAL(timeout()),this,SLOT(timerExpired()));

    filePlaybackSpeed = 10; //Fast-forward speed
    emit  startExport(spikeBox->isChecked(), ModuleDataBox->isChecked(), triggerSelector->currentIndex(), noiseRemoveSelector->currentIndex(), ModuleDataChannelSelector->currentIndex(), ModuleDataFilterSelector->currentIndex());

    progressCheckTimer->start();

}

void ExportDialog::timerExpired() {

    //filePlaybackSpeed++;
    progressBar->setValue(playbackFileCurrentLocation);
}


soundDialog::soundDialog(int currentGain, int currentThresh, QWidget *parent)
    :QWidget(parent) {

    TrodesFont dispFont;

    gainSlider = new QSlider(Qt::Vertical);
    gainSlider->setMaximum(100);
    gainSlider->setMinimum(0);
    gainSlider->setSingleStep(1);
    gainSlider->setValue(currentGain);
    gainSlider->setFont(dispFont);
    gainSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    threshSlider = new QSlider(Qt::Vertical);
    threshSlider->setMaximum(100);
    threshSlider->setMinimum(0);
    threshSlider->setSingleStep(1);
    threshSlider->setValue(currentThresh);
    threshSlider->setFont(dispFont);
    threshSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    gainDisplay = new QLabel;
    gainDisplay->setNum(currentGain);
    threshDisplay = new QLabel;
    threshDisplay->setNum(currentThresh);

    gainTitle = new QLabel(tr("Gain"));
    threshTitle = new QLabel(tr("Threshold"));
    gainTitle->setFont(dispFont);
    threshTitle->setFont(dispFont);

    deviceCombo = new QComboBox;
    deviceCombo->setFixedWidth(100);
    QLabel *deviceLabel = new QLabel;
    deviceLabel->setText("Device");
    QGridLayout *widgetLayout = new QGridLayout;
    QGridLayout *mainLayout = new QGridLayout;


    mainLayout->addWidget(gainTitle,0,0,Qt::AlignCenter);
    mainLayout->addWidget(threshTitle,0,1,Qt::AlignCenter);
    mainLayout->addWidget(gainDisplay,1,0,Qt::AlignCenter);
    mainLayout->addWidget(threshDisplay,1,1,Qt::AlignCenter);
    mainLayout->addWidget(gainSlider,2,0,Qt::AlignHCenter);
    mainLayout->addWidget(threshSlider,2,1,Qt::AlignHCenter);
    mainLayout->setRowStretch(2,1);
    mainLayout->setMargin(10);
    widgetLayout->addLayout(mainLayout,0,0,Qt::AlignCenter);
    widgetLayout->addWidget(deviceLabel,1,0,Qt::AlignCenter);
    widgetLayout->addWidget(deviceCombo,2,0,Qt::AlignCenter);

    connect(gainSlider,SIGNAL(valueChanged(int)),gainDisplay,SLOT(setNum(int)));
    connect(threshSlider,SIGNAL(valueChanged(int)),threshDisplay,SLOT(setNum(int)));

    setLayout(widgetLayout);
    setWindowTitle(tr("Sound"));

}

void soundDialog::closeDialog() {
    //Remember the audio settings for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("sound"));
    settings.setValue(QLatin1String("thresh"), threshSlider->value());
    settings.setValue(QLatin1String("gain"), gainSlider->value());
    settings.setValue(QLatin1String("device"), deviceCombo->currentText());

    settings.endGroup();

    close();
}


waveformGeneratorDialog::waveformGeneratorDialog(double currentModulatorFreq, int currentFreq, int currentAmp, int currentThresh, QWidget *parent)
//waveformGeneratorDialog::waveformGeneratorDialog(int currentFreq, int currentAmp, QWidget *parent)
    :QWidget(parent)
{

    modulatorFreqSpinBox = new QDoubleSpinBox(this);
    modulatorFreqSpinBox->setMaximum(10);
    modulatorFreqSpinBox->setMinimum(0.0);
    modulatorFreqSpinBox->setSingleStep(0.1);
    modulatorFreqSpinBox->setValue(currentModulatorFreq);
    modulatorFreqSpinBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    freqSlider = new QSlider(Qt::Vertical);
    freqSlider->setMaximum(1000);
    freqSlider->setMinimum(1);
    freqSlider->setSingleStep(1);
    freqSlider->setValue(currentFreq);
    freqSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    ampSlider = new QSlider(Qt::Vertical);
    ampSlider->setMaximum(1000);
    ampSlider->setMinimum(0);
    ampSlider->setSingleStep(1);
    ampSlider->setValue(currentAmp);
    ampSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    threshSlider = new QSlider(Qt::Vertical);
    threshSlider->setMaximum(1000);
    threshSlider->setMinimum(0);
    threshSlider->setSingleStep(1);
    threshSlider->setValue(currentThresh);
    threshSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);


    freqDisplay = new QLabel;
    freqDisplay->setNum(currentFreq);
    ampDisplay = new QLabel;
    ampDisplay->setNum(currentAmp);
    threshDisplay = new QLabel;
    threshDisplay->setNum(currentThresh);

    modulatorFreqTitle = new QLabel(tr("Modulator (Hz)"));
    freqTitle = new QLabel(tr("Frequency (Hz)"));
    ampTitle = new QLabel(tr("Amplitude (uV)"));
    threshTitle = new QLabel(tr("Threshold (uV)"));


    QGridLayout *mainLayout = new QGridLayout;

    mainLayout->addWidget(modulatorFreqTitle,0,0,Qt::AlignCenter);
    mainLayout->addWidget(freqTitle,0,1,Qt::AlignCenter);
    mainLayout->addWidget(ampTitle,0,2,Qt::AlignCenter);
    mainLayout->addWidget(threshTitle,0,3,Qt::AlignCenter);

    mainLayout->addWidget(freqDisplay,1,1,Qt::AlignCenter);
    mainLayout->addWidget(ampDisplay,1,2,Qt::AlignCenter);
    mainLayout->addWidget(threshDisplay,1,3,Qt::AlignCenter);

    mainLayout->addWidget(modulatorFreqSpinBox,2,0,Qt::AlignHCenter);
    mainLayout->addWidget(freqSlider,2,1,Qt::AlignHCenter);
    mainLayout->addWidget(ampSlider,2,2,Qt::AlignHCenter);
    mainLayout->addWidget(threshSlider,2,3,Qt::AlignHCenter);

    mainLayout->setRowStretch(2,1);
    mainLayout->setMargin(10);

    connect(freqSlider,SIGNAL(valueChanged(int)),freqDisplay,SLOT(setNum(int)));
    connect(ampSlider,SIGNAL(valueChanged(int)),ampDisplay,SLOT(setNum(int)));
    connect(threshSlider,SIGNAL(valueChanged(int)),threshDisplay,SLOT(setNum(int)));

    setLayout(mainLayout);
    setWindowTitle(tr("Waveform generator"));

}

void waveformGeneratorDialog::closeEvent(QCloseEvent* event) {

    emit windowClosed();
    event->accept();
}



spikeGeneratorDialog::spikeGeneratorDialog(double currentModulatorFreq, int currentFreq, int currentAmp, int currentThresh, QWidget *parent)
  :QWidget(parent)

{
  modulatorFreqSpinBox = new QDoubleSpinBox(this);
  modulatorFreqSpinBox->setMaximum(10);
  modulatorFreqSpinBox->setMinimum(0.0);
  modulatorFreqSpinBox->setSingleStep(0.1);
  modulatorFreqSpinBox->setValue(currentModulatorFreq);
  modulatorFreqSpinBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

  freqSlider = new QSlider(Qt::Vertical);
  freqSlider->setMaximum(1000);
  freqSlider->setMinimum(1);
  freqSlider->setSingleStep(1);
  freqSlider->setValue(currentFreq);
  freqSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

  ampSlider = new QSlider(Qt::Vertical);
  ampSlider->setMaximum(1000);
  ampSlider->setMinimum(0);
  ampSlider->setSingleStep(1);
  ampSlider->setValue(currentAmp);
  ampSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

  threshSlider = new QSlider(Qt::Vertical);
  threshSlider->setMaximum(1000);
  threshSlider->setMinimum(0);
  threshSlider->setSingleStep(1);
  threshSlider->setValue(currentThresh);
  threshSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);


  freqDisplay = new QLabel;
  freqDisplay->setNum(currentFreq);
  ampDisplay = new QLabel;
  ampDisplay->setNum(currentAmp);
  threshDisplay = new QLabel;
  threshDisplay->setNum(currentThresh);

  modulatorFreqTitle = new QLabel(tr("Modulator (Hz)"));
  freqTitle = new QLabel(tr("Frequency (Hz)"));
  ampTitle = new QLabel(tr("Amplitude (uV)"));
  threshTitle = new QLabel(tr("Threshold (uV)"));


  QGridLayout *mainLayout = new QGridLayout;

  mainLayout->addWidget(modulatorFreqTitle,0,0,Qt::AlignCenter);
  mainLayout->addWidget(freqTitle,0,1,Qt::AlignCenter);
  mainLayout->addWidget(ampTitle,0,2,Qt::AlignCenter);
  mainLayout->addWidget(threshTitle,0,3,Qt::AlignCenter);

  mainLayout->addWidget(freqDisplay,1,1,Qt::AlignCenter);
  mainLayout->addWidget(ampDisplay,1,2,Qt::AlignCenter);
  mainLayout->addWidget(threshDisplay,1,3,Qt::AlignCenter);

  mainLayout->addWidget(modulatorFreqSpinBox,2,0,Qt::AlignHCenter);
  mainLayout->addWidget(freqSlider,2,1,Qt::AlignHCenter);
  mainLayout->addWidget(ampSlider,2,2,Qt::AlignHCenter);
  mainLayout->addWidget(threshSlider,2,3,Qt::AlignHCenter);

  mainLayout->setRowStretch(2,1);
  mainLayout->setMargin(10);

  connect(freqSlider,SIGNAL(valueChanged(int)),freqDisplay,SLOT(setNum(int)));
  connect(ampSlider,SIGNAL(valueChanged(int)),ampDisplay,SLOT(setNum(int)));
  connect(threshSlider,SIGNAL(valueChanged(int)),threshDisplay,SLOT(setNum(int)));

  setLayout(mainLayout);
  setWindowTitle(tr("Spikes+Waveform generator"));

}

void spikeGeneratorDialog::closeEvent(QCloseEvent *event)
{
  emit windowClosed();
  event->accept();
}


//-----------------------------------------------------
HeadstageSettingsDialog::HeadstageSettingsDialog(HeadstageSettings settings, QWidget *parent):QWidget(parent)
{
    setMinimumWidth(300);

    currentSettings = settings;
    settingsChanged = false;

    //Create the auto settle controls
    autoSettleBox = new QGroupBox(tr("Auto settle"),this);
    autoSettleBox->setCheckable(true);
    QGridLayout *autoSettleLayout = new QGridLayout;
    autoSettleBox->setToolTip("Amplifiers are automatically settled to prevent ringing if the signal comes above the set amplitude simultaneously for the set percentage of channels.");

    percentChannelsSlider = new QSlider(Qt::Horizontal);
    percentChannelsSlider->setMaximum(100);
    percentChannelsSlider->setMinimum(0);
    percentChannelsSlider->setSingleStep(1);
    //percentChannelsSlider->setValue(currentSettings.percentChannelsForSettle);
    percentChannelsSlider->setValue(round(100.0*((float)currentSettings.percentChannelsForSettle/(float)hardwareConf->NCHAN)));
    percentChannelsSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    threshSlider = new QSlider(Qt::Horizontal);
    threshSlider->setMaximum(2000);
    threshSlider->setMinimum(500);
    threshSlider->setSingleStep(1);
    //threshSlider->setValue(currentSettings.threshForSettle);
    threshSlider->setValue(round( ((float)currentSettings.threshForSettle*AD_CONVERSION_FACTOR)/65536 ));
    threshSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    connect(percentChannelsSlider,SIGNAL(sliderMoved(int)),this,SLOT(percentSliderChanged(int)));
    connect(threshSlider,SIGNAL(sliderMoved(int)),this,SLOT(threshSliderChanged(int)));

    threshIndicator = new QLabel(QString().number(threshSlider->value()));
    percentIndicator = new QLabel(QString().number(percentChannelsSlider->value()));

    percentTitle = new QLabel(tr("Channels over thresh (%)"));
    threshTitle = new QLabel(tr("Threshold (uV)"));

    autoSettleLayout->addWidget(threshTitle,0,0,Qt::AlignLeft);
    autoSettleLayout->addWidget(threshSlider,1,0,Qt::AlignLeft);
    autoSettleLayout->addWidget(percentTitle,2,0,Qt::AlignLeft);
    autoSettleLayout->addWidget(percentChannelsSlider,3,0,Qt::AlignLeft);

    autoSettleLayout->addWidget(threshIndicator,1,1,Qt::AlignRight);
    autoSettleLayout->addWidget(percentIndicator,3,1,Qt::AlignRight);

    autoSettleBox->setLayout(autoSettleLayout);
    autoSettleBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);




    QGridLayout *secRowLayout = new QGridLayout;

    //Create smart ref controls
    smartReferenceBox = new QGroupBox(tr("Sampling sequence correction"),this);
    smartReferenceBox->setCheckable(false);
    QGridLayout *smartReferenceLayout = new QGridLayout;
    smartRefCheckBox = new QCheckBox();
    smartRefCheckBox->setText("Enable");
    smartReferenceBox->setToolTip("Used to correct for sequential sampling of channels if digital referencing is used. Recommended.");

    if (!currentSettings.smartRefAvailable) {
        smartRefCheckBox->setChecked(false);
        smartRefCheckBox->setEnabled(false);
    } else {
        smartRefCheckBox->setChecked(currentSettings.smartRefOn);
    }
    smartRefCheckBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    smartReferenceLayout->addWidget(smartRefCheckBox,0,0);
    smartReferenceBox->setLayout(smartReferenceLayout);
    smartReferenceBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    connect(smartRefCheckBox,SIGNAL(toggled(bool)),this,SLOT(smartRefToggled(bool)));
    secRowLayout->addWidget(smartReferenceBox,0,0,Qt::AlignCenter);

    //Create sensor controls
    sensorBox = new QGroupBox(tr("Motion sensors"),this);
    sensorBox->setToolTip("Toggles on/off available motion sensors.");
    sensorBox->setCheckable(false);
    QGridLayout *sensorLayout = new QGridLayout;
    accelCheckBox = new QCheckBox();
    accelCheckBox->setText("Accel.");
    sensorLayout->addWidget(accelCheckBox,0,0);
    gyroCheckBox = new QCheckBox();
    gyroCheckBox->setText("Gyro");
    sensorLayout->addWidget(gyroCheckBox,0,1);
    magnetCheckBox = new QCheckBox();
    magnetCheckBox->setText("Compass");
    sensorLayout->addWidget(magnetCheckBox,0,2);
    if (!currentSettings.accelSensorAvailable) {
        accelCheckBox->setChecked(false);
        accelCheckBox->setEnabled(false);
    } else {
        accelCheckBox->setChecked(currentSettings.accelSensorOn);
    }

    if (!currentSettings.gyroSensorAvailable) {
        gyroCheckBox->setChecked(false);
        gyroCheckBox->setEnabled(false);
    } else {
        gyroCheckBox->setChecked(currentSettings.gyroSensorOn);
    }

    if (!currentSettings.magSensorAvailable) {
        magnetCheckBox->setChecked(false);
        magnetCheckBox->setEnabled(false);
    } else {
        magnetCheckBox->setChecked(currentSettings.magSensorOn);
    }

    sensorBox->setLayout(sensorLayout);
    sensorBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    connect(accelCheckBox,SIGNAL(toggled(bool)),this,SLOT(accelToggled(bool)));
    connect(gyroCheckBox,SIGNAL(toggled(bool)),this,SLOT(gyroToggled(bool)));
    connect(magnetCheckBox,SIGNAL(toggled(bool)),this,SLOT(magToggled(bool)));
    secRowLayout->addWidget(sensorBox,0,2,Qt::AlignCenter);


    secRowLayout->setColumnStretch(1,1);
    secRowLayout->setMargin(20);

    QGridLayout *thirdRowLayout = new QGridLayout;
    rfBox = new QGroupBox(tr("Radio channel"),this);
    rfBox->setToolTip("Set RF channel");
    rfBox->setCheckable(false);
    QGridLayout *rfLayout = new QGridLayout;
    rfChannelSpinBox = new QSpinBox();
    rfChannelSpinBox->setMinimum(0);
    rfChannelSpinBox->setMaximum(255);
    if (!currentSettings.rfAvailable) {
        rfChannelSpinBox->setEnabled(false);
    } else {
        rfChannelSpinBox->setValue(currentSettings.rfChannel);
    }
    connect(rfChannelSpinBox,SIGNAL(valueChanged(int)),this,SLOT(rfChannelChanged(int)));

    rfLayout->addWidget(rfChannelSpinBox,0,0);

    rfBox->setLayout(rfLayout);
    rfBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    thirdRowLayout->addWidget(rfBox,0,0,Qt::AlignCenter);
    thirdRowLayout->setColumnStretch(1,1);
    thirdRowLayout->setMargin(20);

    okButton = new TrodesButton();
    okButton->setText("Apply");
    okButton->setEnabled(false);
    cancelButton = new TrodesButton();
    cancelButton->setText("Cancel");
    connect(okButton,SIGNAL(pressed()),this,SLOT(okButtonPressed()));
    connect(cancelButton,SIGNAL(pressed()),this,SLOT(cancelButtonPressed()));

    QGridLayout *buttonLayout = new QGridLayout();
    buttonLayout->addWidget(cancelButton,0,1);
    buttonLayout->addWidget(okButton,0,2);
    buttonLayout->setColumnStretch(0,1);
    buttonLayout->setContentsMargins(10,10,10,10);


    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->setMargin(10);
    mainLayout->setContentsMargins(10,10,10,10);
    mainLayout->setVerticalSpacing(10);

    mainLayout->addWidget(autoSettleBox,0,0,Qt::AlignCenter);
    mainLayout->addLayout(secRowLayout,1,0,Qt::AlignCenter);
    mainLayout->addLayout(thirdRowLayout,2,0,Qt::AlignCenter);
    mainLayout->addLayout(buttonLayout,3,0);

    setLayout(mainLayout);

    if (currentSettings.autosettleAvailable) {
        autoSettleBox->setChecked(currentSettings.autoSettleOn);
    } else {
        autoSettleBox->setChecked(false);
        autoSettleBox->setEnabled(false);
    }

    connect(autoSettleBox,SIGNAL(toggled(bool)),this,SLOT(autoSettleOnToggled(bool)));

    setWindowTitle(tr("Headstage settings"));

}

void HeadstageSettingsDialog::rfChannelChanged(int value) {
    currentSettings.rfChannel = value;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::percentSliderChanged(int value) {
    percentIndicator->setText(QString().number(value));
    currentSettings.percentChannelsForSettle = value;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::threshSliderChanged(int value) {
    threshIndicator->setText(QString().number(value));
    currentSettings.threshForSettle = value;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::autoSettleOnToggled(bool on) {
    currentSettings.autoSettleOn = on;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::smartRefToggled(bool on) {
    currentSettings.smartRefOn = on;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::accelToggled(bool on) {
    currentSettings.accelSensorOn = on;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::gyroToggled(bool on) {
    currentSettings.gyroSensorOn = on;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::magToggled(bool on) {
    currentSettings.magSensorOn = on;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::okButtonPressed() {
    setWindowTitle(tr("Sending to hardware..."));
    this->repaint();

    currentSettings.percentChannelsForSettle = round( ((float)percentChannelsSlider->value()/100.0)*(float)hardwareConf->NCHAN );
    currentSettings.threshForSettle = round( ((float)threshSlider->value() * 65536) / AD_CONVERSION_FACTOR );
    emit newSettings(currentSettings); //send the new settings to the source controller
    this->close();
}

void HeadstageSettingsDialog::cancelButtonPressed() {
    this->close();
}

void HeadstageSettingsDialog::closeEvent(QCloseEvent* event) {

    emit windowClosed();
    event->accept();
}

void HeadstageSettingsDialog::resizeEvent(QResizeEvent *event) {
    //autoSettleBox->setGeometry(this->geometry());
    autoSettleBox->setFixedWidth(this->geometry().width()-50);
    percentChannelsSlider->setFixedWidth(this->geometry().width()-120);
    threshSlider->setFixedWidth(this->geometry().width()-120);
}








//-----------------------------------------------------
ControllerSettingsDialog::ControllerSettingsDialog(HardwareControllerSettings settings, QWidget *parent):QWidget(parent)
{
    setMinimumWidth(300);

    currentSettings = settings;
    settingsChanged = false;


    QGridLayout *firstRowLayout = new QGridLayout;
    rfBox = new QGroupBox(tr("Radio channel"),this);
    rfBox->setToolTip("Set RF channel");
    rfBox->setCheckable(false);
    QGridLayout *rfLayout = new QGridLayout;
    rfChannelSpinBox = new QSpinBox();
    rfChannelSpinBox->setMinimum(0);
    rfChannelSpinBox->setMaximum(255);
    /*if (!currentSettings.rfAvailable) {
        rfChannelSpinBox->setEnabled(false);
    } else {
        rfChannelSpinBox->setValue(currentSettings.rfChannel);
    }*/
    connect(rfChannelSpinBox,SIGNAL(valueChanged(int)),this,SLOT(rfChannelChanged(int)));
    rfLayout->addWidget(rfChannelSpinBox,0,0);

    rfBox->setLayout(rfLayout);
    rfBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    firstRowLayout->addWidget(rfBox,0,0,Qt::AlignCenter);
    firstRowLayout->setColumnStretch(1,1);
    firstRowLayout->setMargin(20);

    QGridLayout *secondRowLayout = new QGridLayout;
    samplingRateBox = new QGroupBox(tr("Sampling Rate (kHz)"),this);
    samplingRateBox->setToolTip("Set Sampling Rate");
    samplingRateBox->setCheckable(false);

    QGridLayout *sampLayout = new QGridLayout;
    samplingRateSpinBox = new QSpinBox();
    samplingRateSpinBox->setMinimum(20);
    samplingRateSpinBox->setMaximum(30);
    samplingRateSpinBox->setSingleStep(10);
    samplingRateSpinBox->findChild<QLineEdit*>()->setReadOnly(true);

    samplingRateSpinBox->setValue(currentSettings.samplingRateKhz);
    /*if (!currentSettings.headstageConnected) {
        samplingRateSpinBox->setEnabled(false);
    }*/
    connect(samplingRateSpinBox,SIGNAL(valueChanged(int)),this,SLOT(samplingRateChanged(int)));
    sampLayout->addWidget(samplingRateSpinBox,0,0);

    samplingRateBox->setLayout(sampLayout);
    samplingRateBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    secondRowLayout->addWidget(samplingRateBox,0,0,Qt::AlignCenter);
    secondRowLayout->setColumnStretch(1,1);
    secondRowLayout->setMargin(20);

    okButton = new TrodesButton();
    okButton->setText("Apply");
    okButton->setEnabled(false);
    cancelButton = new TrodesButton();
    cancelButton->setText("Cancel");
    connect(okButton,SIGNAL(pressed()),this,SLOT(okButtonPressed()));
    connect(cancelButton,SIGNAL(pressed()),this,SLOT(cancelButtonPressed()));

    QGridLayout *buttonLayout = new QGridLayout();
    buttonLayout->addWidget(cancelButton,0,1);
    buttonLayout->addWidget(okButton,0,2);
    buttonLayout->setColumnStretch(0,1);
    buttonLayout->setContentsMargins(10,10,10,10);


    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->setMargin(10);
    mainLayout->setContentsMargins(10,10,10,10);
    mainLayout->setVerticalSpacing(10);

    mainLayout->addLayout(firstRowLayout,0,0,Qt::AlignCenter);
    mainLayout->addLayout(secondRowLayout,1,0,Qt::AlignCenter);
    mainLayout->addLayout(buttonLayout,3,0);

    setLayout(mainLayout);

    if (!settings.valid) {
        rfBox->setEnabled(false);
        samplingRateBox->setEnabled(false);
        okButton->setEnabled(false);
    }




    setWindowTitle(tr("Main Control Unit (MCU) settings"));

}

void ControllerSettingsDialog::rfChannelChanged(int value) {
    currentSettings.rfChannel = value;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void ControllerSettingsDialog::samplingRateChanged(int value) {
    currentSettings.samplingRateKhz = value;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void ControllerSettingsDialog::okButtonPressed() {
    setWindowTitle(tr("Sending to hardware..."));
    this->repaint();

    emit newSettings(currentSettings); //send the new settings to the source controller
    this->close();
}

void ControllerSettingsDialog::cancelButtonPressed() {
    this->close();
}

void ControllerSettingsDialog::closeEvent(QCloseEvent* event) {

    emit windowClosed();
    event->accept();
}

void ControllerSettingsDialog::resizeEvent(QResizeEvent *event) {
    //autoSettleBox->setGeometry(this->geometry());
    //autoSettleBox->setFixedWidth(this->geometry().width()-50);

}



//-----------------------------------------------------

//-----------------------------------------------------
//This dialog pops up when the "Annotate" button is pressed.
//Allows the user to add comments to the recording session. All comments are
//stores in file that is separate from the main recording file, but with the same base name.

CommentDialog::CommentDialog(QString fileNameIn, QWidget *parent)
    :QWidget(parent),
    fileName(fileNameIn) {

    bool enableControls = false;
    TrodesFont dispFont;
    samplingRate = 30000;

    if (!fileName.isEmpty()) {
        enableControls = true;
        QFile commentFile(fileName);
    }

    newCommentEdit = new QLineEdit();
    newCommentEdit->setFont(dispFont);
    newCommentEdit->setMinimumWidth(200);
    newCommentEdit->setFrame(true);
    newCommentEdit->setStyleSheet("QLineEdit {border: 2px solid lightgray;"
                                "border-radius: 4px;"
                                "padding: 2px;"
                                "background-color: white}");
    newCommentEdit->setEnabled(enableControls);

    connect(newCommentEdit,SIGNAL(returnPressed()),this,SLOT(saveCurrentComment()));
    connect(newCommentEdit,SIGNAL(textChanged(QString)),this,SLOT(somethingEntered()));

    saveButton = new TrodesButton();
    saveButton->setText("Save");
    saveButton->setEnabled(false);
    saveButton->setVisible(false);

    QLabel* historyLabel = new QLabel();
    historyLabel->setFont(dispFont);
    historyLabel->setText("History:");
    historyLabel->setEnabled(enableControls);
    //lastComment = new QLabel();
    lastComment = new QTextEdit();
    lastComment->setFont(dispFont);
    //sec2lastComment = new QLabel();
    lastComment->setStyleSheet("QFrame {border: 2px solid lightgray;"
                                "border-radius: 4px;"
                                "padding: 2px;"
                                "background-color: white}"
                                "QLabel {color : gray;}");
    lastComment->setMinimumWidth(200);
    lastComment->setEnabled(enableControls);
    lastComment->setReadOnly(true);

    commentLabel = new QLabel();
    commentLabel->setFont(dispFont);
    commentLabel->setText("New comment:");
    commentLabel->setEnabled(enableControls);

    QGridLayout *mainLayout = new QGridLayout;
    QGridLayout *commentLayout = new QGridLayout;
    QGridLayout *buttonLayout = new QGridLayout;

    //commentLayout->addWidget(sec2lastComment,0,0,Qt::AlignCenter);
    commentLayout->addWidget(historyLabel,0,0,Qt::AlignLeft);
    commentLayout->addWidget(lastComment,1,0,Qt::AlignCenter);

    commentLayout->setColumnStretch(0,1);
    commentLayout->setContentsMargins(0,0,0,0);

    buttonLayout->addWidget(saveButton,0,2,Qt::AlignHCenter);
    buttonLayout->setColumnStretch(0,1);

    mainLayout->addLayout(commentLayout,1,0);
    //mainLayout->addWidget(historyFrame,1,0,Qt::AlignRight);
    mainLayout->addWidget(commentLabel,2,0,Qt::AlignLeft);
    mainLayout->addWidget(newCommentEdit,3,0,Qt::AlignCenter);
    mainLayout->addLayout(buttonLayout,4,0);
    //mainLayout->setMargin(10);

    connect(saveButton,SIGNAL(pressed()),this,SLOT(saveCurrentComment()));

    setLayout(mainLayout);
    getHistory();

}

void CommentDialog::setLiveMode(bool yes) {
    if (yes) {
        newCommentEdit->setVisible(true);
        commentLabel->setVisible(true);
    } else {
        newCommentEdit->setVisible(false);
        commentLabel->setVisible(false);
    }
}

void CommentDialog::setSamplingRate(int s) {
    samplingRate = s;
}

void CommentDialog::saveLine() {
    //Saves the current line to file
    if (saveButton->isEnabled()) {
        if (!fileName.isEmpty()) {
            //append the current timestamp to the comment
            QString currentComment = QString("%1  ").arg(currentTimeStamp) + newCommentEdit->text()+"\n";
            //write to the file
            QFile commentFile(fileName);
            commentFile.open(QIODevice::Append);
            commentFile.write(currentComment.toLocal8Bit());
            commentFile.close();
        }
        newCommentEdit->clear();
        //update the history box
        getHistory();
    }
}

void CommentDialog::getHistory() {
    //Read the contents of the file and populate the "history" box

    QString contents;
    QString line;

    if (!fileName.isEmpty()) {
        QFile commentFile(fileName);
        if (commentFile.open(QIODevice::ReadOnly)) {
            QTextStream in(&commentFile);
            while (!in.atEnd()){

               QString line = in.readLine();
               QStringList lineParse = line.split(" ");
               QString displayLine = "";
               bool tOk;
               uint32_t tmpTimeStamp = lineParse.at(0).toULong(&tOk);
               if (tOk) {

                   QString currentTimeString("");

                   int hoursPassed = floor(tmpTimeStamp / (samplingRate * 60 * 60));
                   tmpTimeStamp = tmpTimeStamp - (hoursPassed * 60 * 60 * samplingRate);
                   int minutesPassed = floor(tmpTimeStamp / (samplingRate * 60));
                   tmpTimeStamp = tmpTimeStamp - (minutesPassed * 60 * samplingRate);
                   int secondsPassed = floor(tmpTimeStamp / (samplingRate));
                   tmpTimeStamp = tmpTimeStamp - (secondsPassed * samplingRate);

                   if (hoursPassed < 10)
                       currentTimeString.append("0");
                   currentTimeString.append(QString::number(hoursPassed));
                   currentTimeString.append(":");
                   if (minutesPassed < 10)
                       currentTimeString.append("0");
                   currentTimeString.append(QString::number(minutesPassed));
                   currentTimeString.append(":");
                   if (secondsPassed < 10)
                       currentTimeString.append("0");
                   currentTimeString.append(QString::number(secondsPassed));

                   displayLine.append(currentTimeString);
                   for (int i=1;i<lineParse.length();i++) {
                       displayLine.append(" ");
                       displayLine.append(lineParse.at(i));
                   }
                   contents.append(displayLine);
                   contents.append(QChar::CarriageReturn);
               }

            }

            commentFile.close();
        }
    }

    lastComment->setText(contents);
    //set the scrollbar to show the last few lines
    QScrollBar *bar = lastComment->verticalScrollBar();
    bar->setValue(bar->maximum());

}


void CommentDialog::saveCurrentComment() {

    //add the comment to the file
    saveLine();
    //close();
}


void CommentDialog::somethingEntered() {
    //we don't enable the save button unless something has been entered
    saveButton->setEnabled(true);
}

void CommentDialog::closeEvent(QCloseEvent *event) {

    emit windowOpenState(false);
    event->accept();
    emit windowClosed();
}

//---------------------------------------------
//-------------------------------------------

SignalAnalysisDialog::SignalAnalysisDialog(QWidget *parent)
    :QWidget(parent){

    setGeometry(300,300,300,300);
    setMinimumHeight(300);
    setMinimumWidth(300);

    chart = new QtCharts::QChart();
    chart->legend()->hide();
    chart->setTitle("Power Spectral Density");
    data = new QtCharts::QLineSeries();
    QPen pen = data->pen();
    pen.setWidth(1);
    pen.setBrush(QBrush("black"));
    data->setPen(pen);


    //chart->createDefaultAxes();
    axisX = new QtCharts::QLogValueAxis();
    axisY = new QtCharts::QValueAxis();

    axisX->setTitleText("Frequency (Hz)");
    axisY->setTitleText("Power");

    axisX->setLabelFormat("%d");
    axisX->setBase(10.0);
    axisX->setMinorTickCount(-1);

    //axisY->setLabelFormat("%d");
    axisY->setTickCount(10);

    chart->addAxis(axisX, Qt::AlignBottom); 
    chart->addAxis(axisY, Qt::AlignLeft);



    chartView = new QtCharts::QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    QGridLayout *mainLayout = new QGridLayout;
    QGridLayout *plotLayout = new QGridLayout;
    QGridLayout *controlLayout = new QGridLayout;
    plotLayout->setVerticalSpacing(0);
    plotLayout->setContentsMargins(0,0,0,0);
    //plotLayout->addWidget(plotWindow,0,0);
    plotLayout->addWidget(chartView,0,0);
    //plotLayout->setRowStretch(0,1);
    mainLayout->addLayout(plotLayout,0,0);

    //mainLayout->setContentsMargins(5,5,5,5);
    //mainLayout->setVerticalSpacing(0);

    infoLabel = new TrodesClickableLabel();
    infoLabel->setText("i");
    QFont labelFont;
    labelFont.setPixelSize(30);
    labelFont.setItalic(true);
    labelFont.setBold(true);
    labelFont.setFamily("Times");
    infoLabel->setStyleSheet("QLabel { color : blue; }");
    infoLabel->setFont(labelFont);
    //infoLabel->setMaximumHeight(25);
    char myToolTip[] = "<html><head/><body><p>This tool plots the power spectral density of the " \
                "currently active channel (red highlight on the main screen). " \
                "Click the channel you want to analyze on the main screen, " \
                "and this figure will automatically update to that channel. " \
                "The data comes from the spike processing channel, including any reference " \
                "and filter settings you have applied. To see the spectrum " \
                "from raw data, make sure to turn off referencing and filters " \
                "in the nTrode settings menu." \
                "</p></body></html>";

    infoLabel->setToolTip(myToolTip);
    connect(infoLabel,&TrodesClickableLabel::clicked,infoLabel,&TrodesClickableLabel::showToolTip);



    controlLayout->addWidget(infoLabel,0,0);

    controlLayout->setColumnStretch(1,1);
    //controlLayout->setContentsMargins(10,10,10,10);



    mainLayout->addLayout(controlLayout,1,0);



    setLayout(mainLayout);
    mainLayout->setRowStretch(0,1);


    //plotWindow->show();


    //Remembered settings...
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    //Place the window where it was the last session
    settings.beginGroup(QLatin1String("position"));
    QRect tempPosition = settings.value(QLatin1String("SignalAnalysisPosition")).toRect();
    if (tempPosition.height() > 0) {
        setGeometry(tempPosition);
    }
    settings.endGroup();

    //update();


}

void SignalAnalysisDialog::plot(QList<QPointF> points) {



    chart->removeAllSeries();
    chart->removeAxis(axisX);
    chart->removeAxis(axisY);
    delete  axisX;
    delete axisY;


    data = new QtCharts::QLineSeries();
    QPen pen = data->pen();
    pen.setWidth(1);
    pen.setBrush(QBrush("black"));
    data->setPen(pen);
    data->append(points);

    chart->addSeries(data);


    //data->attachAxis(axisX);
    //data->attachAxis(axisY);

    axisX = new QtCharts::QLogValueAxis();
    axisY = new QtCharts::QValueAxis();

    axisX->setTitleText("Frequency (Hz)");
    axisY->setTitleText("Power");

    axisX->setLabelFormat("%d");
    axisX->setBase(10.0);
    axisX->setMinorTickCount(-1);

    //axisY->setLabelFormat("%d");
    axisY->setTickCount(10);

    chart->addAxis(axisX, Qt::AlignBottom);
    chart->addAxis(axisY, Qt::AlignLeft);

    data->attachAxis(axisX);
    data->attachAxis(axisY);

}

void SignalAnalysisDialog::showInfoBox() {

}

void SignalAnalysisDialog::update() {

    chart->removeAllSeries();

    for (int i=1;i<10000;i = i+10) {
        data->append(QPointF(i,i/100));
    }
    chart->addSeries(data);

    data->attachAxis(axisX);
    data->attachAxis(axisY);


}

void SignalAnalysisDialog::closeEvent(QCloseEvent* event) {

    emit windowClosed();
    QWidget::closeEvent(event);
}
void SignalAnalysisDialog::resizeEvent(QResizeEvent *event) {
    //Remember the new size for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("position"));
    settings.setValue(QLatin1String("SignalAnalysisPosition"), this->geometry());
    settings.endGroup();

    QWidget::resizeEvent(event);
}

//-------------------------------------------
Abstract2DDataPlotWidget::Abstract2DDataPlotWidget(QWidget *parent) {

    setMinimumHeight(10);
    setMinimumWidth(200);
    minX = 0;
    maxX = 10;
    minY = 0;
    maxY = 10;
    numYTicks = 5;
    numXTicks = 5;
    setSizePolicy(QSizePolicy ::Expanding , QSizePolicy ::Expanding );

    calulateTickLabels();


}

void Abstract2DDataPlotWidget::setNumXTicks(int t) {
    if (t > 2) {
        numXTicks = t;
        update();
    }
}

void Abstract2DDataPlotWidget::setNumYTicks(int t) {
    if (t > 2) {
        numYTicks = t;
        update();
    }
}

void Abstract2DDataPlotWidget::setXRange(qreal minR, qreal maxR) {
    maxX = maxR;
    minX = minR;
    update();
}

void Abstract2DDataPlotWidget::setYRange(qreal minR, qreal maxR) {
    maxX = maxR;
    minX = minR;
    update();
}

void Abstract2DDataPlotWidget::calulateTickLabels() {
    xTickLabels.clear();
    if (maxX-minX > 0) {
        qreal step = (maxX-minX)/numXTicks;
        for (int i=0;i<=numXTicks;i++) {
            xTickLabels.append(minX+(i*step));
        }
    }

    yTickLabels.clear();
    if (maxY-minY > 0) {
        qreal step = (maxY-minY)/numYTicks;
        for (int i=0;i<=numYTicks;i++) {
            yTickLabels.append(minY+(i*step));
        }
    }

}

void Abstract2DDataPlotWidget::setXLabel(QString l) {
    xLabel = l;
    update();
}

void Abstract2DDataPlotWidget::setYLabel(QString l) {
    yLabel = l;
    update();
}

void Abstract2DDataPlotWidget::setTitle(QString l) {
    title = l;
    update();
}


void Abstract2DDataPlotWidget::clear() {
    //eventTimes.clear();
    update();
}

void Abstract2DDataPlotWidget::paintEvent(QPaintEvent *event) {
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    QPen pen;
    //We want the lines to be a set number of pixels in width
    //units per pixel = unitrange/window_width
    pen.setWidth(1);

    //pen.setWidth(100*TLength);

    pen.setColor(Qt::black);
    painter.setPen(pen);
    QFont f;
    f.setPixelSize(12);
    painter.setFont(f);
    //painter.setPen(Qt::green);
    painter.setViewTransformEnabled(true);
    painter.setViewport(0,0,width(), height());


    //qreal numRows = eventTimes.length();
    //qreal maxRange = maxY;

    int margin = 30;

    int topOfGraph = margin;
    int bottomOfGraph = height()-margin-20;


    int leftOfGraph = margin+20;
    int rightOfGraph = width()-margin;



    //Draw the background
    QPainterPath backgroundPath;
    backgroundPath.addRect(leftOfGraph,topOfGraph,rightOfGraph-leftOfGraph,bottomOfGraph-topOfGraph);
    //QPen pen(Qt::black, 10);
    //p.setPen(pen);
    painter.fillPath(backgroundPath, Qt::white);
    //painter.drawPath(backgroundPath);

//    //Draw the axes
    painter.drawLine(leftOfGraph,bottomOfGraph,rightOfGraph,bottomOfGraph);
    painter.drawLine(leftOfGraph,bottomOfGraph,leftOfGraph,topOfGraph);

    //Display the title
    QRect TitleBox= QRect(leftOfGraph, 0, rightOfGraph-leftOfGraph, 25);
    painter.drawText(TitleBox, Qt::AlignCenter, title);

    //Display the axis labels
    QRect XLabelBox1= QRect(leftOfGraph, height()-25, rightOfGraph-leftOfGraph, 25);
    painter.drawText(XLabelBox1, Qt::AlignCenter, xLabel);

    painter.rotate(-90);
    QRect YLabelBox= QRect(-bottomOfGraph, 0, bottomOfGraph-topOfGraph, 25);
    painter.drawText(YLabelBox, Qt::AlignCenter, yLabel);
    painter.rotate(90);

//    //Put unit labels on the x axis
    f.setPixelSize(10);
    painter.setFont(f);
    for (int i = 0; i < xTickLabels.length(); i++) {
        int xTickLoc = leftOfGraph+((rightOfGraph-leftOfGraph)*(xTickLabels[i]/(maxX-minX)));
        QRect labelBox = QRect(xTickLoc-10, bottomOfGraph, 20, 20);
        painter.drawText(labelBox, Qt::AlignCenter,
                         QString("%1").arg(xTickLabels[i]));
        painter.drawLine(xTickLoc,bottomOfGraph,xTickLoc,bottomOfGraph+2);
    }



    //Put unit labels on the y axis
    for (int i = 0; i < yTickLabels.length(); i++) {
//        int yTickLoc = bottomOfGraph-((bottomOfGraph-topOfGraph)*(yTickLabels[i]/(maxY)));
        int yTickLoc = bottomOfGraph-((bottomOfGraph-topOfGraph)*(yTickLabels[i]/(maxY-minY)));
        //topOfGraph + (i*((bottomOfGraph - topOfGraph)/(yTickLabels.length()-1)));
        QRect labelBox= QRect(leftOfGraph-margin, yTickLoc-10, 25, 20);
        painter.drawText(labelBox, Qt::AlignCenter,
                         QString("%1").arg(yTickLabels[i]));
        painter.drawLine(leftOfGraph,yTickLoc,leftOfGraph-2,yTickLoc);

    }

    painter.end();
}

void Abstract2DDataPlotWidget::resizeEvent(QResizeEvent *event) {

    update();

}



//-------------------------------------------
RasterPlot::RasterPlot(QWidget *parent) {

    setMinimumHeight(10);
    setMinimumWidth(200);
    minX = 0;
    maxX = 10;
    setSizePolicy(QSizePolicy ::Expanding , QSizePolicy ::Expanding );

    /*
    QVector<qreal> tmpValues;
    for (int i = 0; i < 200; i++) {
        tmpValues.append(i);
    }

    setData(tmpValues);*/

}


void RasterPlot::setXRange(qreal minR, qreal maxR) {
    maxX = maxR;
    minX = minR;
    //xTickLabels.clear();
    //xTickLabels.append(minX);
    //xTickLabels.append(maxX);
    //xTickLabels.append((maxX-minX)/2);
    update();
}

void RasterPlot::setXLabel(QString l) {
    xLabel = l;
    update();
}

void RasterPlot::setYLabel(QString l) {
    yLabel = l;
    update();
}

void RasterPlot::addRaster(const QVector<qreal> &times) {
    eventTimes.push_back(times);
    //setMinimumHeight(eventTimes.length()*4);
    update();
}

void RasterPlot::setRasters(const QVector<QVector<qreal> > &times) {
    eventTimes.clear();
    for (int i=0; i<times.length(); i++) {
        eventTimes.push_back(times[i]);
    }

    //Add y axis ticks
    yTickLabels.clear();
    yTickLabels.append(0);
    yTickLabels.append(eventTimes.length()/2);
    yTickLabels.append(eventTimes.length());

    update();
}

void RasterPlot::clearRasters() {
    eventTimes.clear();
    update();
}

void RasterPlot::paintEvent(QPaintEvent *event) {
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    QPen pen;
    //We want the lines to be a set number of pixels in width
    //units per pixel = unitrange/window_width
    pen.setWidth(1);

    //pen.setWidth(100*TLength);

    pen.setColor(Qt::black);
    painter.setPen(pen);
    QFont f;
    f.setPixelSize(10);
    painter.setFont(f);
    //painter.setPen(Qt::green);
    painter.setViewTransformEnabled(true);
    painter.setViewport(0,0,width(), height());


    qreal numRows = eventTimes.length();
    //qreal maxRange = maxY;

    int margin = 30;

    int topOfGraph = 5;
    int bottomOfGraph = height()-5;
    // int graphHeight = bottomOfGraph-topOfGraph; // UNUSED

    int leftOfGraph = margin+20;
    int rightOfGraph = width()-margin;
    // int graphWidth = rightOfGraph-leftOfGraph; // UNUSED

    int rowSpacing = 1;
    int tickHeight = ((bottomOfGraph - topOfGraph)/numRows) - (2*rowSpacing);
    if (tickHeight < 1) {
        tickHeight = 1;
    }


    if ((maxX-minX) > 0) {
        for (int r=0; r < eventTimes.length(); r++) {
            int currentRowPix = topOfGraph + rowSpacing + (r*((bottomOfGraph - topOfGraph)/numRows));
            for (int e = 0; e < eventTimes[r].length(); e++) {
                if ((eventTimes[r][e] >= minX) && (eventTimes[r][e] <= maxX)) {
                    int xLoc = leftOfGraph+((rightOfGraph-leftOfGraph)*((eventTimes[r][e]-minX)/(maxX-minX)));
                    painter.drawLine(xLoc,currentRowPix,xLoc,currentRowPix+tickHeight);
                }
            }
        }
    }



//    //Draw the axes
//    painter.drawLine(leftOfGraph,bottomOfGraph,rightOfGraph+5,bottomOfGraph);
    painter.drawLine(leftOfGraph,bottomOfGraph,leftOfGraph,topOfGraph-5);

//    //Display the axis labels
//    QRect YLabelBox1= QRect(leftOfGraph, height()-25, rightOfGraph-leftOfGraph, 25);
//    painter.drawText(YLabelBox1, Qt::AlignCenter, xLabel);

    painter.rotate(-90);
    QRect YLabelBox= QRect(-bottomOfGraph, 0, bottomOfGraph-topOfGraph, 25);
    painter.drawText(YLabelBox, Qt::AlignCenter, yLabel);
    painter.rotate(90);

//    //Put unit labels on the x axis

//    for (int i = 0; i < xTickLabels.length(); i++) {
//        int xTickLoc = leftOfGraph+((rightOfGraph-leftOfGraph)*(xTickLabels[i]/(maxX-minX)));
//        QRect labelBox = QRect(xTickLoc-10, bottomOfGraph, 20, 20);
//        painter.drawText(labelBox, Qt::AlignCenter,
//                         QString("%1").arg(xTickLabels[i]));
//        painter.drawLine(xTickLoc,bottomOfGraph,xTickLoc,bottomOfGraph+2);
//    }



    //Put unit labels on the y axis
    for (int i = 0; i < yTickLabels.length(); i++) {
//        int yTickLoc = bottomOfGraph-((bottomOfGraph-topOfGraph)*(yTickLabels[i]/(maxY)));
        int yTickLoc =// bottomOfGraph-((bottomOfGraph-topOfGraph)*((qreal)i/(yTickLabels.length()-1)));
        topOfGraph + (i*((bottomOfGraph - topOfGraph)/(yTickLabels.length()-1)));
        QRect labelBox= QRect(leftOfGraph-margin, yTickLoc-10, 25, 20);
        painter.drawText(labelBox, Qt::AlignCenter,
                         QString("%1").arg(yTickLabels[i]));
        painter.drawLine(leftOfGraph,yTickLoc,leftOfGraph-2,yTickLoc);

    }

    painter.end();
}

void RasterPlot::resizeEvent(QResizeEvent *event) {

    update();

}



//--------------------------------------------

HistogramPlot::HistogramPlot(QWidget *parent) {

    setMinimumHeight(100);
    setMinimumWidth(200);
    maxY = 0.0;

    //setSizePolicy(QSizePolicy ::Expanding , QSizePolicy ::Expanding );



//    QVector<qreal> tmpValues;
//    for (int i = 0; i < 200; i++) {
//        tmpValues.append(i);
//    }

//    setData(tmpValues);
    errorsIndicator = false;
    binsIndicator = false;
    bin1 = new QRectF();
    bin2 = new QRectF();

    margin = 30;
    topOfGraph = 5;
    for(int i = 0; i < 4; i++)
        errBar[i] = false;

    clicking = false;
    barColor = QColor(100,100,100);
}


void HistogramPlot::setXRange(qreal minR, qreal maxR) {
    maxX = maxR;
    minX = minR;
    xTickLabels.clear();
    xTickLabels.append(minX);
    xTickLabels.append(maxX);
    xTickLabels.append(((maxX-minX)/2)+minX);
    update();
}

void HistogramPlot::setXLabel(QString l) {
    xLabel = l;
    update();
}

void HistogramPlot::setYLabel(QString l) {
    yLabel = l;
    update();
}

void HistogramPlot::setColor(QColor c){
    barColor = c;
}

void HistogramPlot::setData(QVector<qreal> bvalues){
    //Copy values and set MaxY
    maxY = 0.0;
    barValues.clear();
    for (int i=0; i<bvalues.length(); i++) {
        barValues.append(bvalues[i]);
        if (barValues[i] > maxY) {
            maxY = barValues[i];
        }
    }
    minX = 0;
    maxX = bvalues.length();

    xTickLabels.clear();
    xTickLabels.append(minX);
    xTickLabels.append(maxX);
    xTickLabels.append(((maxX-minX)/2)+minX);

    yTickLabels.clear();
    yTickLabels.append(maxY);

    update();
}

void HistogramPlot::clearData(){
    barValues.clear();
    update();
}

void HistogramPlot::clickingOn(bool on, int cl){
    clicking = on;
    cluster = cl;
    setMouseTracking(on);
    if(clicking){
        this->setStyleSheet("HistogramPlot:hover {border: 2px solid red;"
                            "border-radius: 4px;"
                            "padding: 2px;}");
    }
}

void HistogramPlot::setErrorBars(QVector<QVector<qreal> > hilowErrValues){
    for(int i = 2; i<hilowErrValues.length(); i+=2)
        for(int j = 0; j<hilowErrValues[i].length(); j++)
            if(errBar[i/2] && hilowErrValues[i][j] > maxY)
                maxY = hilowErrValues[i][j];

    hiloerrorBarValues.clear();
    hiloerrorBarValues = hilowErrValues;
}

void HistogramPlot::setChecked(bool checked[]){
    errorsIndicator = true;
//    for(int i = 0; i < 4; i++){
//        this->checked[i] = checked[i];
//    }
    this->errBar[1] = checked[1];
}

void HistogramPlot::setBinValues(qreal bin1Start, qreal bin1Size, qreal bin2Start, qreal bin2Size){
    this->binsIndicator = true;
    this->bin1Start = bin1Start;
    this->bin1Size = bin1Size;
    this->bin2Start = bin2Start;
    this->bin2Size = bin2Size;
}

void HistogramPlot::mousePressEvent(QMouseEvent *event){
    if(clicking){
        emit PSTHRequest(cluster);
    }
}

void HistogramPlot::mouseMoveEvent(QMouseEvent *event){
    if(clicking){

    }
}

void HistogramPlot::paintEvent(QPaintEvent *event) {
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    QPen pen;

    QStyleOption opt;
    opt.init(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);
    //We want the lines to be a set number of pixels in width
    //units per pixel = unitrange/window_width
    pen.setWidth(1);

    //pen.setWidth(100*TLength);

    pen.setColor(Qt::black);
    painter.setPen(pen);
    QFont f;
    f.setPixelSize(10);
    painter.setFont(f);
    //painter.setPen(Qt::green);
    painter.setViewTransformEnabled(true);
    painter.setViewport(0,0,width(), height());

    qreal numBars = barValues.length();
    maxYDisplay = ceil(maxY/50)*50;
    qreal maxRange = maxY;//maxYDisplay;


    int bottomOfGraph = height()-margin-10;
    int graphHeight = bottomOfGraph-topOfGraph;

    int leftOfGraph = margin+20;
    int rightOfGraph = width()-margin;
    int graphWidth = rightOfGraph-leftOfGraph;

    Qt::GlobalColor colors[4] = {Qt::red, Qt::black, Qt::blue, Qt::magenta};


    //Draw the axes
    painter.drawLine(leftOfGraph,bottomOfGraph,rightOfGraph+5,bottomOfGraph);
    painter.drawLine(leftOfGraph,bottomOfGraph,leftOfGraph,topOfGraph-5);
    for (int c=0; c < barValues.length(); c++) {
        //bar values
        qreal xcorner = leftOfGraph + (c*(graphWidth/numBars));
        qreal ycorner = bottomOfGraph-((barValues[c]/maxRange)*graphHeight);
        qreal barwidth = graphWidth/numBars;
        qreal barheight = (barValues[c]/maxRange)*graphHeight;

        //Draw histogram bars
        QRectF rect = QRectF(xcorner, ycorner, barwidth, barheight);
        //QRectF rect = QRectF(c*(width()/numBars), height()-((barValues[c]/maxRange)*height()), (width()/numBars), ((barValues[c]/maxRange)*height()));
        painter.drawRect(rect);
        painter.fillRect(rect,QBrush(barColor));
    }


    //Previously, there was capability for multiple different error bars on each bar in the histogram
    //They have been disabled in the psth window class, but the code is kept here in case of possible
    //future changes/additions to the error bars.
    if(errorsIndicator){
        int numOfErrBars = 0;
        for(int i = 0; i < 4; i++)
            numOfErrBars += (int)errBar[i];

        for (int c=0; c < barValues.length(); c++) {
            int ithbar = 0;
            qreal xcorner = leftOfGraph + (c*(graphWidth/numBars));
            for(int i = 0; i < 4; i++){
                if(!errBar[i])
                    continue;
                ithbar++;
                painter.setPen(colors[i]);
                qreal xMid = xcorner + (((qreal)(ithbar)/(numOfErrBars+1))*(graphWidth/numBars));
                qreal yMidhigher = bottomOfGraph-((hiloerrorBarValues[2*i][c])*graphHeight/maxRange);
                qreal yMidlower = bottomOfGraph-((hiloerrorBarValues[2*i+1][c])*graphHeight/maxRange);

                QLine line = QLine(xMid, yMidhigher, xMid, yMidlower);
                QLine tick1 = QLine(xMid-2, yMidhigher, xMid+2, yMidhigher);
                QLine tick2 = QLine(xMid-2, yMidlower, xMid+2, yMidlower);

                painter.drawLine(line);
                painter.drawLine(tick1);
                painter.drawLine(tick2);
            }
            painter.setPen(pen);
        }
    }

    if(binsIndicator){
        //Draw bin 1 and bin 2 indicators
        QPen bin1Pen(QColor(255, 0, 0, 100));
        QPen bin2Pen(QColor(0, 0, 255, 100));
        qreal bin1Left = leftOfGraph + (bin1Start*(graphWidth)/numBars);
        qreal bin1Width = bin1Size*(graphWidth/numBars);
        QRectF bin1Rect(bin1Left, topOfGraph, bin1Width, graphHeight);
        qreal bin2Left = leftOfGraph + (bin2Start*(graphWidth)/numBars);
        qreal bin2Width = bin2Size*(graphWidth/numBars);
        QRectF bin2Rect(bin2Left, topOfGraph, bin2Width, graphHeight);
        painter.setPen(bin1Pen);
        painter.drawRect(bin1Rect);
        painter.setPen(bin2Pen);
        painter.drawRect(bin2Rect);
        painter.fillRect(bin1Rect, QColor(255, 0, 0, 15));
        painter.fillRect(bin2Rect, QColor(0, 0, 255, 15));

        painter.setPen(pen);
    }
    //Put unit labels on the x axis

    for (int i = 0; i < xTickLabels.length(); i++) {
        int xTickLoc = leftOfGraph+((rightOfGraph-leftOfGraph)*((xTickLabels[i]-minX)/(maxX-minX)));
        QRect labelBox = QRect(xTickLoc-10, bottomOfGraph, 20, 20);
        painter.drawText(labelBox, Qt::AlignCenter,
                         QString::number(xTickLabels[i], 'f', 1 ));
        painter.drawLine(xTickLoc,bottomOfGraph,xTickLoc,bottomOfGraph+2);
    }

    //Put unit labels on the y axis
    for (int i = 0; i < yTickLabels.length(); i++) {
        int yTickLoc = bottomOfGraph-((bottomOfGraph-topOfGraph)*(yTickLabels[i]/(maxY)));
        QRect labelBox= QRect(leftOfGraph-margin, yTickLoc-10, 25, 20);
        painter.drawText(labelBox, Qt::AlignCenter,
                         QString::number(yTickLabels[i], 'f', 1 ));
        painter.drawLine(leftOfGraph,yTickLoc,leftOfGraph-2,yTickLoc);

    }


    //Display the axis labels
    QRect YLabelBox1= QRect(leftOfGraph, height()-25, rightOfGraph-leftOfGraph, 25);
    painter.drawText(YLabelBox1, Qt::AlignCenter, xLabel);

    painter.rotate(-90);
    QRect YLabelBox= QRect(-bottomOfGraph, 0, bottomOfGraph-topOfGraph, 25);
    painter.drawText(YLabelBox, Qt::AlignCenter, yLabel);
    painter.rotate(90);

    painter.end();
}


void HistogramPlot::resizeEvent(QResizeEvent *event) {
    //scene->setSceneRect(0,0,event->size().width(),event->size().height());
    //scene->setSceneRect(0,0,event->size().width(),event->size().height());
    update();

}

//-------------------------------------------

PSTHDialog::PSTHDialog(QWidget *parent)
    :QWidget(parent){

    setGeometry(300,300,300,300);
    setMinimumHeight(300);
    setMinimumWidth(300);


    rasterWindow = new RasterPlot(this);
    rasterWindow->setYLabel("Trial Number");
    window = new HistogramPlot(this);
    window->setMaximumHeight(200);
    window->setXLabel("Time relative to event (sec)");
    window->setYLabel("Rate (Hz)");

    QGridLayout *mainLayout = new QGridLayout;
    QGridLayout *plotLayout = new QGridLayout;
    plotLayout->setVerticalSpacing(0);
    plotLayout->setContentsMargins(0,0,0,0);
    plotLayout->addWidget(rasterWindow,0,0);
    plotLayout->addWidget(window,1,0);
    plotLayout->setRowStretch(0,3);
    plotLayout->setRowStretch(1,1);
    mainLayout->addLayout(plotLayout,0,0);

    setUpControlPanel();
    //controlLayout->setColumnStretch(0,1);
    mainLayout->addLayout(controlLayout,1,0);
    mainLayout->setRowStretch(0,1);


    setLayout(mainLayout);
    window->show();
    rasterWindow->show();

    //Remembered settings...
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    //Place the window where it was the last session
    settings.beginGroup(QLatin1String("position"));
    QRect tempPosition = settings.value(QLatin1String("PSTHposition")).toRect();
    if (tempPosition.height() > 0) {
        setGeometry(tempPosition);
    }
    settings.endGroup();

    settings.beginGroup(QLatin1String("PSTHsettings"));
    int tempNumBins = settings.value(QLatin1String("numbins")).toInt();
    if (tempNumBins > 0) {
        numBinsSpinBox->setValue(tempNumBins);
    } else {
        numBinsSpinBox->setValue(100);
    }

    int tempMsecRange = settings.value(QLatin1String("msecRange")).toInt();
    if (tempMsecRange > 0) {
        rangeSpinBox->setValue(tempMsecRange);
    } else {
        rangeSpinBox->setValue(500);
    }

    /*absTimeRange = 1.0;
    numBins = 80; //Total number of bins in range
    binSize = (2*absTimeRange)/numBins;*/
    int tmpValue;
//    tmpValue = settings.value(QLatin1String("sdCheck")).toInt();
//    setSDChecked(tmpValue);
//    SDCheckbox->setChecked((bool)tmpValue);

    tmpValue = settings.value(QLatin1String("seCheck")).toInt();
    setSEChecked(tmpValue);
    SECheckbox->setChecked((bool)tmpValue);

//    tmpValue = settings.value(QLatin1String("rCheck")).toInt();
//    setRChecked(tmpValue);
//    RCheckbox->setChecked((bool)tmpValue);

//    tmpValue = settings.value(QLatin1String("ciCheck")).toInt();
//    setCIChecked(settings.value(QLatin1String("ciCheck")).toInt());
//    CICheckbox->setChecked((bool)tmpValue);


    tmpValue = settings.value(QLatin1String("bin1Start")).toInt();
    if(tmpValue)
        binOneStartBox->setValue(tmpValue);
    else
        binOneStartBox->setValue(-200);

    tmpValue = settings.value(QLatin1String("bin1Size")).toInt();
    if(tmpValue)
        binOneSizeBox->setValue(tmpValue);
    else
        binOneSizeBox->setValue(100);

    tmpValue = settings.value(QLatin1String("bin2Start")).toInt();
    if(tmpValue)
        binTwoStartBox->setValue(tmpValue);
    else
        binTwoStartBox->setValue(200);

    tmpValue = settings.value(QLatin1String("bin2Size")).toInt();
    if(tmpValue)
        binTwoSizeBox->setValue(tmpValue);
    else
        binTwoSizeBox->setValue(100);

    settings.endGroup();


    /*
    QVector<uint32_t> trialTimes;
    QVector<uint32_t> eventTimes;

    for (uint32_t t = 5; t < 1000000; t = t+30000) {
        trialTimes.append(t);
    }

    for (uint32_t t = 5; t < 1000000; t = t+5000) {
        eventTimes.append(t);
    }

    plot(trialTimes,eventTimes);*/

}

//Creates everything for bottom of PSTH window
void PSTHDialog::setUpControlPanel(){
    rangeLabel = new QLabel("+/- msec");
    binsLabel = new QLabel("Bins");
    QFont labelFont;
    labelFont.setPixelSize(12);
    rangeLabel->setFont(labelFont);
    binsLabel->setFont(labelFont);
    rangeLabel->setMaximumHeight(25);
    binsLabel->setMaximumHeight(25);

    rangeSpinBox = new QSpinBox(this);
    rangeSpinBox->setFrame(false);
    rangeSpinBox->setStyleSheet("QSpinBox { background-color: white; }");
    rangeSpinBox->setMinimum(50);
    rangeSpinBox->setMaximum(1000);
    rangeSpinBox->setSingleStep(10);
    rangeSpinBox->setFixedSize(50,22);
    //rangeSpinBox->setAlignment(Qt::AlignRight);
    rangeSpinBox->setFocusPolicy(Qt::NoFocus);
    rangeSpinBox->setToolTip(tr("Display range (+/- msec)"));

    numBinsSpinBox = new QSpinBox(this);
    numBinsSpinBox->setFrame(false);
    numBinsSpinBox->setStyleSheet("QSpinBox { background-color: white; }");
    numBinsSpinBox->setMinimum(20);
    numBinsSpinBox->setMaximum(200);
    numBinsSpinBox->setSingleStep(1);
    numBinsSpinBox->setFixedSize(50,22);
    //rangeSpinBox->setAlignment(Qt::AlignRight);
    numBinsSpinBox->setFocusPolicy(Qt::NoFocus);
    numBinsSpinBox->setToolTip(tr("Number of bins"));

//    SDCheckbox = new QCheckBox("Std. Dev",this);
    SECheckbox = new QCheckBox("Std. Error", this);
//    RCheckbox = new QCheckBox("Range", this);
//    CICheckbox = new QCheckBox("Conf. Interval", this);
//    SDCheckbox->setStyleSheet("QCheckBox::indicator:checked{ background: red }");
    SECheckbox->setStyleSheet("QCheckBox::indicator:checked{ background: black }");
//    RCheckbox->setStyleSheet("QCheckBox::indicator:checked{ background: blue }");
//    CICheckbox->setStyleSheet("QCheckBox::indicator:checked{ background: magenta }");

    binOneStartLabel = new QLabel("Bin 1 Start");
    binOneSizeLabel = new QLabel("Bin 1 Size");
    binTwoStartLabel = new QLabel("Bin 2 Start");
    binTwoSizeLabel = new QLabel("Bin 2 Size");
    binOneStartLabel->setFont(labelFont);
    binOneSizeLabel->setFont(labelFont);
    binTwoStartLabel->setFont(labelFont);
    binTwoSizeLabel->setFont(labelFont);

    binOneStartBox = new QSpinBox(this);
    binOneStartBox->setMinimum(-1000);
    binOneStartBox->setMaximum(1000);
    binOneStartBox->setSingleStep(10);
    binOneStartBox->setFrame(false);
    binOneStartBox->setStyleSheet("QSpinBox { background-color: white; }");

    binOneSizeBox = new QSpinBox(this);
    binOneSizeBox->setMinimum(0);
    binOneSizeBox->setMaximum(2*1000);
    binOneSizeBox->setSingleStep(10);
    binOneSizeBox->setFrame(false);

    binTwoStartBox = new QSpinBox(this);
    binTwoStartBox->setMinimum(-1000);
    binTwoStartBox->setMaximum(1000);
    binTwoStartBox->setSingleStep(10);
    binTwoStartBox->setFrame(false);

    binTwoSizeBox = new QSpinBox(this);
    binTwoSizeBox->setMinimum(0);
    binTwoSizeBox->setMaximum(2*1000);
    binTwoSizeBox->setSingleStep(10);
    binTwoSizeBox->setFrame(false);

    binSumStatsText = new QTextEdit();
    binSumStatsText->setText(generateSumStatsText());

    connect(rangeSpinBox,SIGNAL(valueChanged(int)),this,SLOT(setRange(int)));
    connect(numBinsSpinBox,SIGNAL(valueChanged(int)),this,SLOT(setNumBins(int)));
    connect(rangeSpinBox, SIGNAL(valueChanged(int)), this, SIGNAL(rangeChanged(int)));
    connect(numBinsSpinBox, SIGNAL(valueChanged(int)), this, SIGNAL(binsChanged(int)));

//    connect(SDCheckbox, SIGNAL(stateChanged(int)), this, SLOT(setSDChecked(int)));
    connect(SECheckbox, SIGNAL(stateChanged(int)), this, SLOT(setSEChecked(int)));
//    connect(RCheckbox, SIGNAL(stateChanged(int)), this, SLOT(setRChecked(int)));
//    connect(CICheckbox, SIGNAL(stateChanged(int)), this, SLOT(setCIChecked(int)));

    connect(binOneStartBox, SIGNAL(valueChanged(int)), this, SLOT(setBinOneStart(int)));
    connect(binOneSizeBox, SIGNAL(valueChanged(int)), this, SLOT(setBinOneSize(int)));
    connect(binTwoStartBox, SIGNAL(valueChanged(int)), this, SLOT(setBinTwoStart(int)));
    connect(binTwoSizeBox, SIGNAL(valueChanged(int)), this, SLOT(setBinTwoSize(int)));

    /*rangeSpinBox->setMaximumWidth(30);
    numBinsSpinBox->setMaximumWidth(30);
    rangeLabel->setMaximumWidth(30);
    binsLabel->setMaximumWidth(30);*/

    //rangeSpinBox->setMaximumHeight(30);
    //numBinsSpinBox->setMaximumHeight(30);

    controlLayout = new QGridLayout;
    controlLayout->setVerticalSpacing(2);
    controlLayout->setHorizontalSpacing(10);
    controlLayout->setContentsMargins(0,0,0,0);

    int binstatsColSpan = 4;
    generateSumStatsLabel();

//    controlLayout->addWidget(binSumStatsGrid, 0, 1);
    controlLayout->addWidget(binOneStartLabel, 0,2+binstatsColSpan);
    controlLayout->addWidget(binOneSizeLabel, 0,3+binstatsColSpan);

    controlLayout->addWidget(binOneStartBox, 1,2+binstatsColSpan);
    controlLayout->addWidget(binOneSizeBox, 1,3+binstatsColSpan);

    controlLayout->addWidget(binTwoStartLabel, 2,2+binstatsColSpan);
    controlLayout->addWidget(binTwoSizeLabel, 2,3+binstatsColSpan);

    controlLayout->addWidget(binTwoStartBox, 3,2+binstatsColSpan);
    controlLayout->addWidget(binTwoSizeBox, 3,3+binstatsColSpan);

    controlLayout->addWidget(rangeLabel,0,4+binstatsColSpan);
    controlLayout->addWidget(binsLabel,0,5+binstatsColSpan);
    controlLayout->addWidget(rangeSpinBox,1,4+binstatsColSpan);
    controlLayout->addWidget(numBinsSpinBox,1,5+binstatsColSpan);

//    controlLayout->addWidget(SDCheckbox, 0, 6);
    controlLayout->addWidget(SECheckbox, 2, 4+binstatsColSpan);
//    controlLayout->addWidget(RCheckbox, 2, 6);
//    controlLayout->addWidget(CICheckbox, 3, 6);

    controlLayout->setColumnStretch(0, 3);
    controlLayout->setColumnStretch(5, 1);
}

void PSTHDialog::generateSumStatsLabel(){
    //First quick and ugly version to get sumstats on the ui
    minLabel1 = new QLabel();
    medLabel1 = new QLabel();
    meanLabel1 = new QLabel();
    maxLabel1 = new QLabel();
    numLabel1 = new QLabel();
    minLabel2 = new QLabel();
    medLabel2 = new QLabel();
    meanLabel2 = new QLabel();
    maxLabel2 = new QLabel();
    numLabel2 = new QLabel();
    QLabel *bin1 = new QLabel("Bin 1");
    bin1->setStyleSheet("QLabel { color : red; }");
    QLabel *bin2 = new QLabel("Bin 2");
    bin2->setStyleSheet("QLabel { color : blue; }");

    controlLayout->addWidget(new QLabel("Min"), 1, 1);
    controlLayout->addWidget(new QLabel("Med"), 2, 1);
    controlLayout->addWidget(new QLabel("Mean"), 3, 1);
    controlLayout->addWidget(new QLabel("Max"), 4, 1);
    controlLayout->addWidget(new QLabel("Num"), 5, 1);
    controlLayout->addWidget(bin1, 0, 2);
    controlLayout->addWidget(bin2, 0, 4);
    controlLayout->addWidget(minLabel1, 1, 2);
    controlLayout->addWidget(medLabel1, 2, 2);
    controlLayout->addWidget(meanLabel1, 3, 2);
    controlLayout->addWidget(maxLabel1, 4, 2);
    controlLayout->addWidget(numLabel1, 5, 2);
    controlLayout->addWidget(minLabel2, 1, 4);
    controlLayout->addWidget(medLabel2, 2, 4);
    controlLayout->addWidget(meanLabel2, 3, 4);
    controlLayout->addWidget(maxLabel2, 4, 4);
    controlLayout->addWidget(numLabel2, 5, 4);

    controlLayout->setColumnMinimumWidth(1, 10);
    controlLayout->setColumnMinimumWidth(2, 10);
    controlLayout->setColumnMinimumWidth(3, 10);
}


void PSTHDialog::resizeEvent(QResizeEvent *event) {
    //Remember the new size for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("position"));
    settings.setValue(QLatin1String("PSTHposition"), this->geometry());
    settings.endGroup();
}

void PSTHDialog::plot(const QVector<uint32_t> &trialTimesIn, const QVector<uint32_t> &eventTimesIn) {

    trialTimes.clear();
    trialTimes.resize(trialTimesIn.length());
    for (int i=0; i < trialTimesIn.length(); i++) {
        trialTimes[i] = trialTimesIn[i];
    }

    eventTimes.clear();
    eventTimes.resize(eventTimesIn.length());
    for (int i=0; i < eventTimesIn.length(); i++) {
        eventTimes[i] = eventTimesIn[i];
    }

    calcDisplay();

}

//SLOT functions
void PSTHDialog::setNumBins(int nBins) {
    //absTimeRange = 1.0;
    numBins = nBins; //Total number of bins in range
    binSize = (2*absTimeRange)/numBins;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("numbins"), numBins);
    settings.endGroup();
}

void PSTHDialog::setRange(int msecRange) {
    absTimeRange = (qreal)msecRange/1000;
    binSize = (2*absTimeRange)/numBins;
    calcDisplay();
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("msecRange"), msecRange);
    settings.endGroup();
}

void PSTHDialog::setSEChecked(int c){
    checked[1] = (bool)c;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("seCheck"), c);
    settings.endGroup();
}

void PSTHDialog::setSDChecked(int c){
    checked[0] = (bool)c;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("sdCheck"), c);
    settings.endGroup();
}
void PSTHDialog::setRChecked(int c){
    checked[2] = (bool)c;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("rCheck"), c);
    settings.endGroup();
}
void PSTHDialog::setCIChecked(int c){
    checked[3] = (bool)c;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("ciCheck"), c);
    settings.endGroup();
}
void PSTHDialog::setBinOneStart(int num){
    binOneStart = (qreal)num/1000;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("bin1Start"), num);
    settings.endGroup();
}
void PSTHDialog::setBinOneSize(int num){
    binOneSize = (qreal)num/1000;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("bin1Size"), num);
    settings.endGroup();
}
void PSTHDialog::setBinTwoStart(int num){
    binTwoStart = (qreal)num/1000;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("bin2Start"), num);
    settings.endGroup();
}
void PSTHDialog::setBinTwoSize(int num){
    binTwoSize = (qreal)num/1000;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("bin2Size"), num);
    settings.endGroup();
}
//Organizes trials and events according to specified settings to be passed to displays
void PSTHDialog::calcDisplay() {

    uint sampRate = hardwareConf->sourceSamplingRate;
    uint rangeInt = absTimeRange*sampRate;
    QVector<QVector<qreal> > trialEventTimes;
    QVector<QVector<int> > binTrialFreqs;
    numTrials = trialTimes.length();
//    if(numTrials<=1){
//        qDebug() << "Warning: Number of trials too low!";
//        return;
//    }
    QVector<int> binOneCounts(numTrials, 0);
    QVector<int> binTwoCounts(numTrials, 0);

    counts.clear();
    counts.fill(0, numBins);
//    for (int i=0; i < numBins; i++) {
//        counts.push_back(0);
//    }

    int currentEventInd = 0;
    uint32_t trialWindowStart;
    uint32_t trialWindowEnd;
    for (int trialInd=0; trialInd < trialTimes.length(); trialInd++) {
        QVector<qreal> relEventTimes;
        QVector<int> trialFreqs(numBins, 0);

        //Scan the event times until we enter the next trial window
        if (trialTimes[trialInd] > rangeInt) {
            trialWindowStart = trialTimes[trialInd] - rangeInt;
        } else {
            trialWindowStart = 0;
        }
        trialWindowEnd = trialTimes[trialInd] + rangeInt;
        while ((eventTimes[currentEventInd] < trialWindowStart) && (currentEventInd < eventTimes.length())) {
            currentEventInd++;
        }

        //Now bin the spikes within the trial window
        int trialEventInd = 0;
        while ((eventTimes[currentEventInd+trialEventInd] < trialWindowEnd)
               && ((currentEventInd+trialEventInd) < eventTimes.length())) {

            qreal tmpRelEventTime = ((qreal)eventTimes[currentEventInd+trialEventInd]-(qreal)trialTimes[trialInd])/sampRate;
            relEventTimes.append(tmpRelEventTime);

            //Counts for all bins for histogram and for error bars
            for (int i=0; i < numBins; i++) {
                if (tmpRelEventTime <= -absTimeRange + (i*binSize)+binSize) {
                    trialFreqs[i]++;
                    counts[i]++;
                    break;
                }
            }

            //Counts for two user defined bins for sum stats
            if(tmpRelEventTime >= binOneStart && tmpRelEventTime < binOneStart+binOneSize)
                binOneCounts[trialInd]++;
            if(tmpRelEventTime >= binTwoStart && tmpRelEventTime < binTwoStart+binTwoSize)
                binTwoCounts[trialInd]++;

            trialEventInd++;
        }
        binTrialFreqs.append(trialFreqs);
        trialEventTimes.append(relEventTimes);
        binOneCounts[trialInd] /= (binOneSize);
        binTwoCounts[trialInd] /= (binTwoSize);
    }

    //Bin sizes/rates calculation
    QVector<qreal> rates;
    for (int i=0; i < numBins; i++) {
        rates.push_back(counts[i]/(binSize*numTrials));
    }

    //Calculates all error functions so that user can pick which to display in histogramplot
    QVector<QVector<qreal> > eValues;
    eValues = calcErrorBars(binTrialFreqs, 0.95);

    calcSumStats(binOneCounts, 1);
    calcSumStats(binTwoCounts, 2);
//    binSumStatsText->clear();
//    binSumStatsText->setText(generateSumStatsText());

    window->setErrorBars(eValues);
    window->setChecked(checked);
    window->setBinValues((absTimeRange+binOneStart)/binSize,
                    binOneSize/binSize,
                    (absTimeRange+binTwoStart)/binSize,
                    binTwoSize/binSize);
    window->setData(rates);
    window->setXRange(-absTimeRange, absTimeRange);


    rasterWindow->setXRange(-absTimeRange, absTimeRange);
    rasterWindow->setRasters(trialEventTimes);

    /*
    QVector<qreal> tmpValues;
    for (int i = 0; i < 200; i++) {
        tmpValues.append(i);
    }
    window->setData(tmpValues);
    window->setXRange(-absTimeRange, absTimeRange);

    rasterWindow->setXRange(-absTimeRange, absTimeRange);
    QVector<qreal> faketimes;
    faketimes.append(-1.5);
    faketimes.append(-1);
    faketimes.append(-.75);
    faketimes.append(-.1);
    faketimes.append(1.7);
    faketimes.append(3.3);
    faketimes.append(5.6);
    faketimes.append(7.7);

    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    */

}

QString PSTHDialog::generateSumStatsText(){
    QMapIterator<QString, qreal> i(binOneSumStats);
    QMapIterator<QString, qreal> j(binTwoSumStats);
    QString txt = "Stat\t\tBin 1\t\tBin 2\n";

    while(i.hasNext() && j.hasNext()){
        i.next();
        j.next();
        txt += i.key() + "\t\t" + QString::number(i.value(), 'g', 3) + "\t\t";
        txt += QString::number(j.value(), 'g', 3) + "\n";

    }
    return txt;
}

void PSTHDialog::calcSumStats(QVector<int> data, int bin){
    qreal min = DBL_MAX;
    qreal max = DBL_MIN;
    qreal median = 0;
    qreal mean = 0;

    qSort(data);
    for(int i = 0; i < data.length(); i++){
        if(data[i] > max)
            max = data[i];
        if(data[i] < min)
            min = data[i];
        if(i == data.length()/2){
            if(data.length()%2 != 0)
                median = data[i];
            else
                median = (data[i] + data[i-1])/2;
        }
        mean += data[i];
    }
    mean = mean/data.length();

    if(bin == 1){
        minLabel1->setText(QString::number(min, 'f',3));
        medLabel1->setText(QString::number(median, 'f',3));
        meanLabel1->setText(QString::number(mean, 'f', 3));
        maxLabel1->setText(QString::number(max, 'f',3));
        numLabel1->setText(QString::number(data.length()));
    }
    else if(bin==2){
        minLabel2->setText(QString::number(min, 'f',3));
        medLabel2->setText(QString::number(median, 'f', 3));
        meanLabel2->setText(QString::number(mean, 'f', 3));
        maxLabel2->setText(QString::number(max, 'f',3));
        numLabel2->setText(QString::number(data.length()));

    }

    return;
}

QVector<QVector<qreal> > PSTHDialog::calcErrorBars(QVector<QVector<int> > data, qreal pct){
    QVector<QVector<qreal> > allHiLoValues;
    allHiLoValues.append(stdDev(data));
    allHiLoValues.append(stdError(data));
    allHiLoValues.append(range(data));
    allHiLoValues.append(confInt(data, pct));
    return allHiLoValues;
}


//All of the following functions will break if variable --data-- is not of length --numTrials-- and each vector inside
//is not of length --numBins--, serves as implicit check for parameter accuracy

QVector<QVector<qreal> > PSTHDialog::stdDev(QVector<QVector<int> > data){
    QVector<QVector<qreal> > hilowValues;
    QVector<qreal> hiValues;
    QVector<qreal> loValues;

    for(int j = 0; j < numBins; j++){
        qreal avg = 0;
        qreal sumSq = 0;
        avg = counts[j]/(binSize*numTrials);
        for(int i = 0; i < numTrials; i++){
            sumSq += pow(data[i][j]/(binSize) - avg, 2);
        }

        hiValues.push_back(avg+sqrt(sumSq/(numTrials-1)));
        loValues.push_back(avg-sqrt(sumSq/(numTrials-1)));
    }

    hilowValues.push_back(hiValues);
    hilowValues.push_back(loValues);
    return hilowValues;
}

QVector<QVector<qreal> > PSTHDialog::stdError(QVector<QVector<int> > data){
    QVector<QVector<qreal> > hilowValues;
    QVector<qreal> hiValues;
    QVector<qreal> loValues;
    for(int j = 0; j < numBins; j++){
        qreal avg = 0;
        qreal sumSq = 0;
        avg = counts[j]/(binSize*numTrials);
        for(int i = 0; i < numTrials; i++){
            sumSq += pow(data[i][j]/(binSize) - avg, 2);
        }

        hiValues.push_back(avg+sqrt(sumSq/((numTrials-1)*numTrials)));
        loValues.push_back(avg-sqrt(sumSq/((numTrials-1)*numTrials)));
    }

    hilowValues.push_back(hiValues);
    hilowValues.push_back(loValues);
    return hilowValues;
}

QVector<QVector<qreal> > PSTHDialog::range(QVector<QVector<int> > data){
    QVector<QVector<qreal> > hilowValues;
    QVector<qreal> hiValues;
    QVector<qreal> loValues;
    for(int j = 0; j < numBins; j++){
        qreal min = DBL_MAX;
        qreal max = DBL_MIN;
        for(int i = 0; i < numTrials; i++){
            if(data[i][j]/binSize > max)
                max = data[i][j]/binSize;
            else if(data[i][j]/binSize < min)
                min = data[i][j]/binSize;
        }
        hiValues.push_back(max);
        loValues.push_back(min);
    }

    hilowValues.push_back(hiValues);
    hilowValues.push_back(loValues);
    return hilowValues;
}

QVector<QVector<qreal> > PSTHDialog::confInt(QVector<QVector<int> > data, qreal pct){
    QVector<QVector<qreal> > hilowValues;
    QVector<qreal> hiValues;
    QVector<qreal> loValues;
    qreal crit = 1;
    if(pct == 0.9)
        crit = 1.645;
    else if(pct==0.95)
        crit = 1.96;
    else if(pct==0.99)
        crit = 2.576;

    for(int j = 0; j < numBins; j++){
        qreal avg = 0;
        qreal sumSq = 0;
        avg = counts[j]/(binSize*numTrials);
        for(int i = 0; i < numTrials; i++){
            sumSq += pow(data[i][j]/(binSize) - avg, 2);
        }

        hiValues.push_back(avg+crit*sqrt(sumSq/((numTrials-1)*numTrials)));
        loValues.push_back(avg-crit*sqrt(sumSq/((numTrials-1)*numTrials)));
    }

    hilowValues.push_back(hiValues);
    hilowValues.push_back(loValues);
    return hilowValues;
}


//-----------------------------------------------------------------------------------

MultiPlotDialog::MultiPlotDialog(QWidget *parent, QColor c) :
    QWidget(parent)
  , plotColor(c){

    setGeometry(300,300,300,300);
    setMinimumHeight(300);
    setMinimumWidth(300);
    topHistogram = new HistogramPlot(this);
    topHistogram->setXLabel("Time relative to event (sec)");
    topHistogram->setYLabel("Rate (Hz)");
    topHistogram->setColor(plotColor);


    clusterPlotGrid = new QGridLayout();

    topPlotLabel = new QLabel(this);
    topPlotLabel->setText("All data for NTrode ");
    botPlotLabel = new QLabel(this);
    botPlotLabel->setText("Data for clusters ");

    currClInd = 0;
    newNTrode = false;
    currentNTrode = -1;
    currPage = 1;
    plotsPerPage = 8;
    setupGridSize(QString::number(plotsPerPage));


//-------------------------------------------------------------
    rangeLabel = new QLabel("+/- msec");
    binsLabel = new QLabel("Bins");
    rangeLabel->setMaximumHeight(25);
    binsLabel->setMaximumHeight(25);
//    numPlotsLabel->setMaximumHeight(25);

    rangeSpinBox = new QSpinBox(this);
    rangeSpinBox->setFrame(false);
    rangeSpinBox->setStyleSheet("QSpinBox { background-color: white; }");
    rangeSpinBox->setMinimum(50);
    rangeSpinBox->setMaximum(1000);
    rangeSpinBox->setSingleStep(10);
    rangeSpinBox->setFixedSize(50,22);
    rangeSpinBox->setFocusPolicy(Qt::NoFocus);
    rangeSpinBox->setToolTip(tr("Display range (+/- msec)"));

    numBinsSpinBox = new QSpinBox(this);
    numBinsSpinBox->setFrame(false);
    numBinsSpinBox->setStyleSheet("QSpinBox { background-color: white; }");
    numBinsSpinBox->setMinimum(20);
    numBinsSpinBox->setMaximum(200);
    numBinsSpinBox->setSingleStep(1);
    numBinsSpinBox->setFixedSize(50,22);
    numBinsSpinBox->setFocusPolicy(Qt::NoFocus);
    numBinsSpinBox->setToolTip(tr("Number of bins"));

    connect(rangeSpinBox,SIGNAL(valueChanged(int)),this,SLOT(setRange(int)));
    connect(numBinsSpinBox,SIGNAL(valueChanged(int)),this,SLOT(setNumBins(int)));
    connect(rangeSpinBox,SIGNAL(valueChanged(int)),this,SIGNAL(rangeChanged(int)));
    connect(numBinsSpinBox,SIGNAL(valueChanged(int)),this,SIGNAL(binsChanged(int)));

//-------------------------------------------------------------
    QGridLayout *mainLayout = new QGridLayout;
    QGridLayout *plotLayout = new QGridLayout;
    plotLayout->setVerticalSpacing(0);
    plotLayout->setContentsMargins(0,0,0,0);
    plotLayout->addWidget(topHistogram,2,0, 1, 3);
    plotLayout->addLayout(clusterPlotGrid, 4, 0, 1, 3);
    plotLayout->addWidget(topPlotLabel, 1, 0, Qt::AlignCenter);
    plotLayout->addWidget(botPlotLabel, 3, 0, Qt::AlignCenter);
    plotLayout->setRowStretch(2, 1);
    plotLayout->setRowStretch(4, 1);
    plotLayout->setColumnStretch(0, 1);

    QGridLayout *controlLayout = new QGridLayout;
    controlLayout->addWidget(rangeLabel, 0, 1);
    controlLayout->addWidget(binsLabel, 0, 2);
    controlLayout->addWidget(rangeSpinBox, 1, 1);
    controlLayout->addWidget(numBinsSpinBox, 1, 2);
    controlLayout->setVerticalSpacing(2);
    controlLayout->setColumnStretch(0,1);
    mainLayout->addLayout(plotLayout,0,0);
    mainLayout->addLayout(controlLayout, 1,0);
//    mainLayout->setRowStretch(0,1);


    setLayout(mainLayout);
    topHistogram->show();
//-------------------------------------------------------------
    //Remembered settings...
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("position"));
    QRect tempPosition = settings.value(QLatin1String("MPSTHPosition")).toRect();
    if (tempPosition.height() > 0) {
        setGeometry(tempPosition);
    }
    settings.endGroup();

    settings.beginGroup(QLatin1String("PSTHsettings"));
    int tempNumBins = settings.value(QLatin1String("numbins")).toInt();
    if (tempNumBins > 0) {
        numBinsSpinBox->setValue(tempNumBins);
    } else {
        numBinsSpinBox->setValue(100);
    }

    int tempMsecRange = settings.value(QLatin1String("msecRange")).toInt();
    if (tempMsecRange > 0) {
        rangeSpinBox->setValue(tempMsecRange);
    } else {
        rangeSpinBox->setValue(500);
    }

    settings.endGroup();
}

//SLot called with new data
void MultiPlotDialog::plot(const QVector<uint32_t> &trialTimesIn, const QVector<uint32_t> &eventTimesIn, const QVector<QVector<uint32_t> > &clusterEventTimesIn){
    trialTimes.clear();
    trialTimes.resize(trialTimesIn.length());
    for (int i=0; i < trialTimesIn.length(); i++) {
        trialTimes[i] = trialTimesIn[i];
    }

    eventTimes.clear();
    eventTimes.resize(eventTimesIn.length());
    for (int i=0; i < eventTimesIn.length(); i++) {
        eventTimes[i] = eventTimesIn[i];
    }

    clusterEventTimes.clear();
    clusterEventTimes.resize(clusterEventTimesIn.length());
    for(int i=0; i < clusterEventTimesIn.length(); i++){
        clusterEventTimes[i] = clusterEventTimesIn[i];
    }

    calcTopDisplay();
    setupClusterHistograms();
}

//Creates and adds new clusters
void MultiPlotDialog::setupClusterHistograms(){
    for(int i = 0; i < clusterEventTimes.size(); i++){
        if(!clusterEventTimes[i].empty() && !clusterPlots.contains(i)){
            HistogramPlot *tmp = new HistogramPlot(this);
            connect(tmp, SIGNAL(PSTHRequest(int)), this, SLOT(getPSTH(int)));
            tmp->clickingOn(true, i);
            clusterPlots.insert(i, tmp);
        }
    }
    if(clusterPlots.size()==0)
        botPlotLabel->hide();
    else
        botPlotLabel->show();
    displayClusterGrid();
}

//Re-displays the cluster histogram plots
void MultiPlotDialog::displayClusterGrid(){

    int ithPlot = 0;
    QList<int> keys = clusterPlots.keys();
    for(int i = 0; i < keys.size(); i++){
        HistogramPlot *ptr = clusterPlots.value(keys[i]);
        ptr->hide();

        int rownum = ithPlot/gridCols;
        int colnum = ithPlot%gridRows;
        if(plotsPerPage == 2)
            colnum = ithPlot;

        calcClusterHistogramDisplay(keys[i]);
        ptr->setXLabel("Time relative to event (sec): Cluster " + QString::number(keys[i]));
        ptr->setYLabel("Rate (Hz)");
        ptr->show();

        clusterPlotGrid->addWidget(ptr, rownum, colnum);
        ithPlot++;
    }

}

//Calculate bins for top display
void MultiPlotDialog::calcTopDisplay() {

    uint sampRate = hardwareConf->sourceSamplingRate;
    uint rangeInt = absTimeRange*sampRate;
    numTrials = trialTimes.length();
//    if(numTrials<=1){
//        qDebug() << "Warning: Number of trials too low!";
//        return;
//    }

    QVector<int> counts;//.clear();
    counts.fill(0, numBins);

    int currentEventInd = 0;
    uint32_t trialWindowStart;
    uint32_t trialWindowEnd;
    for (int trialInd=0; trialInd < trialTimes.length(); trialInd++) {
        //Scan the event times until we enter the next trial window
        if (trialTimes[trialInd] > rangeInt) {
            trialWindowStart = trialTimes[trialInd] - rangeInt;
        } else {
            trialWindowStart = 0;
        }
        trialWindowEnd = trialTimes[trialInd] + rangeInt;
        while ((eventTimes[currentEventInd] < trialWindowStart) && (currentEventInd < eventTimes.length())) {
            currentEventInd++;
        }

        //Now bin the spikes within the trial window
        int trialEventInd = 0;
        while ((eventTimes[currentEventInd+trialEventInd] < trialWindowEnd)
               && ((currentEventInd+trialEventInd) < eventTimes.length())) {

            qreal tmpRelEventTime = ((qreal)eventTimes[currentEventInd+trialEventInd]-(qreal)trialTimes[trialInd])/sampRate;
            if(tmpRelEventTime >= -absTimeRange){
                int i = (int)floor((tmpRelEventTime+absTimeRange)/(qreal)binSize);
                if(i >= 0 && i < counts.length())
                    counts[i]++;
                else
                    qDebug() << "Error IN PSTHDIALOG";
            }
            trialEventInd++;
        }
    }

    //Bin sizes/rates calculation
    QVector<qreal> rates;
    for (int i=0; i < counts.size(); i++) {
        rates.push_back(counts[i]/(qreal)(binSize*numTrials));
    }
    topHistogram->setData(rates);
    topHistogram->setXRange(-absTimeRange, absTimeRange);
    if(currentNTrode>-1)
        topPlotLabel->setText("All data for NTrode " + QString::number(spikeConf->ntrodes[currentNTrode]->nTrodeId));

}

//Calculates histogram data given a cluster
void MultiPlotDialog::calcClusterHistogramDisplay(int clusterInd){
    uint sampRate = hardwareConf->sourceSamplingRate;
    uint rangeInt = absTimeRange*sampRate;
    numTrials = trialTimes.length();
//    if(numTrials<=1){
//        qDebug() << "Warning: Number of trials too low!";
//        return;
//    }

    QVector<int> clCounts = QVector<int>(numBins, 0);

    int currentEventInd = 0;
    uint32_t trialWindowStart;
    uint32_t trialWindowEnd;
    for (int trialInd=0; trialInd < trialTimes.length(); trialInd++) {
        //Scan the event times until we enter the next trial window
        if (trialTimes[trialInd] > rangeInt) {
            trialWindowStart = trialTimes[trialInd] - rangeInt;
        } else {
            trialWindowStart = 0;
        }
        trialWindowEnd = trialTimes[trialInd] + rangeInt;
        while ((clusterEventTimes[clusterInd][currentEventInd] < trialWindowStart) && (currentEventInd < clusterEventTimes[clusterInd].length())) {
            currentEventInd++;
        }

        //Now bin the spikes within the trial window
        int trialEventInd = 0;
        while ((clusterEventTimes[clusterInd][currentEventInd+trialEventInd] < trialWindowEnd)
               && ((currentEventInd+trialEventInd) < clusterEventTimes[clusterInd].length())) {

            qreal tmpRelEventTime = ((qreal)clusterEventTimes[clusterInd][currentEventInd+trialEventInd]-(qreal)trialTimes[trialInd])/sampRate;
            if(tmpRelEventTime >= -absTimeRange){
                int i = (int)floor((tmpRelEventTime+absTimeRange)/(qreal)binSize);
                if(i >= 0 && i < clCounts.length()) clCounts[i]++;
                else
                    qDebug() << "ERROR in PSTHDIALOGclCounts";
            }
            trialEventInd++;
        }
    }

    //Bin sizes/rates calculation
    QVector<qreal> rates;
    for (int i=0; i < clCounts.size(); i++) {
        rates.push_back(clCounts[i]/(qreal)(binSize*numTrials));
    }
    clusterPlots.value(clusterInd)->setData(rates);
    clusterPlots.value(clusterInd)->setXRange(-absTimeRange, absTimeRange);
}

//Called when signal is generated by parent window
void MultiPlotDialog::update(int ntrode, QVector<uint32_t> deventTimes, QVector<uint32_t> spikeTimes, QVector<QVector<uint32_t> > clusterSpikeTimes){

    if(currentNTrode != ntrode){
        QList<int> keys = clusterPlots.keys();
        for(int i = 0; i < keys.size(); i++){
            clusterPlots.value(keys[i])->hide();
            clusterPlotGrid->removeWidget(clusterPlots.value(keys[i]));
            delete clusterPlots.value(keys[i]);
            clusterPlots.remove(keys[i]);
        }
        clusterPlots.clear();
    }
    currentNTrode = ntrode;
    this->setWindowTitle("nTrode " + QString::number(spikeConf->ntrodes[ntrode]->nTrodeId));
    plot(deventTimes, spikeTimes, clusterSpikeTimes);
}

void MultiPlotDialog::setNumBins(int nBins) {
    //absTimeRange = 1.0;
    numBins = nBins; //Total number of bins in range
    binSize = (2*absTimeRange)/numBins;
    calcTopDisplay();
    displayClusterGrid();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("numbins"), numBins);
    settings.endGroup();
}

void MultiPlotDialog::setRange(int msecRange) {
    absTimeRange = (qreal)msecRange/1000;
    binSize = (2*absTimeRange)/numBins;
    calcTopDisplay();
    displayClusterGrid();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("msecRange"), msecRange);
    settings.endGroup();
}

void MultiPlotDialog::resizeEvent(QResizeEvent *event) {
    //Remember the new size for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("position"));
    settings.setValue(QLatin1String("MPSTHPosition"), this->geometry());
    settings.endGroup();
}

//Tells ntrode display window to open a psth window for this cluster
void MultiPlotDialog::getPSTH(int cl){
    emit requestPSTH(cl, currentNTrode);
}

//Hard coded number of plots shown. To extend, add here and also to dropdown menu in constructor
//Was useful in old implementation, now numplots is hardcoded in constructor
void MultiPlotDialog::setupGridSize(QString numplots){
    switch (numplots.toInt()) {
    case 1:
        gridRows = 1;
        gridCols = 1;
        break;
    case 2:
        gridRows = 1;
        gridCols = 2;
        break;
    case 4:
        gridRows = 2;
        gridCols = 2;
        break;
    case 6:
        gridRows = 2;
        gridCols = 3;
    case 8:
        gridRows = 2;
        gridCols = 4;
    default:
        gridRows = 2;
        gridCols = 2;
        break;
    }
    plotsPerPage = numplots.toInt();
    currPage = 0;
    displayClusterGrid();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("MultiPlotsettings"));
    settings.setValue(QLatin1String("plotsPerPage"), plotsPerPage);
    settings.endGroup();
}

NTrodeSelectionDialog::NTrodeSelectionDialog(QWidget *parent) : QWidget(parent) {
    operation = FO_OR; //OR operation by default

    mainLayout = new QVBoxLayout();

    //ini categories box
    labelCategory = new QLabel(tr("Category:"));
    categorySelector = new QComboBox();
    categorySelector->addItem("All");
    connect(categorySelector, SIGNAL(activated(int)), this, SLOT(categorySelected(int)));

    //Select all tags option
    selectAllTags = new QCheckBox("Select All Tags");
    selectAllTags->setChecked(false);
    connect(selectAllTags, &QCheckBox::stateChanged, this, &NTrodeSelectionDialog::allTagsButtonSelected);

    //ini tags list
    labelTags = new QLabel(tr("Tags:"));
    tagList = new QListWidget();
    connect(tagList, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(tagSelected(QListWidgetItem*)));

    //ini operation box
    labelOperation = new QLabel(tr("Operation:"));
    operationSelector = new QComboBox();
    operationSelector->addItem("nTrodes with one selected tag (OR)");
    operationSelector->addItem("nTrodes with all selected tags (AND)");
    connect(operationSelector, SIGNAL(activated(int)), this, SLOT(operationSelected(int)));

    //clear button
    buttonClear = new QPushButton();
    buttonClear->setText("Clear");
    connect(buttonClear, SIGNAL(released()), this, SLOT(buttonClearPressed()));

    mainLayout->addWidget(labelCategory, 0, Qt::AlignCenter);
    mainLayout->addWidget(categorySelector, 0, Qt::AlignCenter);
    mainLayout->addWidget(labelTags, 0, Qt::AlignCenter);
    mainLayout->addWidget(selectAllTags, 0, Qt::AlignLeft);
    mainLayout->addWidget(tagList, 0, Qt::AlignCenter);
    mainLayout->addWidget(labelOperation, 0, Qt::AlignCenter);
    mainLayout->addWidget(operationSelector, 0, Qt::AlignCenter);
    mainLayout->addWidget(buttonClear, 0, Qt::AlignCenter);

    setLayout(mainLayout);
}

void NTrodeSelectionDialog::update() {
//    qDebug() << "Updating categories and tags";
//    CategoryDictionary cataDict = spikeConf->groupingDict;
    QHash<QString, QHash<QString,QString> > categories = spikeConf->groupingDict.getCategories();
    QHashIterator<QString, QHash<QString,QString> > iter(categories);
    categorySelector->clear();

    categorySelector->addItem("All");
    while (iter.hasNext()) {
        iter.next();
        if (iter.key() == "All") //ignore the All category
            continue;

        categorySelector->addItem(iter.key());
    }

    if (categorySelector->count() > 0) {
        categorySelector->setCurrentIndex(0);
        this->categorySelected(0);
    }

    //When updating the selection dialog, make sure to uncheck all previous selections so the user experience is not befudling
    selectedTags.clear();
    for (int i = 0; i < tagList->count(); i++) {
        tagList->item(i)->setCheckState(Qt::Unchecked);
    }
}

void NTrodeSelectionDialog::categorySelected(int categoryIndex) {
//    qDebug() << "Category Selected: " << categorySelector->currentText();
//    tagSelector->clear();
    QLabel sizingLabel;
//    sizingLabel.setText(tr("All"));
//    int minimumW = sizingLabel.sizeHint().width();

    tagList->clear();

//    QListWidgetItem *firstItem = new QListWidgetItem();
//    firstItem->setText("All");
//    firstItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
//    firstItem->setCheckState(Qt::Unchecked);

//    tagList->insertItem(0, firstItem);

//    bool checkAll = true;
    CategoryDictionary cataDict = spikeConf->groupingDict;
    QString category = categorySelector->currentText();
    QList<GroupingTag> tags;
    if (category == "All") {
        tags = cataDict.getSortedAllTagList();
    }
    else {
        QHashIterator<QString,QString> iter(spikeConf->groupingDict.getCategorysTags(categorySelector->currentText()));
        while (iter.hasNext()) {
            iter.next();
            QString curTagStr = iter.key();
            if (curTagStr == "") //ignore any empty tags
                continue;

            GroupingTag curTag;
            curTag.category = category;
            curTag.tag = curTagStr;
            tags.append(curTag);
        }
    }

    for (int i = 0; i < tags.length(); i++) {
        QListWidgetItem *newTag = new QListWidgetItem();
        GroupingTag curTag = tags.at(i);
        newTag->setText(curTag.toTagString());
        newTag->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);


        if (selectedTags.contains(curTag)) {
            newTag->setCheckState(Qt::Checked);
        }
        else {
//            checkAll = false;
            newTag->setCheckState(Qt::Unchecked);
        }

        tagList->insertItem(tagList->count(), newTag);
    }
//    if (checkAll)
//        firstItem->setCheckState(Qt::Checked);

}

void NTrodeSelectionDialog::allTagsButtonSelected(int state){
    if (selectAllTags->checkState() == Qt::Unchecked) {
        //deactivate all tags
        for (int i = 0; i < tagList->count(); i++) {
            GroupingTag curTag;
            curTag.readInTagStr(tagList->item(i)->text());
            if (selectedTags.contains(curTag))
                selectedTags.remove(curTag);
            tagList->item(i)->setCheckState(Qt::Unchecked);
        }
    }
    else {
        //activate all tags
        for (int i = 0; i < tagList->count(); i++) {
            GroupingTag curTag;
            curTag.readInTagStr(tagList->item(i)->text());
            if (!selectedTags.contains(curTag))
                selectedTags.insert(curTag, 1);
            tagList->item(i)->setCheckState(Qt::Checked);
        }
    }
    emit sig_selectTags((int)operation,selectedTags);
}
void NTrodeSelectionDialog::tagSelected(QListWidgetItem *tag) {
//    qDebug() << "Tag selected: " << tag->text();

    GroupingTag selectedTag;
    selectedTag.readInTagStr(tag->text());

    if (tag->checkState() == Qt::Checked) {
        //deactivate tag
        if (selectedTags.contains(selectedTag)) {
            selectedTags.remove(selectedTag);
        }
        tag->setCheckState(Qt::Unchecked);
    }
    else {
        //activate tag
        if (!selectedTags.contains(selectedTag)) {
            selectedTags.insert(selectedTag, 1);
        }
        tag->setCheckState(Qt::Checked);
    }
    //emit signal to select the requested tags
    emit sig_selectTags((int)operation,selectedTags);

}

void NTrodeSelectionDialog::operationSelected(int opIndex) {
//    qDebug() << "Operation Selected: ";
    if (opIndex == 0) { //OR index
        operation = FO_OR;
    }
    else if (opIndex == 1) { //AND index
        operation = FO_AND;
    }
    emit sig_selectTags((int)operation,selectedTags);
}

void NTrodeSelectionDialog::buttonClearPressed() {
//    qDebug() << "Clear Button Pressed";
    //deactivate all tags
    selectAllTags->setChecked(false);
    selectedTags.clear();
    for (int i = 0; i < tagList->count(); i++) {
        tagList->item(i)->setCheckState(Qt::Unchecked);
    }
    emit sig_clearSelection(); //this could be handled by sig_selectTags, but this will be more efficient
}

StreamDisplayOptionsDialog::StreamDisplayOptionsDialog(bool lfp, bool spike, QWidget *parent){
    mainLayout = new QVBoxLayout();

    lfpbutton = new TrodesButton;
    lfpbutton->setText("LFP");
    if(lfp) lfpbutton->setDown(true);
    connect(lfpbutton, &TrodesButton::pressed, this, &StreamDisplayOptionsDialog::lfpChosen);
    connect(lfpbutton, &TrodesButton::clicked, this, &StreamDisplayOptionsDialog::lfpChosenslot);

    spikebutton = new TrodesButton;
    spikebutton->setText("Spike");
    if(spike) spikebutton->setDown(true);
    connect(spikebutton, &TrodesButton::pressed, this, &StreamDisplayOptionsDialog::spikeChosen);
    connect(spikebutton, &TrodesButton::clicked, this, &StreamDisplayOptionsDialog::spikeChosenslot);

    rawbutton = new TrodesButton;
    rawbutton->setText("Unfiltered");
    if(!lfp && !spike) rawbutton->setDown(true);
    connect(rawbutton, &TrodesButton::pressed, this, &StreamDisplayOptionsDialog::rawChosen);
    connect(rawbutton, &TrodesButton::clicked, this, &StreamDisplayOptionsDialog::rawChosenslot);

    //Section divider line
    QFrame *divLine = new QFrame;
    divLine->setFrameShape(QFrame::HLine);
    divLine->setFrameShadow(QFrame::Sunken);

    //Button to toggle displaying spike ticks
    displayTicksButton = new TrodesButton;
    displayTicksButton->setText("Spike Ticks");
    displayTicksButton->setCheckable(true);
    connect(displayTicksButton, &TrodesButton::toggled, this, &StreamDisplayOptionsDialog::ticksToggled);
    connect(displayTicksButton, &TrodesButton::toggled, this, &StreamDisplayOptionsDialog::ticksToggledSlot);

    mainLayout->addWidget(lfpbutton);
    mainLayout->addWidget(spikebutton);
    mainLayout->addWidget(rawbutton);
    mainLayout->addWidget(divLine);
    mainLayout->addWidget(displayTicksButton);


    setLayout(mainLayout);
}

void StreamDisplayOptionsDialog::lfpChosenslot(){
    lfpbutton->setDown(true);
    spikebutton->setDown(false);
    rawbutton->setDown(false);
}

void StreamDisplayOptionsDialog::spikeChosenslot(){
    lfpbutton->setDown(false);
    spikebutton->setDown(true);
    rawbutton->setDown(false);
}

void StreamDisplayOptionsDialog::rawChosenslot(){
    lfpbutton->setDown(false);
    spikebutton->setDown(false);
    rawbutton->setDown(true);
}

void StreamDisplayOptionsDialog::ticksToggledSlot(bool on){

    displayTicksButton->setDown(on);
}


//TriggerScopeSettingsWidget displays the 'settings' tab for each nTrode.
NTrodeSettingsWidget::NTrodeSettingsWidget(QWidget *parent, int trodeNum) :
  QWidget(parent) {
    attachedMode = false;
    attachedWidget = nullptr;
    posOffset.setX(0);
    posOffset.setY(0);
    panelSize.setHeight(225);
    panelSize.setWidth(485);
    multipleSelected = false;
//    cargrouppanel = new CARGroupPanel;
//    cargrouppanel->setVisible(false);
//    connect(this, &TriggerScopeSettingsWidget::destroyed, cargrouppanel, &CARGroupPanel::deleteLater);
//    connect(cargrouppanel, &CARGroupPanel::groupchanged, this, &TriggerScopeSettingsWidget::updateRefGroupId);
//    connect(cargrouppanel, &CARGroupPanel::updateDisplay, this, [this](int id){
//        if(id > 0) {
//            RefGroupBox->setText("Group " + QString::number(id));
//        }
//        else if(id == 0){
//            RefGroupBox->setText("No Group Ref");
//        }
//        else{
//            RefGroupBox->setText("<Select Group>");
//        }
//        RefTrodeBox->setEnabled(id <= 0);
//        RefChannelBox->setEnabled(id <= 0);
//    });
//    connect(cargrouppanel, &CARGroupPanel::cargroupedited, this, &TriggerScopeSettingsWidget::verifyCARgroupEdits);
//    connect(cargrouppanel, &CARGroupPanel::updateDescription, this, [this](int currentGroup, const QString &text){
//        spikeConf->carGroups[currentGroup-1].description = text;
//        emit this->configChanged();
//    });
//    connect(this, &TriggerScopeSettingsWidget::carPanelReviewed, cargrouppanel, &CARGroupPanel::hwchansReceived);

    TrodesFont dispFont;

//    emptyLabel = new QLabel(tr(""));
//    labels.push_back(emptyLabel);

    nTrodeNumber = trodeNum;
    widgetLayouts.push_back(new QGridLayout(this));
    //widgetLayouts.push_back(new QGridLayout(this));
    //widgetLayouts.push_back(new QGridLayout(this));
    //widgetLayouts.push_back(new QGridLayout(this));

    widgetLayouts[0]->setSpacing(0);
    widgetLayouts[0]->setContentsMargins(7,2,7,2);

    //Create the reference controls
    iniReferenceBox(1, dispFont);

    //Create the filter controls
    iniSpikeFilterBox(4, dispFont);

    //Create the ModuleData controls
    iniLFPBox(3, dispFont);

    //Create the Spike Trigger controls
    iniSpikeTriggerBox(5, dispFont);

    //grouping tag panel
    iniGroupingTagControls(7, dispFont);

    //display settings box
    iniDispBox(6, dispFont);

    // notch filter controls
//    iniNotchFilterBox(2, dispFont);

    //Select buttons
    layoutSelectButtons = new QHBoxLayout();
    widgetLayouts[0]->addLayout(layoutSelectButtons, 8, 0);

    //select by button
    buttonSelectBy = new TrodesButton;
    buttonSelectBy->setText(tr("Select groups..."));
    char tTipSelectGroups[] = "<html><head/><body><p>" \
                  "Select channels to edit together based on their grouping tags. " \
                  "</p></body></html>";
    buttonSelectBy->setToolTip(tTipSelectGroups);
    widgets.append(buttonSelectBy);
    buttonSelectBy->setMaximumSize(buttonSelectBy->sizeHint());
    buttonSelectBy->setMinimumSize(buttonSelectBy->sizeHint());
    connect(buttonSelectBy, SIGNAL(released()), this, SIGNAL(openSelectByDialog()));
    layoutSelectButtons->addWidget(buttonSelectBy, Qt::AlignLeft);

    //Select all button
    buttonSelectAll = new TrodesButton;
    buttonSelectAll->setText(tr("Select All"));
    char tTipSelectAll[] = "<html><head/><body><p>" \
                  "Select all channels to edit together. " \
                  "</p></body></html>";
    buttonSelectAll->setToolTip(tTipSelectAll);
    widgets.append(buttonSelectAll);
    buttonSelectAll->setMaximumSize(buttonSelectAll->sizeHint());
    buttonSelectAll->setMinimumSize(buttonSelectAll->sizeHint());
    connect(buttonSelectAll, &QPushButton::released, this, &NTrodeSettingsWidget::selectAllNtrodes);
    layoutSelectButtons->addWidget(buttonSelectAll, Qt::AlignRight);

    //Add a label at the top to show which nTrode this is controlling
    QFont labelFont;
    labelFont.setPixelSize(14);
    labelFont.setFamily("Arial");
    labelFont.setBold(true);
    QGridLayout* IDlayout = new QGridLayout();
    trodeLabel = new QLabel(this);
    labels.push_back(trodeLabel);
    trodeLabel->setFont(labelFont);
    trodeLabel->setText(QString("nTrode ")+QString::number(spikeConf->ntrodes[trodeNum]->nTrodeId));
//    IDlayout->addWidget(emptyLabel,0,1);
    IDlayout->addWidget(trodeLabel,0,0);
//    linkCheckBox = new QCheckBox(this);
//    widgets.append(linkCheckBox);
//    linkCheckBox->setText("Link settings");
//    linkCheckBox->setFont(dispFont);
//    if (linkChangesBool) {
//        linkCheckBox->setChecked(true);
//    }
//    connect(linkCheckBox,SIGNAL(toggled(bool)),this,SLOT(linkBoxClicked(bool)));
//    IDlayout->addWidget(linkCheckBox,0,2);
    IDlayout->setColumnStretch(1,1);
    widgetLayouts[0]->addLayout(IDlayout,0,0);
    widgetLayouts[0]->setRowStretch(6,1);

    setLayout(widgetLayouts[0]);
    setWindowTitle(tr("nTrode Settings"));

    if (attachedMode) {
        setWindowFlags(Qt::FramelessWindowHint);

        //initialize animations
        a_horizontalExpand = new QPropertyAnimation(this, "geometry");
        connect(a_horizontalExpand, SIGNAL(finished()), this, SLOT(setWidgetsVisible()));
        a_horizontalClose = new QPropertyAnimation(this, "geometry");
        a_minSize = new QPropertyAnimation(this, "minimumSize");
    }
    else {
//        setWindowFlags(Qt::Tool | Qt::WindowStaysOnTopHint);
//        setWindowFlags(Qt::WindowStaysOnTopHint);
    }
    setWidgetsHidden();

}

NTrodeSettingsWidget::~NTrodeSettingsWidget() {
//    qDebug()<< "Deleting the Panel";
//    qDebug() << "   Visble? " << isVisible();
//    qDebug() << "final location: " << this->pos();

    emit saveGeo(this->geometry());

    if (attachedMode) {
        delete a_horizontalExpand;
        delete a_horizontalClose;
        delete a_minSize;
    }
}

//This function is used to pass a hash table of currently selected nTrodes to the TriggerScopeSettingsWidget object
void NTrodeSettingsWidget::setSelectedNTrodes(QHash<int, int> selectedNTrodes) {
//    selectedNTrodes.
    QString multiTooltip = "";
    selectedIndicis.clear();
    selectedIndicis = selectedNTrodes.keys();
    QList<int> selectedIDs = selectedNTrodes.values();
    int curLoadedNTrodeID = spikeConf->ntrodes[nTrodeNumber]->nTrodeId;
    std::sort(selectedIDs.begin(),selectedIDs.end());
    int num = 0;
    bool loadNewNTrode = true;
    for (int i = 0; i < selectedIDs.length(); i++) {
        if (selectedIDs.at(i) == curLoadedNTrodeID)
            loadNewNTrode = false;
        multiTooltip = QString("%1%2, ").arg(multiTooltip).arg(selectedIDs.at(i));
        num++;
    }
    multiTooltip = QString("%1").arg(multiTooltip.left(multiTooltip.length()-2));
    if (loadNewNTrode) { //if ntrode from which setting were previously loaded was unselected, load in new settings
        //load settings from the lowest numbered nTrode in the list of selected
        if (selectedIDs.count() > 0) {
            int newID = selectedIDs.first();
            nTrodeNumber = spikeConf->ntrodes.index(newID);
        }
        else { //no ntrodes selected
            //todo: if no ntrodes were selected, close the panel and uncheck the button;
        }

    }

    if (num > 1) {
        trodeLabel->setText("nTrode: Multiple");
        multipleSelected = true;
    }
    else {
        if (selectedIDs.length() > 0) {
            trodeLabel->setText(QString("nTrode %1").arg(selectedIDs.at(0)));
        }
        else {
            trodeLabel->setText(QString("nTrode %1").arg(spikeConf->ntrodes[nTrodeNumber]->nTrodeId));
        }
        multipleSelected = false;
    }
    trodeLabel->setToolTip(multiTooltip);
    loadNTrodeIntoPanel(spikeConf->ntrodes.at(nTrodeNumber)->nTrodeId);
    setGroupingTagWidgetsVisibility();
    checkNTrodesForConflicts();
//    cargrouppanel->resetButtons();
//    RefGroupBox->setText("No Group Ref");
    auto nt = spikeConf->ntrodes[nTrodeNumber];
    int group = nt->groupRefOn ? nt->refGroup : 0;
    RefGroupBox->setCurrentIndex(group);
    if(group){
        RefTrodeBox->setEnabled(false);
        RefChannelBox->setEnabled(false);
    }
    else{

        RefTrodeBox->setEnabled(true);
        RefChannelBox->setEnabled(true);
    }



}

bool NTrodeSettingsWidget::eventFilter(QObject *obj, QEvent *ev) {
    this->raise(); //This keeps the panel's edge from being separated by a focus line
    if (attachedWidget != NULL && obj == attachedWidget) {
        if (ev->type() == QEvent::Move) {
            QPoint newPos = static_cast<QMoveEvent*>(ev)->pos();
            this->setPosition(newPos.x()+this->posOffset.x(), newPos.y()+this->posOffset.y());
//            resize(static_cast<QResizeEvent*>(ev)->size());
        }
        else if (ev->type() == QEvent::Resize) {
            QSize newSize = static_cast<QResizeEvent*>(ev)->size();
            //only offset we currently care about is the x offset
            this->setPositionOffset(newSize.width(), posOffset.y());
            this->setPosition(attachedWidget->geometry().x()+this->posOffset.x(), attachedWidget->geometry().y()+this->posOffset.y());
        }
    }
    return(QWidget::eventFilter(obj, ev));
}

bool NTrodeSettingsWidget::event(QEvent *ev) {
    if (ev->type() == QEvent::Close) {
        emit closing();
    }

    return(QWidget::event(ev));
}

//void NTrodeSettingsWidget::iniNotchFilterBox(int columnPos, TrodesFont font) {

//    notchFilterBox = new QGroupBox(tr("Notch filter"),this);
//    char toolTipNotchDesc[] = "<html><head/><body><p>"
//                  "Notch filter to reduce noise from the power line. "
//                  "Select the center frequency and the bandwidth of the stopband. "
//                  "Use the checkbox to toggle on/off. Filter: 2nd order"
//                  "</p></body></html>";
//    notchFilterBox->setToolTip(toolTipNotchDesc);
//    widgets.append(notchFilterBox);
//    notchFilterBox->setFont(font);
//    notchFilterBox->setCheckable(true);
//    notchFilterBox->setChecked(spikeConf->ntrodes[nTrodeNumber]->notchFilterOn);
//    notchFilterBox->setFixedHeight(75);
//    widgetLayouts.push_back(new QGridLayout(notchFilterBox));

//    labels.push_back(new QLabel(tr("Freq (Hz)"),this));
//    labels.last()->setFont(font);
//    NOTCHFREQ_LABEL_INDEX = labels.size()-1;
//    notchFrequencyBox = new QComboBox(this);
//    notchFrequencyBox->addItem(QString::number(50));
//    notchFrequencyBox->addItem(QString::number(60));
//    notchFrequencyBox->setCurrentIndex(1);
//    notchFrequencyBox->setToolTip("Center frequency of the notch filter");
//    notchFrequencyBox->setMinimumHeight(notchFrequencyBox->sizeHint().height());
//    QLabel *notchFreqLabel = labels.last();

//    labels.push_back(new QLabel(tr("Bandwidth (Hz)"),this));
//    labels.last()->setFont(font);
//    NOTCHBW_LABEL_INDEX = labels.size()-1;
//    notchBandwidthBox = new QComboBox(this);
//    for(int i=1; i<=10; ++i) {
//        notchBandwidthBox->addItem(QString::number(i));
//    }
//    notchBandwidthBox->setCurrentIndex(9);
//    char toolTipNotchBW[] = "<html><head/><body><p>"
//                            "Bandwidth of the stopband, measured between "
//                            "the -3 dB cutoff frequencies. A 10 Hz "
//                            "bandwidth is recommended for good time "
//                            "domain and phase characteristics."
//                            "</p></body></html>";
//    notchBandwidthBox->setToolTip(toolTipNotchBW);
//    notchBandwidthBox->setMinimumHeight(notchBandwidthBox->sizeHint().height());
//    QLabel *notchBwLabel = labels.last();

//    int groupBoxIndex = widgetLayouts.length() - 1;
//    widgetLayouts[groupBoxIndex]->addWidget(notchFreqLabel,   0,0,Qt::AlignCenter);
//    widgetLayouts[groupBoxIndex]->addWidget(notchBwLabel,     0,1,Qt::AlignCenter);
//    widgetLayouts[groupBoxIndex]->addWidget(notchFrequencyBox,1,0,Qt::AlignCenter);
//    widgetLayouts[groupBoxIndex]->addWidget(notchBandwidthBox,1,1,Qt::AlignCenter);
//    widgetLayouts[groupBoxIndex]->setVerticalSpacing(0);
//    notchFilterBox->setLayout(widgetLayouts[groupBoxIndex]);
//    widgetLayouts[0]->addWidget(notchFilterBox,columnPos,0);

//    // connections
//    connect(notchFilterBox,SIGNAL(clicked(bool)),
//            this,SLOT(updateNotchFilterSwitch(bool)));
//    connect(notchFrequencyBox,SIGNAL(activated(int)),
//            this,SLOT(updateNotchFilterFreq()));
//    connect(notchBandwidthBox, SIGNAL(activated(int)),
//            this,SLOT(updateNotchFilterBW()));
//    // If notch filter settings eventually become part of a workspace file, we should
//    // uncomment the following line
//    //connect(notchFilterBox,SIGNAL(clicked(bool)),this,SIGNAL(configChanged()));

//    notchFilterBox->setMaximumHeight(notchFilterBox->sizeHint().height());

//}

void NTrodeSettingsWidget::iniReferenceBox(int columnPos, TrodesFont font) {
//    int labelIndex = labels.size()-1;
    refBox = new QGroupBox(tr("Referencing Settings"),this);
    char tTipREF[] = "<html><head/><body><p>" \
                  "Reference signal that is subtracted from the chosen filter bands. This calculation occurs at the full sampling rate of the data." \
                  "</p></body></html>";
    refBox->setToolTip(tTipREF);
    widgets.append(refBox);
    refBox->setFont(font);
    widgetLayouts.push_back(new QGridLayout(refBox));
    char tTipNtRef[] = "<html><head/><body><p>" \
                  "Single channel reference. Use a reference channel in the chosen nTrode. If the nTrode has multiple channels, you must also select the subchannel to use." \
                  "</p></body></html>";
    RefTrodeBox = new QComboBox(this);
    RefTrodeBox->setToolTip(tTipNtRef);
    widgets.append(RefTrodeBox);

    RefTrodeBox->setFont(font);
    for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
      RefTrodeBox->addItem(QString::number(spikeConf->ntrodes[i]->nTrodeId));
    }
    //RefTrodeBox->setFixedWidth(50);
    RefTrodeBox->setMinimumHeight(RefTrodeBox->sizeHint().height());


    RefChannelBox = new QComboBox(this);
    RefChannelBox->setToolTip(tTipNtRef);
    widgets.append(RefChannelBox);
    RefChannelBox->setFont(font);
    labels.push_back(new QLabel(tr("nTrode"),this));
    labels.last()->setToolTip(tTipNtRef);
    REFNTRODE_LABEL_INDEX = labels.size()-1;
    labels.last()->setFont(font);
    labels.push_back(new QLabel(tr("Channel"),this));//1
    labels.last()->setToolTip(tTipNtRef);
    REFCHAN_LABEL_INDEX = labels.size()-1;
    labels.last()->setFont(font);

    cargrouplabel = new QLabel("CAR groups");
    char tTipCGL[] = "<html><head/><body><p>" \
                  "Common average reference (CAR). Calculate (and use) a reference channel defined as the average signal of the channels in the chosen group. To create/remove/edit groups, go to the workspace editor and open the Reference Groups tab." \
                  "</p></body></html>";
    cargrouplabel->setToolTip(tTipCGL);

//    RefGroupBox = new QPushButton("<Select Group>");
    RefGroupBox = new QComboBox();
    RefGroupBox->setMinimumHeight(RefGroupBox->sizeHint().height());
    RefGroupBox->setToolTip(tTipCGL);
    RefGroupBox->addItem("No Group Ref");
    for(auto const group : spikeConf->carGroups){
        QString shortdesc = group.description.left(15);
        if(shortdesc != group.description) shortdesc += "...";
        QString item = QString("%1: %2").arg(RefGroupBox->count()).arg(shortdesc);
        RefGroupBox->addItem(item);
    }
    addCARGroupButton = new TrodesButton;
    addCARGroupButton->setText("+");
    char addCARtTip[] = "<html><head/><body><p>" \
                  "Create a new common average reference (CAR) group using the currently selected nTrodes" \
                  "</p></body></html>";
    addCARGroupButton->setToolTip(addCARtTip);
    spikeRefCheckBox = new QCheckBox(tr("Spikes"));
    spikeRefCheckBox->setFont(font);
    spikeRefCheckBox->setMinimumSize(spikeRefCheckBox->sizeHint());
    spikeRefCheckBox->setMinimumHeight(spikeRefCheckBox->sizeHint().height()+15);
    //spikeRefCheckBox->setMinimumWidth(spikeRefCheckBox->sizeHint().width()+10);
    spikeRefCheckBox->setCheckable(true);
    char tTip[] = "<html><head/><body><p>" \
                  "Subract the reference channel in the Spike filter band" \
                  "</p></body></html>";
    spikeRefCheckBox->setToolTip(tTip);

    lfpRefCheckBox = new QCheckBox(tr("LFP"));
    lfpRefCheckBox->setFont(font);
    lfpRefCheckBox->setMinimumSize(lfpRefCheckBox->sizeHint());
    lfpRefCheckBox->setMinimumHeight(lfpRefCheckBox->sizeHint().height()+15);
    //lfpRefCheckBox->setMinimumWidth(lfpRefCheckBox->sizeHint().width()+10);
    lfpRefCheckBox->setCheckable(true);
    char tTiplfp[] = "<html><head/><body><p>" \
                  "Subract the reference channel in the local field potential (LFP) filter band" \
                  "</p></body></html>";
    lfpRefCheckBox->setToolTip(tTiplfp);

    rawRefCheckBox = new QCheckBox(tr("Raw"));
    rawRefCheckBox->setFont(font);
    rawRefCheckBox->setMinimumSize(rawRefCheckBox->sizeHint());
    rawRefCheckBox->setMinimumHeight(rawRefCheckBox->sizeHint().height()+15);
    //rawRefCheckBox->setMinimumWidth(rawRefCheckBox->sizeHint().width()+10);
    rawRefCheckBox->setCheckable(true);
    char tTipRaw[] = "<html><head/><body><p>" \
                  "Subract the reference channel in the raw filter band" \
                  "</p></body></html>";
    rawRefCheckBox->setToolTip(tTipRaw);

    QHBoxLayout *checkBoxLayout = new QHBoxLayout();
    QLabel *ckboxLabel = new QLabel(tr("Apply Ref to: "));
    ckboxLabel->setMinimumWidth(ckboxLabel->sizeHint().width()+5);
    ckboxLabel->setFont(font);
    labels.push_back(ckboxLabel);//2
    checkBoxLayout->addWidget(ckboxLabel, 0, Qt::AlignLeft);
    checkBoxLayout->addWidget(spikeRefCheckBox, 0, Qt::AlignCenter);
    checkBoxLayout->addWidget(lfpRefCheckBox, 0, Qt::AlignCenter);
    checkBoxLayout->addWidget(rawRefCheckBox, 0, Qt::AlignCenter);
    checkBoxLayout->setSpacing(1);

    int groupBoxIndex = widgetLayouts.length() - 1;
    widgetLayouts[groupBoxIndex]->addWidget(labels[0],0,0,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(labels[1],0,1,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(cargrouplabel, 0, 2, Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(RefTrodeBox,1,0,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(RefChannelBox,1,1,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(RefGroupBox, 1, 2, Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(addCARGroupButton, 1, 3, Qt::AlignLeft);
    widgetLayouts[groupBoxIndex]->setHorizontalSpacing(1);

//    widgetLayouts[groupBoxIndex]->addWidget(spikeRefCheckBox, 2, 0, 1, 1, Qt::AlignCenter);
//    widgetLayouts[groupBoxIndex]->addWidget(lfpRefCheckBox, 2, 1, 1, 1, Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addLayout(checkBoxLayout, 2, 0, 5, 0, Qt::AlignCenter);

    widgetLayouts[groupBoxIndex]->setVerticalSpacing(5);
    refBox->setLayout(widgetLayouts[groupBoxIndex]);
//    refBox->setCheckable(true);
    refBox->setChecked(spikeConf->ntrodes[nTrodeNumber]->refOn);
    refBox->setFixedHeight(75);
    widgetLayouts[0]->addWidget(refBox,columnPos,0);

    //Input initial values
    int refID = spikeConf->ntrodes[nTrodeNumber]->refNTrode;
    RefTrodeBox->setCurrentIndex(refID);
    RefChannelBox->clear();
    for (int i = 0; i < spikeConf->ntrodes[refID]->hw_chan.length(); i++)
        RefChannelBox->addItem(QString::number(i+1));

    RefChannelBox->setCurrentIndex(spikeConf->ntrodes[nTrodeNumber]->refChan);
    RefChannelBox->setMinimumHeight(RefChannelBox->sizeHint().height());

    //Connections
//    connect(refBox,SIGNAL(clicked(bool)),this,SLOT(updateRefSwitch(bool)));
    connect(RefTrodeBox,SIGNAL(activated(int)),this,SLOT(updateRefTrode(int)));
    connect(RefChannelBox,SIGNAL(activated(int)),this,SLOT(updateRefChan(int)));
//    connect(RefGroupBox, &QPushButton::clicked, this, &TriggerScopeSettingsWidget::updateRefGroup);
    connect(RefGroupBox, QOverload<int>::of(&QComboBox::activated), this, &NTrodeSettingsWidget::updateRefGroupId);
    connect(addCARGroupButton,&QPushButton::clicked,this,&NTrodeSettingsWidget::addCARGroupButtonPressed);
    connect(spikeRefCheckBox,SIGNAL(clicked(bool)),this,SLOT(updateRefSwitch(bool)));
    connect(lfpRefCheckBox,SIGNAL(clicked(bool)),this,SLOT(updateLFPFilterSwitch(bool)));
    connect(rawRefCheckBox, &QPushButton::clicked, this, &NTrodeSettingsWidget::updateRawRefOn);

//    connect(refBox,SIGNAL(clicked(bool)),this,SIGNAL(configChanged()));
    connect(RefTrodeBox,SIGNAL(activated(int)),this,SIGNAL(configChanged()));
    connect(RefChannelBox,SIGNAL(activated(int)),this,SIGNAL(configChanged()));
    connect(spikeRefCheckBox,SIGNAL(clicked(bool)),this,SIGNAL(configChanged()));
    connect(lfpRefCheckBox,SIGNAL(clicked(bool)),this,SIGNAL(configChanged()));

    refBox->setMaximumHeight(refBox->sizeHint().height());
    refBox->setMinimumHeight(refBox->sizeHint().height());
}

void NTrodeSettingsWidget::iniSpikeFilterBox(int columnPos, TrodesFont font) {
    filterBox = new QGroupBox(tr("Spike filter band"),this);
    char tTipSpFilt[] = "<html><head/><body><p>" \
                  "Bandpass filter used in the spike filter band. Select the lower and uppper bounds. Use the checkbox to toggle on/off. Filter: 2nd order Bessel" \
                  "</p></body></html>";
    filterBox->setToolTip(tTipSpFilt);
    widgets.append(filterBox);
    filterBox->setFont(font);
    widgetLayouts.push_back(new QGridLayout(filterBox));
    labels.push_back(new QLabel(tr("Low (Hz)"),this));//3
    LOWFILTER_LABEL_INDEX = labels.size()-1;
    labels.last()->setFont(font);
    labels.push_back(new QLabel(tr("High (Hz)"),this));//4
    HIGHFILTER_LABEL_INDEX = labels.size()-1;
    labels.last()->setFont(font);
    lowFilterCutoffBox = new QComboBox(this);
    widgets.append(lowFilterCutoffBox);
    lowFilterCutoffBox->setFont(font);
    QList<int> lowList;
    for (int i = 200;i < 700;i+=100) {
        lowList.append(i);
        lowFilterCutoffBox->addItem(QString::number(lowList.last()));
    }
    //Set the current lower filter setting from config file (find nearest available)
    for (int i=0;i<lowList.length();i++) {
        if (lowList[i] <= spikeConf->ntrodes[nTrodeNumber]->lowFilter) {
            lowFilterCutoffBox->setCurrentIndex(i);
        }
    }
    lowFilterCutoffBox->setFixedWidth(60);
    lowFilterCutoffBox->setMinimumHeight(lowFilterCutoffBox->sizeHint().height());
    highFilterCutoffBox = new QComboBox(this);
    widgets.append(highFilterCutoffBox);
    highFilterCutoffBox->setFont(font);
    QList<int> highList;
    for (int i = 5000;i < 7000;i+=1000) {
        highList.append(i);
        highFilterCutoffBox->addItem(QString::number(highList.last()));
    }
    //Set the current upper filter setting from config file (find nearest available)
    for (int i=0;i<highList.length();i++) {
        if (highList[i] <= spikeConf->ntrodes[nTrodeNumber]->highFilter) {
            highFilterCutoffBox->setCurrentIndex(i);
        }
    }
    highFilterCutoffBox->setFixedWidth(70);
    highFilterCutoffBox->setMinimumHeight(highFilterCutoffBox->sizeHint().height());
    int groupBoxIndex = widgetLayouts.length() - 1;
    widgetLayouts[groupBoxIndex]->addWidget(labels[3],0,0,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(labels[4],0,1,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(lowFilterCutoffBox,1,0,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(highFilterCutoffBox,1,1,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->setVerticalSpacing(0);
    filterBox->setLayout(widgetLayouts[groupBoxIndex]);
    filterBox->setCheckable(true);
    filterBox->setChecked(spikeConf->ntrodes[nTrodeNumber]->filterOn);
    filterBox->setFixedHeight(75);
    widgetLayouts[0]->addWidget(filterBox,columnPos,0);

    //connections
    connect(filterBox,SIGNAL(clicked(bool)),this,SLOT(updateFilterSwitch(bool)));
    connect(highFilterCutoffBox,SIGNAL(activated(int)),this,SLOT(updateUpperFilter()));
    connect(lowFilterCutoffBox,SIGNAL(activated(int)),this,SLOT(updateLowerFilter()));

    connect(filterBox,SIGNAL(clicked(bool)),this,SIGNAL(configChanged()));
    connect(highFilterCutoffBox,SIGNAL(activated(int)),this,SIGNAL(configChanged()));
    connect(lowFilterCutoffBox,SIGNAL(activated(int)),this,SIGNAL(configChanged()));

    filterBox->setMaximumHeight(filterBox->sizeHint().height());
}

void NTrodeSettingsWidget::iniSpikeTriggerBox(int columnPos, TrodesFont font) {
    triggerBox = new QGroupBox(tr("Spike trigger"),this);
    char tTipTrigger[] = "<html><head/><body><p>" \
                  "Settings used to detect spikes on this nTrode." \
                  "</p></body></html>";
    triggerBox->setToolTip(tTipTrigger);
    widgets.append(triggerBox);
    triggerBox->setFont(font);
    widgetLayouts.push_back(new QGridLayout(triggerBox));
    labels.push_back(new QLabel(tr("Threshold (-μV)"),this)); //7
    char tTipThresh[] = "<html><head/><body><p>" \
                  "Signal amplitude threshold used to detect a spike.  A negative voltage beyond this value on any of the nTrodes subchannels will trigger the spike detector." \
                  "</p></body></html>";
    triggerBox->setToolTip(tTipThresh);
    THRESH_LABEL_INDEX = labels.size()-1;
    labels.last()->setFont(font);

    threshSpinBox = new QSpinBox();
    widgets.append(threshSpinBox);
    threshSpinBox->setMinimum(10);
    threshSpinBox->setMaximum(4000);
    threshSpinBox->setSingleStep(10);
    threshSpinBox->setToolTip(tr("Trigger threshold (μV)"));
    threshSpinBox->setValue(spikeConf->ntrodes[nTrodeNumber]->thresh.at(0));
    threshSpinBox->setMinimumHeight(threshSpinBox->sizeHint().height());

    int groupBoxIndex = widgetLayouts.length() - 1;
    widgetLayouts[groupBoxIndex]->addWidget(labels[7],0,0,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(threshSpinBox,1,0,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->setVerticalSpacing(0);
    triggerBox->setLayout(widgetLayouts[groupBoxIndex]);
//    triggerBox->setCheckable(true);
//    triggerBox->setChecked(spikeConf->ntrodes[nTrodeNumber]->triggerOn.at(0));
    triggerBox->setFixedHeight(75);
//    connect(triggerBox, SIGNAL(clicked(bool)), this, SLOT(updateSpikeTrigger(bool)));

    widgetLayouts[0]->addWidget(triggerBox,columnPos,0);

    //Connections
//    connect(triggerBox, SIGNAL(clicked(bool)), this, SLOT(updateSpikeTrigger(bool)));
    connect(threshSpinBox, SIGNAL(valueChanged(int)), this, SLOT(updateThresh()));

//    connect(triggerBox,SIGNAL(clicked(bool)),this,SIGNAL(configChanged()));
    connect(threshSpinBox, &QSpinBox::editingFinished, this, &NTrodeSettingsWidget::configChanged);

    triggerBox->setMaximumHeight(triggerBox->sizeHint().height());
}

void NTrodeSettingsWidget::iniLFPBox(int columnPos, TrodesFont font) {
    //The LFP settings are used for more than just sending to modules, so please don't change to name to
    //something like "data to modules, etc".
    ModuleDataFilterBox = new QGroupBox(tr("LFP filter band"),this);
    char tTipLFPFilt[] = "<html><head/><body><p>" \
                  "Lowpass filter used in the local field potential (LFP) filter band. Select the subchannel in the nTrode to process and the filter's upper bound. Use the checkbox to toggle on/off. Filter: 2nd order Bessel" \
                  "</p></body></html>";
    ModuleDataFilterBox->setToolTip(tTipLFPFilt);
    widgets.append(ModuleDataFilterBox);
    ModuleDataFilterBox->setFont(font);
    widgetLayouts.push_back(new QGridLayout(ModuleDataFilterBox));
    labels.push_back(new QLabel(tr("Channel"),this));//5
    MODDATACHAN_LABEL_INDEX = labels.size()-1;
    labels.last()->setFont(font);
    labels.push_back(new QLabel(tr("High cutoff (Hz)"),this)); //6
    MODDATAHIGH_LABEL_INDEX = labels.size()-1;
    labels.last()->setFont(font);
    ModuleDataHighFilterCutoffBox = new QComboBox(this);
    widgets.append(ModuleDataHighFilterCutoffBox);
    ModuleDataHighFilterCutoffBox->setFont(font);
    QList<int> ModuleDatahighList;
    //TODO avaliable filter values should be specified by filter object, not hardcoded
    for (int i = 100;i <= 500;i+=100) {
        ModuleDatahighList.append(i);
        ModuleDataHighFilterCutoffBox->addItem(QString::number(ModuleDatahighList.last()));
    }
    for (int i=0;i<ModuleDatahighList.length();i++) {
        if (ModuleDatahighList[i] <= spikeConf->ntrodes[nTrodeNumber]->moduleDataHighFilter) {
            ModuleDataHighFilterCutoffBox->setCurrentIndex(i);
        }
    }
    ModuleDataHighFilterCutoffBox->setMinimumHeight(ModuleDataHighFilterCutoffBox->sizeHint().height());
    ModuleDataChannelBox = new QComboBox(this);
    widgets.append(ModuleDataChannelBox);
    ModuleDataChannelBox->setFont(font);
    for (int i = 0; i < spikeConf->ntrodes[nTrodeNumber]->hw_chan.length(); i++)
      ModuleDataChannelBox->addItem(QString::number(i+1));

    ModuleDataChannelBox->setCurrentIndex(spikeConf->ntrodes[nTrodeNumber]->moduleDataChan);
    ModuleDataChannelBox->setMinimumHeight(ModuleDataChannelBox->sizeHint().height());
    //Disabling the ability to select main LFP channel until Issue #30 is fixed
    ModuleDataChannelBox->setEnabled(false);

    int groupBoxIndex = widgetLayouts.length() - 1;
    widgetLayouts[groupBoxIndex]->addWidget(labels[5],0,0,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(labels[6],0,1,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(ModuleDataChannelBox,1,0,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(ModuleDataHighFilterCutoffBox,1,1,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->setVerticalSpacing(0);
    ModuleDataFilterBox->setLayout(widgetLayouts[groupBoxIndex]);
    ModuleDataFilterBox->setCheckable(true);
    ModuleDataFilterBox->setFixedHeight(75);
    widgetLayouts[0]->addWidget(ModuleDataFilterBox,columnPos,0);

    //Connections
    connect(ModuleDataFilterBox, SIGNAL(clicked(bool)), SLOT(updateModuleDataFilterSwitch(bool)));
    connect(ModuleDataChannelBox,SIGNAL(activated(int)),this,SLOT(updateModuleDataChannel()));
    connect(ModuleDataHighFilterCutoffBox,SIGNAL(activated(int)),this,SLOT(updateModuleDataUpperFilter()));

    connect(ModuleDataFilterBox,SIGNAL(clicked(bool)),this,SIGNAL(configChanged()));
    connect(ModuleDataChannelBox,SIGNAL(activated(int)),this,SIGNAL(configChanged()));
    connect(ModuleDataHighFilterCutoffBox,SIGNAL(activated(int)),this,SIGNAL(configChanged()));

    ModuleDataFilterBox->setMaximumHeight(ModuleDataFilterBox->sizeHint().height());
}

void NTrodeSettingsWidget::iniGroupingTagControls(int columnPos, TrodesFont font) {
    //make visualizer first
    groupingTagBox = new QGroupBox(tr("Grouping Tags"), this);
    char tTipGroups[] = "<html><head/><body><p>" \
                  "Shown is the list of groups that the nTrode(s) belong to. Both the [category] and the tag name are shown. To add or remove tags, click Add/Remove tags. " \
                  "</p></body></html>";
    groupingTagBox->setToolTip(tTipGroups);
    groupingTagList = new QListWidget();
    groupingTagSharedList = new QListWidget();
    groupingTagAllList = new QListWidget();

    groupingTabView = new QTabWidget();
    groupingTabView->addTab(groupingTagAllList, "All");
    groupingTabView->addTab(groupingTagSharedList, "Shared");

    buttonAddRemoveTags = new TrodesButton;
    buttonAddRemoveTags->setText("Add/Remove Tags");
    groupingDialog = NULL;
    buttonAddRemoveTags->setMaximumSize(buttonAddRemoveTags->sizeHint());
    widgets.append(groupingTagBox);
    widgets.append(groupingTagList);
    widgets.append(buttonAddRemoveTags);
    widgets.append(groupingTabView);

    widgetLayouts.push_back(new QGridLayout(groupingTagBox));
    int groupBoxIndex = widgetLayouts.length() - 1;
    widgetLayouts[groupBoxIndex]->addWidget(groupingTagList, 0, 0, Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(groupingTabView, 0, 0, Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(buttonAddRemoveTags, 1, 0, Qt::AlignLeft);
    widgetLayouts[groupBoxIndex]->setRowStretch(0,1);
    widgetLayouts[0]->addWidget(groupingTagBox, columnPos, 0);

//    connect(buttonAddRemoveTags, SIGNAL(released()), this, SLOT(addRemoveTagsButtonPressed()));
    connect(buttonAddRemoveTags, SIGNAL(released()), this, SLOT(addRemoveTagsButtonPressed()));
}

void NTrodeSettingsWidget::iniDispBox(int columnPos, TrodesFont font) {
    displayBox = new QGroupBox(tr("Display Settings"), this);
    widgets.append(displayBox);
    displayBox->setFont(font);
    widgetLayouts.push_back(new QGridLayout(displayBox));
    labels.push_back(new QLabel(tr("Max Disp (μV)"),this));//8
    MAXDISP_LABEL_INDEX = labels.size()-1;
    labels.last()->setFont(font);
    labels.push_back(new QLabel(tr("Channel Color"),this));//9
    COLOR_LABEL_INDEX = labels.size()-1;
    labels.last()->setFont(font);

    maxDispSpinBox = new QSpinBox();
    widgets.append(maxDispSpinBox);
    maxDispSpinBox->setMinimum(50);
    maxDispSpinBox->setMaximum(4000);
    maxDispSpinBox->setSingleStep(25);
    maxDispSpinBox->setToolTip(tr("Spike Display Range (+/- μV)"));
    maxDispSpinBox->setValue(spikeConf->ntrodes[nTrodeNumber]->maxDisp.at(0));
    maxDispSpinBox->setMinimumHeight(maxDispSpinBox->sizeHint().height());

    buttonColorBox = new ClickableFrame();
    buttonColorBox->setFixedSize(25,25);
    buttonColorBox->setStyleSheet("background-color:#aaaaaa");

    int groupBoxIndex = widgetLayouts.length() - 1;
    widgetLayouts[groupBoxIndex]->addWidget(labels[8],0,0,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(labels[9],0,1,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(maxDispSpinBox,1,0,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(buttonColorBox,1,1,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->setVerticalSpacing(0);
    displayBox->setLayout(widgetLayouts[groupBoxIndex]);

    widgetLayouts[0]->addWidget(displayBox,columnPos,0);

    //Connections
    connect(buttonColorBox, SIGNAL(sig_clicked()), this, SLOT(getColorDialog()));
    connect(maxDispSpinBox, SIGNAL(valueChanged(int)), this, SLOT(updateMaxDisp()));

    connect(maxDispSpinBox, &QSpinBox::editingFinished, this, &NTrodeSettingsWidget::configChanged);

    displayBox->setMaximumHeight(displayBox->sizeHint().height());
}

void NTrodeSettingsWidget::attachToWidget(QWidget *obj) {
    if (!attachedMode)
        return;

    attachedWidget = obj;
    obj->installEventFilter(this);
}

void NTrodeSettingsWidget::loadNTrodeIntoPanel(int ntrodeID) {
//    qDebug() << "Loading ntrode " << ntrodeID << " at index " << spikeConf->ntrodes.index(ntrodeID);

    SingleSpikeTrodeConf *nTrode = spikeConf->ntrodes.at(spikeConf->ntrodes.index(ntrodeID));

    if (groupingDialog != NULL)
        groupingDialog->setVisible(false); //make the grouping dialog box disapear if it hasn't already

    loadRef(nTrode);
//    loadNotchFilter(nTrode);
    loadSpikeFilter(nTrode);
    loadSpikeTrigger(nTrode);
    loadLFP(nTrode);
    loadTags(nTrode);
    loadDisplaySettings(nTrode);
}

void NTrodeSettingsWidget::loadRef(SingleSpikeTrodeConf *nTrode) {
//    refBox->setChecked(nTrode->refOn);
    spikeRefCheckBox->setChecked(nTrode->refOn);
    lfpRefCheckBox->setChecked(nTrode->lfpRefOn);
    rawRefCheckBox->setChecked(nTrode->rawRefOn);
    RefTrodeBox->setCurrentIndex(nTrode->refNTrode);
    RefChannelBox->setCurrentIndex(nTrode->refChan);
}

//void NTrodeSettingsWidget::loadNotchFilter(SingleSpikeTrodeConf *nTrode) {
//    notchFilterBox->setChecked(nTrode->notchFilterOn);
//    notchFrequencyBox->setCurrentText(QString::number(nTrode->notchFreq));
//    notchBandwidthBox->setCurrentText(QString::number(nTrode->notchBW));
//}

void NTrodeSettingsWidget::loadSpikeFilter(SingleSpikeTrodeConf *nTrode) {
    filterBox->setChecked(nTrode->filterOn);
    for (int i = 0; i < lowFilterCutoffBox->count(); i++) {
        if (lowFilterCutoffBox->itemText(i).toInt() >= nTrode->lowFilter) {
            lowFilterCutoffBox->setCurrentIndex(i);
            break;
        }
        else if (i == lowFilterCutoffBox->count()-1) {
            lowFilterCutoffBox->setCurrentIndex(i);
        }
    }
    for (int i = 0; i < highFilterCutoffBox->count(); i++) {
        if (highFilterCutoffBox->itemText(i).toInt() >= nTrode->highFilter) {
            highFilterCutoffBox->setCurrentIndex(i);
            break;
        }
        else if (i == highFilterCutoffBox->count()-1) {
            highFilterCutoffBox->setCurrentIndex(i);
        }
    }
}

void NTrodeSettingsWidget::loadSpikeTrigger(SingleSpikeTrodeConf *nTrode) {
//    triggerBox->setChecked(nTrode->triggerOn.at(0));
    threshSpinBox->setValue(nTrode->thresh.at(0));
//    maxDispSpinBox->setValue(nTrode->maxDisp.at(0));
}

void NTrodeSettingsWidget::loadLFP(SingleSpikeTrodeConf *nTrode) {
    ModuleDataFilterBox->setChecked(nTrode->lfpFilterOn);
    ModuleDataChannelBox->setCurrentIndex(nTrode->moduleDataChan);
    for (int i = 0; i < ModuleDataHighFilterCutoffBox->count(); i++) {
        if (ModuleDataHighFilterCutoffBox->itemText(i).toInt() >= nTrode->moduleDataHighFilter) {
            ModuleDataHighFilterCutoffBox->setCurrentIndex(i);
            break;
        }
        else if (i == ModuleDataHighFilterCutoffBox->count()-1) {
            ModuleDataHighFilterCutoffBox->setCurrentIndex(i);
        }
    }
}

void NTrodeSettingsWidget::loadTags(SingleSpikeTrodeConf *nTrode) {
    allTags.clear(); //clear the selected all tags hash (to be loaded into the panel)
    if (multipleSelected) { // if multiple ntrodes were selected, load all their tags and their shared tags into the tab view
        //All tags
        groupingTagAllList->clear();
        QList<QList<QString> > inputTags;
        for (int k = 0; k < selectedIndicis.length(); k++) {
            SingleSpikeTrodeConf *curNTrode = spikeConf->ntrodes[selectedIndicis.at(k)];

            QHashIterator<GroupingTag, int> i(curNTrode->gTags);

            while (i.hasNext()) {
                i.next();
//                QString curCata = spikeConf->groupingDict.getTagsCategory(i.key());
                QString curCata = i.key().category;
                QString curTag = i.key().tag;
//                qDebug() << "[" << curCata << "] " << curTag;
                bool createNewCata = true;
                for (int j = 0; j < inputTags.length(); j++) {
                    if (inputTags.at(j).at(0) == curCata) { //first item of each sub list will be catagory
                        createNewCata = false;
                        QList<QString> curTagList = inputTags.at(j);
                        bool tagAlreadyInList = false;
                        for (int y = 1; y < curTagList.length(); y++) { //check the list to see if the tag is already in it
                            if (curTagList.at(y) == curTag) {
                                tagAlreadyInList = true;
                                break;
                            }

                        }
                        if (!tagAlreadyInList) { //only add the tag if it's not already in the list
                            curTagList.push_back(curTag);
                            inputTags.replace(j, curTagList);
                        }
                        break;
                    }
                }
                if (createNewCata) {
                    QList<QString> newCataList;
                    newCataList.push_back(curCata);
                    newCataList.push_back(curTag);
                    inputTags.push_back(newCataList);
                }
            }
        }
        //now put tags into the groupingTagAllList
        for (int j = 0; j < inputTags.length(); j++) {
            QString curCata = inputTags.at(j).at(0); //first item of each sub list is the catagorey
            for (int k = 1; k < inputTags.at(j).length(); k++) {
                QString curTag = inputTags.at(j).at(k);
                GroupingTag curGTag;
                curGTag.tag = curTag;
                curGTag.category = curCata;
                allTags.insert(curGTag, 1);
//                QString newRow = QString("[%1] %2").arg(curCata).arg(curTag);
                QString newRow = curGTag.toTagString();
                groupingTagAllList->addItem(newRow);
            }
        }
        //shared tags
        groupingTagSharedList->clear();
        inputTags.clear();

        for (int k = 0; k < selectedIndicis.length(); k++) {
            SingleSpikeTrodeConf *curNTrode = spikeConf->ntrodes[selectedIndicis.at(k)];

            if (k == 0) {
                inputTags = Helper::getSortedTagList(curNTrode);
            }
            else {
                for (int i = 0; i < inputTags.length(); i++) {
                    QList<QString> curTagList = inputTags.at(i);

                    QString curCata = inputTags.at(i).at(0);
                    for (int j = 1; j < curTagList.length(); j++) {
                        QString curTag = curTagList.at(j);
                        GroupingTag curGTag;
                        curGTag.category = curCata;
                        curGTag.tag = curTag;
                        if (!curNTrode->gTags.contains(curGTag)) {
                            curTagList.removeAt(j);
                            j--;
                        }
                    }
                    inputTags.replace(i, curTagList);
                }
            }
        }
        //now put tags into the groupingTagSharedList
        for (int j = 0; j < inputTags.length(); j++) {
            QString curCata = inputTags.at(j).at(0); //first item of each sub list is the catagorey
            for (int k = 1; k < inputTags.at(j).length(); k++) {
                QString curTag = inputTags.at(j).at(k);
                QString newRow = QString("[%1] %2").arg(curCata).arg(curTag);
                groupingTagSharedList->addItem(newRow);
            }
        }

    }
    else { // if one ntode selected, load the tags for that nTrode into the groupingTagList
        groupingTagList->clear();
        QList<QList<QString> > inputTags;
        inputTags = Helper::getSortedTagList(nTrode);

        //now put tags into the groupingTagList
        for (int j = 0; j < inputTags.length(); j++) {
            QString curCata = inputTags.at(j).at(0); //first item of each sub list is the catagorey
            for (int k = 1; k < inputTags.at(j).length(); k++) {
                QString curTag = inputTags.at(j).at(k);
                GroupingTag curGTag;
                curGTag.tag = curTag;
                curGTag.category = curCata;
                allTags.insert(curGTag, 1);
//                QString newRow = QString("[%1] %2").arg(curCata).arg(curTag);
                QString newRow = curGTag.toTagString();
                groupingTagList->addItem(newRow);
            }
        }
    }

}

void NTrodeSettingsWidget::loadDisplaySettings(SingleSpikeTrodeConf *nTrode) {
    maxDispSpinBox->setValue(nTrode->maxDisp.at(0));
    buttonColorBox->setStyleSheet(QString("background-color:%1").arg(nTrode->color.name()));

}

void NTrodeSettingsWidget::setEnabledForStreaming(bool streamOn) {
//    linkCheckBox->setEnabled(!streamOn);
    refBox->setEnabled(!streamOn);
    filterBox->setEnabled(!streamOn);
    ModuleDataFilterBox->setEnabled(!streamOn);
//    RefTrodeBox->setEnabled(!streamOn);
//    RefChannelBox->setEnabled(!streamOn);
    lowFilterCutoffBox->setEnabled(!streamOn);
    highFilterCutoffBox->setEnabled(!streamOn);
    ModuleDataHighFilterCutoffBox->setEnabled(!streamOn);


    //Disabling the ability to select main LFP channel until Issue #30 is fixed
    //ModuleDataChannelBox->setEnabled(false);
    ModuleDataChannelBox->setEnabled(!streamOn);
}

bool NTrodeSettingsWidget::checkNTrodesForConflicts() {
    //Instead of O(n^2) search for conflicts, search across all ntrodes for each widget box


    resetAllLabelsToDefault();
    setAllLabelColors(QColor("black"));
    bool conflict = false;
    QString modifiedNtrodesMsg = "";
//    for (int i = 0; i < selectedIndicis.length(); i++) {
    if(selectedIndicis.length()){
        if (checkSelectedNTrodesForConflicts(spikeConf->ntrodes[selectedIndicis.at(0)])) {
            conflict = true;
            modifiedNtrodesMsg = "  *Mismatched nTrode Settings*";
        }
    }
//    }
//    labelMismatchSettingsWarning->setText(modifiedNtrodesMsg);
    QString toolTipMsg = "Settings across selected nTrodes differ.";
//    labelMismatchSettingsWarning->setToolTip(toolTipMsg);
    return(conflict);
}

//This function checks the specified 'checkedNTrode' against all other selected nTrodes
bool NTrodeSettingsWidget::checkSelectedNTrodesForConflicts(SingleSpikeTrodeConf* checkedNTrode) {
    bool conflict = false;
//    qDebug() << "For nTrode " << checkedNTrode->nTrodeId;

//    for (int i = 0; i < selectedIndicis.length(); i++) {
//        SingleSpikeTrodeConf* curNTrode = spikeConf->ntrodes[selectedIndicis[i]];
//        if (curNTrode->nTrodeId == checkedNTrode->nTrodeId)
//            continue; //no reason to check the nTrode against itself
    for(int i = 0; i < selectedIndicis.length(); i++){
        SingleSpikeTrodeConf* curNTrode = spikeConf->ntrodes[selectedIndicis[i]];
        int group = checkedNTrode->groupRefOn ? checkedNTrode->refGroup : 0;
        int group2 = curNTrode->groupRefOn ? curNTrode->refGroup : 0;
        if (group != group2) {
            QString str = cargrouplabel->text();
            if (!str.contains("*"))
                str.append("*");
            cargrouplabel->setText(str);
            Helper::setWidgetTextPaletteColor(cargrouplabel, QColor("red"));
            conflict = true;
        }
    }

//    for(int i = 0; i < selectedIndicis.length(); i++){
//        SingleSpikeTrodeConf* curNTrode = spikeConf->ntrodes[selectedIndicis[i]];
//        if (checkedNTrode->notchFilterOn != curNTrode->notchFilterOn) {
//            QString str = notchFilterBox->title();
//            if (!str.contains("*"))
//                str.append("*");
//            filterBox->setTitle(str);
//            Helper::setWidgetTextPaletteColor(notchFilterBox, QColor("red"));
//            conflict = true;
//        }
//    }
    for(int i = 0; i < selectedIndicis.length(); i++){
        SingleSpikeTrodeConf* curNTrode = spikeConf->ntrodes[selectedIndicis[i]];
        if (checkedNTrode->notchFreq != curNTrode->notchFreq) {
            QString str = labels.at(NOTCHFREQ_LABEL_INDEX)->text();
            if (!str.contains("*"))
                str.append("*");
            labels.at(NOTCHFREQ_LABEL_INDEX)->setText(str);
            Helper::setWidgetTextPaletteColor(labels.at(NOTCHFREQ_LABEL_INDEX), QColor("red"));
            conflict = true;
        }
    }

    for(int i = 0; i < selectedIndicis.length(); i++){
        SingleSpikeTrodeConf* curNTrode = spikeConf->ntrodes[selectedIndicis[i]];
        if (checkedNTrode->notchBW != curNTrode->notchBW) {
            QString str = labels.at(NOTCHBW_LABEL_INDEX)->text();
            if (!str.contains("*"))
                str.append("*");
            labels.at(NOTCHBW_LABEL_INDEX)->setText(str);
            Helper::setWidgetTextPaletteColor(labels.at(NOTCHBW_LABEL_INDEX), QColor("red"));
            conflict = true;
        }
    }

    for(int i = 0; i < selectedIndicis.length(); i++){
        SingleSpikeTrodeConf* curNTrode = spikeConf->ntrodes[selectedIndicis[i]];
        if (checkedNTrode->filterOn != curNTrode->filterOn) {
            QString str = filterBox->title();
            if (!str.contains("*"))
                str.append("*");
            filterBox->setTitle(str);
            Helper::setWidgetTextPaletteColor(filterBox, QColor("red"));
            conflict = true;
        }
    }
    for(int i = 0; i < selectedIndicis.length(); i++){
        SingleSpikeTrodeConf* curNTrode = spikeConf->ntrodes[selectedIndicis[i]];
        if (checkedNTrode->lowFilter != curNTrode->lowFilter) {
            QString str = labels.at(LOWFILTER_LABEL_INDEX)->text();
            if (!str.contains("*"))
                str.append("*");
            labels.at(LOWFILTER_LABEL_INDEX)->setText(str);
            Helper::setWidgetTextPaletteColor(labels.at(LOWFILTER_LABEL_INDEX), QColor("red"));
            conflict = true;
        }
    }

    for(int i = 0; i < selectedIndicis.length(); i++){
        SingleSpikeTrodeConf* curNTrode = spikeConf->ntrodes[selectedIndicis[i]];
        if (checkedNTrode->highFilter != curNTrode->highFilter) {
            QString str = labels.at(HIGHFILTER_LABEL_INDEX)->text();
            if (!str.contains("*"))
                str.append("*");
            labels.at(HIGHFILTER_LABEL_INDEX)->setText(str);
            Helper::setWidgetTextPaletteColor(labels.at(HIGHFILTER_LABEL_INDEX), QColor("red"));
            conflict = true;
        }
    }

    for(int i = 0; i < selectedIndicis.length(); i++){
        SingleSpikeTrodeConf* curNTrode = spikeConf->ntrodes[selectedIndicis[i]];
        if (checkedNTrode->refOn != curNTrode->refOn) {
            QString str = spikeRefCheckBox->text();
            if (!str.contains("*"))
                str.append("*");
            spikeRefCheckBox->setText(str);
            Helper::setWidgetTextPaletteColor(spikeRefCheckBox, QColor("red"));
            conflict = true;
        }
    }

    for(int i = 0; i < selectedIndicis.length(); i++){
        SingleSpikeTrodeConf* curNTrode = spikeConf->ntrodes[selectedIndicis[i]];
        if (checkedNTrode->lfpRefOn != curNTrode->lfpRefOn) {
            QString str = lfpRefCheckBox->text();
            if (!str.contains("*"))
                str.append("*");
            lfpRefCheckBox->setText(str);
            Helper::setWidgetTextPaletteColor(lfpRefCheckBox, QColor("red"));
            conflict = true;
        }
    }

    for(int i = 0; i < selectedIndicis.length(); i++){
        SingleSpikeTrodeConf* curNTrode = spikeConf->ntrodes[selectedIndicis[i]];
        if (checkedNTrode->lfpFilterOn != curNTrode->lfpFilterOn) {
            QString str = ModuleDataFilterBox->title();
            if (!str.contains("*"))
                str.append("*");
            ModuleDataFilterBox->setTitle(str);
            Helper::setWidgetTextPaletteColor(ModuleDataFilterBox, QColor("red"));
            conflict = true;
        }
    }

    for(int i = 0; i < selectedIndicis.length(); i++){
        SingleSpikeTrodeConf* curNTrode = spikeConf->ntrodes[selectedIndicis[i]];
        if (checkedNTrode->refNTrodeID != curNTrode->refNTrodeID) {
            QString str = labels.at(REFNTRODE_LABEL_INDEX)->text();
            if (!str.contains("*"))
                str.append("*");
            labels.at(REFNTRODE_LABEL_INDEX)->setText(str);
            Helper::setWidgetTextPaletteColor(labels.at(REFNTRODE_LABEL_INDEX), QColor("red"));
            conflict = true;
        }
    }

    for(int i = 0; i < selectedIndicis.length(); i++){
        SingleSpikeTrodeConf* curNTrode = spikeConf->ntrodes[selectedIndicis[i]];
        if (checkedNTrode->refChan != curNTrode->refChan) {
            QString str = labels.at(REFCHAN_LABEL_INDEX)->text();
            if (!str.contains("*"))
                str.append("*");
            labels.at(REFCHAN_LABEL_INDEX)->setText(str);
            Helper::setWidgetTextPaletteColor(labels.at(REFCHAN_LABEL_INDEX), QColor("red"));
            conflict = true;
        }
    }

    for(int i = 0; i < selectedIndicis.length(); i++){
        SingleSpikeTrodeConf* curNTrode = spikeConf->ntrodes[selectedIndicis[i]];
        if (checkedNTrode->moduleDataChan != curNTrode->moduleDataChan) {
            QString str = labels.at(MODDATACHAN_LABEL_INDEX)->text();
            if (!str.contains("*"))
                str.append("*");
            labels.at(MODDATACHAN_LABEL_INDEX)->setText(str);
            Helper::setWidgetTextPaletteColor(labels.at(MODDATACHAN_LABEL_INDEX), QColor("red"));
            conflict = true;
        }
    }

    for(int i = 0; i < selectedIndicis.length(); i++){
        SingleSpikeTrodeConf* curNTrode = spikeConf->ntrodes[selectedIndicis[i]];
        if (checkedNTrode->moduleDataHighFilter != curNTrode->moduleDataHighFilter) {
            QString str = labels.at(MODDATAHIGH_LABEL_INDEX)->text();
            if (!str.contains("*"))
                str.append("*");
            labels.at(MODDATAHIGH_LABEL_INDEX)->setText(str);
            Helper::setWidgetTextPaletteColor(labels.at(MODDATAHIGH_LABEL_INDEX), QColor("red"));
            conflict = true;
        }
    }

//        if (checkedNTrode->triggerOn.at(0) != curNTrode->triggerOn.at(0)) {
//            QString str = triggerBox->title();
//            if (!str.contains("*"))
//                str.append("*");
//            triggerBox->setTitle(str);
//            Helper::setWidgetTextPaletteColor(triggerBox, QColor("red"));
//            conflict = true;
//        }

    for(int i = 0; i < selectedIndicis.length(); i++){
        SingleSpikeTrodeConf* curNTrode = spikeConf->ntrodes[selectedIndicis[i]];
        if (checkedNTrode->thresh.at(0) != curNTrode->thresh.at(0)) {
            QString str = labels.at(THRESH_LABEL_INDEX)->text();
            if (!str.contains("*"))
                str.append("*");
            labels.at(THRESH_LABEL_INDEX)->setText(str);
            Helper::setWidgetTextPaletteColor(labels.at(THRESH_LABEL_INDEX), QColor("red"));
            conflict = true;
        }
    }

    for(int i = 0; i < selectedIndicis.length(); i++){
        SingleSpikeTrodeConf* curNTrode = spikeConf->ntrodes[selectedIndicis[i]];
        if (checkedNTrode->maxDisp.at(0) != curNTrode->maxDisp.at(0)) {
            QString str = labels.at(MAXDISP_LABEL_INDEX)->text();
            if (!str.contains("*"))
                str.append("*");
            labels.at(MAXDISP_LABEL_INDEX)->setText(str);
            Helper::setWidgetTextPaletteColor(labels.at(MAXDISP_LABEL_INDEX), QColor("red"));
            conflict = true;
        }
    }

    for(int i = 0; i < selectedIndicis.length(); i++){
        SingleSpikeTrodeConf* curNTrode = spikeConf->ntrodes[selectedIndicis[i]];
        if (checkedNTrode->color != curNTrode->color) {
            QString str = labels.at(COLOR_LABEL_INDEX)->text();
            if (!str.contains("*"))
                str.append("*");
            labels.at(COLOR_LABEL_INDEX)->setText(str);
            Helper::setWidgetTextPaletteColor(labels.at(COLOR_LABEL_INDEX), QColor("red"));
            conflict = true;
        }
    }

//    for(int i = 0; i < selectedIndicis.length(); i++){
//        SingleSpikeTrodeConf* curNTrode = spikeConf->ntrodes[selectedIndicis[i]];
        if (groupingTagSharedList->count() != groupingTagAllList->count()){
            QString str = groupingTagBox->title();
            if (!str.contains("*"))
                str.append("*");
            groupingTagBox->setTitle(str);
            conflict = true;
        }
//    }


//    }
    return(conflict);
}

//this function resets all label text to their default values
void NTrodeSettingsWidget::resetAllLabelsToDefault() {
    for (int i = 0; i < labels.length(); i++) {
//        QLabel *curLabel = labels.at(i);
        QString str = labels.at(i)->text();
        if (str.contains("*"))
            str.remove("*");
        labels.at(i)->setText(str);
    }
    QString str;
    str = (QString)cargrouplabel->text();
    if (str.contains("*"))
        str.remove("*");
    cargrouplabel->setText(str);

    str = (QString)filterBox->title();
    if (str.contains("*"))
        str.remove("*");
    filterBox->setTitle(str);

    str = spikeRefCheckBox->text();
    if (str.contains("*"))
        str.remove("*");
    spikeRefCheckBox->setText(str);

    str = lfpRefCheckBox->text();
    if (str.contains("*"))
        str.remove("*");
    lfpRefCheckBox->setText(str);

    str = ModuleDataFilterBox->title();
    if (str.contains("*"))
        str.remove("*");
    ModuleDataFilterBox->setTitle(str);

    str = groupingTagBox->title();
    if (str.contains("*"))
        str.remove("*");
    groupingTagBox->setTitle(str);
}

//This function sets the text color of all the labels in the TriggerScopeSettingsWidget to black
void NTrodeSettingsWidget::setAllLabelColors(QColor newColor) {
    for (int i = 0; i < labels.length(); i++) {
        Helper::setWidgetTextPaletteColor(labels.at(i), newColor);
    }
//    Helper::setWidgetTextPaletteColor(notchFilterBox, newColor);
    Helper::setWidgetTextPaletteColor(filterBox, newColor);
//    Helper::setWidgetTextPaletteColor(refBox, newColor);
    Helper::setWidgetTextPaletteColor(spikeRefCheckBox, newColor);
    Helper::setWidgetTextPaletteColor(lfpRefCheckBox, newColor);
    Helper::setWidgetTextPaletteColor(ModuleDataFilterBox, newColor);
    Helper::setWidgetTextPaletteColor(triggerBox, newColor);
    Helper::setWidgetTextPaletteColor(cargrouplabel, newColor);
//    groupingTagBox->setTitle("");
}

//void TriggerScopeSettingsWidget::linkBoxClicked(bool checked) {
//    emit toggleLinkChanges(checked);
//}

void NTrodeSettingsWidget::setMaxDisplay(int newMaxDisp) {
    if (maxDispSpinBox != NULL) {
        maxDispSpinBox->setValue(newMaxDisp);
//        updateMaxDisp(); //***No need for this, signals already change all selected
//        emit configChanged(); //***No need for this, display values not part of config
    }
}

void NTrodeSettingsWidget::setThresh(int newThresh) {
    if (threshSpinBox != NULL) {
        threshSpinBox->setValue(newThresh);
        updateThresh();
        emit configChanged();
    }
}

void NTrodeSettingsWidget::setChanColorBox(QColor newColor) {
    buttonColorBox->setStyleSheet(QString("background-color:%1").arg(newColor.name()));
    checkNTrodesForConflicts();
}

void NTrodeSettingsWidget::updateRefChan(int) {
    updateRefChan();
}


void NTrodeSettingsWidget::updateRefChan() {
//    qDebug() << "UpdateRefChan";
//    //qDebug() << "Update Ref Channel";
    //int newRefTrode  = RefTrodeBox->currentText().toInt()-1;

    int newRefTrode  = RefTrodeBox->currentText().toInt();

    bool foundRefTrode = false;
    int newRefTrodeInd;
    for (int i=0; i<spikeConf->ntrodes.length(); i++) {
        if (spikeConf->ntrodes[i]->nTrodeId == newRefTrode) {
            newRefTrodeInd = i;
            foundRefTrode = true;
            break;
        }
    }

    if (!foundRefTrode) {
        qDebug() << "Error: could not find reference nTrode" << newRefTrode;
        return;
    }

    int newRefChannel  = RefChannelBox->currentText().toInt()-1;


    //When the ref trode menu is changed, the ref channel menu is cleared, and the
    //index is temporarity changed to -1.  We ignore the -1 signal.

    if (newRefChannel > -1) {


        if (linkChangesBool) {
            //int nTrodeIndex = RefTrodeBox->currentIndex();
            //int channelIndex = RefChannelBox->currentIndex();
            emit changeAllRefs(newRefTrodeInd,newRefChannel);
        } else {
            for (int i = 0; i < selectedIndicis.length(); i++) {
                spikeConf->setReference(selectedIndicis[i], newRefTrodeInd, newRefChannel, false);
            }
            emit spikeConf->updatedRef();
//            spikeConf->setReference(nTrodeNumber,newRefTrodeInd,newRefChannel);

        }
        emit updateAudioSettings();
    }
    checkNTrodesForConflicts();
}

void NTrodeSettingsWidget::updateRefSwitch(bool on) {
//    qDebug() << "Update Spike Ref Switch " << on;
    if (linkChangesBool) {
        emit toggleAllRefs(on);
    } else {
        for (int i = 0; i < selectedIndicis.length(); i++) {
            spikeConf->setRefSwitch(selectedIndicis[i],on, false);
        }
        emit spikeConf->updatedRef();
        emit updateAudioSettings();
    }
    checkNTrodesForConflicts();
}

void NTrodeSettingsWidget::updateRawRefOn(bool on){
    for(int i = 0; i < selectedIndicis.length(); ++i){
        spikeConf->setRawRefOn(selectedIndicis[i], on, false);
    }
    emit spikeConf->updatedRef();
    emit updateAudioSettings();
    checkNTrodesForConflicts();
}

void NTrodeSettingsWidget::updateLFPFilterSwitch(bool on) {
//    qDebug() << "Update LFP Ref Switch " << on;
    for (int i = 0; i < selectedIndicis.length(); i++) {
        spikeConf->setLFPRefSwitch(selectedIndicis[i],on, false);
    }
    emit spikeConf->updatedRef();
    emit updateAudioSettings();
    checkNTrodesForConflicts();
}

//void NTrodeSettingsWidget::updateNotchFilterSwitch(bool on) {
////    qDebug() << "Update Notch Filter Switch " << on;
//    for (int i = 0; i < selectedIndicis.length(); i++) {
////        qDebug() << "updateNotchFilterSwitch" << on;
//        spikeConf->setNotchFilterSwitch(selectedIndicis[i], on, false);
//    }
//    emit spikeConf->updatedNotchFilter();
//    emit updateAudioSettings();
//    checkNTrodesForConflicts();
//}

//void NTrodeSettingsWidget::updateNotchFilterFreq() {
//    int newNotchFreq = notchFrequencyBox->currentText().toInt();
//    for (int i = 0; i < selectedIndicis.length(); i++) {
//        spikeConf->setNotchFilterFreq(selectedIndicis[i], newNotchFreq, false);
//    }
//    emit spikeConf->updatedNotchFilter();
//    emit updateAudioSettings();
//    checkNTrodesForConflicts();
//}

//void NTrodeSettingsWidget::updateNotchFilterBW() {
////    qDebug() << "Update Notch Filter BW ";
//    int newNotchBW = notchBandwidthBox->currentText().toInt();
//    for (int i = 0; i < selectedIndicis.length(); i++) {
//        spikeConf->setNotchFilterBW(selectedIndicis[i], newNotchBW, false);
//    }
//    emit spikeConf->updatedNotchFilter();
//    emit updateAudioSettings();
//    checkNTrodesForConflicts();
//}

void NTrodeSettingsWidget::updateFilterSwitch(bool on) {
    //qDebug() << "Update Filter Switch";
    if (linkChangesBool) {
        emit toggleAllFilters(on);
    } else {
        for (int i = 0; i < selectedIndicis.length(); i++) {
            spikeConf->setFilterSwitch(selectedIndicis[i],on, false);
        }
        emit spikeConf->updatedFilterOn();
        emit updateAudioSettings();
    }
    checkNTrodesForConflicts();
}

void NTrodeSettingsWidget::updateLowerFilter() {
    int newLowCutoff = lowFilterCutoffBox->currentText().toInt();
    int newHighCutoff = highFilterCutoffBox->currentText().toInt();
    //qDebug() << "Update Lower Filter";
    if (linkChangesBool) {
        emit changeAllFilters(newLowCutoff,newHighCutoff);
    } else {
        for (int i = 0; i < selectedIndicis.length(); i++) {
            spikeConf->setLowFilter(selectedIndicis[i],newLowCutoff, false);
        }
        emit spikeConf->updatedFilter(selectedIndicis);
        emit updateAudioSettings();
    }
    checkNTrodesForConflicts();
}

void NTrodeSettingsWidget::updateUpperFilter() {
    int newLowCutoff = lowFilterCutoffBox->currentText().toInt();
    int newHighCutoff = highFilterCutoffBox->currentText().toInt();
    //qDebug() << "Update High Filter";
    if (linkChangesBool) {
       emit changeAllFilters(newLowCutoff,newHighCutoff);
    } else {
        for (int i = 0; i < selectedIndicis.length(); i++) {
            spikeConf->setHighFilter(selectedIndicis[i],newHighCutoff, false);
        }
        emit spikeConf->updatedFilter(selectedIndicis);
        emit updateAudioSettings();
    }
    checkNTrodesForConflicts();
}

void NTrodeSettingsWidget::updateModuleDataFilterSwitch(bool on) {
//    qDebug() << "Update LFP Filter Switch " << on;
    for (int i = 0; i < selectedIndicis.length(); i++) {
        spikeConf->setLFPFilterSwitch(selectedIndicis[i],on, false);
    }
    emit spikeConf->updatedLFPFilter();
    checkNTrodesForConflicts();
}

void NTrodeSettingsWidget::updateModuleDataChannel() {
    //qDebug() << "Update Data Channel";
    int newChannel = ModuleDataChannelBox->currentText().toInt();
//    spikeConf->setModuleDataChan(nTrodeNumber,newChannel-1); //convert back to 0-based
    for (int i = 0; i < selectedIndicis.length(); i++) {
        spikeConf->setModuleDataChan(selectedIndicis[i],newChannel-1, false); //convert back to 0-based
        // we also need to addEditModuleToConfig this change to the modules
        emit moduleDataChannelChanged(selectedIndicis[i], newChannel-1);
    }
    emit spikeConf->updatedModuleData();
    checkNTrodesForConflicts();
    // we also need to addEditModuleToConfig this change to the modules
//    emit moduleDataChannelChanged(nTrodeNumber, newChannel-1);
}

void NTrodeSettingsWidget::updateModuleDataUpperFilter() {
    //qDebug() << "Update Data Upper Filter";
    int newCutoff = ModuleDataHighFilterCutoffBox->currentText().toInt();
//    spikeConf->setModuleDataHighFilter(nTrodeNumber,newCutoff);
    for (int i = 0; i < selectedIndicis.length(); i++) {
        spikeConf->setModuleDataHighFilter(selectedIndicis[i],newCutoff, false);
    }
    emit spikeConf->updatedModuleDataFilter(selectedIndicis);
    checkNTrodesForConflicts();
}

void NTrodeSettingsWidget::updateSpikeTrigger(bool on) {
    //qDebug() << "Update Spike Trigger";
    if (linkChangesBool) {
        emit toggleAllTriggers(on);
    } else {
        for (int i = 0; i < selectedIndicis.length(); i++) {
//            spikeConf->setFilterSwitch(selectedIndicis[i],on);
            spikeConf->setTriggerMode(selectedIndicis[i], on);
        }
        emit updateAudioSettings();
    }
    checkNTrodesForConflicts();
}

void NTrodeSettingsWidget::updateThresh() {
    //qDebug() << "Update Thresh";
    if (linkChangesBool) {
        emit changeAllThresholds(threshSpinBox->value());
    } else {
        for (int i = 0; i < selectedIndicis.length(); i++) {
            spikeConf->setThresh(selectedIndicis[i], threshSpinBox->value(), false);
        }
        emit spikeConf->updatedThresh();
        emit sig_threshUpdated(threshSpinBox->value());
        emit updateAudioSettings();
    }
    checkNTrodesForConflicts();
}

void NTrodeSettingsWidget::updateMaxDisp() {
    //qDebug() << "Update Max Disp";
    if (linkChangesBool) {
        emit changeAllMaxDisp(maxDispSpinBox->value());
    } else {
        for (int i = 0; i < selectedIndicis.length(); i++) {
            spikeConf->setMaxDisp(selectedIndicis[i], maxDispSpinBox->value(), false);
        }
        emit spikeConf->updatedMaxDisplay();
        emit sig_maxDisplayUpdated(maxDispSpinBox->value());
        emit updateAudioSettings();
    }
    checkNTrodesForConflicts();
}

void NTrodeSettingsWidget::updateChanColor() {
//    qDebug() << "Update channel color";
    QString colorCode = QString("#%1").arg(buttonColorBox->styleSheet().split("#").last());
    if (linkChangesBool) {
        for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
            spikeConf->setColor(i, QColor(colorCode));
        }
    }
    else {
        for (int i = 0; i < selectedIndicis.length(); i++) {
            spikeConf->setColor(selectedIndicis[i], QColor(colorCode));
        }
    }
    checkNTrodesForConflicts();
}

void NTrodeSettingsWidget::updateRefTrode(int notused) {
    updateRefTrode();
}


void NTrodeSettingsWidget::updateRefTrode() {
//    qDebug() << "UpdateRefTrode";
    int newRefTrode  = RefTrodeBox->currentText().toInt();

    bool foundRefTrode = false;
    int newRefTrodeInd;
    for (int i=0; i<spikeConf->ntrodes.length(); i++) {
        if (spikeConf->ntrodes[i]->nTrodeId == newRefTrode) {
            newRefTrodeInd = i;
            foundRefTrode = true;
            break;
        }
    }

    if (!foundRefTrode) {
        qDebug() << "Error: could not find reference nTrode" << newRefTrode;
        return;
    }


    RefChannelBox->clear();
    for (int i = 0; i < spikeConf->ntrodes[newRefTrodeInd]->hw_chan.length(); i++)
        RefChannelBox->addItem(QString::number(i+1));

//    qDebug() << "Set the Ref NTrode to " << newRefTrode << " at index " << newRefTrodeInd << " chan [" << RefChannelBox->itemText(0) << "]";
    int newRefChan = RefChannelBox->itemText(0).toInt()-1;
    //save the settings
    if (linkChangesBool) {
        emit changeAllRefs(newRefTrodeInd, newRefChan);
    }
    else {
        for (int i = 0; i < selectedIndicis.length(); i++) {
            spikeConf->setReference(selectedIndicis[i], newRefTrodeInd, newRefChan, false);
        }
        emit spikeConf->updatedRef();
    }
    emit updateAudioSettings();

    checkNTrodesForConflicts();
}

void NTrodeSettingsWidget::updateRefGroup(){
//    cargrouppanel->show();
////    cargrouppanel->raise();
}

void NTrodeSettingsWidget::updateRefGroupId(int id){
    bool configchanged = false;
    if(id){
        for (int i = 0; i < selectedIndicis.length(); i++) {
            if(spikeConf->ntrodes[selectedIndicis[i]]->refGroup != id)
                configchanged = true;
            spikeConf->setGroupRef(selectedIndicis[i], true, id, false);
        }
        emit spikeConf->updatedRef();
        RefTrodeBox->setEnabled(false);
        RefChannelBox->setEnabled(false);
    }
    else{
        for (int i = 0; i < selectedIndicis.length(); i++) {
            if(spikeConf->ntrodes[selectedIndicis[i]]->refGroup != id)
                configchanged = true;
            spikeConf->setGroupRef(selectedIndicis[i], false, id, false);
        }
        emit spikeConf->updatedRef();
        RefTrodeBox->setEnabled(true);
        RefChannelBox->setEnabled(true);
    }

    if(configchanged)
        configChanged();

    emit updateAudioSettings();
}

void NTrodeSettingsWidget::updateTags() {
//    qDebug() << "Saving Grouping Tags";

    if (groupingDialog != nullptr) {
//        QList<QString> tagsToAdd = groupingDialog->getTagList();
        QList<GroupingTag> tagsToAdd = groupingDialog->getAddedTags();
        QList<GroupingTag> tagsToRemove = groupingDialog->getRemovedTags();

        for (int i = 0; i < selectedIndicis.length(); i++) { //over all selected nTrodes
            SingleSpikeTrodeConf *curNTrode = spikeConf->ntrodes[selectedIndicis[i]];
//            spikeConf->ntrodes[selectedIndicis[i]]->tags.clear();
            for (int j = 0; j < tagsToAdd.length(); j++) {
                if (!curNTrode->gTags.contains(tagsToAdd.at(j))) { //Add the tag to the nTrode if it doesn't already exist
                    curNTrode->gTags.insert(tagsToAdd.at(j),1);
                }
            }
            for (int j = 0; j < tagsToRemove.length(); j++) {
                if (curNTrode->gTags.contains(tagsToRemove.at(j))) { //remove the tag from the nTrode if it exists in the nTrode
                    curNTrode->gTags.remove(tagsToRemove.at(j));
                }
            }
        }
        groupingDialog->clearAddedTagList();
        groupingDialog->clearRemovedTagList();
    }

    loadTags(spikeConf->ntrodes[nTrodeNumber]); //reload the new tags into the view panel
    checkNTrodesForConflicts();
}

void NTrodeSettingsWidget::addRemoveTagsButtonPressed() {
//    qDebug() << "Summon the add/remove tags dialog!";
//    SingleSpikeTrodeConf *nTrode = spikeConf->ntrodes.at(nTrodeNumber);
    if (groupingDialog == NULL) {
        groupingDialog = new TagGroupingPanel();
        groupingDialog->setTitle("Add/Remove Tags");
        groupingDialog->setWindowFlags(Qt::WindowStaysOnTopHint);
        connect(groupingDialog, SIGNAL(sig_categoryAdded(QString)), this, SLOT(addCategoryToDict(QString)));
        connect(groupingDialog, SIGNAL(sig_newTagAdded(QString,QString)), this, SLOT(addTagToDict(QString,QString)));
        connect(groupingDialog,SIGNAL(sig_enableApplyButtons()),this,SLOT(updateTags()));
        connect(groupingDialog,SIGNAL(sig_enableApplyButtons()),this,SIGNAL(configChanged()));
//        connect(groupingDialog, );
        connect(groupingTabView, SIGNAL(currentChanged(int)), groupingDialog, SLOT(setCurrentTab(int)));
        connect(groupingDialog, SIGNAL(sig_tabChanged(int)), groupingTabView, SLOT(setCurrentIndex(int)));
//        groupingTabView->set

        if (groupingTabView->isVisible()) { //initially we have to set the groupingDialog's index manually
            groupingDialog->setCurrentTab(groupingTabView->currentIndex());
        }
        else
            groupingDialog->setCurrentTab(0);

    }
    groupingDialog->updateDict(spikeConf->groupingDict);

    QList<SingleSpikeTrodeConf> selectedNTrodes;
    for (int i = 0; i < selectedIndicis.length(); i++) {
        SingleSpikeTrodeConf cNtr = *spikeConf->ntrodes.at(selectedIndicis[i]);
        selectedNTrodes.append(cNtr);
    }

    if (selectedNTrodes.length() > 1)
        groupingDialog->loadMultipleNTrodeTags(selectedNTrodes);
    else
        groupingDialog->loadNTrodeTags(allTags);

//    groupingDialog->loadNTrodeTags(allTags); //load all tags from the selected nTrodes
    groupingDialog->loadCurrentCategoryTags();
    groupingDialog->setVisible(!groupingDialog->isVisible());
    groupingDialog->raise();
    groupingDialog->setFocus();
}

void NTrodeSettingsWidget::addCategoryToDict(QString nCata) {
//    qDebug() << "Adding new category to dictionary";
    spikeConf->groupingDict.addCategory(nCata);
}

void NTrodeSettingsWidget::addTagToDict(QString tCata, QString nTag) {
//    qDebug() << "Adding new tag to the dictionary";
    spikeConf->groupingDict.addTagToCategory(tCata, nTag);
}

void NTrodeSettingsWidget::setPanelDim(int width, int height) {
//    qDebug() << "Setting new widget dimensions to (" << width << ", " << height << ")";
    this->setGeometry(this->geometry().x(), this->geometry().y(), width, height);
    panelSize.setWidth(width);
    panelSize.setHeight(height);

}

//set the widgets offset, THIS DOES NOT EFFECT SETPOSITION, this merely keeps track of user set offsets for automatic repositioning
void NTrodeSettingsWidget::setPositionOffset(int xOffset, int yOffset) {
    posOffset.setX(xOffset);
    posOffset.setY(yOffset);
}

//sets the absolute position of the widget
void NTrodeSettingsWidget::setPosition(int xpos, int ypos) {
//    qDebug() << "Setting new widget position to (" << xpos << ", " <<  ypos << ")";
    this->setGeometry(xpos, ypos, this->geometry().width(), this->geometry().height());
}

void NTrodeSettingsWidget::setVisible(bool visible) {
    if (attachedMode) {
        if (visible) { //run expand animation
            runAnimation_Expand(250);
            QWidget::setVisible(visible);
        }
        else { //run close animation
            runAnimation_Close(250);
        }
    }
    else {
        QWidget::setVisible(visible);

        if (visible)
            setWidgetsVisible();
        else
            setWidgetsHidden();

    }
}

void NTrodeSettingsWidget::runAnimation_Expand(int time, bool expandRight) {
    if (!attachedMode)
        return;

    if (expandRight) {
        QPoint curPos;
        curPos.setX(this->geometry().x());
        curPos.setY(this->geometry().y());

        a_horizontalExpand->setDuration(time);
        a_horizontalExpand->setStartValue(QRect(curPos.x(), curPos.y(), 0, panelSize.height()));
        a_horizontalExpand->setEndValue(QRect(curPos.x(), curPos.y(), panelSize.width(), panelSize.height()));
        a_horizontalExpand->start();
    }
}

void NTrodeSettingsWidget::runAnimation_Close(int time, bool closeFromRight) {
    if (!attachedMode)
        return;

    if (closeFromRight) {
        setWidgetsHidden();

        QPoint curPos;
        curPos.setX(this->geometry().x());
        curPos.setY(this->geometry().y());

        a_minSize->setDuration(250);
        a_minSize->setStartValue(panelSize);
        a_minSize->setEndValue(QSize(0,panelSize.height()));
        a_minSize->start();

        a_horizontalClose->setDuration(time);
        a_horizontalClose->setStartValue(QRect(curPos.x(), curPos.y(), panelSize.width(), panelSize.height()));
        a_horizontalClose->setEndValue(QRect(curPos.x(), curPos.y(), 0, panelSize.height()));
        a_horizontalClose->start();
    }
}

void NTrodeSettingsWidget::getColorDialog() {
    QString colorCode = QString("#%1").arg(buttonColorBox->styleSheet().split("#").last());
    QColor newColor = QColorDialog::getColor(colorCode, this, "Select New Channel Color");
    if (newColor.isValid()) {
        buttonColorBox->setStyleSheet(QString("background-color:%1").arg(newColor.name()));
        updateChanColor();
        emit configChanged();
    }
}

void NTrodeSettingsWidget::addCARGroupButtonPressed() {

    bool ok;
    QString text = QInputDialog::getText(this, tr("Create CAR group using selected channels"),
                                             tr("Group name:"), QLineEdit::Normal,
                                             QString("CAR %1").arg(RefGroupBox->count()), &ok);
    if (!ok || text.isEmpty()) return;

    CARGroup cg;
    cg.description = text;
    cg.useCount = 0;

    for(int i = 0; i < selectedIndicis.length(); i++){
        CARChan cgChan;
        SingleSpikeTrodeConf* curNTrode = spikeConf->ntrodes[selectedIndicis[i]];
        cgChan.ntrodeid = curNTrode->nTrodeId;

        for (int j=0;j<curNTrode->triggerOn.length();j++) {
            if ((curNTrode->triggerOn.at(j))||(j == (curNTrode->triggerOn.length()-1))) {
                //This channel has not been turned off (spike detection), so it is probably good. Either that or it is the last checked channel, so we are forced to use it.
                cgChan.chan = j+1;
                cgChan.hw_chan = curNTrode->hw_chan.at(j);
                break;
            }
        }

        cg.chans.append(cgChan);
    }

    spikeConf->carGroups.append(cg);



    QString shortdesc = cg.description.left(15);
    if(shortdesc != cg.description) shortdesc += "...";
    QString item = QString("%1: %2").arg(RefGroupBox->count()).arg(shortdesc);
    RefGroupBox->addItem(item);


    emit configChanged();


}

void NTrodeSettingsWidget::setWidgetsVisible(void) {
    for (int i = 0; i < widgets.length(); i++) {
        widgets.at(i)->setVisible(true);
    }
    for (int i = 0; i < labels.length(); i++) {
        labels.at(i)->setVisible(true);
    }
    setGroupingTagWidgetsVisibility();
//    cargrouppanel->setVisible(false);
}

void NTrodeSettingsWidget::setWidgetsHidden(void) {
    for (int i = 0; i < widgets.length(); i++) {
        widgets.at(i)->setVisible(false);
    }
    for (int i = 0; i < labels.length(); i++) {
        labels.at(i)->setVisible(false);
    }
//    cargrouppanel->setVisible(false);
}

//This function controls which grouping tag widgets are visible.
void NTrodeSettingsWidget::setGroupingTagWidgetsVisibility() {
    if (!this->isVisible())
        return; //only execute if the widget is visible

    if (multipleSelected) {
        groupingTagList->setVisible(false);
        groupingTabView->setVisible(true);
        groupingTagSharedList->setVisible(true);
        groupingTagAllList->setVisible(true);

    }
    else {
        groupingTagList->setVisible(true);
        groupingTabView->setVisible(false);
        groupingTagSharedList->setVisible(false);
        groupingTagAllList->setVisible(false);
    }
}

void NTrodeSettingsWidget::verifyCARgroupEdits(int groupid, QList<int> ntrodes, QList<int> chans, QString description){
    QList<int> hwchans;
    int badrows = 0;
    for(int i = 0; i < ntrodes.length(); ++i){
        bool badrow = false;
        if(ntrodes[i] <= 0
            || ntrodes[i] > spikeConf->ntrodes.length()
            || spikeConf->ntrodes.ID(ntrodes[i])==nullptr
            || chans[i] <= 0
            || chans[i] > spikeConf->ntrodes.ID(ntrodes[i])->hw_chan.length())
        {
            badrow = true;
        }

        if(badrow){
            hwchans.append(-1);
            badrows++;
        }
        else{
            hwchans.append(spikeConf->ntrodes.ID(ntrodes[i])->hw_chan[chans[i]-1]);
        }
    }

    emit carPanelReviewed(hwchans);

    if(groupid != 0){
        //apply changes, ignoring all -1's
        QVector<CARChan> newchans;
        for(int i = 0; i < hwchans.length(); ++i){
            if(hwchans[i] != -1){
                newchans.append({ntrodes[i], chans[i], hwchans[i]});
            }
        }
        spikeConf->carGroups[groupid-1].chans = newchans;
        spikeConf->carGroups[groupid-1].description = description;
        emit configChanged();
    }
}

void NTrodeSettingsWidget::streamingStarted(){
//    cargrouppanel->streamingStarted();
}

void NTrodeSettingsWidget::streamingStopped(){
//    cargrouppanel->streamingStopped();
}

PreferencesPanel::PreferencesPanel(QWidget *parent)
    : QWidget(parent)
{
    setWindowTitle("Preferences");
    QGridLayout *mainlayout = new QGridLayout;
    setLayout(mainlayout);

    createSubDirectoryBox = new QCheckBox("Create subdirectory for recordings");
    createSubDirectoryBox->setChecked(true);
    createSubDirectoryBox->setToolTip("Should Trodes automatically create a new folder for each recording?");
    mainlayout->addWidget(createSubDirectoryBox, 0, 0);

    invertSpikesBox = new QCheckBox("Invert spikes upward for display");
    invertSpikesBox->setChecked(false);
    invertSpikesBox->setToolTip("Default is that spikes go downward. Checking will cause spikes to go upward");
    mainlayout->addWidget(invertSpikesBox, 1, 0);
    connect(invertSpikesBox, &QCheckBox::clicked, this, &PreferencesPanel::invertSpikesSet);

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
    settings.beginGroup(QLatin1String("preferences"));
    QByteArray panelvalues = settings.value(QLatin1String("panelvalues")).toByteArray();
    if(!panelvalues.isEmpty()){
        importSettings(panelvalues);
    }
}

PreferencesPanel::~PreferencesPanel(){

}

void PreferencesPanel::saveSettings(){
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
    settings.beginGroup(QLatin1String("preferences"));
    settings.setValue(QLatin1String("panelvalues"), exportSettings());
    settings.setValue(QLatin1String("panelgeo"), saveGeometry());
    settings.endGroup();
}
bool PreferencesPanel::createSubDirectory() const{
    return createSubDirectoryBox->isChecked();
}

bool PreferencesPanel::invertSpikes() const{
    return invertSpikesBox->isChecked();
}

QByteArray PreferencesPanel::exportSettings() const{
    QByteArray d;
    QDataStream stream(&d, QIODevice::WriteOnly);
    stream << createSubDirectoryBox->isChecked();
    stream << invertSpikesBox->isChecked();
    return d;
}
void PreferencesPanel::importSettings(const QByteArray &settings){
    QDataStream stream(settings);
    bool createsubdir=true, invertspikes=false;
    stream >> createsubdir;
    createSubDirectoryBox->setChecked(createsubdir);
    stream >> invertspikes;
    invertSpikesBox->setChecked(invertspikes);
}

void PreferencesPanel::showEvent(QShowEvent *event){
    if(geo.isEmpty()){
        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
        settings.beginGroup(QLatin1String("preferences"));
        geo = settings.value(QLatin1String("panelgeo")).toByteArray();
        settings.endGroup();
    }
    restoreGeometry(geo);
}

void PreferencesPanel::closeEvent(QCloseEvent *event){
    geo = saveGeometry();
}

