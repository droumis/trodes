#include "benchmarkWidget.h"



BenchmarkWidget::BenchmarkWidget(QWidget *parent) : QWidget(parent) {
    //labelOne = new QLabel(this);
    //labelOne->setText("TEST LABEL HIIII");
    iniSpikeGroup();
    iniPositionGroup();
    iniEventGroup();

    MainLayout = new QVBoxLayout();
    BottomGroupsLayout = new QHBoxLayout();
    BottomBarLayout = new QHBoxLayout();
    buttonApply = new QPushButton();
    statusText = new QLabel();

    statusText->setText(tr(""));

    buttonApply->setText(tr("Apply"));
    //buttonApply->setAutoDefault(true);
    //buttonApply->setDefault(true);
    connect(buttonApply, SIGNAL(released()), this, SLOT(updateBenchConfig()));
    connect(buttonApply, SIGNAL(released()), this, SLOT(changeStatusToSaved()));


    BottomGroupsLayout->addWidget(positionGroup);
    BottomGroupsLayout->addWidget(eventGroup);

    BottomBarLayout->addWidget(statusText, 0, Qt::AlignLeft);
    BottomBarLayout->addWidget(buttonApply, 0, Qt::AlignRight);

    MainLayout->addWidget(spikeGroup);
    MainLayout->addLayout(BottomGroupsLayout);
    MainLayout->addLayout(BottomBarLayout);
    setLayout(MainLayout);
    setWindowTitle(tr("Benchmarking Control Panel"));

    getSettingsFromConfig();
}

void BenchmarkWidget::keyPressEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return) {
        emit buttonApply->released(); //bind apply button to enter key
    }

    QWidget::keyPressEvent(event);
}

void BenchmarkWidget::iniSpikeGroup(void) {

    spikeGroup = new QGroupBox(tr("Spike Latency"));
    enableSpikeDetect = new QCheckBox();
    enableSpikeSend = new QCheckBox();
    enableSpikeReceive = new QCheckBox();

    enableSpikeDetect->setText("Enable Spike Detect");
    enableSpikeSend->setText("Enable Spike Send");
    enableSpikeReceive->setText("Enable Spike Receive");

    enableSpikeDetect->setCheckable(true);
    enableSpikeSend->setCheckable(true);
    enableSpikeReceive->setCheckable(true);

    editFreqSpikeDetect = new QLineEdit("0");
    labelFreqSpikeDetect = new QLabel();
    editFreqSpikeDetect->setEnabled(false);
    labelFreqSpikeDetect->setEnabled(false);
    labelFreqSpikeDetect->setText(tr("Print Delay:"));
    sgSpikeDetectLayout = new QVBoxLayout();
    sgSpikeDetectLayout->addWidget(enableSpikeDetect);
    sgSpikeDetectLayout->addWidget(labelFreqSpikeDetect);
    sgSpikeDetectLayout->addWidget(editFreqSpikeDetect);
    connect(enableSpikeDetect, SIGNAL(toggled(bool)), editFreqSpikeDetect, SLOT(setEnabled(bool)));
    connect(enableSpikeDetect, SIGNAL(toggled(bool)), labelFreqSpikeDetect, SLOT(setEnabled(bool)));
    connect(enableSpikeDetect, SIGNAL(toggled(bool)), this, SLOT(changeStatusToEdited()));
    connect(editFreqSpikeDetect, SIGNAL(textEdited(QString)), this, SLOT(changeStatusToEdited()));

    editFreqSpikeSend = new QLineEdit("0");
    labelFreqSpikeSend = new QLabel();
    editFreqSpikeSend->setEnabled(false);
    labelFreqSpikeSend->setEnabled(false);
    labelFreqSpikeSend->setText(tr("Print Delay:"));
    sgSpikeSendLayout = new QVBoxLayout();
    sgSpikeSendLayout->addWidget(enableSpikeSend);
    sgSpikeSendLayout->addWidget(labelFreqSpikeSend);
    sgSpikeSendLayout->addWidget(editFreqSpikeSend);
    connect(enableSpikeSend, SIGNAL(toggled(bool)), editFreqSpikeSend, SLOT(setEnabled(bool)));
    connect(enableSpikeSend, SIGNAL(toggled(bool)), labelFreqSpikeSend, SLOT(setEnabled(bool)));
    connect(enableSpikeSend, SIGNAL(toggled(bool)), this, SLOT(changeStatusToEdited()));
    connect(editFreqSpikeSend, SIGNAL(textEdited(QString)), this, SLOT(changeStatusToEdited()));

    editFreqSpikeReceive = new QLineEdit("0");
    labelFreqSpikeReceive = new QLabel();
    editFreqSpikeReceive->setEnabled(false);
    labelFreqSpikeReceive->setEnabled(false);
    labelFreqSpikeReceive->setText(tr("Print Delay:"));
    sgSpikeReceiveLayout = new QVBoxLayout();
    sgSpikeReceiveLayout->addWidget(enableSpikeReceive);
    sgSpikeReceiveLayout->addWidget(labelFreqSpikeReceive);
    sgSpikeReceiveLayout->addWidget(editFreqSpikeReceive);
    connect(enableSpikeReceive, SIGNAL(toggled(bool)), editFreqSpikeReceive, SLOT(setEnabled(bool)));
    connect(enableSpikeReceive, SIGNAL(toggled(bool)), labelFreqSpikeReceive, SLOT(setEnabled(bool)));
    connect(enableSpikeReceive, SIGNAL(toggled(bool)), this, SLOT(changeStatusToEdited()));
    connect(editFreqSpikeReceive, SIGNAL(textEdited(QString)), this, SLOT(changeStatusToEdited()));


    sgMainLayout = new QVBoxLayout();
    sgOptionLayout = new QHBoxLayout();
    sgFreqLayout = new QHBoxLayout();
    sgOptionLayout->addLayout(sgSpikeDetectLayout);
    sgOptionLayout->addLayout(sgSpikeSendLayout);
    sgOptionLayout->addLayout(sgSpikeReceiveLayout);
    sgMainLayout->addLayout(sgOptionLayout);

    spikeGroup->setLayout(sgMainLayout);
}

void BenchmarkWidget::iniPositionGroup(void) {
    positionGroup = new QGroupBox(tr("Position Latency"));
    pgMainLayout = new QVBoxLayout();

    enablePosition = new QCheckBox();
    enablePosition->setCheckable(true);
    enablePosition->setText("Enable Position Latency");

    labelFreqPosition = new QLabel();
    labelFreqPosition->setText(tr("Print Delay:"));
    labelFreqPosition->setEnabled(false);

    editFreqPosition = new QLineEdit("0");
    editFreqPosition->setEnabled(false);

    connect(enablePosition, SIGNAL(toggled(bool)), labelFreqPosition, SLOT(setEnabled(bool)));
    connect(enablePosition, SIGNAL(toggled(bool)), editFreqPosition, SLOT(setEnabled(bool)));
    connect(enablePosition, SIGNAL(toggled(bool)), this, SLOT(changeStatusToEdited()));
    connect(editFreqPosition, SIGNAL(textEdited(QString)), this, SLOT(changeStatusToEdited()));

    pgMainLayout->addWidget(enablePosition);
    pgMainLayout->addWidget(labelFreqPosition);
    pgMainLayout->addWidget(editFreqPosition);

    positionGroup->setLayout(pgMainLayout);
}

void BenchmarkWidget::iniEventGroup(void) {
    eventGroup = new QGroupBox(tr("Event Latency"));
    egMainLayout = new QVBoxLayout();

    enableEventSys = new QCheckBox();
    enableEventSys->setCheckable(true);
    enableEventSys->setText("Enable Event System Latency");

    labelFreqEventSys = new QLabel();
    labelFreqEventSys->setText(tr("Print Delay:"));
    labelFreqEventSys->setEnabled(false);

    editFreqEventSys = new QLineEdit("0");
    editFreqEventSys->setEnabled(false);

    connect(enableEventSys, SIGNAL(toggled(bool)), labelFreqEventSys, SLOT(setEnabled(bool)));
    connect(enableEventSys, SIGNAL(toggled(bool)), editFreqEventSys, SLOT(setEnabled(bool)));
    connect(enableEventSys, SIGNAL(toggled(bool)), this, SLOT(changeStatusToEdited()));
    connect(editFreqEventSys, SIGNAL(textEdited(QString)), this, SLOT(changeStatusToEdited()));

    egMainLayout->addWidget(enableEventSys);
    egMainLayout->addWidget(labelFreqEventSys);
    egMainLayout->addWidget(editFreqEventSys);

    eventGroup->setLayout(egMainLayout);
}

void BenchmarkWidget::updateBenchConfig(void) {
    //qDebug() << "Updating benchConfig";
    if (benchConfig != NULL) {
        if (checkFreqAreValid()) {
            //set userEdited boolian flag
            benchConfig->editedByUser();

            //set config to checkbox values
            benchConfig->setBoolRecordSysTime(true);
            benchConfig->setBoolSpikeDetect(enableSpikeDetect->isChecked());
            benchConfig->setBoolSpikeSent(enableSpikeSend->isChecked());
            benchConfig->setBoolSpikeReceived(enableSpikeReceive->isChecked());
            benchConfig->setBoolPositionStreaming(enablePosition->isChecked());
            benchConfig->setBoolEventSys(enableEventSys->isChecked());

            //set config to freq values
            benchConfig->setFreqSpikeDetect(editFreqSpikeDetect->text().toInt());
            benchConfig->setFreqSpikeSent(editFreqSpikeSend->text().toInt());
            benchConfig->setFreqSpikeReceived(editFreqSpikeReceive->text().toInt());
            benchConfig->setFreqPositionStream(editFreqPosition->text().toInt());
            benchConfig->setFreqEventSys(editFreqEventSys->text().toInt());

            emit(signal_benchConfigUpdated());
        }
        else {
            qDebug() << "Error: Invalid frequency inputs detected. (BenchmarkWidget::updateBenchConfig)";
        }
        getSettingsFromConfig(); //reset values if bad inputs detected, clean up inputs if not
    }
    else {
        qDebug() << "Error: BenchmarkingConfig returned NULL (BenchmarkWidget::updateBenchConfig)";
    }
}

void BenchmarkWidget::getSettingsFromConfig(void) {
    if (benchConfig != NULL) {
        //set all checkboxes
        enableSpikeDetect->setChecked(benchConfig->printSpikeDetect());
        enableSpikeSend->setChecked(benchConfig->printSpikeSent());
        enableSpikeReceive->setChecked(benchConfig->printSpikeReceived());
        enablePosition->setChecked(benchConfig->printPositionStreaming());
        enableEventSys->setChecked(benchConfig->printEventSys());

        //set line editors
        editFreqSpikeDetect->setText(QString("%1").arg(benchConfig->getFreqSpikeDetect()));
        editFreqSpikeReceive->setText(QString("%1").arg(benchConfig->getFreqSpikeReceived()));
        editFreqSpikeSend->setText(QString("%1").arg(benchConfig->getFreqSpikeSent()));
        editFreqPosition->setText(QString("%1").arg(benchConfig->getFreqPositionStream()));
        editFreqEventSys->setText(QString("%1").arg(benchConfig->getFreqEventSys()));
    }
    else {
        qDebug() << "Error: BenchmarkingConfig returned NULL (BenchmarkWidget::getSettingsFromConfig)";
        //initialize benchConfig
        benchConfig = new BenchmarkConfig();
        benchConfig->resetDefaultFreq();
        getSettingsFromConfig();
    }
}

//checks if any frequency inputs are invalid (i.e. any characters entered or negative freq)
bool BenchmarkWidget::checkFreqAreValid(void) {
    bool retval = true;
    int frequency;

    frequency = editFreqSpikeDetect->text().toInt(&retval);
    if (!retval || frequency <= 0)
        return(retval);

    frequency = editFreqSpikeSend->text().toInt(&retval);
    if (!retval || frequency <= 0)
        return(retval);

    frequency = editFreqSpikeReceive->text().toInt(&retval);
    if (!retval || frequency <= 0)
        return(retval);

    frequency = editFreqPosition->text().toInt(&retval);
    if (!retval || frequency <= 0)
        return(retval);

    frequency = editFreqEventSys->text().toInt(&retval);
    if (!retval || frequency <= 0)
        return(retval);

    return(retval);
}

void BenchmarkWidget::changeStatusToEdited(void) {
    statusText->setText(tr("Unsaved changes made"));

}

void BenchmarkWidget::changeStatusToSaved(void) {
    statusText->setText(tr("Changes saved"));
}
