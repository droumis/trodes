#include "abstractTrodesSource.h"
#include "globalObjects.h"
extern bool unitTestMode;

AbstractSourceRuntime::AbstractSourceRuntime(){
    quitNow = false;
    acquiring = false;
    lastTimeStamp = 0;
    numConsecJumps = 0;
    packetSizeErrorThrown = false;
    totalDroppedPacketEvents = 0;
    badFrameAlignment = false;
    appendSysClock = false;

}

bool AbstractSourceRuntime::checkForCorrectTimeSequence() {

    bool returnVal = true;
    //If the time does not increase by 1 throw an error
    if ((currentTimeStamp-lastTimeStamp) != 1) {
        if (totalDroppedPacketEvents > 0) {
            //The first packet is often a jump (playback) so we only write a statement after the 2nd (or higher) timestamp jumps
            //qDebug() << "Jump in timestamps: " << ((double)currentTimeStamp-(double)lastTimeStamp) << currentTimeStamp;
        }
        numConsecJumps++;
        totalDroppedPacketEvents = totalDroppedPacketEvents + 1;

        if (currentTimeStamp > lastTimeStamp) {
            if (currentTimeStamp-lastTimeStamp > 30000) {
                //Huge jump in timestamps might mean that the packet is corrupted. Probably best to discard.
                returnVal = false;
            }
        } else if (lastTimeStamp > currentTimeStamp) {
            //Backwards time.  Probably a corrupted packet. Best to discard.
            returnVal = false;
        }

        if (!packetSizeErrorThrown && numConsecJumps > 5) {

            qDebug() << "Signal error-- packet size does not match workspace. Check workspace file.";
            packetSizeErrorThrown = true;
            emit timeStampError(true);
        }
    } else {
        numConsecJumps = 0;
        if (packetSizeErrorThrown) {
            packetSizeErrorThrown = false;
            emit timeStampError(false); //reset the error indicator
        }
    }
    lastTimeStamp = currentTimeStamp;
    return returnVal;
}

void AbstractSourceRuntime::calculateHeaderSize() {
//    if (appendSysClock) {
//        headerSize = hardwareConf->headerSize-4;  //Remove the 8 bytes (4 16-bit values) defined in the config for sys clock
//    } else {
        headerSize = hardwareConf->headerSize;
//    }
}


bool AbstractSourceRuntime::checkFrameAlignment(unsigned char *packet) {
    if (*packet != 0x55) {
        //We don't have alignment!  So now we drop everything
        //and try to find it. We need two 0x55's separated by PACKET_SIZE.
        if (!packetSizeErrorThrown) {
            qDebug() << "Bad frame alignment in source packet";
        }
        badFrameAlignment = true;
        tempSyncByteLocation = 0;
        findNextSyncByte(packet);
        return false;
    }

    badFrameAlignment = false;
    return true;
}


void AbstractSourceRuntime::findNextSyncByte(unsigned char *packet) {
    //Look for the next sync byte in the current packet
    for (unsigned int i = 0; i < PACKET_SIZE; i++) {
        if (*(packet+i) == 0x55) {
            tempSyncByteLocation = i;
            break;
        }
    }
}

void AbstractSourceRuntime::calculateReferences(){
    for(int i = 0; i < spikeConf->carGroups.length(); ++i){
        if(spikeConf->carGroups[i].useCount == 0){
            //if this car group is not being used, skip it
            continue;
        }
        if(spikeConf->carGroups[i].chans.length() == 0){
            //if this car group is empty, skip it
            continue;
        }
        int32_t accum = 0;
        for(auto const &chan : spikeConf->carGroups[i].chans){
            accum += rawData.data[(rawData.writeIdx*hardwareConf->NCHAN) + chan.hw_chan];
        }
        const int carvalind = rawData.writeIdx*spikeConf->carGroups.length() + i;
        //divide by 0 exception not possible since if chans is empty, skip calculations
        rawData.carvals[carvalind] = accum/spikeConf->carGroups[i].chans.length();
    }
}

//---------------------------------------
//AbstractTrodesSource

AbstractTrodesSource::AbstractTrodesSource () {

    startCommandValue = command_startNoECU; // start data capture without ECU

    //startCommandValue = 0x64; // start data capture with ECU
    connectErrorThrown = false;
    reinitMode = false;

    appendSysClock = false;
    restartThreadAfterShutdown = false;




}

void AbstractTrodesSource::setECUConnected(bool ECUconnected) {
    if (ECUconnected) {
        startCommandValue = command_startWithECU;
    } else {
        startCommandValue = command_startNoECU;
    }
}

void AbstractTrodesSource::setAppendSysClock(bool clOn) {

    appendSysClock = clOn;

}


void AbstractTrodesSource::setUpThread(AbstractSourceRuntime *rtPtr) {

    rtPtr->appendSysClock = appendSysClock; //Should the source thread append the computer's system clock to each packet?
    workerThread = new QThread();
    rtPtr->moveToThread(workerThread);
    rtPtr->connect(this, SIGNAL(startRuntime()), SLOT(Run()));
    connect(this, SIGNAL(startRuntime()), this, SLOT(setThreadRunning()));
    connect(rtPtr, SIGNAL(finished()), workerThread, SLOT(quit()));
    connect(rtPtr,SIGNAL(finished()), rtPtr, SLOT(deleteLater()));
    connect(rtPtr,SIGNAL(finished()), this, SLOT(setThreadNotRunning()));
    connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));
    connect(rtPtr,SIGNAL(timeStampError(bool)),this,SIGNAL(timeStampError(bool)));
    connect(rtPtr,SIGNAL(failure()),this,SLOT(RunTimeError()));
    workerThread->setObjectName("DataSource");
    workerThread->start();
}

void AbstractTrodesSource::setThreadRunning() {
    threadRunning = true;
}

void AbstractTrodesSource::setThreadNotRunning() {
    threadRunning = false;
    if (restartThreadAfterShutdown) {
        restartThread();
    }

}

bool AbstractTrodesSource::isThreadRunning() {
    return threadRunning;
}

quint64 AbstractTrodesSource::getTotalDroppedPacketEvents() {
    return 0;
}

void AbstractTrodesSource::RunTimeError() {

    //Here we should display a message that something has gone wrong,
    //but perhaps not stop data acquisition altogether?
    //connectErrorThrown = true;
    //StopAcquisition();
    qDebug() << "An error occured during data acquisition.";

    if (!unitTestMode) {
        QMessageBox messageBox;
        messageBox.critical(0,"Error","An error occured during data acquisition.");
        messageBox.setFixedSize(500,200);
    }

}

void AbstractTrodesSource::restartThread() {

}

void AbstractTrodesSource::StartSimulation() {

}

void AbstractTrodesSource::SendSettleCommand() {

}

void AbstractTrodesSource::SendSettleChannel(int byteInPacket, quint8 bit, int delay, quint8 triggerState) {

}

void AbstractTrodesSource::SendFunctionTrigger(int funcNum) {

}

void AbstractTrodesSource::SendHeadstageSettings(HeadstageSettings s) {

}

HeadstageSettings AbstractTrodesSource::GetHeadstageSettings() {
    HeadstageSettings s;
    return s;
}

HardwareControllerSettings AbstractTrodesSource::GetControllerSettings() {
    HardwareControllerSettings s;
    return s;
}

void AbstractTrodesSource::SendControllerSettings(HardwareControllerSettings s) {

}

void AbstractTrodesSource::SendSDCardUnlock() {

}

void AbstractTrodesSource::ConnectToSDCard() {

}

void AbstractTrodesSource::ReconfigureSDCard(int numChannels) {

}

int AbstractTrodesSource::MeasurePacketLength(HeadstageSettings settings){

}
