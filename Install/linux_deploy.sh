#!/bin/sh
echo "Cleaning up unnecessary Qt libs"
#echo $PWD
#ls ./bin/linux
rm -rf ./bin/linux/Qt/lib/libQt5Q*
rm -rf ./bin/linux/Qt/lib/libQt5Bluetooth.so*
rm -rf ./bin/linux/Qt/lib/libQt53D*
rm -rf ./bin/linux/Qt/lib/libQt5Des*
rm -rf ./bin/linux/Qt/lib/libQt5Sql.so*
rm -rf ./bin/linux/Qt/plugins/designer/
rm -rf ./bin/linux/Qt/plugins/gamepads/
rm -rf ./bin/linux/Qt/plugins/qmltooling/
rm -rf ./bin/linux/Qt/plugins/printsupport/
rm -rf ./bin/linux/Qt/plugins/sqldrivers/
rm -rf ./bin/linux/Qt/lib/libQt5P*

echo "Deleting debugLogs/"
rm -rf ./bin/linux/debugLogs


#echo "Creating tar.gz for .debug files"
#tar -zcf bin/Trodes_$1$2$3.tar.gz bin/linux/*.debug
rm -rf ./bin/linux/*.debug

#echo "Creating tar.gz for release, located in $PWD/bin/"
#tar -zcf bin/Trodes_Ubuntu16_$1_$2_$3.tar.gz bin/linux/

echo "Done"
return 0
