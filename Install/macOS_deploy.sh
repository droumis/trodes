#!/bin/sh
if [ -z "$macdeploy" ];
then
    macdeploy=~/Qt/5.9.8/clang_64/bin/macdeployqt
fi

if [ -z "$deployargs" ];
then
    deployargs=-always-overwrite
fi

if [ -z "$signargs" ];
then
    signargs="--force --verify --verbose --deep --sign"
fi

set -x
#Deploy Trodes
$macdeploy bin/macOS/Trodes.app $deployargs -executable=bin/macOS/exportanalog -executable=bin/macOS/exportLFP -executable=bin/macOS/exportdio -executable=bin/macOS/exporthwtimes -executable=bin/macOS/exportmda -executable=bin/macOS/exportofflinesorter -executable=bin/macOS/exportphy -executable=bin/macOS/exportspikes -executable=bin/macOS/exporttime -executable=bin/macOS/mergesdrecording -executable=bin/macOS/sdtorec -executable=bin/macOS/splitrec
codesign $signargs "Developer ID Application: SpikeGadgets LLC (4CWC89BLE7)" bin/macOS/Trodes.app

declare -a modules=("cameraModule.app"
                "stateScript.app"
                "stateScriptEditor.app"
                )

for i in "${modules[@]}"
do
    $macdeploy bin/macOS/$i $deployargs
    codesign $signargs "Developer ID Application: SpikeGadgets LLC (4CWC89BLE7)" bin/macOS/$i
done


rm -rf bin/macOS/*.dSYM
rm -rf bin/macOS/debugLogs
