from unittest import TestCase
from trodes_data import TrodesAnimalInfo


class TestTrodesAnimalInfo(TestCase):
    def test_get_raw_paths(self):
        anim_raw_path = TrodesAnimalInfo.get_raw_paths('/opt/data36/jason', ['kanye'])
        self.assertListEqual(anim_raw_path, ['/opt/data36/jason/kanye/raw'])

    def test_get_day_paths(self):
        anim_day_raw_path = TrodesAnimalInfo._get_day_dirs('/opt/data36/jason/kanye/raw')
        print(anim_day_raw_path)

    def test_get_day_rec_paths(self):
        self.fail()
