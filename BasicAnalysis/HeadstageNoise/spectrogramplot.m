filename = '20151124_templeton_100uv_fastsettle_newripfilt_nobakgnd.rec';
directory = '/opt/data36/daliu/trodes/templeton/';




channel = [1,1];

import_trodes = readTrodesFileContinuous(strcat(directory, filename),channel,0);

timestamp = import_trodes.timestamps;
rawData = import_trodes.channelData;
%%

starttime = 5.5;
endtime = 6.5;
figure(1);
subplot(3,1,1);
subtimestamps = timestamp(starttime*30000:endtime*30000);
plot(subtimestamps, rawData(starttime*30000:endtime*30000));
title('Raw Trace');
xlabel('Time (s)');
ylabel('raw');
xlim([subtimestamps(1),subtimestamps(end)]);

subplot(3,1,2);
spectrogram(rawData(starttime*30000:endtime*30000),1500,0,[100:10:400],30000,'yaxis');
colormap(jet);
title('Spectrogram (25ms, 10Hz window)');



fsdata_filename = '20151124_templeton_100uv_fastsettle_newripfilt_nobakgnd_fsdatastate.csv';

fsdata = csvread(strcat(directory,fsdata_filename));

fsdata_subtime_ind = lookup(fsdata(:,1)/30000, subtimestamps);
subfsdata = fsdata((fsdata_subtime_ind > 1) & (fsdata_subtime_ind < max(fsdata_subtime_ind)),:);
fsdata_timestamp = subfsdata(:,1)/30000;

subplot(3,1,3);
plot(fsdata_timestamp, subfsdata(:,5));
hold on;
plot(fsdata_timestamp, subfsdata(:,6),'r');
times_in_lockout = fsdata_timestamp(subfsdata(:,4)==1);
plot(times_in_lockout ,zeros(length(times_in_lockout),1)-100,'.');
hold off;
xlim([fsdata_timestamp(1),fsdata_timestamp(end)]);