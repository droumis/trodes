var classscatter_window =
[
    [ "scatterWindow", "classscatter_window.html#a5e7acc392c9c91672bf945840f811ce4", null ],
    [ "~scatterWindow", "classscatter_window.html#a2bec2f115e25258c189dc37ff01c7453", null ],
    [ "initializeGL", "classscatter_window.html#a5f7ee9e3c2f67505fb916190a8fc9425", null ],
    [ "paintEvent", "classscatter_window.html#a05dd32b08f327c73676f04c4da99bf83", null ],
    [ "resizeGL", "classscatter_window.html#ab77870280bb0ea6407e62ccd4d9a33f3", null ],
    [ "setChannels", "classscatter_window.html#aa6372ba13f137ee2757a6fe3fbeb6db9", null ],
    [ "setDataPoint", "classscatter_window.html#acec64269d45cd3f32a46d297a1333e5c", null ],
    [ "setMaxDisplay", "classscatter_window.html#a645725d0979f465ec158c74e6bee361c", null ],
    [ "updateAxes", "classscatter_window.html#afde141bab9b7cca812518ef817089686", null ],
    [ "bufferSize", "classscatter_window.html#a87ba5b4f1690c50e2225240eb11d5fad", null ],
    [ "currentXdisplay", "classscatter_window.html#a07a2c0aacfde8a4dc420a69011d28425", null ],
    [ "currentYdisplay", "classscatter_window.html#a2b9f5c4365a3ff32bdb72741ce6178bd", null ],
    [ "displayData", "classscatter_window.html#a3773b49a98abf2e5e78e66f9cdb9cb74", null ],
    [ "initialized", "classscatter_window.html#aa67e316f255a227c137fbf5246e6144a", null ],
    [ "maxAmplitude", "classscatter_window.html#a9c5b817ddc699fb410fae3a913590285", null ],
    [ "plotNewPoints", "classscatter_window.html#a8f1daa12f4c93d2bd5e900d3d9bd9d97", null ],
    [ "replotScatter", "classscatter_window.html#afbe57c1dbcb67eab17f08a48daf62021", null ]
];