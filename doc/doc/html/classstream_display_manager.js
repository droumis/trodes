var classstream_display_manager =
[
    [ "streamDisplayManager", "classstream_display_manager.html#aecc0bb7488e62311ea02bfd38d407eae", null ],
    [ "~streamDisplayManager", "classstream_display_manager.html#a4f4419973bb70a30c798acd3f29755f6", null ],
    [ "newAudioChannel", "classstream_display_manager.html#a1bf60e97d4d565d568d4f6b764c9511a", null ],
    [ "relayChannelClick", "classstream_display_manager.html#abb71b68207eb662f010cb95adda369b0", null ],
    [ "trodeSelected", "classstream_display_manager.html#aa016491a2135dab39f83edc48f58441c", null ],
    [ "updateAudioHighlightChannel", "classstream_display_manager.html#ad0fd87b484a56e29c189296872151c41", null ],
    [ "columnsPerPage", "classstream_display_manager.html#a3721e721dbf705cdc30c76a24a2117e7", null ],
    [ "eegDisplayLayout", "classstream_display_manager.html#aaa042c99f35d47c7264a895e28588ae1", null ],
    [ "eegDisplayWidgets", "classstream_display_manager.html#ae106b7ce2d121573d6e2142b9ef17637", null ],
    [ "glStreamWidgets", "classstream_display_manager.html#a801a0c296477711cf6132876a15abfe0", null ],
    [ "isHeaderDisplayPage", "classstream_display_manager.html#ae42cd9e9c78278d8e6d442902ab0b8d0", null ],
    [ "streamDisplayChannels", "classstream_display_manager.html#a903c95e4400ca8d26d157e0d01b48641", null ]
];