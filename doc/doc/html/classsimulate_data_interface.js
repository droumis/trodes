var classsimulate_data_interface =
[
    [ "simulateDataInterface", "classsimulate_data_interface.html#a691045b91967962e77a2215aa5fd0c4b", null ],
    [ "~simulateDataInterface", "classsimulate_data_interface.html#a65b7274f481b02c4d78da284790b21f9", null ],
    [ "acquisitionStarted", "classsimulate_data_interface.html#a7e0868da1a9cb844d2cbe0603ffbf12e", null ],
    [ "acquisitionStopped", "classsimulate_data_interface.html#ad886c457b3c379bf7cf8bb57bcea551a", null ],
    [ "CloseInterface", "classsimulate_data_interface.html#a36a6ec7ca79341f7f2c696ab545d6b92", null ],
    [ "getAmplitude", "classsimulate_data_interface.html#a3dfdd7407dd351d4e292fc15a2e5582a", null ],
    [ "getFrequency", "classsimulate_data_interface.html#a81b8d199783b933ab84f7118c27193b5", null ],
    [ "InitInterface", "classsimulate_data_interface.html#a00848b84465c150797edaeb382167ed7", null ],
    [ "setAmplitude", "classsimulate_data_interface.html#ac49d9a8f5978dea6677e9f105d6b7217", null ],
    [ "setFrequency", "classsimulate_data_interface.html#a911c7f3097b08c853a6f1d0e35545b37", null ],
    [ "StartAcquisition", "classsimulate_data_interface.html#ad586e5ff3e98cc32653f161ad4858eb0", null ],
    [ "startRuntime", "classsimulate_data_interface.html#a376663ca23db7cb7f5636433680c78aa", null ],
    [ "stateChanged", "classsimulate_data_interface.html#ade7b7aae4e508a87348dfca93cebc5e4", null ],
    [ "StopAcquisition", "classsimulate_data_interface.html#addf2e9222c7a75c33b7057c0a64d2b87", null ],
    [ "acquisitionThread", "classsimulate_data_interface.html#a81fa1f5eebd1b78fc1bf98ca937e20a6", null ],
    [ "state", "classsimulate_data_interface.html#a5b778d8d1c3eeaa4001ce23f531c3af2", null ]
];