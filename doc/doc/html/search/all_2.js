var searchData=
[
  ['balternatesetting',['bAlternateSetting',['../structlibusb__interface__descriptor.html#a2c94bce217e8ecd5fd23728510e9c838',1,'libusb_interface_descriptor']]],
  ['bcddevice',['bcdDevice',['../structlibusb__device__descriptor.html#afb5e0fc6f0cfe51de900d35506fb9317',1,'libusb_device_descriptor']]],
  ['bcdusb',['bcdUSB',['../structlibusb__device__descriptor.html#af0682f293291db942b5d253092d472e4',1,'libusb_device_descriptor']]],
  ['bconfigurationvalue',['bConfigurationValue',['../structlibusb__config__descriptor.html#a04549d99d38080857e0256fcad8dcce7',1,'libusb_config_descriptor']]],
  ['bdescriptortype',['bDescriptorType',['../structlibusb__device__descriptor.html#a63c4b109725ce829fc81c5c9cad8c87a',1,'libusb_device_descriptor::bDescriptorType()'],['../structlibusb__endpoint__descriptor.html#a609c257394a574af229293bddf26986e',1,'libusb_endpoint_descriptor::bDescriptorType()'],['../structlibusb__interface__descriptor.html#affe977ae83d3bbf07513d87ebfb6ed0e',1,'libusb_interface_descriptor::bDescriptorType()'],['../structlibusb__config__descriptor.html#abc4f9a751c5b2ed968b045cb17528f74',1,'libusb_config_descriptor::bDescriptorType()']]],
  ['bdeviceclass',['bDeviceClass',['../structlibusb__device__descriptor.html#ab86c8f43e75d4d54fccbc199cfa9703b',1,'libusb_device_descriptor']]],
  ['bdeviceprotocol',['bDeviceProtocol',['../structlibusb__device__descriptor.html#a480a0b5345a2e59987e20fa7247c3f0e',1,'libusb_device_descriptor']]],
  ['bdevicesubclass',['bDeviceSubClass',['../structlibusb__device__descriptor.html#a9c3a91102d3d53d9414d0dda0191c5ab',1,'libusb_device_descriptor']]],
  ['bendpointaddress',['bEndpointAddress',['../structlibusb__endpoint__descriptor.html#a111d087a09cbeded8e15eda9127e23d2',1,'libusb_endpoint_descriptor']]],
  ['binterfaceclass',['bInterfaceClass',['../structlibusb__interface__descriptor.html#a19dcf80e6a5fedf3d3673630897ad649',1,'libusb_interface_descriptor']]],
  ['binterfacenumber',['bInterfaceNumber',['../structlibusb__interface__descriptor.html#a75046f443d3330f590f3d3fb1f9df863',1,'libusb_interface_descriptor']]],
  ['binterfaceprotocol',['bInterfaceProtocol',['../structlibusb__interface__descriptor.html#a5062bbb17fc76e6c3624ea7ca68d554c',1,'libusb_interface_descriptor']]],
  ['binterfacesubclass',['bInterfaceSubClass',['../structlibusb__interface__descriptor.html#a0b9b890f84694a4e6a0103944e1247f4',1,'libusb_interface_descriptor']]],
  ['binterval',['bInterval',['../structlibusb__endpoint__descriptor.html#a3194f3f04ebd860d59cbdb07d758f9d8',1,'libusb_endpoint_descriptor']]],
  ['blength',['bLength',['../structlibusb__device__descriptor.html#affda0be3fe1c37092ddc7cb120428f30',1,'libusb_device_descriptor::bLength()'],['../structlibusb__endpoint__descriptor.html#aa47a5fa31c179e7cb92818c0572c18a3',1,'libusb_endpoint_descriptor::bLength()'],['../structlibusb__interface__descriptor.html#a646b7bf875e5b927796d47e57c9182b8',1,'libusb_interface_descriptor::bLength()'],['../structlibusb__config__descriptor.html#a18ccd46a86328fd9c849e17ca8f01738',1,'libusb_config_descriptor::bLength()']]],
  ['bmattributes',['bmAttributes',['../structlibusb__endpoint__descriptor.html#a932b84417c46467f9916ecf7b679160b',1,'libusb_endpoint_descriptor::bmAttributes()'],['../structlibusb__config__descriptor.html#adb65fe86ce28394110caad982c566525',1,'libusb_config_descriptor::bmAttributes()']]],
  ['bmaxpacketsize0',['bMaxPacketSize0',['../structlibusb__device__descriptor.html#a3b60170f077c9b26fc9f86e0cdb1d28a',1,'libusb_device_descriptor']]],
  ['bmrequesttype',['bmRequestType',['../structlibusb__control__setup.html#a39b148c231d675492ccd2383196926bf',1,'libusb_control_setup']]],
  ['bnumconfigurations',['bNumConfigurations',['../structlibusb__device__descriptor.html#a0f3f80cd931628a0531a815b59d067dd',1,'libusb_device_descriptor']]],
  ['bnumendpoints',['bNumEndpoints',['../structlibusb__interface__descriptor.html#a487bd2058bfbe749d370c6e7d3add20e',1,'libusb_interface_descriptor']]],
  ['bnuminterfaces',['bNumInterfaces',['../structlibusb__config__descriptor.html#a2c89b2d0a9ec0440801b97a8f8145792',1,'libusb_config_descriptor']]],
  ['brefresh',['bRefresh',['../structlibusb__endpoint__descriptor.html#a9176a6d206a48731244e33a89a2bea0b',1,'libusb_endpoint_descriptor']]],
  ['brequest',['bRequest',['../structlibusb__control__setup.html#a1b80a28b8d4e8586fc54358194e70087',1,'libusb_control_setup']]],
  ['bsynchaddress',['bSynchAddress',['../structlibusb__endpoint__descriptor.html#ab8408ca33f4e135039b1900c6f50ce6d',1,'libusb_endpoint_descriptor']]],
  ['buffer',['buffer',['../structlibusb__transfer.html#a7fa594567e074191ce8f28b5fb4a3bea',1,'libusb_transfer']]]
];
