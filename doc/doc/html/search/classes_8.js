var searchData=
[
  ['libusb_5fconfig_5fdescriptor',['libusb_config_descriptor',['../structlibusb__config__descriptor.html',1,'']]],
  ['libusb_5fcontrol_5fsetup',['libusb_control_setup',['../structlibusb__control__setup.html',1,'']]],
  ['libusb_5fdevice_5fdescriptor',['libusb_device_descriptor',['../structlibusb__device__descriptor.html',1,'']]],
  ['libusb_5fendpoint_5fdescriptor',['libusb_endpoint_descriptor',['../structlibusb__endpoint__descriptor.html',1,'']]],
  ['libusb_5finterface',['libusb_interface',['../structlibusb__interface.html',1,'']]],
  ['libusb_5finterface_5fdescriptor',['libusb_interface_descriptor',['../structlibusb__interface__descriptor.html',1,'']]],
  ['libusb_5fiso_5fpacket_5fdescriptor',['libusb_iso_packet_descriptor',['../structlibusb__iso__packet__descriptor.html',1,'']]],
  ['libusb_5fpollfd',['libusb_pollfd',['../structlibusb__pollfd.html',1,'']]],
  ['libusb_5ftransfer',['libusb_transfer',['../structlibusb__transfer.html',1,'']]],
  ['libusb_5fversion',['libusb_version',['../structlibusb__version.html',1,'']]]
];
